﻿/*SCREENS*/
/*service*/
var scr,start,oos,main,serviceSelect,balanceScr,historyCheque,specCancelWait,pin,msgResult,requestResult;
/*cashin*/
var cashin,depositSelectAdjunctionFrom,depositSelectAdjunctionCurrency,depositSelectAdjunctionMyCard,depositSelectAdjunctionAnotherCard;
/*help*/
var helpMenu;
/*settings*/
var settingsChangePin,settingsMenu, settingsAddSIM,settingsSecure3D,settingsSMSInfo,settingsInternetBank,settingsCardRequisites,settingsCardLimits;
/*transfer*/
var transferMenu,transferToSchet,transferChooseIdType,transferToCard,transferToCompany,transferRequisitesInput,transferCardInput,transferInputAmount,
	transferSend,transferToCardRecipient;
/*cashout*/
var cashoutInputAmount, giveMoney;
/*Ekassir*/
var ekassir;
/*SCREENS END*/

var m_HostServiceName, m_HostServiceState = "error", m_HostScreen, m_HostScreenText, m_HostData, m_HostAmount, m_HostPAN,
  m_Timeout = 0, 
  m_MoneyHistory, m_ATMMoneyInfo, m_FastCash, m_CheckSum, m_Currency;

var m_UserData, m_UserDataNew;
var m_session, m_HostParams;

var m_ATMFunctions = {acceptor:true, dispenser:true, printer:true};

function printButtonRefresh(value){
	scr.setButton("print", "", m_ATMFunctions.printer, !value, (!value)? '{"icon": "../../graphics/print-balance.svg"}' : '{"icon": "../../graphics/print-balance.svg", "themes":["wait"]}', onBalancePrintButton);
	window.external.exchange.refreshScr();
}
function setBalanceAndPrintButtons(_balanceReqFlag, _printReqFlag){
	scr.setButton("print", "", m_ATMFunctions.printer, !_printReqFlag, (!_printReqFlag)? '{"icon": "../../graphics/print-balance.svg"}' : '{"icon": "../../graphics/print-balance.svg", "themes":["wait"]}', onBalancePrintButton);
	if(_balanceReqFlag){
		scr.setButton("showremains", "Показать остаток", '{"icon": "","state":"wait"}', onBalanceShowButton);
	}
	else
		scr.setButton("showremains", "Показать остаток", '{"icon": "","state":""}', onBalanceShowButton);

	//scr.setButton("showremains", "Показать остаток", '{"icon": "","state":""}', onBalanceShowButton);
	//scr.setButton("print", "", true, true, '{"icon": ""}', onBalancePrintButton);
}
function onTimeoutButton(args) {
	var _name, _args;
	if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
		_name = args[0];
		if(args.length > 1)
			_args = args[1];
		else
			_args = "";
	}
	else {
		_name = "";
		_args = args;
	}
	alertMsgLog(scr.name+' onTimeoutButton, value: '+_args);
	m_Timeout == 0;
	if(_args == 'Да'){
		scr.setModalMessage("", "", -1, false, "", onTimeoutButton);
		window.external.exchange.RefreshScr();
	}
	else{
		scr.cancel();
	}
}
function onTimeout(name) {
	alertMsgLog(scr.name+' onTimeout');
	if(m_Timeout == 0) {
		alertMsgLog(scr.name+' onTimeout with '+m_Timeout);
		m_Timeout = 1;
		scr.setModalMessage("Вам требуется ещё время?", "Да,Нет", -1, true, "", onTimeoutButton);
		window.external.exchange.RefreshScr();
	}
	else {
		m_Timeout == 0;
		scr.cancel();
	}
}

function parseBalance(data){
		s = data;
		var charHelp = '';
		var PosArr = new Array();
		for (var i=0; i < s.length; i++)
		{
			charHelp = s.charAt(i);
			if (charHelp == '' || charHelp == '|')
				PosArr.push(i);
		}
		var z;
		var StrArr = new Array();
		for (var i=0; i < PosArr.length - 1; i++)
		{
			z = s.substring(PosArr[i] + 1, PosArr[i + 1]);
			StrArr.push(z);
		}
		z = s.substring(PosArr[PosArr.length - 1] + 1);
		StrArr.push(z);
		if (typeof StrArr[0] != 'undefined')
		{
			var temp = StrArr[0].substring(2);
			while (temp.substring(0,1) == ' ')
			{
				temp = temp.substring(1);
			}
			//temp = temp.split(' ')[0];
			temp = temp.replace(' RUR', '');

			//document.getElementById('ds').innerHTML = temp;
		}
		else
			temp = 0;
	return temp;
}

function onBalancePrintButton(name) {
	if(!m_session.balancePrintReq) {
		m_session.balancePrintReq = true;
		m_session.balancePrintNeed = true;
		printButtonRefresh(m_session.balancePrintNeed);
		alertMsgLog('onBalancePrintButton addCall');
		addRequestHandler("balanceprint", onBalancePrintService);
		window.external.exchange.ExecNdcService("balanceprint", "");
	}
	else if(!m_session.balancePrintNeed) {
		m_session.balancePrintNeed = true;
		printButtonRefresh(m_session.balancePrintNeed);
	}
}
function onBalancePrintService(args) {
	parseHostAnswer(args);
	
	if(m_HostServiceState == 'ok') {
		alertMsgLog('onBalancePrintService, Service '+m_HostServiceName+', HostScreen '+m_HostScreen+'.');
		if(m_HostScreen == 'wait') {
			return 'ok';
		}
		else if(m_HostScreen == 'error') {
			m_session.balancePrintReq = false;
			m_session.balancePrintNeed = false;
			printButtonRefresh(m_session.balancePrintNeed);
			return 'ok';
		}
		else if(m_HostScreen == 'balance') {
			m_session.balancePrintReq = false;
			m_session.balancePrintNeed = false;
			printButtonRefresh(m_session.balancePrintNeed);
			return 'ok';
		}
		else {
			m_session.balancePrintReq = false;
			m_session.balancePrintNeed = false;
			printButtonRefresh(m_session.balancePrintNeed);
			return 'ok';
		}
	}
	else {
		alertMsgLog('onBalancePrintService state is '+m_HostServiceState);
		m_session.balancePrintReq = false;
		m_session.balancePrintNeed = false;
		printButtonRefresh(m_session.balancePrintNeed);
		return 'ok';
	}
	return 'ok';
}

function onBalanceShowButton(name) {
	
	alertMsgLog('[SCRIPT] onBalanceShowButton');
	
	if(!m_session.balanceShow) {
		if(m_session.balance == 0) {
			alertMsgLog('[SCRIPT] onBalanceShowButton addCall');
			if(!m_session.balanceShowReq){
				addRequestHandler("balance", onBalanceShowService);
				window.external.exchange.ExecNdcService("balance", "");
				m_session.balanceShowReq = true;
			}
			if(m_session.serviceName != 'pin_balance'){
				scr.setButton("showremains", "Показать остаток", '{"icon": "","state":"wait"}', onBalanceShowButton);
				window.external.exchange.refreshScr();
			}
			else
				alertMsgLog('[SCRIPT] onBalanceShowButton pin_balance request');
		}
		else {
			scr.setButton("showremains", AddSpace(m_session.balance) + ' Руб.', '{"icon": "","state":"show"}', onBalanceShowButton);
			window.external.exchange.refreshScr();
			m_session.balanceShow = true;
			setTimeout('if(m_session.balanceShow) onBalanceShowButton("showremains");',2000);
		}
	}
	else {
		scr.setButton("showremains", "Показать остаток", '{"icon": "","state":""}', onBalanceShowButton);
		window.external.exchange.refreshScr();
		m_session.balanceShow = false;
	}
}
function onBalanceShowService(args) {
	parseHostAnswer(args);
	
	if(m_HostServiceState == 'ok') {
		alertMsgLog(' onBalanceShowService, Service '+m_HostServiceName+', HostScreen: '+m_HostScreen+'.');
		if(m_HostScreen == 'wait') {
			return 'ok';
		}
		else if(m_HostScreen == 'error') {
			m_session.balanceShowReq = false;
			m_session.balanceShow = false;
			if(m_session.serviceName != 'pin_balance'){
				scr.setLabel("balance", "Ошибка получения баланса", "");
				scr.setButton("showremains", "Показать остаток", '{"icon": "","state":""}', onBalanceShowButton);
				window.external.exchange.refreshScr();
			}
			return 'ok';
		}
		else if(m_HostScreen == 'balance') {
			var helpBalance = m_HostScreenText;
			
			m_session.balance = parseFloat(parseBalance(helpBalance));
			if(m_session.serviceName != 'pin_balance'){
				scr.setButton("showremains", AddSpace(m_session.balance) + ' Руб.', '{"icon": "","state":"show"}', onBalanceShowButton);
				window.external.exchange.refreshScr();
				m_session.balanceShow = true;
				setTimeout('if(m_session.balanceShow) onBalanceShowButton("showremains");',2000);
			}
			m_session.balanceShowReq = false;
			return 'ok';
		}
		else {
			m_session.balanceShowReq = false;
			m_session.balanceShow = false;
			
			scr.setLabel("balance", "Ошибка получения баланса", "");
			scr.setButton("showremains", "Показать остаток", '{"icon": "","state":""}', onBalanceShowButton);
			window.external.exchange.refreshScr();
			
			return 'ok';
		}
	}
	else {
		alertMsgLog('onBalanceShowService state is '+m_HostServiceState);			
		m_session.balanceShowReq = false;
		m_session.balanceShow = false;
		scr.setButton("showremains", "Показать остаток", '{"icon": "","state":""}', onBalanceShowButton);
		window.external.exchange.refreshScr();
		return 'ok';
	}
	return 'ok';
}

function sendPrintResult(args) {
	alertMsgLog('sendPrintResult val: '+args);
	if(args == '1') {
		m_session.balancePrintNeed = false;
		printButtonRefresh(m_session.balancePrintNeed);
	}
}
function getPaySystem(pan) {
	var iconPath = '';
	var resObj = {}
	if(pan.length < 6){
		iconPath =  "another-noname.svg";
		resObj.value = "*0000";
		resObj.our = false;
	}
	else {
		resObj.value = '*'+pan.substr(pan.length-4);
		var intBin = parseInt(pan.substr(0,4));
		if(isNaN(intBin)){
			iconPath =  "another-noname.svg";
			resObj.our = false;
		}
		else{
			if(isOpenCard(pan)){
				resObj.our = true;
				iconPath = 'our';
			}
			else{
				resObj.our = false;
				iconPath = 'another';
			}
			if(intBin >= 4000 && intBin <= 4999)
				iconPath +=  "-visa.svg";
			else if(intBin >= 2200 && intBin <= 2204)
				iconPath +=  "-mir.svg";
			else if(intBin >= 5100 && intBin <= 5599)
				iconPath +=  "-mastercard.svg";
			else
				iconPath +=  "-noname.svg";
		}
	}
	resObj.ext = '{"icon":"../../graphics/cards/'+iconPath+'"}';
	return resObj;
}
function isOpenCard(pan) {
	var ownCard = [220029,429037,429038,429039,429040,404586,484800,405870,446065,458493,413307,407178,414076,467485,467486,467487,405869,544218,544962,532301,531674,670518,530403,530183,539714,676231,558620,549025,549024,472840,472841,472842,472843,472844,532837,548106,670587,411900,424553,424554,424555,456515,456516,490986,494343,410323,437540,437541,467033,486031,531318,557808,557809,558298,676642,516906,523510,525932,529034,529047,530175,554521,536114,406790,406791,409755,409701,409756,425656,426896,437351,464843,479302,676968,552681,520634,515758,518803,515668,416038,479716,479777,479715,479718,420574,427856,425184,425181,425185,425182,425183,428165,428166,430291,676951,520349,552671,518796,552219,522459,539896,548764,520324,434146,434147,434148,485649,535108,544499,544573,547449,549848,676697,428228,443884,443885,514017,515243,529260,532130];
	var binInt = parseInt(pan.substr(0,6));
	if(binInt == NaN)
		return false;
	for(var i=0;i<ownCard.length;++i)
		if(ownCard[i] == binInt)
			return true;
	return false;
}

function hidePAN(data){
	if(data.indexOf("PAN") != -1){
		var helpJson = {};
		try{
			helpJson = JSON.parse(data);
			//alert("JSON parse ok, PAN:"+(helpJson["PAN"] != 'undefined'? helpJson["PAN"] : 'undefined'));
			if(typeof helpJson["PAN"] != 'undefined'){
				//alert((helpJson["PAN"]).substr(helpJson["PAN"].length-4));
				return data.replace(helpJson["PAN"], '*'+(helpJson["PAN"]).substr(helpJson["PAN"].length-4));
			}
			else
				return data;
		}
		catch(e){
			//alert("ERROR JSON PAN parse");
			return data;
		}
	}
	else
		return data;
}
function parseHostAnswer(data){
	var _HostDataJsonObj;
	m_HostServiceName = '';
	m_HostData = '';
	m_HostServiceState = 'error';
	m_HostScreen = '';
	m_HostScreenText = '';
	m_HostAmount = '';
	//m_HostPAN = '';
	
	if(typeof data != 'undefined' && data.constructor === Array && data.length > 0){
		m_HostServiceName = data[0];
		if(data.length > 1)
			m_HostData = data[1];
	}
	else
		m_HostData = data;
	alertMsgLog('[SCRIPT] '+scr.name+' Service: ' + m_HostServiceName + '; Data: ' + hidePAN(m_HostData));
	if(m_HostData != "") {
		_HostDataJsonObj = JSON.parse(m_HostData);
		if(typeof _HostDataJsonObj["Result"] != 'undefined')
			m_HostServiceState = _HostDataJsonObj["Result"];
		if(typeof _HostDataJsonObj["Scr"] != 'undefined')
			m_HostScreen = _HostDataJsonObj["Scr"];
		if(typeof _HostDataJsonObj["HostScreen"] != 'undefined')
			m_HostScreenText = _HostDataJsonObj["HostScreen"];
		if(typeof _HostDataJsonObj["NDCAmount"] != 'undefined')
			m_HostAmount = _HostDataJsonObj["NDCAmount"];
		if(typeof _HostDataJsonObj["PAN"] != 'undefined')
			m_HostPAN = _HostDataJsonObj["PAN"];

	}
	alertMsgLog('[SCRIPT] '+scr.name+' returned from Host state: ' + m_HostServiceState + '; screen: ' + m_HostScreen+'; screenText: '+m_HostScreenText);
}
function controlPinLength(help, init){
	var pinLengthStart = 4;
	if(typeof init != 'undefined')
		pinLengthStart = init;
	if(help.length > pinLengthStart)
		return help.length;
	if(help.length == 0)
		return pinLengthStart;
	else
		return pinLengthStart;
}
function getFastCashButtons(moneyATM, history){
	var amntMass = [];
	var i,j,k,help;
	if(typeof moneyATM == 'string' && moneyATM != '')
		moneyATM = JSON.parse(moneyATM);
	if(typeof history == 'string' && history != '')
		history = JSON.parse(history);
	if(typeof moneyATM != 'undefined' && moneyATM != '')
		for(i = 0; i < moneyATM.length; ++i){
			if(moneyATM[i].count > 0){
				help = parseInt(moneyATM[i].denomination);
				for(j = 0; j < i; ++j)
					if(help < amntMass[j]){
						if(j == 0 || help != amntMass[j-1]){
							amntMass.splice(j, 0, help);
							//alert(amntMass);
						}
						break;
					}
				if(j == i){
					amntMass.push(help);
					//alert(amntMass);
				}
			}
		}
	//alert('history.length: '+history.operations.length);
	if(typeof history != 'undefined' && history != ''){
		if(amntMass.length > 4){
			amntMass.splice(4, amntMass.length - 4);
			//amntMass.length = 4;
			//alert(amntMass);
		}
		i = 0;
		while(amntMass.length < 7 && i < history.operations.length){
			help = parseInt(history.operations[i].amount);
			//alert('history.operations['+i+']='+help);
			for(j = 0; j < amntMass.length; ++j)
				if(help < amntMass[j]){
					if(j == 0 || (help != amntMass[j-1])){
						//проверка, можно ли выдать данную сумму
						amntMass.splice(j, 0, help);
						//alert(amntMass);
					}
					break;
				}
			if(j == amntMass.length && (help != amntMass[j-1])){
				//проверка, можно ли выдать данную сумму
				amntMass.push(help);
				//alert(amntMass);
			}
			i++;
		}
	}
	if(typeof moneyATM != 'undefined' && moneyATM != ''){
		i = 0;
		j = 0;
		k = 2;
		while(amntMass.length < 7 && k < 7){
			for(i = 0; (i < moneyATM.length) && (amntMass.length < 7); ++i){
				if(moneyATM[i].count >= k){
					help = parseInt(moneyATM[i].denomination) * k;
					for(j = 0; j < amntMass.length; ++j)
						if(help < amntMass[j]){
							if(j == 0 || (help != amntMass[j-1])){
								amntMass.splice(j, 0, help);
								//alert(amntMass);
							}
							break;
						}
					if(j == amntMass.length && (help != amntMass[j-1])){
						amntMass.push(help);
						//alert(amntMass);
					}
				}
			}
			k++;
		}
	}
	if(typeof moneyATM == 'undefined' || moneyATM == '')
		amntMass = [500, 900, 1000, 1500, 3000, 4000, 5000];
	
	var fastCashHelp = '';
	if(typeof amntMass != 'undefined' && amntMass.constructor === Array && amntMass.length > 0)
		for(var i = 0; i < amntMass.length; ++i)
			fastCashHelp += (fastCashHelp == '' ? '' : ',')+'{\"text\":\"'+amntMass[i]
			  +'\",\"value\":'+amntMass[i]+'}';
	return fastCashHelp;
}

function getHistory(uid) {
	m_MoneyHistory = undefined;
	window.external.exchange.wbCustom.receiveCardHolderOperationsInfo(uid);
	var historyGet = function(someArgs){
		if(someArgs != '')
			m_MoneyHistory = someArgs;
		alertMsgLog('[history]: '+(m_MoneyHistory == null? 'null':m_MoneyHistory));
		try{
			//var helpJSON = JSON.parse(m_MoneyHistory);
			m_UserData = JSON.parse(m_MoneyHistory);
			m_UserDataNew = JSON.parse(m_MoneyHistory);
			alertMsgLog('JSON: '+m_UserData);
		}
		catch(e){
			alertMsgLog('JSON: '+e.message);
			m_UserData = undefined;
			m_UserDataNew = undefined;			
		}
	}
	addRequestHandler('clientHistory', historyGet);
}
function getCurrencyList(atmMoney){
	try{
		var money = JSON.parse(atmMoney);
	}
	catch(e){
		alertMsgLog('getCurrencyList JSON error: '+e.message);
		return ['rub'];
	}
	var currencyList = [];
	for (var i = 0; i < money.length; i++)
	{
		var new_cur = true;
		for (var j = 0; j < currencyList.length; j++)
		{
			if(money[i].currency == currencyList[j])
			{
				new_cur = false;
				break;
			}
		}
		if(new_cur)
			currencyList.push(money[i].currency);
	}
	return currencyList;
}
function getUserDataField(fieldName){
	if(typeof m_UserDataNew != 'undefined' && typeof (m_UserDataNew[fieldName]) != 'undefined')
		return m_UserDataNew[fieldName];
	else
		return '';
}
function saveToHistory(amnt, curr, check){
	var jsonHelp = {};
	var trueData = {"type":"cashout", "amount": amnt, "currency":curr, "printcheck":check, "count":1};
	var dataStr = '';
	try{
		jsonHelp = JSON.parse(m_MoneyHistory);
		alertMsgLog('saveToHistory history: '+m_MoneyHistory);
		alertMsgLog('saveToHistory history: '+jsonHelp['operations']);
		if(typeof jsonHelp['operations'] == 'undefined'){
			jsonHelp['operations'] = [];
			jsonHelp['operations'].push(trueData);
		}
		else {
			var i, j;
			for(i = 0; i < jsonHelp['operations'].length; ++i){
				if(jsonHelp['operations'][i]['amount'] == amnt){
					j = jsonHelp['operations'][i]['count'];
					if(typeof j != 'undefined'){
						jsonHelp['operations'][i]['count']= j+1;
					}
					else{
						jsonHelp['operations'][i]['count'] = 2;
					}
					jsonHelp['operations'][i]['printcheck'] = check;
					break;
				}
			}
			if(i == jsonHelp['operations'].length){
				jsonHelp['operations'].push(trueData);
			}
		}
		jsonHelp['printcheck'] = check;
		dataStr = 'some","operations":'+JSON.stringify(jsonHelp.operations)+', "printcheck":'+check+', "some":"';
	}
	catch(e){
		alertMsgLog('saveToHistory: '+e.message);
		dataStr = 'some","operations":['+JSON.stringify(trueData)+'], "printcheck":'+check+', "some":"';
	}
	if(typeof jsonHelp['_id'] == 'undefined'){
		alertMsgLog('saveToHistory id: 1111111111111111, data: '+dataStr);
		window.external.exchange.wbCustom.saveCardHolderOperationInfo('1111111111111111', dataStr);
	}
	else{
		alertMsgLog('saveToHistory id: '+jsonHelp['_id']+', data: '+dataStr);
		window.external.exchange.wbCustom.saveCardHolderOperationInfo(jsonHelp['_id'], dataStr);
	}
}
function getATMMoney(){
	m_ATMMoneyInfo = window.external.wbCassetes.getMoneyList();
	//m_ATMMoneyInfo = window.external.wbCassetes.getMoneyList("");
	//m_ATMMoneyInfo = window.external.wbCassetes.getMoneyList("CU");
	alertMsgLog('MoneyInfo type: '+(typeof m_ATMMoneyInfo));
	alertMsgLog('MoneyInfo: '+ m_ATMMoneyInfo);

	if(typeof m_ATMMoneyInfo != 'string' || m_ATMMoneyInfo == '[]' || m_ATMMoneyInfo == ''){
		m_ATMMoneyInfo = "[{\"cassette\":0,\"currency\":\"643\",\"denomination\":\"100\",\"count\":100},{\"cassette\":1,\"currency\":\"643\",\"denomination\":\"200\",\"count\":100},{\"cassette\":2,\"currency\":\"643\",\"denomination\":\"200\",\"count\":100},{\"cassette\":3,\"currency\":\"643\",\"denomination\":\"500\",\"count\":100},{\"cassette\":4,\"currency\":\"643\",\"denomination\":\"1000\",\"count\":100},{\"cassette\":5,\"currency\":\"643\",\"denomination\":\"2000\",\"count\":100},{\"cassette\":6,\"currency\":\"643\",\"denomination\":\"5000\",\"count\":100},"
		+"{\"cassette\":7,\"currency\":\"978\",\"denomination\":\"10\",\"count\":100}]";
		m_CheckSum = undefined;
	}
	else
		m_CheckSum = window.external.wbCassetes;
		//m_CheckSum = undefined;
	m_Currency = new CurrencyClass(getCurrencyList(m_ATMMoneyInfo));
	alertMsgLog('MoneyInfo: '+m_ATMMoneyInfo);	
}
function getATMFuncStatus(){
	var resObj = {};
	try{
		help = window.external.wbCassetes.getCashDispenserState();
		if(help == 'OK' || help == "WARNING")
			resObj.dispenser = true;
		else
			resObj.dispenser = false;
		alertMsgLog('getATMFuncStatus|Dispenser|'+help);
	}
	catch(e){
		alertMsgLog('getATMFuncStatus|Dispenser|'+e.message);
		resObj.dispenser = false;
	}
	try{
		help = window.external.wbCassetes.getBanknoteAcceptorState();
		if(help == 'OK' || help == "WARNING")
			resObj.acceptor = true;
		else
			resObj.acceptor = false;
		alertMsgLog('getATMFuncStatus|Acceptor|'+help);
	}
	catch(e){
		alertMsgLog('getATMFuncStatus|Acceptor|'+e.message);
		resObj.acceptor = false;
	}
	try{
		help = window.external.wbCassetes.getPaperState();
		if(help == 'OK' || help == "WARNING" || help == 'LOW' || help == 'FULL'){
			help = window.external.wbCassetes.getReceiptPrinterState();
			if(help == 'OK' || help == "WARNING"){
				resObj.printer = true;
			}
			else
				resObj.printer = false;
		}
		else
			resObj.printer = false;
		alertMsgLog('getATMFuncStatus|Printer|'+help);
	}
	catch(e){
		alertMsgLog('getATMFuncStatus|Printer|'+e.message);
		resObj.printer = false;
	}
	return resObj;
}
//m_ATMFunctions = {deposit:true, withdraw:true, print:true};
//acceptor:true, dispenser:true, printer:true

var start = function(args) {
    var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle")
				scr.nextScreen(main);
			else if(m_HostServiceName == "oos")
				scr.nextScreen(oos);
			else
				scr.cancel();
		}
		else
			scr.cancel();
		return;
	}
	scr.addCall("HostScriptAddon", onService);
	
	alertMsgLog('[SCRIPT] '+scr.name+'. Экран инициализации, args: ' + (typeof args != 'undefined' ? args : "undefined"));
	m_session = new SessionVariables();
	if(typeof args != 'undefined' && args.constructor != Array && args == 1){
		scr.setLabel("text", "Пожалуйста, подождите...", "");	
		scr.setLabel("loader","60", '{"loader":"loader"}');
		scr.setImage("bg","../../graphics/BG_blur.jpg","");
		//scr.setButton("quit","",false,false,'{"icon": ""}',onError);
		//scr.setButton("main_menu","",false,false,'{"icon": ""}',onError);
		//scr.setImage("smile","../../graphics/icon-ok.svg","");
		//scr.setImage("smile","../../graphics/icon-loader.svg","");
		scr.render("wait_message");
	}
	else {
		scr.setLabel("text", "Инициализация системы...", "");	
		scr.setLabel("loader","60", '{"loader":"loader"}');
		scr.setImage("bg","../../graphics/BG_blur.jpg","");
		//scr.setButton("quit","",false,false,'{"icon": ""}',onError);
		//scr.setButton("main_menu","",false,false,'{"icon": ""}',onError);
		//scr.setImage("smile","../../graphics/icon-smile-2.svg","");
		//scr.setImage("smile","../../graphics/icon-loader.svg","");
		scr.render("wait_message");
	}
	//window.external.exchange.ExecNdcService("idle", "");
}
var oos = function() {
    var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle")
				scr.nextScreen(main);
			else if(m_HostServiceName == "oos")
				scr.nextScreen(oos);
			else
				scr.cancel();
		}
		else
			scr.cancel();
	}
	scr.addCall("HostScriptAddon", onService);
	alertMsgLog('[SCRIPT] oos');
	
	scr.setLabel("text", "Инициализация системы...", "");	
	scr.setLabel("loader","60", '{"loader":"loader"}');
	scr.setImage("bg","../../graphics/BG_blur.jpg","");
	//scr.setButton("quit","",false,false,'{"icon": ""}',onError);
	//scr.setButton("main_menu","",false,false,'{"icon": ""}',onError);
	//scr.setImage("smile","../../graphics/icon-smile-2.svg","");
	//scr.setImage("smile","../../graphics/icon-loader.svg","");
	scr.render("wait_message");
	alertMsgLog('[SCRIPT] '+scr.name+'. Страница OOS');
}
var main = function() {
    var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	function addElements(){
		scr.setLabel("header1", "Здравствуйте!", "");
		scr.setLabel("header2", "Для начала работы Вы можете:", "");
		scr.setLabel("earphones", "... или подключить наушники для активации голосового меню", "");
		scr.setLabel("switch_lang", "EN", "");

		scr.setButton("show", "", "", onShow);
		
		var extObj = {};
		extObj.type = "unclickable";
		extObj.icon = "img/qs-icon-1.svg";
		scr.setButton("insert", "Вставить карту", true, true,
		  JSON.stringify(extObj), onButtonEmpty2);
		
		extObj.icon = "img/qs-icon-2.svg";		
		scr.setButton("apply", "Приложить карту", true, false,
		  JSON.stringify(extObj), onButtonEmpty2);
		  
		
		extObj.icon = "img/qs-icon-3.svg";		
		scr.setButton("qrcode", "Считать штрих код", true, false,
		  JSON.stringify(extObj), onButtonEmpty2);
		
		extObj.type = "clickable";
		extObj.icon = "img/qs-icon-4.svg";		
		scr.setButton("nocard", "Войти без карты", true, true,
		  JSON.stringify(extObj), onButtonEmpty3);		
		
		extObj = {};
		extObj.icon = "";
		scr.setButton("switch_lang", "Switch to English", JSON.stringify(extObj), onButton);
		
		scr.setImage("bg","../../graphics/BG_blur.jpg","");
	}
	var onButton = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+'. onButton ' + name);
		//getHistory('676280389000000005');
		//window.external.exchange.ExecNdcService("payments", "");
		m_session.lang = m_session.lang == 'ru' ? 'en' : 'ru';
	
		addElements();
		scr.render("insert_card");
	}
	var onShow = function(name) {
		alertMsgLog(scr.name+'. onShow ' + name);
		scr.setTimeout("5000", "", onInsertTimeout);
		scr.render('insert_card');
	}
	var onInsertTimeout = function(name) {
		alertMsgLog(scr.name+'. onInsertTimeout ' + name);
		scr.setTimeout("0", "", onInsertTimeout);
		scr.render('insert_card_advertising');
	}
	var onButtonEmpty = function(name) {
		alert('onButtonEmpty');
		alertMsgLog(scr.name+'. onButtonEmpty ' + name);
		var help = 'some","operations":['
		  +'{"type":"cashout", "amount": 100, "currency":643, "printcheck":true},'
		  +'{"type":"cashout", "amount": 3900, "currency":643, "printcheck":true},'
		  +'{"type":"cashout", "amount": 18000, "currency":643, "printcheck":true},'
			+' {"type":"cashout", "amount": 2300, "currency":643, "printcheck":false}'
			+'], \"some\":\"';
			
		//window.external.exchange.wbCustom.saveCardHolderOperationInfo('676280389000000005', help);
		//window.external.exchange.wbCustom.saveCardHolderOperationInfo('222222222222222222', help);
		//m_HostPAN = '676280389000000005';
		saveToHistory(900, 643, false);
		//alertMsgLog('done');
		//getHistory("1234");
		
		
	}
	var onButtonEmpty1 = function(name) {
		alert('onButtonEmpty1');
		alertMsgLog(scr.name+'. onButtonEmpty1 ' + name);
		var help = 'some\",{"operations":['
		//  +'{"type":"cashout", "amount": 100, "currency":643, "printcheck":true},'
		//  +'{"type":"cashout", "amount": 3900, "currency":643, "printcheck":true},'
		//  +'{"type":"cashout", "amount": 18000, "currency":643, "printcheck":true},'
			'{\"type\":\"cashout\", \"amount\": 1900, \"currency\":643, \"printcheck\":false}'
			+']}, \"some\":\"';
		//window.external.exchange.wbCustom.saveCardHolderOperationInfo('676280389000000005', help);
		//alertMsgLog('done');
		//getHistory('676280389000000005');
		getHistory('222222222222222222');
	}
	var onButtonEmpty3 = function(name){
		window.external.exchange.ExecNdcService("payments", "");
		m_session.isCard = false;
	}
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					//scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else if(m_HostScreen == 'wait_init'){
					//scr.nextScreen();
					scr.nextScreen(msgResult, ['Читаем карту...','wait']);
					return;
				}
				else if(m_HostScreen == 'wait_read_chip'){
					//scr.nextScreen();
					scr.nextScreen(msgResult, ['Проверяем оборудование...','wait']);
					return;
				}
				else if(m_HostScreen == 'wait_init_pay'){
					//scr.nextScreen();
					scr.nextScreen(msgResult, ['Инициализируем платежи...','wait']);
					return;
				}
				else if(m_HostScreen == 'pin_other'){
					m_session.ownCard = false;
					return;
				}
				else
					scr.cancel();
			}
			else if(m_HostServiceName == "before_pin"){
				if(m_HostScreen == "main"){
					//scr.nextScreen(pin);
					//alertMsgLog('PAN: '+m_HostPAN);
					if(m_session.ownCard)
						window.external.exchange.ExecNdcService("pin","");
					else
						window.external.exchange.ExecNdcService("pin_other","");
					m_session.cardIcon = getPaySystem(m_HostPAN);
					//alert("ACHTUNG1");
					getHistory(m_HostPAN);
					//m_session.ownCard = isOpenCard(m_HostPAN);
					return;
				}
				else if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else
					scr.cancel();
			}
			else if(m_HostServiceName == "webius_menu"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					scr.nextScreen(serviceSelect);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "oos")
				scr.nextScreen(oos);
			else
				scr.cancel();
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' not ok');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	//window.external.exchange.CopyNDCFolder();
	//setTimeout("window.external.RereadWebIUSConfigs();", 200);
	
	var res = window.external.RefreshExeConfig();
	alertMsgLog("[REFRESH] RefreshExeConfig : " + res);
	res = window.external.RefreshNdcCfg();
	alertMsgLog("[REFRESH] RefreshNdcCfg : " + res);
	res = window.external.RefreshUrl();
	alertMsgLog("[REFRESH] RefreshUrl : " + res);
	if(res == 0)
		window.external.exchange.ExecNdcService("idle", "");
		
	
	addElements();
	//scr.render("insert_card");
	scr.render("insert_card_advertising");

	getATMMoney();
	m_HostPAN = '';
	m_session.balanceShow = false;
	m_session.balanceShowReq = false;
	m_session.balancePrintNeed = false;
	m_session.balancePrintReq = false;
	m_session = new SessionVariables();
	if(typeof m_CheckSum != 'undefined')
		alertMsgLog('Ожидание клиента testAmount: 1000 is '+m_CheckSum.isAmountExist(1000, m_Currency.getSelectedCode(), true));	
	alertMsgLog(scr.name+'. Ожидание клиента');
}
var pin = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	function addElements(type){
		m_session.MaxPinLength = ndcdata.GetModuleVariable('NDC','NDCPINLENGTH');
		alertMsgLog('[PIN] maxlength: '+m_session.MaxPinLength);
		if(typeof type == 'undefined'){
			scr.setImage("bg","../../graphics/BG_blur.jpg","");
			scr.setLabel("switch_lang", "EN", "");	
			scr.setLabel("card",m_CardIcon.value, m_CardIcon.ext);
			
			scr.setButton("switch_lang", "Switch to English", '{"icon": "", "display_group":"bottom_line"}', onButton3);	
			if(m_session.MaxPinLength == 4)
				scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "themes":["btn-white-red"], "display_group":"bottom_line"}', onButton1);
			else
				scr.setButton("logout", "Продолжить", '{"icon": "", "themes":["btn-green"], "display_group":"bottom_line"}', onContinue);
			scr.setInput("pin_code", "", "","",false,true,"{\"pin_code\": true,\"length\": "+m_session.pin.length+"}","text" ,onInput);
		}
		else if(type == 'err'){
			scr.setImage("bg","../../graphics/BG_blur.jpg","");
			scr.setLabel("switch_lang", "EN", "");	
			
			scr.setLabel("attempts_left","Неверный ПИН", "");
			scr.setLabel("forgot_pin","Забыли ПИН?", "");
			scr.setLabel("change_pin_text","Смените его на&nbsp;новый в&nbsp;мобильном приложении", "");
			scr.setLabel("change_pin_text2","или по&nbsp;телефону", "");
			scr.setLabel("change_pin_phone","8 800 700-78-77", "");
			
			
			scr.setButton("switch_lang", "Switch to English", '{"icon": "", "display_group":"bottom_line"}', onButton3);
			if(m_session.MaxPinLength == 4)
				scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "themes":["btn-white-red"], "display_group":"bottom_line"}', onButton1);
			else
				scr.setButton("logout", "Продолжить", '{"icon": "", "themes":["btn-green"], "display_group":"bottom_line"}', onContinue);
			
			scr.setInput("pin_code", "", "","",false,true,"{\"pin_code\": true,\"length\": "+m_session.pin.length+"}","text" ,onInput, "Error");
		}
	}
	var onEmptyButton = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. EmptyButton ' + name);
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		m_session.serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onContinue = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', .продолжить на Вводе пин-кода.');
		var help = {};
			help['pinValue'] = m_session.pin.value;
		window.external.exchange.ExecNdcService("enter", JSON.stringify(help));
	}
	var onButton1 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Завершить на Вводе пин-кода.');
		window.external.exchange.ExecNdcService("cancel", "");
	}
	var onButton2 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжить на Вводе пин-кода.');
		window.external.exchange.ExecNdcService("enter", "");
	}
	var onButton3 = function(name){
		var help = onLangNotReady.join().toString();
		scr.setModalMessage('English version of the interface is in its preparatory phase. We are sorry for the inconvenience.', help, -1, true, '{"loader":"ellipse", "icon": "../../graphics/icon-smile-2.svg","options_settings":[{"name":"logout","icon":""}]}', onLangNotReadyCall);
		
		window.external.exchange.RefreshScr();
		return;
	}
	var onMoreTimeCall = function(args){
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onMoreTime, value: '+_args);
		
		if(_args == onMoreTime[0]){
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("nomoretime", "");
			m_session.serviceName = "return";
		}
		else{
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("moretime", "");
			m_session.serviceName = "moretime";
		}
	}
	var onLangNotReadyCall = function(args){
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onLangNotReadyCall, value: '+_args);
		scr.setModalMessage("", "", -1, false, "", onTimeoutButton);
		scr.render("pin_code");
		return;
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			if(m_HostServiceName == 'idle'){
				if(m_HostScreen == 'pin'){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == 'wait'){
					window.external.exchange.refreshScr();
					//scr.nextScreen(wait);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+', '+m_HostServiceName+' with unknown screen '+m_HostScreen);
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'before_pin'){
				if(m_HostScreen == 'pin'){
					//scr.nextScreen(pin);
					if(m_session.serviceName == 'pin_error')
						addElements('err');
					else
						addElements();
					
					m_session.balance = 0;
					scr.render("pin_code");
					return;
				}
				else if(m_HostScreen == 'wait'){
					//window.external.exchange.refreshScr();
					//scr.nextScreen(wait);
					return;
				}
				else if(m_HostScreen == 'main'){
					if(m_session.ownCard)
						window.external.exchange.ExecNdcService("pin","");
					else
						window.external.exchange.ExecNdcService("pin_other","");
					return;
				}
				else if(m_HostScreen == 'need_more_time'){
					var help = onMoreTime.join().toString();
					scr.setModalMessage('Вам требуется еще время?', help, -1, true, '{"icon": "","rotate":true,"loader":"countdown","count": 25,"options_settings":[{"name":"logout","icon":"", "theme":"btn-red"},{"name":"cancel","icon":""}]}', onMoreTimeCall);
					window.external.exchange.RefreshScr();
					return;
				}
				else if(m_HostScreen == 'card_return'){
					scr.nextScreen(msgResult, ["Пожалуйста, подождите...", "end"]);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+', '+m_HostServiceName+' with unknown screen '+m_HostScreen);
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'webius_menu'){
				if(m_HostScreen == 'main') {
					alertMsgLog('[SCRIPT] '+scr.name+'. service: '+m_HostServiceName+'; state is ok');
					if(m_session.serviceName != 'pin_balance'){
						
						scr.setInput("pin_code", m_session.pin.value, "","",false,true,"{\"pin_code\": true,\"length\": "+m_session.pin.length+"}","text" ,onInput, "Wait");
						scr.setButton("switch_lang", "Switch to English", true, false, '{"icon": "", "display_group":"bottom_line"}', onEmptyButton);	
						if(m_session.MaxPinLength == 4)
							scr.setButton("logout", "Забрать карту", true, false, '{"icon": "../../graphics/icon-pick-card-red.svg", "themes":["btn-white-red"], "display_group":"bottom_line"}', onEmptyButton);
						else
							scr.setButton("logout", "Продолжить", true, false, '{"icon": "", "themes":["btn-green"], "display_group":"bottom_line"}', onEmptyButton);
						window.external.exchange.RefreshScr();
						//scr.render("pin_code");
						
						m_session.serviceName = "pin_balance";
						m_session.balanceShowReq = false;
						m_session.balanceShow = false;
						onBalanceShowButton(name);
					}
					else{
						m_session.serviceName = '';
						scr.nextScreen(serviceSelect);
					}
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+', '+m_HostServiceName+' with unknown screen '+m_HostScreen);
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'pin_error'){
				m_session.serviceName = 'pin_error';
				window.external.exchange.ExecNdcService("need_pin", "");
				
				addElements('err');
				scr.render('pin_code_error');
				//scr.nextScreen(msgResult,["Неверно введенный пин-код", "end"]);
				return;
			}
			else if(m_HostServiceName == 'request_ok'){
				scr.nextScreen(requestResult, ['ok', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'request_err'){
				scr.nextScreen(requestResult, ['err', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'selfincass_impossible'){
				scr.nextScreen(requestResult, ['selfincass_impossible', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'request_impossible'){
				scr.nextScreen(requestResult, ['req_impossible', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'request_not_performed'){
				scr.nextScreen(requestResult, ['request_not_performed', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "err"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_allowed'){
				scr.nextScreen(requestResult,["request_not_allowed", "pinchange"]);
				return;
			}
			else if(m_HostServiceName == 'incorrect_amount'){
				scr.nextScreen(requestResult,["incorrect_amount", "pinchange"]);
				return;
			}
			else if(m_HostServiceName == 'incorrect_amount2'){
				scr.nextScreen(requestResult,["incorrect_amount2", "pinchange"]);
				return;
			}
			else if(m_HostServiceName == 'not_enough_money'){
				scr.nextScreen(requestResult, ['not_enough_money', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'limit_exceeded'){
				scr.nextScreen(requestResult, ['limit_exceeded', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'amount_to_big'){
				scr.nextScreen(requestResult, ['amount_to_big', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'pinchange_not_allowed'){
				scr.nextScreen(requestResult,['pinchange_not_allowed', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'card_seized'){
				scr.nextScreen(msgResult,['Ваша карта задержана<br>по требованию банка-эмитента', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'card_not_serviced'){
				scr.nextScreen(msgResult,['Извините, карта не обслуживается', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'card_expired'){
				scr.nextScreen(msgResult,['Ваша карта просрочена', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'no_account'){
				scr.nextScreen(msgResult,['Нет требуемого счета', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'pin_try_exceeded'){
				scr.nextScreen(msgResult,['Превышено максимально допустимое<br>число попыток ввода пин-кода', 'err']);
				return;
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else {
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' from host '+m_HostServiceState+', return to start');
			scr.cancel();
			return;
		}
	}
	var onInput = function(args){
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = args;
			else {
				pKey = args[0];
				help = args[1];
			}
		}
		m_session.pin.length = controlPinLength(help);
		m_session.pin.value = help;
	}
	scr.addCall("HostScriptAddon", onService);
	alertMsgLog('[SCRIPT] '+scr.name+'. Ввод пин-кода. HostScreen' + m_HostScreen);
	var onMoreTime = ['Завершить', 'Продолжить'];
	var onLangNotReady = ['Close'];
	if(m_session.serviceName == 'pin_error')
		addElements('err');
	else
		addElements();
	
	m_session.balance = 0;
	scr.render("pin_code");
}

var serviceSelect = function() {
	
    var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton2 = function(name){
		//
	}
	
	var onButton3 = function(name){		
		scr.nextScreen(depositSelectAdjunctionFrom);
	}
	
	var onButton4 = function(name){	
		scr.nextScreen(cashoutInputAmount);
	}
	
	var onButton5 = function(name){
		//window.external.exchange.ExecNdcService("ekassir", "");
		//scr.nextScreen(ekassir,1);
		scr.nextScreen(msgResult,"Ветка в разработке");
	}
	var onButton6 = function(name){		
		scr.nextScreen(transferMenu);
	}
	var onButton7 = function(name){		
	scr.nextScreen(helpMenu);
	}
	var onButton8 = function(name){		
		m_session.serviceName = 'main_menu';
		scr.nextScreen(settingsMenu, 0);
	}
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		//scr.cancel();
	}	
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			if(m_HostServiceName == 'idle'){
				scr.nextScreen(main);
				return;
			}
			else if(m_HostServiceName == 'pin_error'){
				scr.nextScreen(msgResult,["Неверно введенный пин-код", "end"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "end"]);
				return;
			}
			else if(m_HostServiceName == 'webius_menu'){
				alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					return;
				}
				else{
					alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
					return;
				}
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else if(m_HostServiceName == 'spec_cancel'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'end_session'){
					scr.nextScreen(start, 1);
					return;
				}
				else{
					scr.nextScreen(msgResult,"Ошибка");
					return;
				}
			}
			else{
				scr.nextScreen(msgResult,"Ошибка");
				return;
			}
		}
		else if(m_HostServiceState == 'cancel'){
			alertMsgLog(scr.name+' from host status cancel, return to start');
			scr.cancel();
			return;
		}
		else{
			alertMsgLog(scr.name+' from host unknown state'+m_HostServiceState+', cancelling.');
			scr.cancel();
			return;
		}

	}
	scr.addCall("HostScriptAddon", onService);
	
	m_ATMFunctions = getATMFuncStatus();
	//scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onBalanceShowButton);		
	//scr.setButton("showremainson", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremains\"}", onBalanceShowButton);		
	//scr.setButton("print", "", '{"icon": ""}', onBalancePrintButton);
	setBalanceAndPrintButtons(m_session.balanceShowReq, m_session.balancePrintNeed);
	//scr.setButton("deposit", "Пополнить",true, m_ATMFunctions.acceptor && m_session.cardIcon.our && m_session.ownCard, '{"icon": ""}', onButton3);
	scr.setButton("deposit", "Пополнить",true, m_ATMFunctions.acceptor && m_session.ownCard, '{"icon": ""}', onButton3);
	scr.setButton("withdrawal", "Снять наличные",true, m_ATMFunctions.dispenser, '{"icon": ""}', onButton4);
	scr.setButton("pay", "Оплатить", true, false, '{"icon": ""}', onButton5);
	scr.setButton("send", "Перевести", true, false, '{"icon": ""}', onButton6);
	scr.setButton("help", "Помощь", true, false, '{"icon": "../../graphics/icon_help.svg"}', onButton7);
	scr.setButton("settings", "Настройки карты", true, true/*m_session.cardIcon.our*/, '{"icon": "../../graphics/icon_settings.svg"}', onButton8);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onCancel);
	else
		scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);
	
	scr.setImage("bg","../../graphics/BG_blur.jpg","");
	scr.setLabel("card",m_session.cardIcon.value, m_session.cardIcon.ext);
	//scr.setImage("card","../../graphics/card.svg","");

	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setLabel("title", "Выберите услугу", "");
	scr.setLabel("note", "Select service", "");
	
	//scr.setTimeout("5000", "", onTimeout);
	scr.render("main_menu");
	alertMsgLog('[SCRIPT] '+scr.name+'. Выбор услуги.');
}

var cashoutInputAmount = function(args){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	function deleteInfoLabels(){
		scr.deleteLabel('comission');
		scr.deleteLabel('max_sum_text');
		scr.deleteLabel('error_text1');
		scr.deleteLabel('error_text2');		
	}
	function setPopularAmounts(_RowInd, _StartInd, _Count){
		var fastCashHelp = '';
		for(var j = _StartInd; j < m_session.fashCash.length && j < _StartInd + _Count; ++j)
			fastCashHelp += (fastCashHelp==''?'{':',{')+'"text":"'+AddSpace(m_session.fashCash[j])+' '+m_Currency.getSelectedSymbol()+'","value":'+m_session.fashCash[j]+'}';
		if(fastCashHelp != '')
			scr.setLabel("title_popular"+(_RowInd != 1? _RowInd.toString(): ""), "Быстрый набор", '{"values":['+fastCashHelp+'], "display_group":"fast_btns'+(_RowInd != 1? _RowInd.toString(): "")+'"}');
		else
			scr.deleteLabel("title_popular"+(_RowInd != 1? _RowInd.toString(): ""));
	}
	var onButton3 = function(name){//снять
		scr.nextScreen(giveMoney,[amount, 1]);
	}
	var onButton4 = function(name){
		scr.nextScreen(giveMoney,4900);
	}	
	var onButton5 = function(name){
		scr.nextScreen(giveMoney,500);
	}	
	var onButton6 = function(name){
		scr.nextScreen(giveMoney,10000);
	}	
	var onButton7 = function(name){//Вернуться в меню
		scr.nextScreen(serviceSelect);
	}
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		//scr.cancel();
	}	
	
	var onList = function(args){
		alertMsgLog(scr.name+' onList args '+args);
		var pKey = "", help = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0){
			if(typeof args[1] == 'undefined')
				help = args;
			else{
				pKey = args[0];
				help = args[1];
				if(typeof args[2] != 'undefined' && args[2] == 'fast_btns'){
					alertMsgLog('m_FastButtonFlag!');
					m_FastButtonFlag = true;
				}
			}
		}
		
		m_Currency.setSelected(help);
		//alert('Selected curr code: '+m_Currency.getSelectedCode());
		alertMsgLog('currency: '+m_Currency.getSelectedCode()+', amount: '+amount);
		scr.setList("currency", m_Currency.getNames(), m_Currency.selected, m_Currency.getJSON(), onList);
		deleteInfoLabels();
		window.external.exchange.RefreshScr();
	}
	
	var onInput = function(args){
		alertMsgLog('[SCRIPT] '+scr.name+' onAmount args '+args);
		var pKey = "";
		var amountLast = amount;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = parseFloat(args);
			else {
				pKey = args[0];
				help = parseFloat(args[1]);
				if(typeof args[2] != 'undefined' && args[2] == 'fast_btns'){
					alertMsgLog('m_FastButtonFlag!');
					m_FastButtonFlag = true;
				}
			}
			if(typeof help == 'number' && !isNaN(help))
				amount = help;
			else
				amount = 0;
		}
		else
			amount = 0;
		
		alertMsgLog('amountLast: '+amountLast+', amount: '+amount);
		if(m_FastButtonFlag && (amountLast.toString().length == amount.toString().length+1)){
			m_FastButtonFlag = false;
			amount = 0;
		}
		//if(amountLast!= amount)
		//	m_FastButtonFlag = false;
		scr.setInput("sum", amount.toString(), "", "0", false, true, '{"maxsum": 100000, "maxlength": 9, "empty": "0","display_group": "sum_place"}', "text", onInput, "None");
		m_session.fashCash = m_FastCash.getGeneral(m_Currency.getSelectedCode(),amount);
		checkboxShow = m_FastCash.validAmount(m_Currency.getSelectedCode(),amount);
		if(!checkboxShow){
			setPopularAmounts(1, 0, 3);
			setPopularAmounts(2, 3, 3);
			//setPopularAmounts(3, 6, 3);
		}
		else{
			scr.deleteLabel("title_popular");
			scr.deleteLabel("title_popular2");
			scr.deleteLabel("title_popular3");
		}
		
		fastCashHelp = '';
		for(var j = 1; j < 10; ++j)
			fastCashHelp += (fastCashHelp==''?'{':',{')+'"text":"'+j+'","type":"digit"'+((!m_FastCash.button[j])?',"disabled": true':'')+'}';
		scr.setLabel("keyboard", "", '{"values": ['+fastCashHelp+',{"text": "Размен","type": "bynotes","disabled": true},{"text": "0","type": "digit"'+((!m_FastCash.button[0])?',"disabled": true':'')+'},{"text": "Удалить","type": "delete"}],"display_group": "fast_btns"}');
		//scr.setList("currency", m_Currency.getNames(), m_Currency.selected, m_Currency.getJSON(), onList);
		
		if(amount != 0){
			scr.setButton("take", "Снять наличные", true, checkboxShow, '{"icon": ""}', onButton3);
			//scr.setButton("back", "Вернуться в меню", false, false, '{"icon": ""}', onButton7);
			scr.setButton("back", "Вернуться в меню", true, true, '{"icon": ""}', onButton7);
			scr.setInput("checkbox_print", "true", "", "", checkboxShow, checkboxShow, "", "checkbox", onCheckBox, "", m_session.checkFlag);
		}
		else{
			checkboxShow = false;
			scr.setButton("back", "Вернуться в меню", true, true, '{"icon": ""}', onButton7);
			scr.setButton("take", "Снять наличные", true, false, '{"icon": ""}', onButton3);
			scr.setInput("checkbox_print", "true", "", "", checkboxShow, checkboxShow, "", "checkbox", onCheckBox, "", m_session.checkFlag);
		}

		deleteInfoLabels();
		window.external.exchange.RefreshScr();
	}
	var onCheckBox = function(args){
		alertMsgLog('[SCRIPT] '+scr.name+' onCheckBox args: '+args);
		var checkHelp;
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = args;
			else {
				pKey = args[0];
				help = args[1];
			}
			help = help.toLowerCase();
			if(help == 'true')
				checkHelp = true;
			else
				checkHelp = false;
		}
		else
			checkHelp = false;
		alertMsgLog('[Print]: '+checkHelp);
		m_session.checkFlag = checkHelp;
		scr.setInput("checkbox_print", "true", "", "", checkboxShow, checkboxShow, "", "checkbox", onCheckBox, "", m_session.checkFlag);
	}
	
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			if(m_HostServiceName == 'idle'){
				scr.nextScreen(main);
				return;
			}
			else if(m_HostServiceName == 'pin_error'){
				scr.nextScreen(msgResult,["Неверно введенный пин-код", "end"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "end"]);
				return;
			}
			else if(m_HostServiceName == 'webius_menu'){
				alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					return;
				}
				else{
					alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
					return;
				}
			}
			else if(m_HostServiceName == 'spec_cancel'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'end_session'){
					scr.nextScreen(start, 1);
					return;
				}
				else{
					scr.nextScreen(msgResult,"Ошибка");
					return;
				}
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else{
				scr.nextScreen(msgResult,"Ошибка");
				return;
			}
		}
		else if(m_HostServiceState == 'cancel'){
			alertMsgLog(scr.name+' from host status cancel, return to start');
			scr.cancel();
			return;
		}
		else{
			alertMsgLog(scr.name+' from host unknown state'+m_HostServiceState+', cancelling.');
			scr.cancel();
			return;
		}

	}
	scr.addCall("HostScriptAddon", onService);

	var _message = [], _type = 'info';
	if(typeof args != 'undefined' && args.constructor === Array && args.length > 0){
		_type = args[0];
		if(args.length > 1)
			for(var j = 1; j < args.length; ++j)
				_message[j-1] = args[j];
	}
	else if(typeof args != 'undefined')
		_message[0] = args;
	if(_message.length > 0){
		if(_type == 'info'){
			if(_message.length > 1){
				scr.setLabel('error_text1',_message[0],'{"display_group":"sum_error", "themes":["blue"]}');
				scr.setLabel('error_text2',_message[1],'{"display_group":"sum_error", "themes":["blue"]}');
			}
			else{
				scr.setLabel('error_text1',' ','{"display_group":"sum_error", "themes":["blue"]}');
				scr.setLabel('error_text2',_message[0],'{"display_group":"sum_error", "themes":["blue"]}');
			}
			scr.setImage("error","../../graphics/mes-info.svg","");
		}
		else if (_type == "err"){
			if(_message.length > 1){
				scr.setLabel('error_text1',_message[0],'{"display_group":"sum_error"}');
				scr.setLabel('error_text2',_message[1],'{"display_group":"sum_error"}');
			}
			else{
				scr.setLabel('error_text1',' ','{"display_group":"sum_error"}');
				scr.setLabel('error_text2',_message[0],'{"display_group":"sum_error"}');
			}
			scr.setImage("error","../../graphics/mes-error.svg","");
		}
		else {
			if(_message.length > 1){
				scr.setLabel('error_text1',_message[0],'{"display_group":"sum_error", "themes":["grey"]}');
				scr.setLabel('error_text2',_message[1],'{"display_group":"sum_error", "themes":["grey"]}');
			}
			else{
				scr.setLabel('error_text1',' ','{"display_group":"sum_error", "themes":["grey"]}');
				scr.setLabel('error_text2',_message[0],'{"display_group":"sum_error", "themes":["grey"]}');
			}
		}
	}
	
	var amount = 0;
	var m_FastButtonFlag = false;
	var checkboxShow = false;
	m_session.checkFlag = false;
	setBalanceAndPrintButtons(m_session.balanceShowReq, m_session.balancePrintNeed);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onCancel);
	else
		scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);	
	scr.setInput("sum", "0", "", "0", false, true, '{"maxsum": 100000, "maxlength": 9, "empty": "0","display_group": "sum_place"}', "text", onInput, "None");					

	m_FastCash = new FastCash(m_ATMMoneyInfo, m_MoneyHistory, m_session.balance, true);
	m_Currency = new CurrencyClass(m_FastCash.getCurrencyList());//['rub', 'euro', 'dollar']
	m_session.fashCash = m_FastCash.getGeneral(m_Currency.getSelectedCode(),0);
	
	alertMsgLog('fastcash: ' + m_session.fashCash.join());
	
	setPopularAmounts(1, 0, 3);
	setPopularAmounts(2, 3, 3);
	//setPopularAmounts(3, 6, 3);
	
	scr.setList("currency", m_Currency.getNames(), m_Currency.selected, m_Currency.getJSON(), onList);
	
	fastCashHelp = '';
	//alert('digits: '+m_FastCash.button);
	for(var j = 1; j < 10; ++j)
		fastCashHelp += (fastCashHelp==''?'{':',{')+'"text":"'+j+'","type":"digit"'+((!m_FastCash.button[j])?',"disabled": true':'')+'}';
	scr.setLabel("keyboard", "", '{"values": ['+fastCashHelp+',{"text": "Размен","type": "bynotes","disabled": true},{"text": "0","type": "digit"'+((!m_FastCash.button[0])?',"disabled": true':'')+'},{"text": "Удалить","type": "delete"}],"display_group": "fast_btns"}');
	
	scr.setButton("back", "Вернуться в меню", true, true, '{"icon": ""}', onButton7);
	scr.setButton("take", "Снять наличные", true, false, '{"icon": ""}', onButton3);

	scr.setLabel("print_receipt", "Распечатать чек<br>после операции?", '{"display_group": "print_receipt"}');
	scr.setInput("checkbox_print", "true", "", "", checkboxShow, checkboxShow, "", "checkbox", onCheckBox, "", getUserDataField('printcheck'));
	
	scr.setButton("receipt", "receipt", '{"icon": "../../graphics/icon_check.png"}', onButton6);
	
	scr.setImage("bg","../../graphics/BG_blur.jpg","");
	scr.setLabel("card",m_session.cardIcon.value, m_session.cardIcon.ext);
	scr.setImage("error","../../graphics/mes-error.svg","");
	scr.setImage("offer","../../graphics/offer-icon.png","");

	scr.setLabel("title_sum", "Введите сумму:", "");	
	scr.render("cashout_amount");
}
var giveMoney = function(args){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }	
	scr.addOnError(onError);
	var onContinueCall = function(args) {
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onContinueCall, value: '+_args);
		
		if(_args == onContinueOptions[1]){
			scr.setModalMessage("", "", -1, false, "", onTimeoutButton);
			window.external.exchange.ExecNdcService("cancel", "");
		}
		else{
			window.external.exchange.ExecNdcService("continue", "");
		}
	}
	var onEmptyButton = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', onEmptyButton '+name+'.');
	}
	var onTimeout = function(args){
		alertMsgLog(scr.name+' onTimeout');
		window.external.exchange.refreshScr();
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			alertMsgLog('[SCRIPT] '+scr.name+', Service '+m_HostServiceName+'.');
			if(m_HostServiceName == 'cashout'){
				step  = 0;
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					alertMsgLog('[SCRIPT] '+scr.name+' host cashout choosen');
					//scr.setWait(true, "Секундочку, я считаю...", '{"icon": "", "rotate": true, "loader":"loader"}');
					//window.external.exchange.refreshScr();
					return;
				}
				else if(m_HostScreen == 'continue'){
					scr.setLabel("modal_text2",'Продолжить работу?', "");
			
					var help = onContinueOptions.join().toString();
					scr.setModalMessage('Операция не разрешена.', onContinueOptions.join(), -1, true, '{"icon": "","options_settings":[{"name":"continue","icon":""},{"name":"logout","icon":"../../graphics/icon-logout.svg"}]}', onContinueCall);
					scr.setWait(false, "Жду ответа...", "");
					//window.external.exchange.refreshScr();
					scr.render("cashout_amount");
				}
				else if(m_HostScreen == 'card_return'){
					scr.setLabel('wait_text2', 'Сначала заберите карту', "");	
					scr.setWait(true, "Успешно.", '{"icon": "../../graphics/icon-pick-card.svg", "rotate":true,"loader":"countdown","count":30}');
					scr.render("cashout_amount");
					//window.external.exchange.refreshScr();
					//scr.render("take_card");
				}
				else if(m_HostScreen == 'money_take'){
					window.external.exchange.scrData.flush();
					//scr.setLabel("wait_text2", "", "");
					//scr.setModalMessage("", "", -1, false, "", onEmptyButton);
					//scr.setWait(true, "Возьмите деньги", '{"icon": "../../graphics/icon-pick-card.svg","rotate":true,"loader":"countdown","count":60}');
					//window.external.exchange.refreshScr();
					//scr.render('cashout_amount');
					scr.setLabel("text", "Возьмите деньги", "");
					scr.setLabel("loader","60", '{"loader":"countdown"}');
					scr.setImage("bg","../../graphics/BG_blur.jpg","");
					scr.render("wait_message");
					m_session.balance = 0;
					return;
				}
				else if(m_HostScreen == 'end_session'){
					alertMsgLog('[Print]: '+m_session.checkFlag);
					saveToHistory(amount, m_Currency.getSelectedCode(), m_session.checkFlag);
					scr.nextScreen(msgResult, ["Операция завершена...", "end"]);
					return;

					//scr.setWait(true, "Спасибо", '{"icon": "../../graphics/icon-ok.svg", "loader":"ellipse","theme":"blue"}');
					//window.external.exchange.refreshScr();
					//window.external.exchange.refreshScr();
					//scr.render('cashout_amount');
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'pin_error'){
				scr.nextScreen(msgResult,["Неверно введенный пин-код", "end"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "end"]);
				return;
			}
			else if(m_HostServiceName == 'idle'){
				scr.nextScreen(main);
				return;
			}
			else if(m_HostServiceName == 'webius_menu'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					if(step != 1)
						scr.nextScreen(serviceSelect);
					else
						alertMsgLog('previous operation finished');
					return;
				}
				else{
					scr.nextScreen(msgResult,["Ошибка выдачи денег", "err"]);
					return;
				}
			}
			else if(m_HostServiceName == 'request_err'){
				scr.nextScreen(requestResult, ['err', 'cashout']);
				return;
			}
			else if(m_HostServiceName == 'selfincass_impossible'){
				scr.nextScreen(requestResult, ['selfincass_impossible', 'cashout']);
				return;
			}
			else if(m_HostServiceName == 'request_impossible'){
				scr.nextScreen(requestResult, ['req_impossible', 'cashout']);
				return;
			}
			else if(m_HostServiceName == 'request_not_performed'){
				scr.nextScreen(requestResult, ['request_not_performed', 'cashout']);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "err"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_allowed'){
				scr.nextScreen(requestResult,["request_not_allowed", "cashout"]);
				return;
			}
			else if(m_HostServiceName == 'incorrect_amount'){
				scr.nextScreen(requestResult,["incorrect_amount", "cashout"]);
				return;
			}
			else if(m_HostServiceName == 'incorrect_amount2'){
				scr.nextScreen(requestResult,["incorrect_amount2", "cashout"]);
				return;
			}
			else if(m_HostServiceName == 'not_enough_money'){
				scr.nextScreen(requestResult, ['not_enough_money', 'cashout']);
				return;
			}
			else if(m_HostServiceName == 'limit_exceeded'){
				scr.nextScreen(requestResult, ['limit_exceeded', 'cashout']);
				return;
			}
			else if(m_HostServiceName == 'amount_to_big'){
				scr.nextScreen(requestResult, ['amount_to_big', 'cashout']);
				return;
			}
			else if(m_HostServiceName == 'pinchange_not_allowed'){
				scr.nextScreen(requestResult,['pinchange_not_allowed', 'cashout']);
				return;
			}
			else if(m_HostServiceName == 'card_seized'){
				scr.nextScreen(msgResult,['Ваша карта задержана<br>по требованию банка-эмитента', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'card_not_serviced'){
				scr.nextScreen(msgResult,['Извините, карта не обслуживается', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'card_expired'){
				scr.nextScreen(msgResult,['Ваша карта просрочена', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'no_account'){
				scr.nextScreen(msgResult,['Нет требуемого счета', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'pin_try_exceeded'){
				scr.nextScreen(msgResult,['Превышено максимально допустимое<br>число попыток ввода пин-кода', 'err']);
				return;
			}
			else if(m_HostServiceName == 'spec_cancel'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'end_session'){
					scr.nextScreen(start, 1);
					return;
				}
				else{
					scr.nextScreen(msgResult,["Ошибка выдачи денег", "err"]);
					return;
				}
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else{
				scr.nextScreen(msgResult,["Ошибка выдачи денег", "err"]);
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' state is '+m_HostServiceState+', need to cancel.');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	var onContinueOptions = ['Продолжить','Выйти'];
	var amount = 0, step = 0;
	if(typeof args != 'undefined' && args.constructor === Array && args.length > 0){
		amount = args[0];
		if(args.length > 1)
			step = args[1];
	}
	else
		amount = args;
	
	scr.setButton("showremains", "Показать остаток", '{"icon": "../../graphics/print-balance-pressed.svg"}', onEmptyButton);
	scr.setButton("print", "", true, false, '{"icon": ""}', onEmptyButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onEmptyButton);
	else
		scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onEmptyButton);	
	scr.setInput("sum", amount.toString(), "", "0", false, true, '{"maxsum": 100000, "maxlength": 9, "empty": "0","display_group": "sum_place"}', "text", onEmptyButton, "None");
	scr.setInput("checkbox_print", "true", "", "", true, true, "", "checkbox", onEmptyButton, "", m_session.checkFlag);

	scr.setList("currency", m_Currency.getNames(), m_Currency.selected, m_Currency.getJSON(), onEmptyButton);
	
	
	scr.setButton("receipt", "receipt", '{"icon": "../../graphics/icon_check.png"}', onEmptyButton);
	
	scr.setImage("bg","../../graphics/BG_blur.jpg","");
	//scr.setImage("card","../../graphics/card.svg","");
	scr.setLabel("card",m_session.cardIcon.value, m_session.cardIcon.ext);
	scr.setImage("error","../../graphics/mes-error.svg","");
	scr.setImage("offer","../../graphics/offer-icon.png","");

	scr.setLabel("title_sum", "Введите сумму", "");	
	
	scr.setLabel("print_receipt", "Распечатать чек после операции?", '{"display_group": "print_receipt"}');

	//var fastCashHelp = '';
	//for(var j = 0; j < m_session.fashCash.length; ++j)
	//	fastCashHelp += (fastCashHelp==''?'{':',{')+'"text":"'+AddSpace(m_session.fashCash[j])+' '+m_Currency.getSelectedSymbol()+'","value":'+m_session.fashCash[j]+'}';
	//if(fastCashHelp != '')
	//	scr.setLabel("title_popular", "", '{"values":['+fastCashHelp+'], "display_group":"fast_btns"}');
	
	scr.setLabel("keyboard", "", '{"values": [{"text": "1","type": "digit"},{"text": "2","type": "digit"},{"text": "3","type": "digit"},{"text": "4","type": "digit"},{"text": "5","type": "digit"},{"text": "6","type": "digit"},{"text": "7","type": "digit"},{"text": "8","type": "digit"},{"text": "9","type": "digit"},{"text": "Размен","type": "bynotes","disabled": true},{"text": "0","type": "digit"},{"text": "Удалить","type": "delete"}],"display_group": "fast_btns"}');
	
	scr.setButton("take", "Снять наличные", '{"icon": ""}', onEmptyButton);		
	//scr.render("cashout_amount");
	
	scr.setWait(true, "Жду ответа...", '{"icon": "", "rotate": true, "loader":"loader"}');
	window.external.exchange.RefreshScr();
	
	var help = {};
	help['amount'] = amount;
	if(m_session.checkFlag)
		window.external.exchange.ExecNdcService("cashoutprint", JSON.stringify(help));
	else
		window.external.exchange.ExecNdcService("cashout", JSON.stringify(help));
}

var ekassir = function(step){
    var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else
					scr.cancel();
			}
			else if(m_HostServiceName == "webius_menu"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					scr.nextScreen(serviceSelect);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "oos")
				scr.nextScreen(oos);
			else if(m_HostServiceName == "ekassir"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'ekassir'){
					scr.nextScreen(ekassir, 2);
					return;
				}
				else if(m_HostScreen == 'wait'){
					scr.nextScreen(ekassir, 3);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else
				scr.cancel();
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' not ok');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	if(step == 1)
		window.external.exchange.ExecNdcService("ekassir", "");
	else if(step == 2){
		scr.render("ekassir");
		alertMsgLog('[SCRIPT] '+scr.name+'. Ekassir.');
	}
	else {
		scr.setLabel("title", "Экран ожидания", "");
		scr.render("test");
		alertMsgLog('[SCRIPT] '+scr.name+'. Экран ожидания.');
	}
}

var helpMenu = function() {
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){
		scr.nextScreen(msgResult,"Ближайшие банкоматы бла бла бла");
	}
	var onButton2 = function(name){		
		scr.nextScreen(msgResult,"Ближайшие отделения бла бла бла");
	}
	
	var onButton3 = function(name){		
		scr.nextScreen(serviceSelect);
	}
	
	var onCancel = function(name){		
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	scr.setButton("next_bankomat", "Ближайший банкомат", "{\"icon\": \"img/icon-5-1.svg\"}", onButton1);
	scr.setButton("next_office", "Ближайшее отделение", "{\"icon\": \"img/icon-5-2.svg\"}", onButton2);
	scr.setButton("popup_cancel", "Отмена", '{"icon": ""}', onButton3);
	scr.setButton("popup_exit", "Выйти", '{"icon": ""}', onCancel);
	
	
	scr.setLabel("phone_text", "Бесплатный звонок по России", "");
	scr.setLabel("phone", "8 800 700-78-77", "");
	scr.render("help_menu");	
}

var cashin = function(step) {
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	function deleteElements(fullFlag){
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(false, "Жду ответа...", "");
		scr.deleteLabel("modal_text2");
		scr.deleteLabel("wait_text2");
		scr.deleteLabel("popup_text");
		scr.deleteLabel("popup_sum");
		scr.deleteLabel("popup_title_sum");
		scr.deleteLabel("popup_comission");
		scr.deleteLabel("bynotes");
	}
	var onEmptyButton = function(){
	}
	var onMoneyCall = function(args) {
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onMoneyCall, value: '+_args);
		
		if(_args == onMoneyCallOptions[0]){
			scr.setModalMessage("", "", -1, false, "", onTimeoutButton);
			window.external.exchange.ExecNdcService("return", "");
			m_session.serviceName = "return";
		}
		else{
			window.external.exchange.ExecNdcService("cancel", "");
			m_session.serviceName = "cancel";
		}
	}
	var onMoneyAccept = function(args){
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onMoneyAccept, value: '+_args);
		
		if(_args == onMoneyAcceptOptions[0]){
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("return", "");
			m_session.serviceName = "return";
		}
		else if(_args == onMoneyAcceptOptions[1]){
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("add", "");
			m_session.serviceName = "add";
		}
		else{
			m_session.balance = 0;
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("accept", "");
			m_session.serviceName = "accept";
		}
	}
	var onMoreTimeCall = function(args){
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onMoreTime, value: '+_args);
		
		if(_args == onMoreTime[0]){
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("nomoretime", "");
			m_session.serviceName = "return";
		}
		else{
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("moretime", "");
			m_session.serviceName = "moretime";
		}
	}
	var onButton1 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. cashin зачислить ' + name);
		window.external.exchange.ExecNdcService("accept", "");
	}
	var onButton2 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. cashin добавить ' + name);
		window.external.exchange.ExecNdcService("add", "");
	}
	var onButton3 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. cashin отменить ' + name);
		window.external.exchange.ExecNdcService("cancel", "");
	}
	var onButton4 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжить для запроса.');
		window.external.exchange.ExecNdcService("request", "");
	}
	var onCancel = function(name){
		scr.setLabel("text", "Ну как так?", "");	
		scr.setLabel("loader","60", '{"loader":"ellipse", "icon":"../../graphics/icon-smile-1.svg"}');
		//scr.setButton("quit","123",false,false,'{"icon": ""}',onError);
		//scr.setButton("main_menu","12321",false,false,'{"icon": ""}',onError);
		//scr.setImage("smile","../../graphics/icon-smile-3.svg","");
		scr.render("wait_message");		
		window.external.exchange.ExecNdcService("cancel", "");
	}
	var onSpecCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		m_session.serviceName = 'f015cancel';
		window.external.exchange.ExecNdcService("f015cancel", "");
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			alertMsgLog('[SCRIPT] '+scr.name+', Service '+m_HostServiceName+'.');
			if(m_HostServiceName == 'webius_menu'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					//scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else if(m_HostScreen == 'main'){
					if(step != 1){
						if(m_session.serviceName == "cancel"){
							alertMsgLog('ServiceName cancelspec');
							window.external.exchange.ExecNdcService("cancelspec","");
						}
						else{
							alertMsgLog('return to serviceSelect');
							scr.nextScreen(serviceSelect);
						}
					}
					else
						alertMsgLog('previous operation finished');
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "spec_cancel"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					scr.nextScreen(specCancelWait, 1);
					return;
				}
				else if(m_HostScreen == 'return_card'){
					scr.nextScreen(specCancelWait, 2);
					return;
				}
				else if(m_HostScreen == 'end_session'){
					scr.nextScreen(specCancelWait, 3);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(specCancelWait, 4);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'pin_error'){
				scr.nextScreen(msgResult,["Неверно введенный пин-код", "end"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "end"]);
				return;
			}
			else if(m_HostServiceName == 'cashin'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				var canRefresh = false;
				//if(step != 3 && step != 5 && step !=11)
					canRefresh = true;
				if(m_HostScreen == 'wait'){
					step = 2;
					return;
				}
				else if(m_HostScreen == 'wait_show'){
					if(step == 14)
						return;
					else {
						step = 4;
						//scr.nextScreen(cashin, 4);
						deleteElements();
						scr.setWait(true, "Считаем...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
						if(canRefresh)
							window.external.exchange.RefreshScr();
						else
							scr.render("deposit_select_currency");
					}
					return;
				}
				else if(m_HostScreen == 'wait_device'){
					step = 13;
					//scr.nextScreen(cashin, 13);
					deleteElements();
					scr.setWait(true, "Подготавливаем оборудование...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
					if(canRefresh)
						window.external.exchange.RefreshScr();
					else
						scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'wait_device_close'){
					step = 15;
					//scr.nextScreen(cashin, 15);
					deleteElements();
					scr.setWait(true, "Закрываем купюроприемник...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
					if(canRefresh)
						window.external.exchange.RefreshScr();
					else
						scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'wait_money_check_return'){
					step = 14;
					//scr.nextScreen(cashin, 14);
					deleteElements();
					scr.setWait(true, "Отсчитываем деньги...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
					if(canRefresh)
						window.external.exchange.RefreshScr();
					else
						scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'money_insert'){
					step = 3;
					//scr.nextScreen(cashin, 3);
					deleteElements();
					scr.setLabel("modal_text2",'не более 200 купюр', "");
					var help = onMoneyCallOptions.join().toString();
					//scr.setModalMessage('Вносите рубли пачкой', onMoneyCallOptions.join(), -1, true, '{"icon": "","loader":"countdown","count": 90,"options_settings":[{"name":"cancel","icon":""},{"name":"logout","icon":"../../graphics/icon-logout.svg"}]}', onMoneyCall);
					scr.setModalMessage('Вносите рубли пачкой', onMoneyCallOptions.join(), -1, true, '{"icon": "","rotate":true,"loader":"countdown","count": 90,"options_settings":[{"name":"logout","icon":"","theme":"btn-white-red"}]}', onMoneyCall);
					//window.external.exchange.RefreshScr();
					scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'money_check'){
					if(step == 11)
						return;
					else{
						step = 4;
						//scr.nextScreen(cashin, 4);
						deleteElements();
						scr.setWait(true, "Считаем...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
						if(canRefresh)
							window.external.exchange.RefreshScr();
						else
							scr.render("deposit_select_currency");
					}
					return;
				}
				else if(m_HostScreen == 'money_menu'){
					step = 5;
					//scr.nextScreen(cashin, 5);
					deleteElements();
					scr.setLabel("popup_text", 'Будет зачислено:', "");
					scr.setLabel("popup_sum", m_HostScreenText.trim()+' ₽', "");
					scr.setLabel("popup_title_sum", 'Внесено: '+m_HostScreenText.trim()+' ₽', "");
					scr.setLabel("popup_comission", 'Комиссия: 0 ₽', "");
					scr.setLabel("bynotes", 'Купюры:', '{"values":['+window.external.exchange.getAllAcceptedNotes("643")+'],"display_group": "bynotes"}');
					var help = onMoneyAcceptOptions.join().toString();
					scr.setModalMessage('', help, 0, true, '{"options_settings":[{"name":"back","icon":""},{"name":"add","icon":""},{"name":"deposit","icon":"","theme":"btn-green"}]}', onMoneyAccept);
					//window.external.exchange.RefreshScr();
					scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'money_info'){
					step = 6;
					//scr.nextScreen(cashin, 6);
					deleteElements();
					scr.setLabel("warning", "Будет зачисленно: "+m_HostAmount, "");			
					scr.setLabel("loader","60", '{"loader":"ellipse", "icon":"../../graphics/icon-smile-1.svg"}');
					if(canRefresh)
						window.external.exchange.RefreshScr();
					else
						scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'wait_request'){
					if(step != 7){
						step = 7;
						//scr.nextScreen(cashin, 7);
						deleteElements();
						scr.setModalMessage("", "", -1, false, "", onEmptyButton);
						scr.setWait(true, "Зачисляем...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
						if(canRefresh)
							window.external.exchange.RefreshScr();
						else
							scr.render("deposit_select_currency");
					}
					return;
				}
				else if(m_HostScreen == 'wait_data_to_send'){
					if(step != 17){
						step = 17;
						//scr.nextScreen(cashin, 7);
						deleteElements();
						scr.setModalMessage("", "", -1, false, "", onEmptyButton);
						scr.setWait(true, "Собираем данные...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
						if(canRefresh)
							window.external.exchange.RefreshScr();
						else
							scr.render("deposit_select_currency");
					}
					return;
				}
				else if(m_HostScreen == 'wait_data_chip'){
					if(step != 18){
						step = 18;
						//scr.nextScreen(cashin, 7);
						deleteElements();
						scr.setModalMessage("", "", -1, false, "", onEmptyButton);
						scr.setWait(true, "Формируем запрос...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
						if(canRefresh)
							window.external.exchange.RefreshScr();
						else
							scr.render("deposit_select_currency");
					}
					return;
				}
				else if(m_HostScreen == 'error'){
					step = 8;
					//scr.nextScreen(cashin, 8);
					deleteElements();
					scr.setModalMessage("", "", -1, false, "", onEmptyButton);
					scr.setWait(true, "Ошибка зачисления...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
					if(canRefresh)
						window.external.exchange.RefreshScr();
					else
						scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'card_return'){
					step = 9;
					//scr.setWait(true, "Заберите карту...", '{"icon": "../../graphics/icon-pick-card.svg", "rotate":true,"loader":"countdown","count":30}');
					//scr.render("deposit_select_currency");
					//return;

					deleteElements();
					scr.setLabel("text", "Заберите Вашу карту", "");
					scr.setLabel("loader","30", '{"loader":"countdown"}');
					scr.setImage("bg","../../graphics/BG_blur.jpg","");
					scr.render("wait_message");
					m_session.balance = 0;
					return;
				}
				else if(m_HostScreen == 'end_session'){
					step = 10;
					//scr.nextScreen(cashin, 10);
					deleteElements();
					scr.setWait(true, "Пожалуйста, подождите...", '{"icon": "../../graphics/icon-ok.svg", "loader":"ellipse","theme":"blue"}');
					if(canRefresh)
						window.external.exchange.RefreshScr();
					else
						scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'money_return'){
					step = 11;
					//scr.nextScreen(cashin, 11);
					deleteElements();
					scr.setLabel("wait_text2", "банкноты", "");
					scr.setModalMessage("", "", -1, false, "", onEmptyButton);
					scr.setWait(true, "Заберите непринятые", '{"icon": "","rotate":true,"loader":"countdown","count":60}');
					scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'selfincass_impossible'){
					if(step != 16){
						step = 16;
						//scr.nextScreen(cashin, 8);
						deleteElements();
						scr.setModalMessage("", "", -1, false, "", onEmptyButton);
						scr.setWait(true, "Ошибка зачисления...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
						if(canRefresh)
							window.external.exchange.RefreshScr();
						else
							scr.render("deposit_select_currency");
					}
					else {
						deleteElements();
						scr.setLabel("wait_text2", "купюры", "");
						scr.setModalMessage("", "", -1, false, "", onEmptyButton);
						scr.setWait(true, "Заберите непринятые", '{"icon": "","rotate":true,"loader":"loader","count":60}');
						scr.render("deposit_select_currency");
						return;
					}
					return;
				}
				else if(m_HostScreen == 'need_more_time'){
					step = 12;
					//scr.nextScreen(cashin, 12);
					deleteElements();
					var help = onMoreTime.join().toString();
					scr.setModalMessage('Вам требуется еще время?', help, -1, true, '{"icon": "","loader":"countdown","count": 25,"options_settings":[{"name":"logout","icon":""},{"name":"cancel","icon":""}]}', onMoreTimeCall);
					scr.render("deposit_select_currency");
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'idle'){
				alertMsgLog('[SCRIPT] '+scr.name+' host '+m_HostScreen+' in '+m_HostServiceName);
				if(m_HostScreen == 'wait'){
					scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else if(m_HostScreen == ''){
					scr.nextScreen(main);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'selfincass_impossible'){
				scr.nextScreen(requestResult, ['cashin_impossible', 'cashin']);
				return;
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else if(m_HostServiceName == 'card_captured'){
				scr.nextScreen(msgResult,['Ваша карта задержана', 'err']);
				return;
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' state is '+m_HostServiceState+', need to cancel.');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	var onMoneyAcceptOptions = ['Вернуть','Добавить','Зачислить'];
	var onMoneyCallOptions = ['Отменить операцию'];
	//var onMoneyCallOptions = ['Отменить операцию','Выйти'];
	var onMoreTime = ['Завершить', 'Продолжить'];
	{//кнопки на верхней части подложки
		scr.setImage("bg","../../graphics/BG_blur.jpg","");
		scr.setLabel("card",m_session.cardIcon.value, m_session.cardIcon.ext);
		//scr.setImage("card","../../graphics/card.svg","");
		scr.setButton("showremains", "Показать остаток", '{"icon": "","state":""}', onEmptyButton);
		scr.setButton("print", "", '{"icon": ""}', onEmptyButton);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onEmptyButton);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onEmptyButton);
		scr.setButton("roubles", "Рубли", true, false, '{"icon": ""}', onEmptyButton);
		scr.setButton("dollars", "Доллары",true,false, '{"icon": ""}',"", onEmptyButton);
		scr.setButton("euro", "Евро", true,false,'{"icon": ""}',"",onEmptyButton);
	}

	if(step == 1){
		//m_session.balance = 0;
		scr.setWait(true, "Подготавливаем оборудование...", '{"icon": "", "loader":"loader"}');
		scr.render("deposit_select_currency");
		
		window.external.exchange.ExecNdcService("cashin", "");
	}
	else if(step == 2){
		scr.setLabel("warning", "Подготавливаем оборудование", "");	
		scr.setImage("smile","../../graphics/icon-smile-1.svg","");
		scr.render("deposit_select_currency");
	}
	else if(step == 3){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ожидание: устройство принимает не более 40 купюр за одну операцию. '+m_HostScreen);
		scr.setLabel("modal_text2",'не более 200 купюр', "");
		var help = onMoneyCallOptions.join().toString();
		//scr.setModalMessage('Вносите рубли пачкой', onMoneyCallOptions.join(), -1, true, '{"icon": "","loader":"countdown","count": 90,"options_settings":[{"name":"cancel","icon":""},{"name":"logout","icon":"../../graphics/icon-logout.svg"}]}', onMoneyCall);
		scr.setModalMessage('Вносите рубли пачкой', onMoneyCallOptions.join(), -1, true, '{"icon": "","rotate":true,"loader":"countdown","count": 90,"options_settings":[{"name":"logout","icon":"","theme":"btn-white-red"}]}', onMoneyCall);
		scr.render("deposit_select_currency");
	}
	else if(step == 4){
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(true, "Считаем...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
		scr.render("deposit_select_currency");
	}
	else if(step == 5){
		//scr.setLabel("modal_left_text", 'Вы внесли '+m_HostScreenText+' ₽', "");
		//scr.setLabel("modal_left_text2", 'Будет зачислено', "");
		//scr.setLabel("modal_big_text", m_HostScreenText+' ₽', "");
		//getVar("0"+i);
		//var bynotesLabel = "", notesCount = 0;
		//var notesHelp = [{value:10, text:'10 ₽'},{value:10, text:'10 ₽'},{value:50, text:'50 ₽'},{value:100, text:'100 ₽'},{value:500, text:'500 ₽'},{value:1000, text:'1000 ₽'},{value:5000, text:'5000 ₽'}];
		//for(var i = 1; i < 6;++i
		//{
		//	notesCount = getVar("0"+i);
		//	bynotesLabel += (bynotesLabel != '' ? ',{':'{')+'"text":"'+notesHelp[i].text+'", "value":'+notesHelp[i].value+', "left":50,"quantity":'+notesCount+'}';
		//}
		/*bynotes:
		{
			value: 'Купюры:',
			ext: {
				values: [
					{
						text: '5000 ₽',
						value: 5000,
						left: 50,
						quantity: 4,
					},
					{
						text: '200 ₽',
						value: 200,
						left: 0,
						quantity: 0,
					},
					{
						text: '2000 ₽',
						value: 2000,
						left: 20,
						quantity: 2,
					},
					{
						text: '100 ₽',
						value: 100,
						left: 0,
						quantity: 0,
					},
					{
						text: '1000 ₽',
						value: 1000,
						left: 500,
						quantity: 200,
					},
					{
						text: '50 ₽',
						value: 50,
						left: 0,
						quantity: 0,
					},
					{
						text: '500 ₽',
						value: 500,
						left: 100,
						quantity: 1,
					},
					{
						text: '10 ₽',
						value: 10,
						left: 100,
						quantity: 3,
					},
				],
				display_group: 'bynotes',
			},
		}*/
		

		scr.setLabel("popup_text", 'Будет зачислено:', "");
		scr.setLabel("popup_sum", m_HostScreenText.trim()+' ₽', "");
		scr.setLabel("popup_title_sum", 'Внесено: '+m_HostScreenText.trim()+' ₽', "");
		scr.setLabel("popup_comission", 'Комиссия: 0 ₽', "");
		scr.setLabel("bynotes", 'Купюры:', '{"values":['+window.external.exchange.getAllAcceptedNotes("643")+'],"display_group": "bynotes"}');
		var help = onMoneyAcceptOptions.join().toString();
		scr.setModalMessage('', help, 0, true, '{"options_settings":[{"name":"back","icon":""},{"name":"add","icon":""},{"name":"deposit","icon":"","theme":"btn-green"}]}', onMoneyAccept);
		
		//scr.render("deposit_select_currency");
		scr.render("deposit_amount_accepted");
	}
	else if(step == 6){
		scr.setLabel("warning", "Будет зачисленно: "+m_HostAmount, "");			
		scr.setLabel("loader","60", '{"loader":"ellipse", "icon":"../../graphics/icon-smile-1.svg"}');
		//scr.setButton("quit","Продолжить",true,true,'{"icon": ""}',onButton4);
		//scr.setImage("smile","../../graphics/icon-smile-1.svg","");		
		scr.render("wait_message");				
	}
	else if(step == 7){
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(true, "Зачисляем...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
		scr.render("deposit_select_currency");
	}
	else if(step == 8){
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(true, "Ошибка зачисления...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
		scr.render("deposit_select_currency");
	}
	else if(step == 9){
		scr.setWait(true, "Заберите карту...", '{"icon": "../../graphics/icon-pick-card.svg", "rotate":true,"loader":"countdown","count":30}');
		scr.render("deposit_select_currency");
	}
	else if(step == 10){
		scr.setWait(true, "Пожалуйста, подождите...", '{"icon": "../../graphics/icon-ok.svg", "loader":"ellipse","theme":"blue"}');
		scr.render("deposit_select_currency");
	}
	else if(step == 11){
		//scr.setLabel("wait_text2", "банкноты", "");
		scr.setLabel("wait_text2", "купюры", "");
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(true, "Заберите непринятые", '{"icon": "","rotate":true,"loader":"countdown","count":90}');
		scr.render("deposit_select_currency");
	}
	else if(step == 12){
		var help = onMoreTime.join().toString();
		scr.setModalMessage('Вам требуется еще время?', help, -1, true, '{"icon": "","loader":"countdown","count": 25,"options_settings":[{"name":"logout","icon":""},{"name":"cancel","icon":""}]}', onMoreTimeCall);
		scr.render("deposit_select_currency");
	}
	else if(step == 13){
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(true, "Подготавливаем оборудование...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
		scr.render("deposit_select_currency");
	}
	else if(step == 14){
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(true, "Отсчитываем деньги...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
		scr.render("deposit_select_currency");
	}
	else if(step == 15){
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(true, "Закрываем купюроприемник...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
		scr.render("deposit_select_currency");
	}
	else if(step == 16){
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(true, "Ошибка зачисления...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
		scr.render("deposit_select_currency");
	}
}
var depositSelectAdjunctionFrom = function() {
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	var onButton5 = function(name){
		scr.nextScreen(depositSelectAdjunctionMyCard,0);
	}
	var onButton4 = function(name){
		scr.nextScreen(depositSelectAdjunctionCurrency);		
	}
	
	var onButton6 = function(name){
		scr.nextScreen(depositSelectAdjunctionAnotherCard);		
	}
	var onButton3 = function(name){
		m_session.serviceName = 'depositFrom';
		scr.nextScreen(settingsMenu, 0);		
	}
	var onButton7 = function(name){
		scr.nextScreen(helpMenu);
	}
	var onMainMenu = function(name){
		scr.nextScreen(serviceSelect);
	}
	
	var onCancel = function(name){		
		window.external.exchange.ExecNdcService("cancel", "");
		//scr.cancel();
	}
	
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			if(m_HostServiceName == 'idle'){
				scr.nextScreen(main);
				return;
			}
			else if(m_HostServiceName == 'pin_error'){
				scr.nextScreen(msgResult,["Неверно введенный пин-код", "end"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "end"]);
				return;
			}
			else if(m_HostServiceName == 'webius_menu'){
				alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					return;
				}
				else{
					alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
					return;
				}
			}
			else if(m_HostServiceName == 'spec_cancel'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'end_session'){
					scr.nextScreen(start, 1);
					return;
				}
				else{
					scr.nextScreen(msgResult,"Ошибка");
					return;
				}
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else{
				scr.nextScreen(msgResult,"Ошибка");
				return;
			}
		}
		else if(m_HostServiceState == 'cancel'){
			alertMsgLog(scr.name+' from host status cancel, return to start');
			scr.cancel();
			return;
		}
		else{
			alertMsgLog(scr.name+' from host unknown state'+m_HostServiceState+', cancelling.');
			scr.cancel();
			return;
		}

	}
	scr.addCall("HostScriptAddon", onService);
	
	
	scr.setLabel("balance", "Ваш баланс...", "");	
	scr.setImage("bg","../../graphics/BG_blur.jpg","");
	scr.setLabel("card",m_session.cardIcon.value, m_session.cardIcon.ext);
	//scr.setImage("card","../../graphics/card.svg","");
	//scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onBalanceShowButton);		
	//scr.setButton("showremainson", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremains\"}", onBalanceShowButton);		
	//scr.setButton("print", "", '{"icon": ""}', onBalancePrintButton);
	setBalanceAndPrintButtons(m_session.balanceShowReq, m_session.balancePrintNeed);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onCancel);
	else
		scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);
	//scr.setButton("settings", "Настройки карты",true, /*m_session.cardIcon.our*/true, '{"icon": "../../graphics/icon_settings.svg"}', onButton3);
	//scr.setButton("help", "Помощь",true,false, '{"icon": "../../graphics/icon_help.svg"}', onButton7);
	scr.setButton("back", "Вернуться в меню", '{"icon": ""}', onMainMenu);
	
	scr.setButton("cash", "Наличными", '{"icon": "img/icon-4-1.svg"}', onButton4);
	scr.setButton("ourcard", "Со своей карты&nbsp;или&nbsp;счета",true,false, '{"icon": "img/icon-4-2.svg"}', onButton5);
	scr.setButton("extcard", "С карты другого&nbsp;банка",true,false, '{"icon": "img/icon-4-3.svg"}', onButton6);	
	
	
	scr.render("deposit_select_source");
}
var depositSelectAdjunctionCurrency = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	var onButton1 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Balance on screen ' + name);		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();	
	}	
	var onButton8 = function(name){
		scr.nextScreen(cashin, 1);
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		m_session.serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		//scr.cancel();
	}	
	
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			if(m_HostServiceName == 'idle'){
				scr.nextScreen(main);
				return;
			}
			else if(m_HostServiceName == 'pin_error'){
				scr.nextScreen(msgResult,["Неверно введенный пин-код", "end"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "end"]);
				return;
			}
			else if(m_HostServiceName == 'webius_menu'){
				alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					return;
				}
				else{
					alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
					return;
				}
			}
			else if(m_HostServiceName == 'spec_cancel'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'end_session'){
					scr.nextScreen(start, 1);
					return;
				}
				else{
					scr.nextScreen(msgResult,"Ошибка");
					return;
				}
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else{
				scr.nextScreen(msgResult,"Ошибка");
				return;
			}
		}
		else if(m_HostServiceState == 'cancel'){
			alertMsgLog(scr.name+' from host status cancel, return to start');
			scr.cancel();
			return;
		}
		else{
			alertMsgLog(scr.name+' from host unknown state'+m_HostServiceState+', cancelling.');
			scr.cancel();
			return;
		}

	}
	scr.addCall("HostScriptAddon", onService);
	
	if(m_Currency.length == 1){
		scr.nextScreen(cashin, 1);
		return;
	}
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setImage("bg","../../graphics/BG_blur.jpg","");
	scr.setLabel("card",m_session.cardIcon.value, m_session.cardIcon.ext);
	//scr.setImage("card","../../graphics/card.svg","");
	//scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onBalanceShowButton);		
	//scr.setButton("showremainson", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremains\"}", onBalanceShowButton);		
	//scr.setButton("print", "", '{"icon": ""}', onBalancePrintButton);
	setBalanceAndPrintButtons(m_session.balanceShowReq, m_session.balancePrintNeed);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onCancel);
	else
		scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);
	
	scr.setButton("roubles", "Рубли", '{"icon": ""}', onButton8);
	scr.setButton("dollars", "Доллары",true,false, '{"icon": ""}',"", onError);
	scr.setButton("euro", "Евро", true,false,'{"icon": ""}',"",onError);
	
	scr.setLabel("dollar", "Курс доллара 1$ = 67 Руб.", "");
	scr.setLabel("euro", "Курс Евро 1E = 70 Руб.", "");
	//scr.setLabel("conversion", "Текст под кнопками, краткое объяснение про конвертации 15 слов максимум. Ещё пара слов для объёма", "");
	scr.render("deposit_select_currency");
	
}
var depositSelectAdjunctionMyCard = function(type){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	var onButton1 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Balance on screen ' + name);		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();
	}
	
	var onButton8 = function(name){		
		scr.nextScreen(msgResult, "Успешно зачисленно "+amount+" Руб");
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		m_session.serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onAmount = function(args){
		alertMsgLog('[SCRIPT] '+scr.name+' onAmount args '+args);
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = parseFloat(arg);
			else {
				pKey = args[0];
				help = parseFloat(args[1]);
			}
			if(typeof help == 'number')
				amount = help;
			else
				amount = 0;
		}
		else
			amount = 0;
	}
	
	var amount = 0;
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", '{"icon": "","pair":"showremainson"}', onButton1);		
	scr.setButton("print", "", '{"icon": ""}', onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onCancel);
	else
		scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);	
	scr.setList("mbkList", "1234567890,0987654321,5555555565", 0, "", onList);
	scr.setInput("amount", "", "", "Введите сумму", "", onAmount);
	scr.setLabel("note", "Выберите источник платежа:", "");
	scr.setLabel("comment", "Комиссия за перевод не взымается", "");
	scr.setButton("button3", "Пополнить", "", onButton8);
	if(type == 1)
	{
		scr.setLabel("peny", "комиссия за перевод составит 2%", "");
		scr.setLabel("label1", "Будет зачисленно:", "");
		scr.setLabel("toAdjunct", "12312 250 Руб", "");
	}
	
	scr.render("test");	
}
var depositSelectAdjunctionAnotherCard = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	var onButton1 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Balance on screen ' + name);		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();
	}
	var onButton8 = function(name){		
		scr.nextScreen(depositSelectAdjunctionMyCard,1);
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		m_session.serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onAmount = function(args){
		alertMsgLog('[SCRIPT] '+scr.name+' onAmount args '+args);
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = parseFloat(arg);
			else {
				pKey = args[0];
				help = parseFloat(args[1]);
			}
			if(typeof help == 'number')
				amount = help;
			else
				amount = 0;
		}
		else
			amount = 0;
	}
	
	var amount = 0;
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", '{"icon": ""}', onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onCancel);
	else
		scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);	
	
	scr.setInput("pan", "", "", "Номер карты", "", onAmount);
	scr.setInput("expire", "", "", "Месяц и год", "", onAmount);
	scr.setInput("cvc", "", "", "CVC", "", onAmount);
	
	scr.setLabel("comment", "Приложите карту чтобы считать её номер", "");
	scr.setButton("button3", "Пополнить", "", onButton8);
	
	scr.render("test");
	
}

var settingsMenu = function(type) {
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }	
	scr.addOnError(onError);
	
	var onButton1 = function(name){
		if(type == 0)
			scr.nextScreen(settingsChangePin,1);
		else
			scr.nextScreen(settingsInternetBank);
	}
	var onButton2 = function(name){		
		if(type == 0)
			scr.nextScreen(settingsAddSIM,1);
		else
			scr.nextScreen(settingsCardRequisites,1);
	}
	
	var onButton3 = function(name){		
		if(type == 0)
			scr.nextScreen(msgResult,"3DSecure бла бла бла");
		else
			scr.nextScreen(msgResult,"Что-то с лимитами");
	}
	
	var onButton4 = function(name){		
		if(type == 0)
			scr.nextScreen(settingsSMSInfo);		
	}
	
	var onCancel = function(name){		
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onButton5 = function(name){
		if(m_session.serviceName == 'depositFrom')
			scr.nextScreen(depositSelectAdjunctionFrom);
		else
			scr.nextScreen(serviceSelect);
	}
	var onList = function(name){		
		type = type == 0?1:0;
		//scr.nextScreen(settingsMenu,type);
	}
	var onButtonHistory = function(name){
		scr.nextScreen(historyCheque, 1);
	}
	
	scr.setButton("tab_security", "Безопасность", '{"icon": ""}', onList);
	scr.setButton("tab_services", "Услуги", '{"icon": ""}', onList);
	scr.setButton("change_pin", "Изменить ПИН", "{\"icon\": \"img/ml-icon-1.svg\",\"group\": \"tablist1\"}", onButton1);
	scr.setButton("link_sim", "Привязать SIM-карту", true, false, "{\"icon\": \"img/ml-icon-2.svg\",\"group\": \"tablist1\"}", onButton2);
	scr.setButton("secure3d", "3D Secure", true, false, "{\"icon\": \"img/ml-icon-3.svg\",\"group\": \"tablist1\"}", onButton3);
	scr.setButton("sms_info", "SMS-Инфо", true, false, "{\"icon\": \"img/ml-icon-4.svg\",\"group\": \"tablist1\"}", onButton4);	
	scr.setButton("internet_bank", "Подключить Интернет-банк", true, false, "{\"icon\": \"img/ml-icon-5.svg\",\"group\": \"tablist2\"}", onButton1);
	scr.setButton("bank_params", "Реквизиты и счета", true, false, "{\"icon\": \"img/ml-icon-6.svg\",\"group\": \"tablist2\"}", onButton2);
	scr.setButton("limits", "Лимиты", true, false, "{\"icon\": \"img/ml-icon-7.svg\",\"group\": \"tablist2\"}", onButton3);
	//scr.setButton("history", "Напечатать минивыписку", "{\"icon\": \"img/ml-icon-7.svg\",\"group\": \"tablist2\"}", onButtonHistory);
	
	
	//scr.setButton("cancel", "Вернуться в меню", '{"icon": ""}', onButton5);
	//scr.setButton("logout2", "Выйти", '{"icon": "../../graphics/icon-logout-red.svg"}', onCancel);
	scr.setButton("logout2", "Закрыть", '{"icon": ""}', onButton5);
	
	
	scr.render("card_settings_menu");
}
var settingsChangePin = function(step){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onEmptyButton = function(name){
	}
	var onButton1 = function(name){
		alertMsgLog(scr.name+' onButton1 '+name);
		m_session.serviceName = 'return';
		window.external.exchange.ExecNdcService("cancel", "");
	}
	var onButton2 = function(name){
		alertMsgLog(scr.name+' onButton2 '+name);
		m_session.serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
	}
	var onButton3 = function(name){
		alertMsgLog(scr.name+' onButton2 '+name);
		window.external.exchange.ExecNdcService("pin", "");
		m_session.serviceName = "return";
	}
	var onButton4 = function(name){
		alertMsgLog(scr.name+' onButton2 '+name);
		window.external.exchange.ExecNdcService("main_menu", "");
		m_session.serviceName = "return";
	}
	var onButton5 = function(name){
		alertMsgLog(scr.name+' onButton2 '+name);
		m_session.serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancelspec", "");
	}
	var onContinue = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжаем на serviceSelect.');
		scr.nextScreen(serviceSelect);
	}
	var onEnter = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Enter ' + name);
		m_session.serviceName = 'enter';
		var help = {};
			help['pinValue'] = m_session.pin.value;
		window.external.exchange.ExecNdcService("enter", JSON.stringify(help));
	}
	var onMainMenu = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжаем на serviceSelect.');
		window.external.exchange.ExecNdcService("pin", "");
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		m_session.serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancelspec", "");
		//scr.cancel();
	}
	var onInput = function(args){
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = args;
			else {
				pKey = args[0];
				help = args[1];
			}
		}
		m_session.pin.length = controlPinLength(help);
		m_session.pin.value = help;
	}
	var onCall = function(args) {
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onCall, value: '+_args);
		
		if(_args == callOptions[0]){
			scr.setModalMessage("", "", -1, false, "", onTimeoutButton);
			window.external.exchange.ExecNdcService("main_menu", "");
			m_session.serviceName = "return";
		}
		else{
			window.external.exchange.ExecNdcService("cancelspec", "");
			m_session.serviceName = "cancel";
		}
	}
	var onGoodCall = function(args) {
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onCall, value: '+_args);
		
		if(_args == goodCallOptions[0]){
			scr.setModalMessage("", "", -1, false, "", onTimeoutButton);
			window.external.exchange.ExecNdcService("pin", "");
			m_session.serviceName = "return";
		}
		else{
			window.external.exchange.ExecNdcService("cancelspec", "");
			m_session.serviceName = "cancel";
		}
	}
	var onMoreTimeCall = function(args){
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onMoreTime, value: '+_args);
		
		if(_args == onMoreTime[0]){
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("nomoretime", "");
			m_session.serviceName = "return";
		}
		else{
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("moretime", "");
			m_session.serviceName = "moretime";
		}
	}
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					//scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else {
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "webius_menu"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					scr.nextScreen(serviceSelect);
					//scr.nextScreen(settingsChangePin, 7);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "pinchange"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'pin'){
					scr.nextScreen(settingsChangePin, 2);
					return;
				}
				else if(m_HostScreen == 'pin_second'){
					scr.nextScreen(settingsChangePin, 3);
					return;
				}
				else if(m_HostScreen == 'pin_error'){
					scr.nextScreen(settingsChangePin, 4);
					return;
				}
				else if(m_HostScreen == 'wait_request'){
					//scr.nextScreen(settingsChangePin, 5);
					//scr.setButton("logout", "Забрать карту", true, false, '{"icon": "../../graphics/icon-pick-card-red.svg", "display_group":"bottom_line"}', onButton2);
					if(m_session.MaxPinLength > 4)
					//	scr.setButton("logout", "Забрать карту", true, false, '{"icon": "../../graphics/icon-pick-card-red.svg", "themes":["btn-white-red"], "display_group":"bottom_line"}', onButton1);
					//else
						scr.setButton("logout", "Продолжить", true, false, '{"icon": "", "themes":["btn-green"], "display_group":"bottom_line"}', onContinue);
					scr.setButton("back", "Вернуться в меню", true, false, '{"icon": "","display_group": "bottom_line"}', onButton1);
					scr.setInput("pin_code", "", "","",false,true,"{\"pin_code\": true,\"length\": "+m_session.pin.length+"}","text" ,onInput, "Wait");
					window.external.exchange.RefreshScr();
					
					return;
				}
				else if(m_HostScreen == 'wait'){
					//scr.nextScreen(settingsChangePin, 5);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(settingsChangePin, 6);
					return;
				}
				else if(m_HostScreen == 'pin_cancel'){
					if(m_session.serviceName == 'return')
						window.external.exchange.ExecNdcService('main_menu','');
					else
						window.external.exchange.ExecNdcService('cancelspec','');
					return;
				}
				else if(m_HostScreen == 'need_more_time'){
					var help = onMoreTime.join().toString();
					scr.setModalMessage('Вам требуется еще время?', help, -1, true, '{"icon": "","rotate":true,"loader":"countdown","count": 25,"options_settings":[{"name":"logout","icon":"", "theme":"btn-red"},{"name":"cancel","icon":""}]}', onMoreTimeCall);
					//scr.render("deposit_select_currency");
					scr.render("pin_code");
					//window.external.exchange.RefreshScr();
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+', Service: '+m_HostServiceName+', HostScreen: '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'request_ok'){
				scr.nextScreen(requestResult, ['ok', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'request_err'){
				scr.nextScreen(requestResult, ['err', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'selfincass_impossible'){
				scr.nextScreen(requestResult, ['selfincass_impossible', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'request_impossible'){
				scr.nextScreen(requestResult, ['req_impossible', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'request_not_performed'){
				scr.nextScreen(requestResult, ['request_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "err"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_allowed'){
				scr.nextScreen(requestResult,["request_not_allowed", "pinchange"]);
				return;
			}
			else if(m_HostServiceName == 'incorrect_amount'){
				scr.nextScreen(requestResult,["incorrect_amount", "pinchange"]);
				return;
			}
			else if(m_HostServiceName == 'incorrect_amount2'){
				scr.nextScreen(requestResult,["incorrect_amount2", "pinchange"]);
				return;
			}
			else if(m_HostServiceName == 'not_enough_money'){
				scr.nextScreen(requestResult, ['not_enough_money', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'limit_exceeded'){
				scr.nextScreen(requestResult, ['limit_exceeded', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'amount_to_big'){
				scr.nextScreen(requestResult, ['amount_to_big', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'pinchange_not_allowed'){
				scr.nextScreen(requestResult,['pinchange_not_allowed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'card_seized'){
				scr.nextScreen(msgResult,['Ваша карта задержана<br>по требованию банка-эмитента', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'card_not_serviced'){
				scr.nextScreen(msgResult,['Извините, карта не обслуживается', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'card_expired'){
				scr.nextScreen(msgResult,['Ваша карта просрочена', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'no_account'){
				scr.nextScreen(msgResult,['Нет требуемого счета', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'pin_try_exceeded'){
				scr.nextScreen(msgResult,['Превышено максимально допустимое<br>число попыток ввода пин-кода', 'err']);
				return;
			}
			else if(m_HostServiceName == "spec_cancel"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					scr.nextScreen(specCancelWait, 1);
					return;
				}
				else if(m_HostScreen == 'return_card'){
					scr.nextScreen(specCancelWait, 2);
					return;
				}
				else if(m_HostScreen == 'end_session'){
					scr.nextScreen(specCancelWait, 3);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(specCancelWait, 4);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+', Service: '+m_HostServiceName+', HostScreen: '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "before_pin"){
				if(m_HostScreen == 'main'){
					if(m_session.ownCard)
						window.external.exchange.ExecNdcService("pin","");
					else
						window.external.exchange.ExecNdcService("pin_other","");
					return;
				}
				else if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					//scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else {
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "oos"){
				scr.nextScreen(oos);
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	var callOptions = ['Вернуться в меню', 'Забрать карту'];
	var goodCallOptions = ['Продолжить', 'Забрать карту'];
	var onMoreTime = ['Завершить', 'Продолжить'];
	
	{//общий набор элементов на экране
		scr.setButton("back", "Вернуться в меню", '{"icon": "","display_group": "bottom_line"}', onButton1);
		//scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "display_group":"bottom_line"}', onButton2);
		//scr.setButton("logout", "Продолжить", '{"icon": "", "themes":["btn-white-grey"], "display_group":"bottom_line"}', onEnter);
		if(m_session.MaxPinLength != 4)
			scr.setButton("logout", "Продолжить", '{"icon": "", "themes":["btn-green"], "display_group":"bottom_line"}', onEnter);


		scr.setImage("bg","../../graphics/BG_blur.jpg","");
		scr.setLabel("card",m_session.cardIcon.value, m_session.cardIcon.ext);
		
		m_session.pin.length = controlPinLength('', 4);
		scr.setInput("pin_code", "", "","",false,true,"{\"pin_code\": true,\"length\": "+m_session.pin.length+"}","text" ,onInput);
	}
	
	if(step == 1){
		window.external.exchange.ExecNdcService("pinchange","");
	}
	else if(step ==  2){
		scr.setLabel("title", "Введите новый пин", "");
		scr.setLabel("safety_pin_text", "Никогда не сообщайте Ваш ПИН сторонним лицам", "");
		scr.setLabel("safety_pin_text2", "Не пишите ПИН на карте и не храните рядом с картой", "");

		scr.render("pin_code");
	}
	else if(step ==  3){
		scr.setLabel("title", "Повторите новый пин", "");
		scr.setLabel("safety_pin_text", "Никогда не сообщайте Ваш ПИН сторонним лицам", "");
		scr.setLabel("safety_pin_text2", "Не пишите ПИН на карте и не храните рядом с картой", "");

		scr.render("pin_code");
	}
	else if(step == 4){
		scr.setLabel("title", "Введите новый пин", "");
		scr.setLabel("safety_pin_text", "Введенные значения не совпадают", "");
		scr.setLabel("safety_pin_text2", "Введите новый пин-код заново", "");
		scr.setInput("pin_code", "", "","",false,true,"{\"pin_code\": true,\"length\": "+m_session.pin.length+"}","text" ,onInput, "Error");
		
		scr.render("pin_code");
	}
	else if(step == 5){
		scr.setButton("logout", "Забрать карту", true, false, '{"icon": "../../graphics/icon-pick-card-red.svg", "display_group":"bottom_line"}', onButton2);
		scr.setButton("back", "Вернуться в меню", true, false, '{"icon": "","display_group": "bottom_line"}', onButton1);
		scr.setInput("pin_code", "", "","",false,true,"{\"pin_code\": true,\"length\": "+m_session.pin.length+"}","text" ,onInput, "Wait");

		scr.render("pin_code");

		//scr.setWait(true, "Секундочку...", '{"icon": "", "rotate": true, "loader":"loader"}');
		//scr.render("pin_code");
		//scr.deleteButton("back");
		//scr.deleteButton("logout");
		//scr.deleteInput("pin_code");
		//scr.setButton("menu_main", "Продолжить", '{"icon": ""}', onButton3);
		//scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "theme":"btn-white-red"}', onButton5);
		//scr.setImage("smile","../../graphics/icon-smile-1.svg","");
		//scr.setLabel("text", 'Секундочку...', "");
		//scr.setLabel("loader", "60", '{"loader":"loader"}');
		//scr.render("wait_message");
	}
	else if(step == 6){
		//scr.setWait(true, "Операция не может быть выполнена", "{\"icon\": \"../../graphics/icon-smile-2.svg\", \"theme\":\"blue\"}");
		//scr.render("pin_code");
		scr.deleteButton("back");
		scr.deleteButton("logout");
		scr.deleteInput("pin_code");
		//scr.setButton("menu_main", "Продолжить", '{"icon": ""}', onButton3);
		//scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "theme":"btn-white-red"}', onButton5);
		//scr.setImage("smile","../../graphics/icon-smile-1.svg","");
		scr.setLabel("text", 'Операция не может быть выполнена', "");
		scr.setLabel("loader", "60", '{"loader":"ellipse", "icon": "../../graphics/icon-smile-3.svg"}');
		scr.render("wait_message");	
	}
	else if(step == 7){
		//scr.setModalMessage('Ваш ПИН успешно изменен', goodCallOptions.join(), -1, true, '{"icon": "../../graphics/icon-smile-1.svg","options_settings":[{"name":"cancel","icon":""},{"name":"logout","icon":"../../graphics/icon-logout.svg"}]}', onGoodCall);
		//scr.render("pin_code");
		scr.deleteButton("back");
		scr.deleteButton("logout");
		scr.deleteInput("pin_code");
		scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButton3);
		//scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "theme":"btn-white-red"}', onButton5);
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-logout.svg"}', onButton5);
		scr.setLabel("warning", 'Ваш ПИН успешно изменен', "");
		scr.setImage("smile","../../graphics/icon-smile-1.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(step == 8){
		var help = callOptions.join().toString();
		//scr.setModalMessage('Невозможно выполнить операцию', callOptions.join(), -1, true, '{"icon": "../../graphics/icon-smile-3.svg","options_settings":[{"name":"cancel","icon":""},{"name":"logout","icon":"../../graphics/icon-logout.svg"}]}', onCall);

		//scr.render("pin_code");
		scr.deleteButton("back");
		scr.deleteButton("logout");
		scr.deleteInput("pin_code");
		scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButton4);
		//scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "theme":"btn-white-red"}', onButton5);
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-logout.svg"}', onButton5);
		scr.setLabel("warning", 'Невозможно выполнить операцию', "");
		scr.setImage("smile","../../graphics/icon-smile-3.svg","");
		scr.render("wait_message_buttons");	
	}
}
var settingsAddSIM = function(step){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	var onButton1 = function(name){		
		scr.nextScreen(msgResult,"Вы успешно привязали новую SIM-карту");
	}
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	
	var onInput = function(args){		
	}
	
	switch(step)
	{
		case 1:
			scr.setInput("phone", "", "", "Введите номер телефона для привязки новой SIM-карты", "", onInput);
			scr.setButton("button1", "Продолжить", "", onButton1);
			if(m_session.isCard)
				scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onCancel);
			else
				scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);				
			break;
		default:
			scr.nextScreen(msgResult,"Ошибка привязки SIM-карты");
			return;
			break;
	}
		
	
	
	scr.render("test");
}
var settingsSMSInfo = function(){
	scr.nextScreen(msgResult,"Вы успешно подключили SMS-Инфо");
}
var settingsInternetBank = function(){
	scr.nextScreen(msgResult,"Вы успешно подключили Интернет-Банк");
}
var settingsCardRequisites = function(step){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	var onButton1 = function(name){
		if(step == 2)
			scr.nextScreen(msgResult,"Реквизиты отправлены");	
	}
	var onButton2 = function(name){
		scr.nextScreen(settingsCardRequisites,2);
	}
	var onButton3 = function(name){
		scr.nextScreen(settingsMenu,1);
	}
	
	var onInput = function(args){		
	}
	
	switch(step)
	{
		case 1:
			
			scr.setLabel("requisites1", "Банк получатель: ФК Открытие", "{\"group\": \"requisites\",\"title\": \"Банк получатель\"}");
			scr.setLabel("requisites2", "3010282619288172928", "{\"group\": \"requisites\",\"title\": \"Корр. счет\"}");
			scr.setLabel("requisites3", "029273628", "{\"group\": \"requisites\",\"title\": \"БИК\"}");
			scr.setLabel("requisites4", "Константин Джон", "{\"group\": \"requisites\",\"title\": \"Получатель\"}");
			scr.setLabel("requisites5", "1029171888881728918", "{\"group\": \"requisites\",\"title\": \"Счет получателя платежа\"}");
			scr.setButton("print_page", "Распечатать", '{"icon": ""}', onButton1);
			scr.setButton("forward", "Переслать", '{"icon": ""}', onButton2);
			scr.setButton("return", "Выйти", '{"icon": ""}', onButton3);		
			scr.render("requisites");
			return;
			break;
		case 2:
			scr.setLabel("title", "Введите номер телефона для отправки реквизитов счета", "");			
			scr.setInput("phone", "+8 (___)___-__-__", "+8 (___)___-__-__", "Номер", true, true, "", "phone", onInput);			
			scr.setButton("continue", "Переслать", '{"icon": ""}', onButton1);
			scr.setButton("return", "Назад", '{"icon": ""}', onButton1);
			scr.render("ipnut_phone");
			return;
			break;			
		default:
			scr.nextScreen(msgResult,"Ошибка отправки реквизитов");
			return;
			break;
	}
	scr.render("test");
	
}

var requestResult = function(args){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButtonCancel = function(name){
		alertMsgLog(scr.name+' onButtonCancel '+name);
		m_session.serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancelspec", "");
	}
	var onButtonMainMenu = function(name){
		alertMsgLog(scr.name+' onButtonMainMenu '+name);
		m_session.serviceName = 'webius_menu';
		scr.nextScreen(serviceSelect);
	}
	var onButtonToPin = function(name){
		alertMsgLog(scr.name+' onButtonToPin '+name);
		m_session.serviceName = '';
		window.external.exchange.ExecNdcService("before_pin","");
	}
	
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					//scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else {
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "before_pin"){
				if(m_HostScreen == "main"){
					if(m_session.cardIcon.our)
						window.external.exchange.ExecNdcService("pin","");
					else
						window.external.exchange.ExecNdcService("pin_other","");
					return;
				}
				else if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else
					scr.cancel();
			}
			else if(m_HostServiceName == "webius_menu"){
				alertMsgLog('[SCRIPT] '+scr.name+', Service: '+m_HostServiceName+', HostScreen: '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					//scr.nextScreen(serviceSelect);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+', Service: '+m_HostServiceName+', HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'request_ok'){
				scr.nextScreen(requestResult, ['ok', _servName]);
				return;
			}
			else if(m_HostServiceName == 'request_err'){
				scr.nextScreen(requestResult, ['ok', _servName]);
				return;
			}
			else if(m_HostServiceName == 'selfincass_impossible'){
				if(_state == 'cashin_impossible')
					scr.nextScreen(requestResult, ['cashin_impossible_money_return', _servName]);
				else
					scr.nextScreen(requestResult, ['selfincass_impossible', _servName]);
				return;
			}
			else if(m_HostServiceName == 'request_impossible'){
				scr.nextScreen(requestResult, ['req_impossible', _servName]);
				return;
			}
			else if(m_HostServiceName == 'request_not_performed'){
				scr.nextScreen(requestResult, ['request_not_performed', _servName]);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "err"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_allowed'){
				scr.nextScreen(requestResult,["request_not_allowed", _servName]);
				return;
			}
			else if(m_HostServiceName == 'incorrect_amount'){
				scr.nextScreen(requestResult,["incorrect_amount", _servName]);
				return;
			}
			else if(m_HostServiceName == 'incorrect_amount2'){
				scr.nextScreen(requestResult,["incorrect_amount2", _servName]);
				return;
			}
			else if(m_HostServiceName == 'not_enough_money'){
				scr.nextScreen(requestResult, ['not_enough_money', _servName]);
				return;
			}
			else if(m_HostServiceName == 'limit_exceeded'){
				scr.nextScreen(requestResult, ['limit_exceeded', _servName]);
				return;
			}
			else if(m_HostServiceName == 'amount_to_big'){
				scr.nextScreen(requestResult, ['amount_to_big', _servName]);
				return;
			}
			else if(m_HostServiceName == 'pinchange_not_allowed'){
				scr.nextScreen(requestResult, ['pinchange_not_allowed', _servName]);
				return;
			}
			else if(m_HostServiceName == 'card_seized'){
				scr.nextScreen(msgResult,['Ваша карта задержана<br>по требованию банка-эмитента', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'card_not_serviced'){
				scr.nextScreen(msgResult,['Извините, карта не обслуживается', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'card_expired'){
				scr.nextScreen(msgResult,['Ваша карта просрочена', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'no_account'){
				scr.nextScreen(msgResult,['Нет требуемого счета', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'pin_try_exceeded'){
				scr.nextScreen(msgResult,['Превышено максимально допустимое<br>число попыток ввода пин-кода', 'err']);
				return;
			}
			else if(m_HostServiceName == "spec_cancel"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					scr.nextScreen(specCancelWait, 1);
					return;
				}
				else if(m_HostScreen == 'return_card'){
					scr.nextScreen(specCancelWait, 2);
					return;
				}
				else if(m_HostScreen == 'end_session'){
					scr.nextScreen(specCancelWait, 3);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(specCancelWait, 4);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "oos"){
				scr.nextScreen(oos);
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	var _state, _servName = '';
	if(typeof args != 'undefined' && args.constructor === Array && args.length > 0){
		_state = args[0];
		if(args.length > 1)
			_servName = args[1];
	}
	else
		_state = args;

	if(_state == 'ok'){
		if(_servName == 'pinchange')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonToPin);
		else if(_servName != 'pin_balance')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonMainMenu);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "themes":["btn-white-red"]}', onButtonCancel);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg", "themes":["btn-red"]}', onButtonCancel);
		if(_servName == 'pinchange')
			scr.setLabel("warning", 'Ваш ПИН успешно изменен', "");
		else
			scr.setLabel("warning", 'Запрос успешно обработан', "");
		scr.setImage("smile","../../graphics/icon-smile-1.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(_state == 'err'){
		if(_servName != 'pin_balance')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonMainMenu);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "themes":["btn-white-red"]}', onButtonCancel);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg", "themes":["btn-red"]}', onButtonCancel);
		scr.setLabel("warning", 'Невозможно выполнить операцию', "");
		scr.setImage("smile","../../graphics/icon-smile-2.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(_state == 'req_impossible'){
		scr.setLabel("text", 'Извините,<br>операция невозможна', "");
		scr.setLabel("loader","60", '{"loader":"ellipse","icon":"../../graphics/icon-smile-2.svg"}');
		scr.render("wait_message");
		setTimeout('scr.nextScreen(serviceSelect);', 5000);
	}
	else if(_state == 'cashin_impossible'){
		//scr.setLabel("text", 'Извините,<br>прием денег невозможен', "");
		//scr.setLabel("loader","60", '{"loader":"ellipse","icon":"../../graphics/icon-smile-2.svg"}');
		scr.setLabel("text", 'Отсчитываем купюры...', "");
		scr.setLabel("loader","60", '{"loader":"loader"}');
		scr.render("wait_message");
	}
	else if(_state == 'cashin_impossible_money_return'){
		//scr.setLabel("text", 'Извините,<br>прием денег невозможен', "");
		//scr.setLabel("loader","60", '{"loader":"ellipse","icon":"../../graphics/icon-smile-2.svg"}');
		scr.setLabel("text", 'Заберите непринятые<br>купюры', "");
		scr.setLabel("loader","60", '{"loader":"loader"}');
		scr.render("wait_message");
	}
	else if(_state == 'selfincass_impossible'){
		if(_servName != 'pin_balance')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonMainMenu);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "themes":["btn-white-red"]}', onButtonCancel);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg", "themes":["btn-red"]}', onButtonCancel);
		scr.setLabel("warning", 'Извините,<br>самоинкассация недоступна', "");
		scr.setImage("smile","../../graphics/icon-smile-2.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(_state == 'request_not_performed'){
		scr.setLabel("text", 'Невозможно выполнить операцию', "");
		scr.setLabel("loader","60", '{"loader":"ellipse","icon":"../../graphics/icon-smile-2.svg"}');
		scr.render("wait_message");
		setTimeout('scr.nextScreen(serviceSelect);', 5000);
	}
	else if(_state == 'request_not_allowed'){
		if(_servName != 'pin_balance')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonMainMenu);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "themes":["btn-white-red"]}', onButtonCancel);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg", "themes":["btn-red"]}', onButtonCancel);
		scr.setLabel("warning", 'Операция не разрешена', "");
		scr.setImage("smile","../../graphics/icon-smile-2.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(_state == 'pinchange_not_allowed'){
		if(_servName != 'pin_balance')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonMainMenu);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "themes":["btn-white-red"]}', onButtonCancel);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg", "themes":["btn-red"]}', onButtonCancel);
		scr.setLabel("warning", 'К сожалению, для вашей карты<br>недоступна смена пин-кода в банкомате', "");
		scr.setImage("smile","../../graphics/icon-smile-2.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(_state == 'incorrect_amount'){
		if(_servName != 'pin_balance')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonMainMenu);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "themes":["btn-white-red"]}', onButtonCancel);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg", "themes":["btn-red"]}', onButtonCancel);
		scr.setLabel("warning", 'Запрошена некорректная сумма', "");
		scr.setImage("smile","../../graphics/icon-smile-2.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(_state == 'incorrect_amount2'){
		if(_servName != 'pin_balance')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonMainMenu);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "themes":["btn-white-red"]}', onButtonCancel);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg", "themes":["btn-red"]}', onButtonCancel);
		scr.setLabel("warning", 'Пожалуйста, выберите другую сумму<br>(должна быть кратна 100)', "");
		scr.setImage("smile","../../graphics/icon-smile-2.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(_state == 'limit_exceeded'){
		if(_servName != 'pin_balance')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonMainMenu);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "themes":["btn-white-red"]}', onButtonCancel);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg", "themes":["btn-red"]}', onButtonCancel);
		scr.setLabel("warning", 'Превышен лимит выдачи', "");
		scr.setImage("smile","../../graphics/icon-smile-2.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(_state == 'amount_to_big'){
		scr.nextScreen(cashoutInputAmount, ['err', 'Пожалуйста,', 'выберите меньшую сумму']);
		return;
	}
	else if(_state == 'not_enough_money'){
		scr.nextScreen(cashoutInputAmount, ['err', 'Недостаточно средств на счете']);
		return;
	}
	else{
		scr.setLabel("text", "Невозможно выполнить операцию", "");	
		scr.setLabel("loader","60", '{"loader":"ellipse","icon":"../../graphics/icon-smile-2.svg"}');
		scr.render("wait_message");	
		setTimeout('scr.nextScreen(serviceSelect);', 5000);
		return;
	}
}

var transferMenu = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	
	var onButton1 = function(name){
		scr.nextScreen(transferChooseIdType);		
	}
	var onButton2 = function(name){
		scr.nextScreen(transferToCard);		
	}
	var onButton3 = function(name){
		scr.nextScreen(transferToSchet);
	}
	var onButton4 = function(name){
		scr.nextScreen(transferToCompany);
	}
	var onButton5 = function(name){
	}	
	
	var onCancel = function(name){		
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	
	
	scr.setButton("quit", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);
	scr.setLabel("header", "Переводы", "");
	scr.setLabel("switch_lang", "EN", "");
	scr.setButton("other_account", "Перевести другим людям", "{\"icon\": \"img/qs-icon-1.svg\",\"group\": \"actions\"}", onButton1);
	scr.setButton("other_card", "С карты на карту", "{\"icon\": \"img/qs-icon-2.svg\",\"group\": \"actions\"}", onButton2);
	scr.setButton("my_account", "Между моими счетами", "{\"icon\": \"img/qs-icon-3.svg\",\"group\": \"actions\"}", onButton3);	
	scr.setButton("by_requisites", "По реквизитам юр. лицам", "{\"icon\": \"img/qs-icon-4.svg\",\"group\": \"actions\"}", onButton4);	
	
	scr.setButton("switch_lang", "EN", "", onButton5);	
	
	
	scr.render("transfer_menu");	
}
var transferToCompany = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	var onButton3 = function(name){
		scr.nextScreen(transferInputAmount);
	}	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onInput = function(args){		
	}
	
	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", '{"icon": ""}', onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onCancel);
	else
		scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);	
	scr.setLabel("title", "Введите реквизиты получателя", "");
	scr.setInput("bik", "", "", "БИК банка-получателя", "", onInput);
	scr.setInput("recipient", "", "", "Получатель", "", onInput);
	scr.setInput("schet", "", "", "Номер счета получателя", "", onInput);
	scr.setInput("inn", "", "", "ИНН", "", onInput);
	scr.setInput("kpp", "", "", "КПП", "", onInput);
	scr.setInput("nds", "", "", "Выберите НДС", "", onInput);
	scr.setButton("button3", "Продолжить", "", onButton3);	
	
	scr.render("test")
}
var transferToSchet = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	var onButton3 = function(name){
		scr.nextScreen(transferInputAmount);
	}	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onInput = function(args){		
	}
	
	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", '{"icon": ""}', onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onCancel);
	else
		scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);	
	scr.setLabel("source", "Выберите источник платежа", "");
	scr.setList("sourcekList", "1234567890,0987654321,5555555565", 0, "", onList);
	scr.setLabel("source", "Выберите получателя платежа", "");
	scr.setList("sourcekList", "3333333333,44444444444444,7777777777", 0, "", onList);
	
	scr.setButton("button3", "Продолжить", "", onButton3);	
	
	scr.render("test");
}
var transferToCard = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	var onButton3 = function(name){
		scr.nextScreen(transferToCardRecipient);
	}	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onInput = function(args){		
	}
	
	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", '{"icon": ""}', onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onCancel);
	else
		scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);	
	
	scr.setLabel("source", "Ист очник платежа", "");
	scr.setInput("card", "", "", "Номер карты", "", onInput);		
	
	scr.setLabel("note", "Введите сумму", "");	
	scr.setInput("expire", "", "", "Месяц и год", "", onInput);	
	
	scr.setButton("button3", "Продолжить", "", onButton3);		
	scr.render("test");
}
var transferToCardRecipient = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	var onButton3 = function(name){
		scr.nextScreen(transferSend);
	}	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onInput = function(args){		
	}
	
	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", '{"icon": ""}', onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onCancel);
	else
		scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);		
	
	scr.setInput("card", "", "", "Номер карты получателя платежа", "", onInput);
	scr.setInput("amuont", "", "", "Введите сумму", "", onInput);	
	
	scr.setButton("button3", "Продолжить", "", onButton3);		
	scr.render("test");
}
var transferChooseIdType = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	
	var onButton3 = function(name){		
		scr.nextScreen(transferRequisitesInput);
	}
	
	var onButton4 = function(name){
		scr.nextScreen(transferCardInput);
	}
	var onButton7 = function(name){		
	scr.nextScreen(helpMenu);
	}
	var onButton8 = function(name){		
		scr.nextScreen(settingsMenu, 1);
	}
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			alertMsgLog('[SCRIPT] '+scr.name+', Service '+m_HostServiceName+'.');
			if(m_HostServiceName == 'webius_menu'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else if(m_HostScreen == 'main'){
					scr.nextScreen(serviceSelect);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' state is '+m_HostServiceState+', need to cancel.');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	//scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	//scr.setButton("print", "", '{"icon": ""}', onBalancePrintButton);
	setBalanceAndPrintButtons(m_session.balanceShowReq, m_session.balancePrintNeed);
	scr.setButton("requisites", "По реквизитам счета", "{\"icon\": \"img/icon-4-1.svg\"}", onButton3);		
	scr.setButton("numbercard", "По номеру карты", "{\"icon\": \"img/icon-4-2.svg\"}", onButton4);
	scr.setButton("help", "Помощь", '{"icon": "../../graphics/icon_help.svg"}', onButton7);
	scr.setButton("settings", "Настройки карты", true, /*m_session.cardIcon.our*/true, '{"icon": "../../graphics/icon_settings.svg"}', onButton8);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onCancel);
	else
		scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);
	
	scr.setLabel("balance", "Ваш баланс...", "");
	
	
	
	scr.render("transfer_choose_id_type");
	
}
var transferRequisitesInput = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	var onButton3 = function(name){
		scr.nextScreen(transferInputAmount);
	}	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onInput = function(args){		
	}
	
	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", '{"icon": ""}', onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onCancel);
	else
		scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);	
	scr.setLabel("source", "Выберите источник платежа", "");
	scr.setList("mbkList", "1234567890,0987654321,5555555565", 0, "", onList);
	scr.setInput("bik", "", "", "БИК банка-получателя", "", onInput);
	scr.setInput("recipient", "", "", "Получатель", "", onInput);
	scr.setInput("schet", "", "", "Номер счета", "", onInput);
	scr.setInput("uip", "", "", "УИП (если есть)", "", onInput);
	scr.setButton("button3", "Продолжить", "", onButton3);	
	
	scr.render("test");
	
}
var transferCardInput = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	var onButton3 = function(name){
		scr.nextScreen(transferInputAmount);
	}	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onInput = function(args){		
	}
	
	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", '{"icon": ""}', onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onCancel);
	else
		scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);	
	
	scr.setLabel("source", "Выберите источник платежа", "");
	scr.setList("mbkList", "1234567890,0987654321,5555555565", 0, "", onList);
	
	scr.setInput("card", "", "", "Введите номер карты получателя:", "", onInput);	
	
	scr.setButton("button3", "Продолжить", "", onButton3);		
	scr.render("test");
}
var transferInputAmount = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	var onButton3 = function(name){
		scr.nextScreen(transferSend);
	}	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onInput = function(args){		
	}
	
	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", '{"icon": ""}', onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-white-small.svg"}', onCancel);
	else
		scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onCancel);	
	scr.setInput("amount", "", "", "Введите сумму", "", onInput);	
	scr.setInput("nazn", "", "", "Введите назначение платежа:", "", onInput);	
	scr.setLabel("comment", "10% - комиссия при превышении суточного лимита снятия. Максимальная сумма для снятия 200 000 руб.", "");	
	scr.setButton("button3", "Перевести", "", onButton3);		
	scr.render("test");
}
var transferSend = function(){
	scr.nextScreen(msgResult,"Успешно зачисленно 12 250 Руб.");
}

var historyCheque = function(step) {
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	var onContinue = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжаем на serviceSelect.');
		scr.nextScreen(serviceSelect);
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		m_session.serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancelspec", "");
		//scr.cancel();
	}
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					return;
				}
				else {
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "webius_menu"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					//scr.nextScreen(serviceSelect);
					scr.nextScreen(historyCheque, 6);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "history"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					scr.nextScreen(historyCheque, 2);
					return;
				}
				else if(m_HostScreen == 'return_card'){
					scr.nextScreen(historyCheque, 3);
					return;
				}
				else if(m_HostScreen == 'end_session'){
					scr.nextScreen(historyCheque, 4);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(historyCheque, 5);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "spec_cancel"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					scr.nextScreen(specCancelWait, 1);
					return;
				}
				else if(m_HostScreen == 'return_card'){
					scr.nextScreen(specCancelWait, 2);
					return;
				}
				else if(m_HostScreen == 'end_session'){
					scr.nextScreen(specCancelWait, 3);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(specCancelWait, 4);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "oos"){
				scr.nextScreen(oos);
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	if(step == 1){
		window.external.exchange.ExecNdcService("history", "");
	}
	else if(step == 2){
		alertMsgLog('wait запрос на сервер');
		//scr.setLabel("title", "выполняется запрос на сервер", "");
		//scr.render("test");
		scr.setImage("bg","../../graphics/BG_blur.jpg","");
		//scr.setImage("card","../../graphics/card.svg","");
		scr.setLabel("card",m_session.cardIcon.value, m_session.cardIcon.ext);
		scr.setImage("error","../../graphics/mes-error.svg","");
		scr.setImage("offer","../../graphics/offer-icon.png","");
		scr.setWait(true, "Жду ответа...", '{"icon": "", "rotate": true, "loader":"loader"}');
		scr.render("cashout_amount");

	}
	else if(step == 3){
		//alertMsgLog('wait возврат карты');
		//scr.setLabel("note1", "Заберите Вашу карту.", "");
		//scr.render("test");
		scr.setWait(true, "Заберите карту...", '{"icon": "../../graphics/icon-pick-card.svg", "loader":"countdown", "count":60, "rotate": true}');
		//scr.render('cashout_amount');
		window.external.exchange.refreshScr();
	}
	else if(step == 4){
		//alertMsgLog('wait завершение сеанса');
		//scr.setLabel("note1", "Не забудьте Вашу карту.", "");
		//scr.setLabel("note2", "Сеанс окончен. До свидания!", "");
		//scr.render("test");
		//scr.setWait(true, "Спасибо", "{\"icon\": \"../../graphics/icon-ok.svg\", \"theme\":\"blue\"}");
		scr.setWait(true, "Пожалуйста, подождите...", '{"icon": "../../graphics/icon-ok.svg", "loader":"ellipse","theme":"blue"}');
		window.external.exchange.refreshScr();
	}
	else if(step == 5){
		alertMsgLog('wait ошибка запроса');
		//scr.setLabel("note1", "не удалось получить ответ от сервера", "");
		//scr.setLabel("note2", "Попробуйте ещё раз.", "");
		//scr.render("test");
		
		scr.setWait(true, "Операция не может быть выполнена", "{\"icon\": \"../../graphics/icon-smile-2.svg\", \"theme\":\"blue\"}");
		window.external.exchange.refreshScr();
	}
	else if(step == 6){
		//alertMsgLog('main_menu_history');
		//scr.setLabel("note1", "попробуем напечатать чек!", "");
		//scr.setButton("continue","Продолжаем","",onContinue);
		//scr.setButton("cancel","Завершить","",onCancel);
		//scr.render("test");
		scr.nextScreen(serviceSelect);
	}
}

var specCancelWait = function(step) {
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					return;
				}
				else {
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "webius_menu"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					//scr.nextScreen(serviceSelect);
					scr.nextScreen(histroyCheque, 6);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "spec_cancel"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					scr.nextScreen(specCancelWait, 1);
					return;
				}
				else if(m_HostScreen == 'return_card'){
					scr.nextScreen(specCancelWait, 2);
					return;
				}
				else if(m_HostScreen == 'end_session'){
					scr.nextScreen(specCancelWait, 3);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(specCancelWait, 4);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "oos"){
				scr.nextScreen(oos);
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	if(step == 1){
		alertMsgLog('wait экран ожидания');
		scr.setLabel("title", "Пожалуйста, подождите.", "");
		scr.render("test");
	}
	else if(step == 2){
		alertMsgLog('wait возврат карты');
		scr.setLabel("note1", "Заберите Вашу карту.", "");
		scr.render("test");
	}
	else if(step == 3){
		//alertMsgLog('wait завершение сеанса');
		//scr.setLabel("note1", "Не забудьте Вашу карту.", "");
		//scr.setLabel("note2", "Сеанс окончен. До свидания!", "");
		//scr.render("test");

		//scr.setWait(true, "Спасибо", '{"icon": "../../graphics/icon-ok.svg", "loader":"ellipse","theme":"blue"}');
		//scr.render('deposit_select_source');
		scr.setLabel("text", "Пожалуйста, подождите...", "");	
		scr.setLabel("loader","60", '{"loader":"loader"}');
		//scr.setButton("quit","123",false,false,'{"icon": ""}',onError);
		//scr.setButton("main_menu","12321",false,false,'{"icon": ""}',onError);
		//scr.setImage("smile","../../graphics/icon-ok.svg","");
		scr.render("wait_message");

	}
	else if(step == 4){
		alertMsgLog('wait ошибка запроса');
		scr.setLabel("note1", "не удалось получить ответ от сервера", "");
		scr.setLabel("note2", "Попробуйте ещё раз.", "");
		scr.render("test");
	}
}

var msg_err = function(someArgs) {
	var onError =  function () {
        scr.cancel(); 
    }
	scr.addOnError(onError);

	var onCancel = function() {
		alertMsgLog(scr.name+' onCancel');
		scr.cancel();
	}

	var msg = '';
	if(typeof someArgs != 'undefined') {
		if(someArgs.constructor === Array && someArgs.length > 0)
			msg = someArgs[0];
		else
			msg = someArgs;
	}
	else
		msg = 'Зафиксирована непредвиденная ошибка! Попробуйте провести операцию еще раз.';

	var onService = function(args) {
		parseHostAnswer(args);
		scr.cancel();
		return;
	}
	scr.addCall("HostScriptAddon", onService);

	scr.setLabel("text", msg, "");	
	scr.setLabel("loader","60", '{"loader":"loader"}');
	scr.setImage("bg","../../graphics/BG_blur.jpg","");
	scr.render("wait_message");
}

var msgResult = function(args){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onTakeCardTimeout = function(name) {
		alertMsgLog(scr.name+'. onTakeCardTimeout ' + name);
		scr.setLabel("text", "Заберите карту,<br>иначе банкомат её проглотит", "");
		scr.setLabel("loader","24", '{"loader":"countdown"}');
		scr.setTimeout("0", "", onTakeCardTimeout);
		scr.render("wait_message");
	}
	var onButton1 = function(name){
		scr.nextScreen(serviceSelect);
	}		
	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle"){
				if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					//scr.nextScreen();
					//scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else if(m_HostScreen == 'wait_read_chip'){
					//scr.nextScreen();
					scr.nextScreen(msgResult, ['Проверяем оборудование...','wait']);
					return;
				}
				else if(m_HostScreen == 'pin_other'){
					m_session.ownCard = false;
					return;
				}
				else 
					scr.cancel();
				return;
			}
			else if(m_HostServiceName == "before_pin"){
				if(m_HostScreen == "main"){
					//scr.nextScreen(pin);
					//alertMsgLog('PAN: '+m_HostPAN);
					m_session.cardIcon = getPaySystem(m_HostPAN);
					//if(m_session.ownCard)
					if(m_session.cardIcon.our)
						window.external.exchange.ExecNdcService("pin","");
					else
						window.external.exchange.ExecNdcService("pin_other","");
					//alert("ACHTUNG");
					getHistory(m_HostPAN);
					//m_session.ownCard = isOpenCard(m_HostPAN);
					return;
				}
				else if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else
					scr.cancel();
			}
			else if(m_HostServiceName == "webius_menu"){
				if(m_HostScreen == 'main'){
					scr.nextScreen(serviceSelect);
					return;
				}
				else{
					window.external.exchange.ExecNdcService("cancel","");
					return;
				}
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else if(m_HostServiceName == 'error_card_read'){
				scr.nextScreen(msgResult,["Ошибка чтения карты","end"]);
				return;
			}
			else if(m_HostServiceName == "cancel"){
				if(m_HostScreen == 'card_captured'){
					scr.nextScreen(msgResult,['Ваша карта задержана', 'err']);
					return;
				}
				else
					scr.cancel();
			}
			else if(m_HostServiceName == "oos")
				scr.nextScreen(oos);
			else
				scr.cancel();
		}
		else
			scr.cancel();
		return;
	}
	scr.addCall("HostScriptAddon", onService);
	
	var message, type;
	if(typeof args != 'undefined' && args.constructor === Array && args.length > 0){
		message = args[0];
		if(args.length > 1)
			type = args[1];
	}
	else
		message = args;
	
	if(typeof type == 'undefined'){
		scr.setLabel("text", message, "");	
		scr.setLabel("loader","60", '{"loader":"loader"}');
		//scr.setImage("smile","../../graphics/icon-loader.svg","");
		scr.setImage("bg","../../graphics/BG_blur.jpg","");
		scr.render("wait_message");
	}
	else{
		if(type == 'end'){
			//scr.setLabel("warning", "Пожалуйста, подождите...", "");	
			scr.setLabel("text", message, "");	
			scr.setLabel("loader","60", '{"loader":"loader"}');
			scr.setImage("bg","../../graphics/BG_blur.jpg","");
			//scr.setImage("smile","../../graphics/icon-loader.svg","");
			scr.render("wait_message");
		}
		else if(type == 'card_return'){
			scr.setLabel("text", "Заберите карту...", "");	
			//scr.setLabel("loader","30", '{"loader":"countdown"}');
			scr.setLabel("loader","5", '{"loader":"ellipse", "icon":"../../graphics/icon-pick-card.svg"}');
			scr.setImage("bg","../../graphics/BG_blur.jpg","");
			//scr.setImage("smile","../../graphics/icon-loader.svg","");
			scr.setTimeout("6000", "", onTakeCardTimeout);
			scr.render("wait_message");
		}
		else if(type == 'card_read'){
			scr.setLabel("text", "Считываю карту...", "");	
			scr.setLabel("loader","60", '{"loader":"loader"}');
			scr.setImage("bg","../../graphics/BG_blur.jpg","");
			//scr.setImage("smile","../../graphics/icon-loader.svg","");
			scr.render("wait_message");
		}
		else if(type == 'wait'){
			if(message == '')
				scr.setLabel("text", "Пожалуйста, подождите...", "");
			else
				scr.setLabel("text", message, "");
			//scr.setImage("smile","../../graphics/icon-loader.svg","");
			scr.setImage("bg","../../graphics/BG_blur.jpg","");
			scr.setLabel("loader","60", '{"loader":"loader"}');
			
			//text: 'Секундочку, я считаю...',
			//ext:
			//{
			//	icon: '',
			//	rotate: true,
			//	loader: 'loader',
			//}
			
			scr.render("wait_message");
		}
		else if(type == 'err'){
			scr.setLabel("text", message, "");
			scr.setImage("bg","../../graphics/BG_blur.jpg","");
			//scr.setImage("smile","../../graphics/icon-smile-2.svg","");
			scr.setLabel("loader","60", '{"loader":"ellipse", "icon":"../../graphics/icon-smile-2.svg"}');
			scr.render("wait_message");
		}
		else{
			scr.cancel();
		}
	}
}

function initScreens() {
	scr = new Screen(start,"");	
	start = new Screen(start, "start");
	main = new Screen(main,"main");
	oos = new Screen(oos, "oos");
	
	serviceSelect = new Screen(serviceSelect, "serviceSelect");
	historyCheque = new Screen(historyCheque, "historyCheque");
	specCancelWait = new Screen(specCancelWait, "specCancelWait");
	cashin = new Screen(cashin, "cashin");
	pin = new Screen(pin, "pin");
	msg_err = new Screen(msg_err, "msg_err");
	msgResult = new Screen(msgResult,"msgResult");
	requestResult = new Screen(requestResult,"requestResult");
	depositSelectAdjunctionFrom = new Screen(depositSelectAdjunctionFrom,"depositSelectAdjunctionFrom");
	depositSelectAdjunctionCurrency = new Screen(depositSelectAdjunctionCurrency,"depositSelectAdjunctionCurrency");
	depositSelectAdjunctionMyCard = new Screen(depositSelectAdjunctionMyCard,"selectAdjunctionCard");
	depositSelectAdjunctionAnotherCard = new Screen(depositSelectAdjunctionAnotherCard,"depositSelectAdjunctionAnotherCard");
	helpMenu = new Screen(helpMenu,"helpMenu");
	settingsMenu = new Screen(settingsMenu,"settingsMenu");
	settingsChangePin = new Screen(settingsChangePin,"settingsMenu");
	settingsAddSIM = new Screen(settingsAddSIM,"settingsMenu");
	settingsSecure3D = new Screen(settingsSMSInfo,"settingsMenu");
	settingsSMSInfo = new Screen(settingsSMSInfo,"settingsMenu");
	settingsInternetBank = new Screen(settingsInternetBank,"settingsInternetBank");
	settingsCardRequisites = new Screen(settingsCardRequisites,"settingsCardRequisites");
	settingsCardLimits = new Screen(settingsCardLimits,"settingsCardLimits");
	transferMenu = new Screen(transferMenu,"transferMenu");
	transferChooseIdType = new Screen(transferChooseIdType,"transferChooseIdType");
	transferToCard = new Screen(transferToCard,"transferToCard");
	transferToSchet = new Screen(transferToSchet,"transferToSchet");
	transferToCompany = new Screen(transferToCompany,"transferToCompany");
	transferCardInput = new Screen(transferCardInput,"transferCardInput");
	transferRequisitesInput = new Screen(transferRequisitesInput,"transferRequisitesInput");
	transferInputAmount = new Screen(transferInputAmount,"transferInputAmount");
	transferSend = new Screen(transferSend,"transferSend");
	transferToCardRecipient = new Screen(transferToCardRecipient,"transferToCardRecipient");
	cashoutInputAmount = new Screen(cashoutInputAmount,"cashoutInputAmount");
	giveMoney = new Screen(giveMoney,"giveMoney");
	
	ekassir = new Screen(ekassir,"ekassir");
}


function AddSpace(a)
{
	a = a.toString();
	var res = "";
	if(a.length <= 3)
		return a;
	var startIndex = a.length ;
	var subInd = a.indexOf(',');
	if(subInd == -1)
		subInd = a.indexOf('.');
	if(subInd > -1){
		startIndex = subInd;
		res = a.substr(subInd);
	}
	var step = 3;
	while(startIndex - step > 0){
		startIndex-=step;
	        res = a.substr(startIndex,step) + (res == ''?'':(' ' + res));
	}
	res =  a.substr(0,startIndex)+ (res == ''?'':(' ' + res));
	alertMsgLog("AddSapce: "+res);
	return res;
}