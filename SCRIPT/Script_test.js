﻿/* SCREENS*/
/*service*/
var start,oos,main,language,serviceSelect,wait,errorWait,balanceScr,balanceService,historyCheque,specCancelWait,transfer,cashout,cashin,spec,pin,msgResult,requestResult;
var giveMoney, fakePin;
/*cashin*/
var depositSelectAdjunctionFrom,depositSelectAdjunctionCurrency,depositSelectAdjunctionMyCard,depositSelectAdjunctionAnotherCard;
/*help*/
var helpMenu;
/*settings*/
var settingsChangePin,settingsMenu, settingsAddSIM,settingsSecure3D,settingsSMSInfo,settingsInternetBank,settingsCardRequisites,settingsCardLimits;
/*transfer*/
var transferMenu,transferToSchet,transferChooseIdType,transferToCard,transferToCompany,transferRequisitesInput,transferCardInput,transferInputAmount,
	transferSend,transferToCardRecipient;
/*cashout*/
var cashoutInputAmount;
/*Ekassir*/
var ekassir;
/* SCREENS*/
var langToShow = 'rus';
var balance = '';
var balanceShow = false, balanceShowReq = false, balancePrintNeed = false, balancePrintReq = false;
var serviceName = '';
var m_HostServiceName, m_HostServiceState = "error",
  m_HostScreen, m_HostScreenText, m_HostData, m_HostAmount,
  m_CheckFlag, m_Timeout = 0, m_HostPAN,
  m_MoneyHistory, m_ATMMoneyInfo, m_FastCash, m_CheckSum,
  m_OpenOwnCard, m_BalanceCommissionAllow, m_Currency;

var m_UserData, m_UserDataNew;
var m_session;

var m_PinError = true;
var pinLength = controlPinLength('', 4);
var pinValue = '';
var m_CardIcon = {value:'*0000',ext:'{"icon":"../../graphics/cards/another-noname.svg"}', our:false};
var m_ATMFunctions = {acceptor:true, dispenser:true, printer:true};

function printButtonRefresh(value){
	scr.setButton("print", "", m_ATMFunctions.printer, !value, "{\"icon\": \"\"}", onBalancePrintButton);
	window.external.exchange.refreshScr();
}
function setBalanceAndPrintButtons(_balanceReqFlag, _printReqFlag){
	scr.setButton("print", "", m_ATMFunctions.printer, !_printReqFlag, "{\"icon\": \"\"}", onBalancePrintButton);
	if(_balanceReqFlag){
		scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"state\":\"wait\"}", onBalanceShowButton);
	}
	else
		scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"state\":\"\"}", onBalanceShowButton);

	//scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"state\":\"\"}", onBalanceShowButton);
	//scr.setButton("print", "", true, true, "{\"icon\": \"\"}", onBalancePrintButton);
}
function onTimeoutButton(args) {
	var _name, _args;
	if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
		_name = args[0];
		if(args.length > 1)
			_args = args[1];
		else
			_args = "";
	}
	else {
		_name = "";
		_args = args;
	}
	alertMsgLog(scr.name+' onTimeoutButton, value: '+_args);
	m_Timeout == 0;
	if(_args == 'Да'){
		scr.setModalMessage("", "", -1, false, "", onTimeoutButton);
		window.external.exchange.RefreshScr();
	}
	else{
		scr.cancel();
	}
}
function onTimeout(name) {
	alertMsgLog(scr.name+' onTimeout');
	if(m_Timeout == 0) {
		alertMsgLog(scr.name+' onTimeout with '+m_Timeout);
		m_Timeout = 1;
		scr.setModalMessage("Вам требуется ещё время?", "Да,Нет", -1, true, "", onTimeoutButton);
		window.external.exchange.RefreshScr();
	}
	else {
		m_Timeout == 0;
		scr.cancel();
	}
}

function parseBalance(data){
		s = data;
		var charHelp = '';
		var PosArr = new Array();
		for (var i=0; i < s.length; i++)
		{
			charHelp = s.charAt(i);
			if (charHelp == '' || charHelp == '|')
				PosArr.push(i);
		}
		var z;
		var StrArr = new Array();
		for (var i=0; i < PosArr.length - 1; i++)
		{
			z = s.substring(PosArr[i] + 1, PosArr[i + 1]);
			StrArr.push(z);
		}
		z = s.substring(PosArr[PosArr.length - 1] + 1);
		StrArr.push(z);
		if (typeof StrArr[0] != 'undefined')
		{
			var temp = StrArr[0].substring(2);
			while (temp.substring(0,1) == ' ')
			{
				temp = temp.substring(1);
			}
			//temp = temp.split(' ')[0];
			temp = temp.replace(' RUR', '');

			//document.getElementById('ds').innerHTML = temp;
		}
		else
			temp = 0;
	return temp;
}

function onBalancePrintButton(name) {
	if(!balancePrintReq) {
		balancePrintReq = true;
		balancePrintNeed = true;
		printButtonRefresh(balancePrintNeed);
		alertMsgLog('onBalancePrintButton addCall');
		addRequestHandler("balanceprint", onBalancePrintService);
		window.external.exchange.ExecNdcService("balanceprint", "");
	}
	else if(!balancePrintNeed) {
		balancePrintNeed = true;
		printButtonRefresh(balancePrintNeed);
	}
}
function onBalancePrintService(args) {
	parseHostAnswer(args);
	
	if(m_HostServiceState == 'ok') {
		alertMsgLog('onBalancePrintService, Service '+m_HostServiceName+', HostScreen '+m_HostScreen+'.');
		if(m_HostScreen == 'wait') {
			return 'ok';
		}
		else if(m_HostScreen == 'error') {
			balancePrintReq = false;
			balancePrintNeed = false;
			printButtonRefresh(balancePrintNeed);
			return 'ok';
		}
		else if(m_HostScreen == 'balance') {
			balancePrintReq = false;
			balancePrintNeed = false;
			printButtonRefresh(balancePrintNeed);
			return 'ok';
		}
		else {
			balancePrintReq = false;
			balancePrintNeed = false;
			printButtonRefresh(balancePrintNeed);
			return 'ok';
		}
	}
	else {
		alertMsgLog('onBalancePrintService state is '+m_HostServiceState);
		balancePrintReq = false;
		balancePrintNeed = false;
		printButtonRefresh(balancePrintNeed);
		return 'ok';
	}
	return 'ok';
}

function onBalanceShowButton(name) {
	
	alertMsgLog('[SCRIPT] onBalanceShowButton');
	
	if(!balanceShow) {
		if(m_session.balance == 0) {
			alertMsgLog('[SCRIPT] onBalanceShowButton addCall');
			if(!balanceShowReq){
				addRequestHandler("balance", onBalanceShowService);
				window.external.exchange.ExecNdcService("balance", "");
				balanceShowReq = true;
			}
			if(m_session.serviceName != 'pin_balance'){
				scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"state\":\"wait\"}", onBalanceShowButton);
				window.external.exchange.refreshScr();
			}
			else
				alertMsgLog('[SCRIPT] onBalanceShowButton pin_balance request');
		}
		else {
			scr.setButton("showremains", AddSpace(m_session.balance) + ' Руб.', "{\"icon\": \"\",\"state\":\"show\"}", onBalanceShowButton);
			window.external.exchange.refreshScr();
			balanceShow = true;
			setTimeout('if(balanceShow) onBalanceShowButton("showremains");',2000);
		}
	}
	else {
		scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"state\":\"\"}", onBalanceShowButton);
		window.external.exchange.refreshScr();
		balanceShow = false;
	}
}
function onBalanceShowService(args) {
	parseHostAnswer(args);
	
	if(m_HostServiceState == 'ok') {
		alertMsgLog(' onBalanceShowService, Service '+m_HostServiceName+', HostScreen: '+m_HostScreen+'.');
		if(m_HostScreen == 'wait') {
			return 'ok';
		}
		else if(m_HostScreen == 'error') {
			balanceShowReq = false;
			balanceShow = false;
			if(m_session.serviceName != 'pin_balance'){
				scr.setLabel("balance", "Ошибка получения баланса", "");
				scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"state\":\"\"}", onBalanceShowButton);
				window.external.exchange.refreshScr();
			}
			return 'ok';
		}
		else if(m_HostScreen == 'balance') {
			var helpBalance = m_HostScreenText;
			
			m_session.balance = parseFloat(parseBalance(helpBalance));
			if(m_session.serviceName != 'pin_balance'){
				scr.setButton("showremains", AddSpace(m_session.balance) + ' Руб.', "{\"icon\": \"\",\"state\":\"show\"}", onBalanceShowButton);
				window.external.exchange.refreshScr();
				balanceShow = true;
				setTimeout('if(balanceShow) onBalanceShowButton("showremains");',2000);
			}
			else
				balanceShowReq = false;
			balanceShowReq = false;
			return 'ok';
		}
		else {
			balanceShowReq = false;
			balanceShow = false;
			
			scr.setLabel("balance", "Ошибка получения баланса", "");
			scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"state\":\"\"}", onBalanceShowButton);
			window.external.exchange.refreshScr();
			
			return 'ok';
		}
	}
	else {
		alertMsgLog('onBalanceShowService state is '+m_HostServiceState);			
		balanceShowReq = false;
		balanceShow = false;
		scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"state\":\"\"}", onBalanceShowButton);
		window.external.exchange.refreshScr();
		return 'ok';
	}
	return 'ok';
}

function sendPrintResult(args) {
	alertMsgLog('sendPrintResult val: '+args);
	if(args == '1') {
		balancePrintNeed = false;
		printButtonRefresh(balancePrintNeed);
	}
}
function getPaySystem(pan) {
	var iconPath = '';
	var resObj = {}
	if(pan.length < 6){
		iconPath =  "another-noname.svg";
		resObj.value = "*0000";
		resObj.our = false;
	}
	else {
		resObj.value = '*'+pan.substr(pan.length-4);
		var intBin = parseInt(pan.substr(0,4));
		if(isNaN(intBin)){
			iconPath =  "another-noname.svg";
			resObj.our = false;
		}
		else{
			if(isOpenCard(pan)){
				resObj.our = true;
				iconPath = 'our';
			}
			else{
				resObj.our = false;
				iconPath = 'another';
			}
			if(intBin >= 4000 && intBin <= 4999)
				iconPath +=  "-visa.svg";
			else if(intBin >= 2200 && intBin <= 2204)
				iconPath +=  "-mir.svg";
			else if(intBin >= 5100 && intBin <= 5599)
				iconPath +=  "-mastercard.svg";
			else
				iconPath +=  "-noname.svg";
		}
	}
	resObj.ext = '{"icon":"../../graphics/cards/'+iconPath+'"}';
	return resObj;
}
function isOpenCard(pan) {
	var ownCard = [220029,429037,429038,429039,429040,404586,484800,405870,446065,458493,413307,407178,414076,467485,467486,467487,405869,544218,544962,532301,531674,670518,530403,530183,539714,676231,558620,549025,549024,472840,472841,472842,472843,472844,532837,548106,670587,411900,424553,424554,424555,456515,456516,490986,494343,410323,437540,437541,467033,486031,531318,557808,557809,558298,676642,516906,523510,525932,529034,529047,530175,554521,536114,406790,406791,409755,409701,409756,425656,426896,437351,464843,479302,676968,552681,520634,515758,518803,515668,416038,479716,479777,479715,479718,420574,427856,425184,425181,425185,425182,425183,428165,428166,430291,676951,520349,552671,518796,552219,522459,539896,548764,520324,434146,434147,434148,485649,535108,544499,544573,547449,549848,676697,428228,443884,443885,514017,515243,529260,532130];
	var binInt = parseInt(pan.substr(0,6));
	if(binInt == NaN)
		return false;
	for(var i=0;i<ownCard.length;++i)
		if(ownCard[i] == binInt)
			return true;
	return false;
}

function hidePAN(data){
	if(data.indexOf("PAN") != -1){
		var helpJson = {};
		try{
			helpJson = JSON.parse(data);
			//alert("JSON parse ok, PAN:"+(helpJson["PAN"] != 'undefined'? helpJson["PAN"] : 'undefined'));
			if(typeof helpJson["PAN"] != 'undefined'){
				//alert((helpJson["PAN"]).substr(helpJson["PAN"].length-4));
				return data.replace(helpJson["PAN"], '*'+(helpJson["PAN"]).substr(helpJson["PAN"].length-4));
			}
			else
				return data;
		}
		catch(e){
			//alert("ERROR JSON PAN parse");
			return data;
		}
	}
	else
		return data;
}
function parseHostAnswer(data){
	var _HostDataJsonObj;
	m_HostServiceName = '';
	m_HostData = '';
	m_HostServiceState = 'error';
	m_HostScreen = '';
	m_HostScreenText = '';
	m_HostAmount = '';
	//m_HostPAN = '';
	
	if(typeof data != 'undefined' && data.constructor === Array && data.length > 0){
		m_HostServiceName = data[0];
		if(data.length > 1)
			m_HostData = data[1];
	}
	else
		m_HostData = data;
	alertMsgLog('[SCRIPT] '+scr.name+' Service: ' + m_HostServiceName + '; Data: ' + hidePAN(m_HostData));
	if(m_HostData != "") {
		_HostDataJsonObj = JSON.parse(m_HostData);
		if(typeof _HostDataJsonObj["Result"] != 'undefined')
			m_HostServiceState = _HostDataJsonObj["Result"];
		if(typeof _HostDataJsonObj["Scr"] != 'undefined')
			m_HostScreen = _HostDataJsonObj["Scr"];
		if(typeof _HostDataJsonObj["HostScreen"] != 'undefined')
			m_HostScreenText = _HostDataJsonObj["HostScreen"];
		if(typeof _HostDataJsonObj["NDCAmount"] != 'undefined')
			m_HostAmount = _HostDataJsonObj["NDCAmount"];
		if(typeof _HostDataJsonObj["PAN"] != 'undefined')
			m_HostPAN = _HostDataJsonObj["PAN"];

	}
	alertMsgLog('[SCRIPT] '+scr.name+' returned from Host state: ' + m_HostServiceState + '; screen: ' + m_HostScreen+'; screenText: '+m_HostScreenText);
}
function controlPinLength(help, init){
	var pinLengthStart = 4;
	if(typeof init != 'undefined')
		pinLengthStart = init;
	if(help.length > pinLengthStart)
		return help.length;
	if(help.length == 0)
		return pinLengthStart;
	else
		return pinLengthStart;
}
function getFastCashButtons(moneyATM, history){
	var amntMass = [];
	var i,j,k,help;
	if(typeof moneyATM == 'string' && moneyATM != '')
		moneyATM = JSON.parse(moneyATM);
	if(typeof history == 'string' && history != '')
		history = JSON.parse(history);
	if(typeof moneyATM != 'undefined' && moneyATM != '')
		for(i = 0; i < moneyATM.length; ++i){
			if(moneyATM[i].count > 0){
				help = parseInt(moneyATM[i].denomination);
				for(j = 0; j < i; ++j)
					if(help < amntMass[j]){
						if(j == 0 || help != amntMass[j-1]){
							amntMass.splice(j, 0, help);
							//alert(amntMass);
						}
						break;
					}
				if(j == i){
					amntMass.push(help);
					//alert(amntMass);
				}
			}
		}
	//alert('history.length: '+history.operations.length);
	if(typeof history != 'undefined' && history != ''){
		if(amntMass.length > 4){
			amntMass.splice(4, amntMass.length - 4);
			//amntMass.length = 4;
			//alert(amntMass);
		}
		i = 0;
		while(amntMass.length < 7 && i < history.operations.length){
			help = parseInt(history.operations[i].amount);
			//alert('history.operations['+i+']='+help);
			for(j = 0; j < amntMass.length; ++j)
				if(help < amntMass[j]){
					if(j == 0 || (help != amntMass[j-1])){
						//проверка, можно ли выдать данную сумму
						amntMass.splice(j, 0, help);
						//alert(amntMass);
					}
					break;
				}
			if(j == amntMass.length && (help != amntMass[j-1])){
				//проверка, можно ли выдать данную сумму
				amntMass.push(help);
				//alert(amntMass);
			}
			i++;
		}
	}
	if(typeof moneyATM != 'undefined' && moneyATM != ''){
		i = 0;
		j = 0;
		k = 2;
		while(amntMass.length < 7 && k < 7){
			for(i = 0; (i < moneyATM.length) && (amntMass.length < 7); ++i){
				if(moneyATM[i].count >= k){
					help = parseInt(moneyATM[i].denomination) * k;
					for(j = 0; j < amntMass.length; ++j)
						if(help < amntMass[j]){
							if(j == 0 || (help != amntMass[j-1])){
								amntMass.splice(j, 0, help);
								//alert(amntMass);
							}
							break;
						}
					if(j == amntMass.length && (help != amntMass[j-1])){
						amntMass.push(help);
						//alert(amntMass);
					}
				}
			}
			k++;
		}
	}
	if(typeof moneyATM == 'undefined' || moneyATM == '')
		amntMass = [500, 900, 1000, 1500, 3000, 4000, 5000];
	
	var fastCashHelp = '';
	if(typeof amntMass != 'undefined' && amntMass.constructor === Array && amntMass.length > 0)
		for(var i = 0; i < amntMass.length; ++i)
			fastCashHelp += (fastCashHelp == '' ? '' : ',')+'{\"text\":\"'+amntMass[i]
			  +'\",\"value\":'+amntMass[i]+'}';
	return fastCashHelp;
}

function getHistory(uid) {
	m_MoneyHistory = undefined;
	window.external.exchange.wbCustom.receiveCardHolderOperationsInfo(uid);
	var historyGet = function(someArgs){
		if(someArgs != '')
			m_MoneyHistory = someArgs;
		alertMsgLog('[history]: '+(m_MoneyHistory == null? 'null':m_MoneyHistory));
		try{
			//var helpJSON = JSON.parse(m_MoneyHistory);
			m_UserData = JSON.parse(m_MoneyHistory);
			m_UserDataNew = JSON.parse(m_MoneyHistory);
			alertMsgLog('JSON: '+m_UserData);
		}
		catch(e){
			alertMsgLog('JSON: '+e.message);
			m_UserData = undefined;
			m_UserDataNew = undefined;			
		}
	}
	addRequestHandler('clientHistory', historyGet);
}
function getCurrencyList(atmMoney){
	try{
		var money = JSON.parse(atmMoney);
	}
	catch(e){
		alertMsgLog('getCurrencyList JSON error: '+e.message);
		return ['rub'];
	}
	var currencyList = [];
	for (var i = 0; i < money.length; i++)
	{
		var new_cur = true;
		for (var j = 0; j < currencyList.length; j++)
		{
			if(money[i].currency == currencyList[j])
			{
				new_cur = false;
				break;
			}
		}
		if(new_cur)
			currencyList.push(money[i].currency);
	}
	return currencyList;
}
function getUserDataField(fieldName){
	if(typeof m_UserDataNew != 'undefined' && typeof (m_UserDataNew[fieldName]) != 'undefined')
		return m_UserDataNew[fieldName];
	else
		return '';
}
function saveToHistory(amnt, curr, check){
	var jsonHelp = {};
	var trueData = {"type":"cashout", "amount": amnt, "currency":curr, "printcheck":check, "count":1};
	var dataStr = '';
	try{
		jsonHelp = JSON.parse(m_MoneyHistory);
		alertMsgLog('saveToHistory history: '+m_MoneyHistory);
		alertMsgLog('saveToHistory history: '+jsonHelp['operations']);
		if(typeof jsonHelp['operations'] == 'undefined'){
			jsonHelp['operations'] = [];
			jsonHelp['operations'].push(trueData);
		}
		else {
			var i, j;
			for(i = 0; i < jsonHelp['operations'].length; ++i){
				if(jsonHelp['operations'][i]['amount'] == amnt){
					j = jsonHelp['operations'][i]['count'];
					if(typeof j != 'undefined'){
						jsonHelp['operations'][i]['count']= j+1;
					}
					else{
						jsonHelp['operations'][i]['count'] = 2;
					}
					jsonHelp['operations'][i]['printcheck'] = check;
					break;
				}
			}
			if(i == jsonHelp['operations'].length){
				jsonHelp['operations'].push(trueData);
			}
		}
		jsonHelp['printcheck'] = check;
		dataStr = 'some","operations":'+JSON.stringify(jsonHelp.operations)+', "printcheck":'+check+', "some":"';
	}
	catch(e){
		alertMsgLog('saveToHistory: '+e.message);
		dataStr = 'some","operations":['+JSON.stringify(trueData)+'], "printcheck":'+check+', "some":"';
	}
	if(typeof jsonHelp['_id'] == 'undefined'){
		alertMsgLog('saveToHistory id: 1111111111111111, data: '+dataStr);
		window.external.exchange.wbCustom.saveCardHolderOperationInfo('1111111111111111', dataStr);
	}
	else{
		alertMsgLog('saveToHistory id: '+jsonHelp['_id']+', data: '+dataStr);
		window.external.exchange.wbCustom.saveCardHolderOperationInfo(jsonHelp['_id'], dataStr);
	}
}
function getATMMoney(){
	m_ATMMoneyInfo = window.external.wbCassetes.getMoneyList();
	//m_ATMMoneyInfo = window.external.wbCassetes.getMoneyList("");
	//m_ATMMoneyInfo = window.external.wbCassetes.getMoneyList("CU");
	alertMsgLog('MoneyInfo type: '+(typeof m_ATMMoneyInfo));
	alertMsgLog('MoneyInfo: '+ m_ATMMoneyInfo);

	if(typeof m_ATMMoneyInfo != 'string' || m_ATMMoneyInfo == '[]' || m_ATMMoneyInfo == ''){
		m_ATMMoneyInfo = "[{\"cassette\":0,\"currency\":\"643\",\"denomination\":\"100\",\"count\":100},{\"cassette\":1,\"currency\":\"643\",\"denomination\":\"200\",\"count\":100},{\"cassette\":2,\"currency\":\"643\",\"denomination\":\"200\",\"count\":100},{\"cassette\":3,\"currency\":\"643\",\"denomination\":\"500\",\"count\":100},{\"cassette\":4,\"currency\":\"643\",\"denomination\":\"1000\",\"count\":100},{\"cassette\":5,\"currency\":\"643\",\"denomination\":\"2000\",\"count\":100},{\"cassette\":6,\"currency\":\"643\",\"denomination\":\"5000\",\"count\":100},"
		+"{\"cassette\":7,\"currency\":\"978\",\"denomination\":\"10\",\"count\":100}]";
		m_CheckSum = undefined;
	}
	else
		m_CheckSum = window.external.wbCassetes;
		//m_CheckSum = undefined;
	m_Currency = new CurrencyClass(getCurrencyList(m_ATMMoneyInfo));
	alertMsgLog('MoneyInfo: '+m_ATMMoneyInfo);	
}
function getATMFuncStatus(){
	var resObj = {};
	try{
		help = window.external.wbCassetes.getCashDispenserState();
		if(help == 'OK' || help == "WARNING")
			resObj.dispenser = true;
		else
			resObj.dispenser = false;
		alertMsgLog('getATMFuncStatus|Dispenser|'+help);
	}
	catch(e){
		alertMsgLog('getATMFuncStatus|Dispenser|'+e.message);
		resObj.dispenser = false;
	}
	try{
		help = window.external.wbCassetes.getBanknoteAcceptorState();
		if(help == 'OK' || help == "WARNING")
			resObj.acceptor = true;
		else
			resObj.acceptor = false;
		alertMsgLog('getATMFuncStatus|Acceptor|'+help);
	}
	catch(e){
		alertMsgLog('getATMFuncStatus|Acceptor|'+e.message);
		resObj.acceptor = false;
	}
	try{
		help = window.external.wbCassetes.getPaperState();
		if(help == 'OK' || help == "WARNING" || help == 'LOW' || help == 'FULL'){
			help = window.external.wbCassetes.getReceiptPrinterState();
			if(help == 'OK' || help == "WARNING"){
				resObj.printer = true;
			}
			else
				resObj.printer = false;
		}
		else
			resObj.printer = false;
		alertMsgLog('getATMFuncStatus|Printer|'+help);
	}
	catch(e){
		alertMsgLog('getATMFuncStatus|Printer|'+e.message);
		resObj.printer = false;
	}
	return resObj;
}
//m_ATMFunctions = {deposit:true, withdraw:true, print:true};
//acceptor:true, dispenser:true, printer:true

var start = function(args) {
    var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle")
				scr.nextScreen(main);
			else if(m_HostServiceName == "oos")
				scr.nextScreen(oos);
			else
				scr.cancel();
		}
		else
			scr.cancel();
		return;
	}
	scr.addCall("HostScriptAddon", onService);
	
	alertMsgLog('[SCRIPT] '+scr.name+'. Экран инициализации');
	m_session = new SessionVariables();
	if(typeof args == 'undefined'){
		scr.setLabel("text", "Пожалуйста, подождите...", "");	
		scr.setLabel("loader","60", '{"loader":"loader"}');
		scr.setImage("bg","../../graphics/BG_blur.jpg","");
		//scr.setButton("quit","",false,false,"{\"icon\": \"\"}",onError);
		//scr.setButton("main_menu","",false,false,"{\"icon\": \"\"}",onError);
		//scr.setImage("smile","../../graphics/icon-smile-2.svg","");
		//scr.setImage("smile","../../graphics/icon-loader.svg","");
		scr.render("wait_message");
	}
	else {
		scr.setLabel("text", "Пожалуйста, подождите...", "");	
		scr.setLabel("loader","60", '{"loader":"loader"}');
		scr.setImage("bg","../../graphics/BG_blur.jpg","");
		//scr.setButton("quit","",false,false,"{\"icon\": \"\"}",onError);
		//scr.setButton("main_menu","",false,false,"{\"icon\": \"\"}",onError);
		//scr.setImage("smile","../../graphics/icon-ok.svg","");
		//scr.setImage("smile","../../graphics/icon-loader.svg","");
		scr.render("wait_message");
	}
	window.external.exchange.ExecNdcService("idle", "");
}
var oos = function() {
    var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle")
				scr.nextScreen(main);
			else if(m_HostServiceName == "oos")
				scr.nextScreen(oos);
			else
				scr.cancel();
		}
		else
			scr.cancel();
	}
	scr.addCall("HostScriptAddon", onService);
	alertMsgLog('[SCRIPT] oos');
	
	scr.setLabel("title", "OOS", "");
	scr.render("test");
	alertMsgLog('[SCRIPT] '+scr.name+'. Страница OOS');
}
var main = function() {
    var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	function addElements(){
		scr.setLabel("header1", "Здравствуйте!", "");
		scr.setLabel("header2", "Для начала работы Вы можете:", "");
		scr.setLabel("earphones", "... или подключить наушники для активации голосового меню", "");
		scr.setLabel("switch_lang", "EN", "");

		scr.setButton("show", "", "", onShow);
		
		var extObj = {};
		extObj.type = "unclickable";
		extObj.icon = "img/qs-icon-1.svg";
		scr.setButton("insert", "Вставить карту", true, true,
		  JSON.stringify(extObj), onButtonEmpty2);
		
		extObj.icon = "img/qs-icon-2.svg";		
		scr.setButton("apply", "Приложить карту", true, false,
		  JSON.stringify(extObj), onButtonEmpty2);
		  
		
		extObj.icon = "img/qs-icon-3.svg";		
		scr.setButton("qrcode", "Считать штрих код", true, false,
		  JSON.stringify(extObj), onButtonEmpty2);
		
		extObj.type = "clickable";
		extObj.icon = "img/qs-icon-4.svg";		
		scr.setButton("nocard", "Войти без карты", true, true,
		  JSON.stringify(extObj), onButtonEmpty3);		
		
		extObj = {};
		extObj.icon = "";
		scr.setButton("switch_lang", "Switch to English", JSON.stringify(extObj), onButton);
		
		scr.setImage("bg","../../graphics/BG_blur.jpg","");
	}
	var onButton = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+'. onButton ' + name);
		//getHistory('676280389000000005');
		//window.external.exchange.ExecNdcService("payments", "");
		m_session.lang = m_session.lang == 'ru' ? 'en' : 'ru';
	
		addElements();
		scr.render("insert_card");
	}
	var onShow = function(name) {
		alertMsgLog(scr.name+'. onShow ' + name);
		scr.setTimeout("5000", "", onInsertTimeout);
		scr.render('insert_card');
	}
	var onInsertTimeout = function(name) {
		alertMsgLog(scr.name+'. onInsertTimeout ' + name);
		scr.setTimeout("0", "", onInsertTimeout);
		scr.render('insert_card_advertising');
	}
	var onButtonEmpty = function(name) {
		alert('onButtonEmpty');
		alertMsgLog(scr.name+'. onButtonEmpty ' + name);
		var help = 'some","operations":['
		  +'{"type":"cashout", "amount": 100, "currency":643, "printcheck":true},'
		  +'{"type":"cashout", "amount": 3900, "currency":643, "printcheck":true},'
		  +'{"type":"cashout", "amount": 18000, "currency":643, "printcheck":true},'
			+' {"type":"cashout", "amount": 2300, "currency":643, "printcheck":false}'
			+'], \"some\":\"';
			
		//window.external.exchange.wbCustom.saveCardHolderOperationInfo('676280389000000005', help);
		//window.external.exchange.wbCustom.saveCardHolderOperationInfo('222222222222222222', help);
		//m_HostPAN = '676280389000000005';
		saveToHistory(900, 643, false);
		//alertMsgLog('done');
		//getHistory("1234");
		
		
	}
	var onButtonEmpty1 = function(name) {
		alert('onButtonEmpty1');
		alertMsgLog(scr.name+'. onButtonEmpty1 ' + name);
		var help = 'some\",{"operations":['
		//  +'{"type":"cashout", "amount": 100, "currency":643, "printcheck":true},'
		//  +'{"type":"cashout", "amount": 3900, "currency":643, "printcheck":true},'
		//  +'{"type":"cashout", "amount": 18000, "currency":643, "printcheck":true},'
			'{\"type\":\"cashout\", \"amount\": 1900, \"currency\":643, \"printcheck\":false}'
			+']}, \"some\":\"';
		//window.external.exchange.wbCustom.saveCardHolderOperationInfo('676280389000000005', help);
		//alertMsgLog('done');
		//getHistory('676280389000000005');
		getHistory('222222222222222222');
	}
	var onButtonEmpty2 = function(name) {
		alertMsgLog(scr.name+'. onButtonEmpty2 ' + name);
		m_PinError = true;
		//var helpText = window.external.exchange.getAllAcceptedNotes("643");
		//alertMsgLog(scr.name+'. notes: '+helpText);
		//var help = JSON.parse('['+helpText+']');
	}
	var onButtonEmpty3 = function(name){
		//window.external.exchange.ExecNdcService("payments", "");
		//m_session.isCard = false;
		scr.nextScreen(pin);
		return;
	}
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					//scr.nextScreen();
					scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else if(m_HostScreen == 'wait_init'){
					//scr.nextScreen();
					scr.nextScreen(msgResult, ['Считываем информацию с карты...','wait']);
					return;
				}
				else if(m_HostScreen == 'wait_init_pay'){
					//scr.nextScreen();
					scr.nextScreen(msgResult, ['Инициализируем платежи...','wait']);
					return;
				}
				else if(m_HostScreen == 'pin_other'){
					m_session.ownCard = false;
					return;
				}
				else
					scr.cancel();
			}
			else if(m_HostServiceName == "before_pin"){
				if(m_HostScreen == "main"){
					//scr.nextScreen(pin);
					//alertMsgLog('PAN: '+m_HostPAN);
					if(m_session.ownCard)
						window.external.exchange.ExecNdcService("pin","");
					else
						window.external.exchange.ExecNdcService("pin_other","");
					m_CardIcon = getPaySystem(m_HostPAN);
					//alert("ACHTUNG1");
					getHistory(m_HostPAN);
					//m_OpenOwnCard = isOpenCard(m_HostPAN);
					return;
				}
				else if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					//scr.nextScreen(wait);
					scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else
					scr.cancel();
			}
			else if(m_HostServiceName == "webius_menu"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					scr.nextScreen(serviceSelect);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "oos")
				scr.nextScreen(oos);
			else
				scr.cancel();
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' not ok');
			scr.cancel();
			return;
		}
	}
	//scr.addCall("HostScriptAddon", onService);
	setTimeout("window.external.RereadWebIUSConfigs();", 100);
	
	addElements();
	//scr.render("insert_card");
	scr.render("insert_card_advertising");

	getATMMoney();
	m_HostPAN = '';
	pinLength = controlPinLength('', 4);
	balanceShow = false;
	balanceShowReq = false;
	balancePrintNeed = false;
	balancePrintReq = false;
	m_session = new SessionVariables();
	if(typeof m_CheckSum != 'undefined')
		alertMsgLog('Ожидание клиента testAmount: 1000 is '+m_CheckSum.isAmountExist(1000, m_Currency.getSelectedCode(), true));	
	alertMsgLog(scr.name+'. Ожидание клиента');
}
var pin = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	function addElements(type){
		if(typeof type == 'undefined'){
			scr.setImage("bg","../../graphics/BG_blur.jpg","");
			scr.setLabel("switch_lang", "EN", "");	
			scr.setLabel("card","1088", '{"icon": "../../graphics/cards/our-visa.svg", "themes": ["our"]}');
			
			scr.setLabel("attempts_left","Осталось 2 попытки", "");
			scr.setLabel("forgot_pin","Забыли ПИН?", "");
			scr.setLabel("change_pin_text","Смените его на&nbsp;новый в&nbsp;мобильном приложении", "");
			scr.setLabel("change_pin_text2","или по&nbsp;телефону", "");
			scr.setLabel("change_pin_phone","8 800 700-78-77", "");
			
			scr.setButton("switch_lang", "Switch to English", '{"icon": "", "display_group":"bottom_line"}', onButton3);	
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "display_group":"bottom_line"}', onButton1);
			
			scr.setInput("pin_code", "******", "","",false,true,"{\"pin_code\": true,\"length\": 6}","text" ,onInput, 'Fill');
		}
		else if(type == 'err'){
			scr.setImage("bg","../../graphics/BG_blur.jpg","");
			scr.setLabel("switch_lang", "EN", "");	
			
			scr.setLabel("attempts_left","Неверный ПИН", "");
			scr.setLabel("forgot_pin","Забыли ПИН?", "");
			scr.setLabel("change_pin_text","Смените его на&nbsp;новый в&nbsp;мобильном приложении", "");
			scr.setLabel("change_pin_text2","или по&nbsp;телефону", "");
			scr.setLabel("change_pin_phone","8 800 700-78-77", "");
			
			
			scr.setButton("switch_lang", "Switch to English", '{"icon": "", "display_group":"bottom_line"}', onButton3);
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "display_group":"bottom_line"}', onButton1);
			
			scr.setInput("pin_code", "", "","",false,true,"{\"pin_code\": true,\"length\": "+pinLength+"}","text" ,onInput, "Error");
		}
	}
	var onEmptyButton = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. EmptyButton ' + name);
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onButton1 = function(name) {
		//alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Завершить на Вводе пин-кода.');
		//window.external.exchange.ExecNdcService("cancel", "");
		//scr.nextScreen(serviceSelect);
		scr.nextScreen(depositSelectAdjunctionFrom);
		//m_session.serviceName = '06-1';
		//scr.nextScreen(cashoutInputAmount, ["info", "Кредитный лимит этой карты", "не позволяет снять такую сумму"]);

		return;
	}
	var onButton2 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжить на Вводе пин-кода.');
		window.external.exchange.ExecNdcService("enter", "");
	}
	var onButton3 = function(name){
		m_session.lang = m_session.lang == 'ru' ? 'en' : 'ru';
	
		addElements();
		scr.render("pin_code");
	}
	var onMoreTimeCall = function(args){
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onMoreTime, value: '+_args);
		
		if(_args == onMoreTime[0]){
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("nomoretime", "");
			serviceName = "return";
		}
		else{
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("moretime", "");
			serviceName = "moretime";
		}
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			if(m_HostServiceName == 'idle'){
				if(m_HostScreen == 'pin'){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == 'wait'){
					window.external.exchange.refreshScr();
					//scr.nextScreen(wait);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+', '+m_HostServiceName+' with unknown screen '+m_HostScreen);
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'before_pin'){
				if(m_HostScreen == 'pin'){
					//scr.nextScreen(pin);
					if(m_session.serviceName == 'pin_error')
						addElements('err');
					else
						addElements();
					
					m_session.balance = 0;
					scr.render("pin_code");
					return;
				}
				else if(m_HostScreen == 'wait'){
					//window.external.exchange.refreshScr();
					//scr.nextScreen(wait);
					return;
				}
				else if(m_HostScreen == 'main'){
					if(m_session.ownCard)
						window.external.exchange.ExecNdcService("pin","");
					else
						window.external.exchange.ExecNdcService("pin_other","");
					return;
				}
				else if(m_HostScreen == 'need_more_time'){
					var help = onMoreTime.join().toString();
					scr.setModalMessage('Вам требуется еще время?', help, -1, true, '{"icon": "","rotate":true,"loader":"countdown","count": 25,"options_settings":[{"name":"logout","icon":"", "theme":"btn-red"},{"name":"cancel","icon":""}]}', onMoreTimeCall);
					scr.render("deposit_select_currency");
					//window.external.exchange.RefreshScr();
					return;
				}
				else if(m_HostScreen == 'card_return'){
					scr.nextScreen(msgResult, ["Пожалуйста, подождите...", "end"]);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+', '+m_HostServiceName+' with unknown screen '+m_HostScreen);
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'webius_menu'){
				if(m_HostScreen == 'main') {
					alertMsgLog('[SCRIPT] '+scr.name+'. service: '+m_HostServiceName+'; state is ok');
					if(m_session.serviceName != 'pin_balance'){
						
						scr.setInput("pin_code", pinValue, "","",false,true,"{\"pin_code\": true,\"length\": "+pinLength+"}","text" ,onInput, "Wait");
						scr.setButton("switch_lang", "Switch to English", true, false, '{"icon": "", "display_group":"bottom_line"}', onButton3);	
						scr.setButton("logout", "Забрать карту", true, false, '{"icon": "../../graphics/icon-pick-card-red.svg", "display_group":"bottom_line"}', onButton1);
						window.external.exchange.refreshScr();
						
						m_session.serviceName = "pin_balance";
						balanceShowReq = false;
						balanceShow = false;
						onBalanceShowButton(name);
					}
					else{
						m_session.serviceName = '';
						scr.nextScreen(serviceSelect);
					}
					//if(m_PinError)
					//{
					//	setTimeout('scr.nextScreen(fakePin);', 1000);
					//	//scr.setInput("pin_code", pinValue, "","",false,true,"{\"pin_code\": true,\"length\": "+pinLength+"}","text" ,onInput, "Error");
					//	//window.external.exchange.refreshScr();
					//	m_PinError = false;
					//}else
						//setTimeout('scr.nextScreen(serviceSelect);', 1000);
					//scr.nextScreen(serviceSelect);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+', '+m_HostServiceName+' with unknown screen '+m_HostScreen);
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'pin_error'){
				m_session.serviceName = 'pin_error';
				window.external.exchange.ExecNdcService("need_pin", "");
				
				addElements('err');
				scr.render('pin_code_error');
				//scr.nextScreen(msgResult,["Неверно введенный пин-код", "end"]);
				return;
			}
			else if(m_HostServiceName == 'request_ok'){
				scr.nextScreen(requestResult, ['ok', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'request_err'){
				scr.nextScreen(requestResult, ['err', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'request_impossible'){
				scr.nextScreen(requestResult, ['req_impossible', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'request_not_performed'){
				scr.nextScreen(requestResult, ['request_not_performed', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "err"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_allowed'){
				scr.nextScreen(requestResult,["request_not_allowed", "pinchange"]);
				return;
			}
			else if(m_HostServiceName == 'incorrect_amount'){
				scr.nextScreen(requestResult,["incorrect_amount", "pinchange"]);
				return;
			}
			else if(m_HostServiceName == 'incorrect_amount2'){
				scr.nextScreen(requestResult,["incorrect_amount2", "pinchange"]);
				return;
			}
			else if(m_HostServiceName == 'not_enough_money'){
				scr.nextScreen(requestResult, ['not_enough_money', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'limit_exceeded'){
				scr.nextScreen(requestResult, ['limit_exceeded', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'amount_to_big'){
				scr.nextScreen(requestResult, ['amount_to_big', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'pinchange_not_allowed'){
				scr.nextScreen(requestResult,['pinchange_not_allowed', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'card_seized'){
				scr.nextScreen(msgResult,['Ваша карта задержана<br>по требованию банка-эмитента', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'card_not_serviced'){
				scr.nextScreen(msgResult,['Извините, карта не обслуживается', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'card_expired'){
				scr.nextScreen(msgResult,['Ваша карта просрочена', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'no_account'){
				scr.nextScreen(msgResult,['Нет требуемого счета', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pin_balance']);
				return;
			}
			else if(m_HostServiceName == 'pin_try_exceeded'){
				scr.nextScreen(msgResult,['Превышено максимально допустимое<br>число попыток ввода пин-кода', 'err']);
				return;
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else {
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' from host '+m_HostServiceState+', return to start');
			scr.cancel();
			return;
		}
	}
	var onInput = function(args){
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = args;
			else {
				pKey = args[0];
				help = args[1];
			}
		}
		pinLength = controlPinLength(help);
		pinValue = help;
	}
	//scr.addCall("HostScriptAddon", onService);
	alertMsgLog('[SCRIPT] '+scr.name+'. Ввод пин-кода. HostScreen' + m_HostScreen);
	var onMoreTime = ['Завершить', 'Продолжить'];
	if(m_session.serviceName == 'pin_error')
		addElements('err');
	else
		addElements();
	
	m_session.balance = 0;
	scr.render("pin_code");
	
	//scr.setButton("cancel", "Завершить", "", onButton1);
	
	//scr.setLabel("forgot_pin", "Забыли ПИН?", "");
	//scr.setLabel("change_pin_text", "Смените его на&nbsp;новый в&nbsp;мобильном приложении", "");
	//scr.setLabel("change_pin_text2", "или по&nbsp;телефону ", "");
	//scr.setLabel("change_pin_phone", "8 800 700-78-77", "");
	//scr.setLabel("switch_lang", "EN", "");	

	//scr.setImage("bg","../../graphics/BG_blur.jpg","");
	//scr.setLabel("card","1088", '{"icon": "../../graphics/cards/our-visa.svg", "themes": ["our"]}');
	//scr.setImage("card","../../graphics/card.svg","");
	
	//scr.setButton("switch_lang", "Switch to English", '{"icon": "", "display_group":"bottom_line"}', onButton3);	
	//scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card.svg", "display_group":"bottom_line"}', onButton1);
	
	//scr.setInput("pin_code", "", "","",false,true,"{\"pin_code\": true,\"length\": "+pinLength+"}","text" ,onInput);
	//scr.render("pin_code_max");
	
	//scr.render("test");
	/*else if(step == 2){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ожидание: Пожалуйста, возьмите деньги. '+m_HostScreen);
		scr.setLabel("title", "Пожалуйста, возьмите деньги", "");
		scr.render("test");
	}
	else if(step == 3){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ожидание: Ваша операция успешно завершена. '+m_HostScreen);
		scr.setLabel("title", "Ваша операция успешно завершена", "");
		scr.render("test");
	}
	else if(step == 4){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ожидание: После завершения всех транзакций не забудьте забрать Вашу карту. '+m_HostScreen);
		scr.setLabel("title", "После завершения всех транзакций не забудьте забрать Вашу карту", "");
		scr.render("test");
	}
	else if(step == 5){
		alertMsgLog('[SCRIPT] '+scr.name+'.  Запрос на продолжение. '+m_HostScreen);
		scr.setButton("button1", "Продолжить", "", onButton3);
		scr.setButton("button2", "Завершить", "", onButton4);

		scr.setButton("cancel", "Отмена", "", onCancel);
		
		scr.setLabel("title", "Хотите продолжить операции?", "");
		scr.render("test");
	}
	else if(step == 6){
		alertMsgLog('[SCRIPT] '+scr.name+'.  Баланс на экране и запрос на продолжение. '+m_HostScreen);
		scr.setButton("button1", "Да", "", onButton3);
		scr.setButton("button2", "Нет", "", onButton4);

		scr.setButton("cancel", "Отмена", "", onCancel);
		
		scr.setLabel("title", "Вы желаете продолжить операции?", "");
		{
			//|'80m|(1|FM +137713.15 KZT|
			var helpIndex = -1;
			var balanceString = '';
			for(var i = 0; i < 3 ; ++i) {
				helpIndex = m_HostScreenText.indexOf('|', helpIndex+1);
			}
			if(helpIndex != -1){
				balanceString = m_HostScreenText.substring(helpIndex+4, m_HostScreenText.indexOf('|', helpIndex+1) != -1 ? m_HostScreenText.indexOf('|', helpIndex+1) : 0);
				scr.setLabel("balance", balanceString, "");
			}
		}
		scr.render("test");
	}
	else if(step == 7){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ожидание завершения. '+m_HostScreen);
		scr.setLabel("title", "Пожалуйста, подождите", "");
		scr.render("test");
	}
	else if(step == 8){
		alertMsgLog('[SCRIPT] '+scr.name+'.  Ожидание завершения по запросу клиента. '+m_HostScreen);
		scr.setLabel("title", "Транзакция прекращена по Вашей инициативе, до свидания", "");
		scr.render("test");
		scr.nextScreen(start);
	}
	*/
}
var fakePin = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Завершить на Вводе пин-кода.');
		window.external.exchange.ExecNdcService("cancel", "");
	}
	var onButton2 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжить на Вводе пин-кода.');
		scr.setInput("pin_code", pinValue, "","",false,true,"{\"pin_code\": true,\"length\": "+pinLength+"}","text" ,onInput, "Wait");
		window.external.exchange.refreshScr();
		setTimeout('scr.nextScreen(serviceSelect);', 1000);
	}
	var onButton3 = function(name){
	}
	var onInput = function(args){
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = args;
			else {
				pKey = args[0];
				help = args[1];
			}
		}
		pinLength = controlPinLength(help);
		pinValue = help;
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			if(m_HostServiceName == 'idle'){
				if(m_HostScreen == ''){
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'pin'){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == 'wait'){
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+', '+m_HostServiceName+' with unknown screen '+m_HostScreen);
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'before_pin'){
				if(m_HostScreen == 'pin'){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == 'wait'){
					//window.external.exchange.refreshScr();
					//scr.nextScreen(wait);
					return;
				}
				else if(m_HostScreen == 'card_return'){
					scr.nextScreen(msgResult, ["Пожалуйста, подождите...", "end"]);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+', '+m_HostServiceName+' with unknown screen '+m_HostScreen);
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'webius_menu'){
				if(m_HostScreen == 'main') {
					scr.nextScreen(serviceSelect);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+', '+m_HostServiceName+' with unknown screen '+m_HostScreen);
					scr.cancel();
					return;
				}

			}
			else {
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' from host '+m_HostServiceState+', return to start');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	scr.setLabel("switch_lang", "EN", "");	

	scr.setImage("bg","../../graphics/BG_blur.jpg","");
	scr.setLabel("card","1088", '{"icon": "../../graphics/cards/our-visa.svg", "themes": ["our"]}');
	
	scr.setLabel("attempts_left","Осталось 2 попытки", "");
	scr.setLabel("forgot_pin","Забыли ПИН?", "");
	scr.setLabel("change_pin_text","Смените его на&nbsp;новый в&nbsp;мобильном приложении", "");
	scr.setLabel("change_pin_text2","или по&nbsp;телефону", "");
	scr.setLabel("change_pin_phone","8 800 700-78-77", "");
	
	
	scr.setButton("switch_lang", "Switch to English", '{"icon": "", "display_group":"bottom_line"}', onButton3);	
	scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout-red.svg", "display_group":"bottom_line"}', onButton1);
	scr.setButton("continue", "Продолжить", '{"icon": ""}', onButton2);
	
	pinLength = controlPinLength('', 4);
	scr.setInput("pin_code", "", "","",false,true,"{\"pin_code\": true,\"length\": "+pinLength+"}","text" ,onInput, "Error");
	//scr.render("pin_code_max");
	scr.render("pin_code_error");
}
var language = function() {
    var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Russian ' + name);
		serviceName = 'language';
		langToShow = 'rus';
		var help = {};
			help['language'] = 'button.120';
		window.external.exchange.ExecNdcService("language", JSON.stringify(help));
	}
	var onButton2 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Kazakh ' + name);
		serviceName = 'language';
		langToShow = 'kaz';
		var help = {};
			help['language'] = 'button.120';
		window.external.exchange.ExecNdcService("language", JSON.stringify(help));
	}
	var onButton3 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Chinese ' + name);
		serviceName = 'language';
		langToShow = 'chn';
		var help = {};
			help['language'] = 'button.120';
		window.external.exchange.ExecNdcService("language", JSON.stringify(help));
	}
	var onButton4 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Turkish ' + name);
		serviceName = 'language';
		langToShow = 'tur';
		var help = {};
			help['language'] = 'button.120';
		window.external.exchange.ExecNdcService("language", JSON.stringify(help));
	}
	var onButton5 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. English ' + name);
		serviceName = 'language';
		langToShow = 'eng';
		var help = {};
			help['language'] = 'button.120';
		window.external.exchange.ExecNdcService("language", JSON.stringify(help));
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			if(m_HostServiceName == "webius_menu"){
				alertMsgLog('[SCRIPT] '+scr.name+' webius_menu needed');
				scr.nextScreen(serviceSelect);
				return;
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' host unknown return, need to cancel');
				scr.cancel();
				return;
			}
		}
		else{
			alertMsgLog('[SCRIPT] '+scr.name+' state is not ok, service: '+m_HostServiceName);
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	scr.setButton("button1", "Русский", "", onButton1);
	scr.setButton("button2", "Казахский", "", onButton2);
	scr.setButton("button3", "Китайский", "", onButton3);
	scr.setButton("button4", "Турецкий", "", onButton4);
	scr.setButton("button5", "Английский", "", onButton5);
	scr.setButton("cancel", "Отмена", "", onCancel);
	
	scr.setLabel("title", "Выберите Язык", "");
	scr.setLabel("note", "language", "");
	
	scr.render("test");
	alertMsgLog('[SCRIPT] '+scr.name+'. Выбор языка.');
}

var serviceSelect = function() {
	
    var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton2 = function(name){
		//
	}
	
	var onButton3 = function(name){		
		scr.nextScreen(depositSelectAdjunctionFrom);
	}
	
	var onButton4 = function(name){	
		m_session.serviceName == '06-1';
		scr.nextScreen(cashoutInputAmount);
	}
	
	var onButton5 = function(name){
		//window.external.exchange.ExecNdcService("ekassir", "");
		//scr.nextScreen(ekassir,1);
		scr.nextScreen(msgResult,"Ветка в разработке");
	}
	var onButton6 = function(name){		
		scr.nextScreen(transferMenu);
	}
	var onButton7 = function(name){		
	scr.nextScreen(helpMenu);
	}
	var onButton8 = function(name){		
		scr.nextScreen(settingsMenu, 0);
	}
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		//scr.cancel();
	}	
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			if(m_HostServiceName == 'idle'){
				scr.nextScreen(main);
				return;
			}
			else if(m_HostServiceName == 'pin_error'){
				scr.nextScreen(msgResult,["Неверно введенный пин-код", "end"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "end"]);
				return;
			}
			else if(m_HostServiceName == 'webius_menu'){
				alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					return;
				}
				else{
					alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
					return;
				}
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else if(m_HostServiceName == 'spec_cancel'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'end_session'){
					scr.nextScreen(start, 1);
					return;
				}
				else{
					scr.nextScreen(msgResult,"Ошибка");
					return;
				}
			}
			else{
				scr.nextScreen(msgResult,"Ошибка");
				return;
			}
		}
		else if(m_HostServiceState == 'cancel'){
			alertMsgLog(scr.name+' from host status cancel, return to start');
			scr.cancel();
			return;
		}
		else{
			alertMsgLog(scr.name+' from host unknown state'+m_HostServiceState+', cancelling.');
			scr.cancel();
			return;
		}

	}
	scr.addCall("HostScriptAddon", onService);
	
	m_ATMFunctions = getATMFuncStatus();
	//scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onBalanceShowButton);		
	//scr.setButton("showremainson", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremains\"}", onBalanceShowButton);		
	//scr.setButton("print", "", "{\"icon\": \"\"}", onBalancePrintButton);
	setBalanceAndPrintButtons(balanceShowReq, balancePrintNeed);
	scr.setButton("deposit", "Пополнить",true, m_ATMFunctions.acceptor, "{\"icon\": \"\"}", onButton3);
	scr.setButton("withdrawal", "Снять наличные",true, m_ATMFunctions.dispenser, "{\"icon\": \"\"}", onButton4);
	scr.setButton("pay", "Оплатить", true, false, "{\"icon\": \"\"}", onButton5);
	scr.setButton("send", "Перевести", true, false, "{\"icon\": \"\"}", onButton6);
	scr.setButton("help", "Помощь", true, false, "{\"icon\": \"../../graphics/icon_help.svg\"}", onButton7);
	scr.setButton("settings", "Настройки карты", true, true/*m_CardIcon.our*/, "{\"icon\": \"../../graphics/icon_settings.svg\"}", onButton8);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", "{\"icon\": \"../../graphics/icon-pick-card-white-small.svg\"}", onCancel);
	else
		scr.setButton("logout", "Выйти", "{\"icon\": \"../../graphics/icon-logout.svg\"}", onCancel);
	
	scr.setImage("bg","../../graphics/BG_blur.jpg","");
	scr.setLabel("card","1088", '{"icon": "../../graphics/cards/our-visa.svg", "themes": ["our"]}');
	//scr.setImage("card","../../graphics/card.svg","");

	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setLabel("title", "Выберите услугу", "");
	scr.setLabel("note", "Select service", "");
	
	//scr.setTimeout("5000", "", onTimeout);
	scr.render("main_menu");
	alertMsgLog('[SCRIPT] '+scr.name+'. Выбор услуги.');
}

var ekassir = function(step){
    var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					scr.nextScreen(wait);
					return;
				}
				else
					scr.cancel();
			}
			else if(m_HostServiceName == "webius_menu"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					scr.nextScreen(serviceSelect);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "oos")
				scr.nextScreen(oos);
			else if(m_HostServiceName == "ekassir"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'ekassir'){
					scr.nextScreen(ekassir, 2);
					return;
				}
				else if(m_HostScreen == 'wait'){
					scr.nextScreen(ekassir, 3);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else
				scr.cancel();
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' not ok');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	if(step == 1)
		window.external.exchange.ExecNdcService("ekassir", "");
	else if(step == 2){
		scr.render("ekassir");
		alertMsgLog('[SCRIPT] '+scr.name+'. Ekassir.');
	}
	else {
		scr.setLabel("title", "Экран ожидания", "");
		scr.render("test");
		alertMsgLog('[SCRIPT] '+scr.name+'. Экран ожидания.');
	}
}

var helpMenu = function() {
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){
		scr.nextScreen(msgResult,"Ближайшие банкоматы бла бла бла");
	}
	var onButton2 = function(name){		
		scr.nextScreen(msgResult,"Ближайшие отделения бла бла бла");
	}
	
	var onButton3 = function(name){		
		scr.nextScreen(serviceSelect);
	}
	
	var onCancel = function(name){		
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	scr.setButton("next_bankomat", "Ближайший банкомат", "{\"icon\": \"img/icon-5-1.svg\"}", onButton1);
	scr.setButton("next_office", "Ближайшее отделение", "{\"icon\": \"img/icon-5-2.svg\"}", onButton2);
	scr.setButton("popup_cancel", "Отмена", "{\"icon\": \"\"}", onButton3);
	scr.setButton("popup_exit", "Выйти", "{\"icon\": \"\"}", onCancel);
	
	
	scr.setLabel("phone_text", "Бесплатный звонок по России", "");
	scr.setLabel("phone", "8 800 700-78-77", "");
	scr.render("help_menu");	
}

var depositSelectAdjunctionFrom = function() {
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	var onButton1 = function(name){
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	var onButton5 = function(name){
		scr.nextScreen(depositSelectAdjunctionMyCard,0);
	}
	var onButton4 = function(name){
		scr.nextScreen(depositSelectAdjunctionCurrency);		
	}
	
	var onButton6 = function(name){
		scr.nextScreen(depositSelectAdjunctionAnotherCard);		
	}
	var onButton3 = function(name){
		scr.nextScreen(settingsMenu, 0);
	}
	var onButton7 = function(name){		
	scr.nextScreen(helpMenu);
	}
	
	var onCancel = function(name){		
		window.external.exchange.ExecNdcService("cancel", "");
		//scr.cancel();
	}
	
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			if(m_HostServiceName == 'idle'){
				scr.nextScreen(main);
				return;
			}
			else if(m_HostServiceName == 'pin_error'){
				scr.nextScreen(msgResult,["Неверно введенный пин-код", "end"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "end"]);
				return;
			}
			else if(m_HostServiceName == 'webius_menu'){
				alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					return;
				}
				else{
					alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
					return;
				}
			}
			else if(m_HostServiceName == 'spec_cancel'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'end_session'){
					scr.nextScreen(start, 1);
					return;
				}
				else{
					scr.nextScreen(msgResult,"Ошибка");
					return;
				}
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else{
				scr.nextScreen(msgResult,"Ошибка");
				return;
			}
		}
		else if(m_HostServiceState == 'cancel'){
			alertMsgLog(scr.name+' from host status cancel, return to start');
			scr.cancel();
			return;
		}
		else{
			alertMsgLog(scr.name+' from host unknown state'+m_HostServiceState+', cancelling.');
			scr.cancel();
			return;
		}

	}
	scr.addCall("HostScriptAddon", onService);
	
	
	scr.setLabel("balance", "Ваш баланс...", "");	
	scr.setImage("bg","../../graphics/BG_blur.jpg","");
	scr.setLabel("card","1088", '{"icon": "../../graphics/cards/our-visa.svg", "themes": ["our"]}');
	//scr.setImage("card","../../graphics/card.svg","");
	//scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onBalanceShowButton);		
	//scr.setButton("showremainson", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremains\"}", onBalanceShowButton);		
	//scr.setButton("print", "", "{\"icon\": \"\"}", onBalancePrintButton);
	setBalanceAndPrintButtons(balanceShowReq, balancePrintNeed);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", "{\"icon\": \"../../graphics/icon-pick-card-white-small.svg\"}", onCancel);
	else
		scr.setButton("logout", "Выйти", "{\"icon\": \"../../graphics/icon-logout.svg\"}", onCancel);
	scr.setButton("settings", "Настройки карты",true, /*m_CardIcon.our*/true, "{\"icon\": \"../../graphics/icon_settings.svg\"}", onButton3);
	scr.setButton("help", "Помощь",true,true, "{\"icon\": \"../../graphics/icon_help.svg\"}", onButton7);
	
	scr.setButton("cash", "Наличными", "{\"icon\": \"img/icon-4-1.svg\"}", onButton4);
	scr.setButton("ourcard", "Со своей карты&nbsp;или&nbsp;счета",true,true, "{\"icon\": \"img/icon-4-2.svg\"}", onButton5);
	scr.setButton("extcard", "С карты другого&nbsp;банка",true,true, "{\"icon\": \"img/icon-4-3.svg\"}", onButton6);	
	
	
	scr.render("deposit_select_source");	
}
var depositSelectAdjunctionCurrency = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	var onButton1 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Balance on screen ' + name);		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();	
	}	
	var onButton8 = function(name){
		scr.nextScreen(cashin, 1);
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		//scr.cancel();
	}	
	
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			if(m_HostServiceName == 'idle'){
				scr.nextScreen(main);
				return;
			}
			else if(m_HostServiceName == 'pin_error'){
				scr.nextScreen(msgResult,["Неверно введенный пин-код", "end"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "end"]);
				return;
			}
			else if(m_HostServiceName == 'webius_menu'){
				alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					return;
				}
				else{
					alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
					return;
				}
			}
			else if(m_HostServiceName == 'spec_cancel'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'end_session'){
					scr.nextScreen(start, 1);
					return;
				}
				else{
					scr.nextScreen(msgResult,"Ошибка");
					return;
				}
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else{
				scr.nextScreen(msgResult,"Ошибка");
				return;
			}
		}
		else if(m_HostServiceState == 'cancel'){
			alertMsgLog(scr.name+' from host status cancel, return to start');
			scr.cancel();
			return;
		}
		else{
			alertMsgLog(scr.name+' from host unknown state'+m_HostServiceState+', cancelling.');
			scr.cancel();
			return;
		}

	}
	scr.addCall("HostScriptAddon", onService);
	
	if(m_Currency.length == 1){
		scr.nextScreen(cashin, 1);
		return;
	}
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setImage("bg","../../graphics/BG_blur.jpg","");
	scr.setLabel("card","1088", '{"icon": "../../graphics/cards/our-visa.svg", "themes": ["our"]}');
	//scr.setImage("card","../../graphics/card.svg","");
	//scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onBalanceShowButton);		
	//scr.setButton("showremainson", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremains\"}", onBalanceShowButton);		
	//scr.setButton("print", "", "{\"icon\": \"\"}", onBalancePrintButton);
	setBalanceAndPrintButtons(balanceShowReq, balancePrintNeed);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", "{\"icon\": \"../../graphics/icon-pick-card-white-small.svg\"}", onCancel);
	else
		scr.setButton("logout", "Выйти", "{\"icon\": \"../../graphics/icon-logout.svg\"}", onCancel);
	
	scr.setButton("roubles", "Рубли", "{\"icon\": \"\"}", onButton8);
	scr.setButton("dollars", "Доллары",true,false, "{\"icon\": \"\"}","", onError);
	scr.setButton("euro", "Евро", true,false,"{\"icon\": \"\"}","",onError);
	
	scr.setLabel("dollar", "Курс доллара 1$ = 67 Руб.", "");
	scr.setLabel("euro", "Курс Евро 1E = 70 Руб.", "");
	//scr.setLabel("conversion", "Текст под кнопками, краткое объяснение про конвертации 15 слов максимум. Ещё пара слов для объёма", "");
	scr.render("deposit_select_currency");
	
}
var depositSelectAdjunctionMyCard = function(type){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	var onButton1 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Balance on screen ' + name);		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();
		//scr.nextScreen(historyCheque, 1);
		//window.external.exchange.ExecNdcService("history", "");
	}
	
	var onButton8 = function(name){		
		scr.nextScreen(msgResult, "Успешно зачисленно "+amount+" Руб");
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onAmount = function(args){
		alertMsgLog('[SCRIPT] '+scr.name+' onAmount args '+args);
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = parseFloat(arg);
			else {
				pKey = args[0];
				help = parseFloat(args[1]);
			}
			if(typeof help == 'number')
				amount = help;
			else
				amount = 0;
		}
		else
			amount = 0;
	}
	
	var amount = 0;
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", "{\"icon\": \"\"}", onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", "{\"icon\": \"../../graphics/icon-pick-card-white-small.svg\"}", onCancel);
	else
		scr.setButton("logout", "Выйти", "{\"icon\": \"../../graphics/icon-logout.svg\"}", onCancel);	
	scr.setList("mbkList", "1234567890,0987654321,5555555565", 0, "", onList);
	scr.setInput("amount", "", "", "Введите сумму", "", onAmount);
	scr.setLabel("note", "Выберите источник платежа:", "");
	scr.setLabel("comment", "Комиссия за перевод не взымается", "");
	scr.setButton("button3", "Пополнить", "", onButton8);
	if(type == 1)
	{
		scr.setLabel("peny", "комиссия за перевод составит 2%", "");
		scr.setLabel("label1", "Будет зачисленно:", "");
		scr.setLabel("toAdjunct", "12312 250 Руб", "");
	}
	
	scr.render("test");	
}
var depositSelectAdjunctionAnotherCard = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	var onButton1 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Balance on screen ' + name);		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();
		//scr.nextScreen(historyCheque, 1);
		//window.external.exchange.ExecNdcService("history", "");
	}
	var onButton8 = function(name){		
		scr.nextScreen(depositSelectAdjunctionMyCard,1);
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onAmount = function(args){
		alertMsgLog('[SCRIPT] '+scr.name+' onAmount args '+args);
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = parseFloat(arg);
			else {
				pKey = args[0];
				help = parseFloat(args[1]);
			}
			if(typeof help == 'number')
				amount = help;
			else
				amount = 0;
		}
		else
			amount = 0;
	}
	
	var amount = 0;
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", "{\"icon\": \"\"}", onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", "{\"icon\": \"../../graphics/icon-pick-card-white-small.svg\"}", onCancel);
	else
		scr.setButton("logout", "Выйти", "{\"icon\": \"../../graphics/icon-logout.svg\"}", onCancel);	
	
	scr.setInput("pan", "", "", "Номер карты", "", onAmount);
	scr.setInput("expire", "", "", "Месяц и год", "", onAmount);
	scr.setInput("cvc", "", "", "CVC", "", onAmount);
	
	scr.setLabel("comment", "Приложите карту чтобы считать её номер", "");
	scr.setButton("button3", "Пополнить", "", onButton8);
	
	scr.render("test");
	
}

var cashoutInputAmount = function(args){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	function deleteInfoLabels(){
		scr.deleteLabel('comission');
		scr.deleteLabel('max_sum_text');
		scr.deleteLabel('error_text1');
		scr.deleteLabel('error_text2');		
	}
	function setPopularAmounts(_RowInd, _StartInd, _Count){
		var fastCashHelp = '';
		for(var j = _StartInd; j < m_session.fashCash.length && j < _StartInd + _Count; ++j)
			fastCashHelp += (fastCashHelp==''?'{':',{')+'"text":"'+AddSpace(m_session.fashCash[j])+' '+m_Currency.getSelectedSymbol()+'","value":'+m_session.fashCash[j]+'}';
		if(fastCashHelp != '')
			scr.setLabel("title_popular"+(_RowInd != 1? _RowInd.toString(): ""), "Быстрый набор", '{"values":['+fastCashHelp+'], "display_group":"fast_btns'+(_RowInd != 1? _RowInd.toString(): "")+'"}');
	}
	var onButton2 = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onButton3 = function(name){//снять
		scr.nextScreen(giveMoney,[amount, 1]);
	}
	var onButton4 = function(name){
		scr.nextScreen(giveMoney,4900);
	}	
	var onButton5 = function(name){
		scr.nextScreen(giveMoney,500);
	}	
	var onButton6 = function(name){
		scr.nextScreen(giveMoney,10000);
	}	
	var onButton7 = function(name){//Вернуться в меню
		scr.nextScreen(serviceSelect);
	}
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		//scr.cancel();
	}	
	
	var onList = function(args){
		alertMsgLog(scr.name+' onList args '+args);
		var pKey = "", help = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0){
			if(typeof args[1] == 'undefined')
				help = args;
			else{
				pKey = args[0];
				help = args[1];
				if(typeof args[2] != 'undefined' && args[2] == 'fast_btns'){
					alertMsgLog('m_FastButtonFlag!');
					m_FastButtonFlag = true;
				}
			}
		}
		
		m_Currency.setSelected(help);
		//alert('Selected curr code: '+m_Currency.getSelectedCode());
		alertMsgLog('currency: '+m_Currency.getSelectedCode()+', amount: '+amount);
		scr.setList("currency", m_Currency.getNames(), m_Currency.selected, m_Currency.getJSON(), onList);
		deleteInfoLabels();
		window.external.exchange.RefreshScr();
	}
	
	var onInput = function(args){
		alertMsgLog('[SCRIPT] '+scr.name+' onAmount args '+args);
		var pKey = "";
		var amountLast = amount;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = parseFloat(args);
			else {
				pKey = args[0];
				help = parseFloat(args[1]);
				if(typeof args[2] != 'undefined' && args[2] == 'fast_btns'){
					alertMsgLog('m_FastButtonFlag!');
					m_FastButtonFlag = true;
				}
			}
			if(typeof help == 'number' && !isNaN(help))
				amount = help;
			else
				amount = 0;
		}
		else
			amount = 0;
		
		alertMsgLog('amountLast: '+amountLast+', amount: '+amount);
		if(m_FastButtonFlag && (amountLast.toString().length == amount.toString().length+1)){
			m_FastButtonFlag = false;
			amount = 0;
		}
		//if(amountLast!= amount)
		//	m_FastButtonFlag = false;
		scr.setInput("sum", amount.toString(), "", "0", false, true, "", "text", onInput, "None");
		m_session.fashCash = m_FastCash.getGeneral(m_Currency.getSelectedCode(),amount);
		var fastCashHelp = '';
		checkboxShow = m_FastCash.validAmount(m_Currency.getSelectedCode(),amount);
		if(!checkboxShow){
			for(var j = 0; j < m_session.fashCash.length; ++j)
				fastCashHelp += (fastCashHelp==''?'{':',{')+'"text":"'+AddSpace(m_session.fashCash[j])+' '+m_Currency.getSelectedSymbol()+'","value":'+m_session.fashCash[j]+'}';
			if(fastCashHelp != '' && !m_FastButtonFlag)
				scr.setLabel("title_popular", "", "{\"values\":["+fastCashHelp+"], \"display_group\":\"fast_btns\"}");
			else
				scr.deleteLabel("title_popular");
		}
		else
			scr.deleteLabel("title_popular");
		
		fastCashHelp = '';
		for(var j = 1; j < 10; ++j)
			fastCashHelp += (fastCashHelp==''?'{':',{')+'"text":"'+j+'","type":"digit"'+((!m_FastCash.button[j])?',"disabled": true':'')+'}';
		scr.setLabel("keyboard", "", '{"values": ['+fastCashHelp+',{"text": "Размен","type": "bynotes","disabled": true},{"text": "0","type": "digit"'+((!m_FastCash.button[0])?',"disabled": true':'')+'},{"text": "Удалить","type": "delete"}],"display_group": "fast_btns"}');
		//scr.setList("currency", m_Currency.getNames(), m_Currency.selected, m_Currency.getJSON(), onList);
		
		if(amount != 0){
			scr.setButton("take", "Снять наличные", true, checkboxShow, "{\"icon\": \"\"}", onButton3);
			//scr.setButton("back", "Вернуться в меню", false, false, "{\"icon\": \"\"}", onButton7);
			scr.setButton("back", "Вернуться в меню", true, true, "{\"icon\": \"\"}", onButton7);
			scr.setInput("checkbox_print", "true", "", "", checkboxShow, checkboxShow, "", "checkbox", onCheckBox, "", m_CheckFlag);
		}
		else{
			checkboxShow = false;
			scr.setButton("back", "Вернуться в меню", true, true, "{\"icon\": \"\"}", onButton7);
			scr.setButton("take", "Снять наличные", true, false, "{\"icon\": \"\"}", onButton3);
			scr.setInput("checkbox_print", "true", "", "", checkboxShow, checkboxShow, "", "checkbox", onCheckBox, "", m_CheckFlag);
		}

		deleteInfoLabels();
		window.external.exchange.RefreshScr();
	}
	var onCheckBox = function(args){
		alertMsgLog('[SCRIPT] '+scr.name+' onCheckBox args: '+args);
		var checkHelp;
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = args;
			else {
				pKey = args[0];
				help = args[1];
			}
			help = help.toLowerCase();
			if(help == 'true')
				checkHelp = true;
			else
				checkHelp = false;
		}
		else
			checkHelp = false;
		alertMsgLog('[Print]: '+checkHelp);
		m_CheckFlag = checkHelp;
		scr.setInput("checkbox_print", "true", "", "", checkboxShow, checkboxShow, "", "checkbox", onCheckBox, "", m_CheckFlag);
	}
	
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			if(m_HostServiceName == 'idle'){
				scr.nextScreen(main);
				return;
			}
			else if(m_HostServiceName == 'pin_error'){
				scr.nextScreen(msgResult,["Неверно введенный пин-код", "end"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "end"]);
				return;
			}
			else if(m_HostServiceName == 'webius_menu'){
				alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					return;
				}
				else{
					alertMsgLog(scr.name+', HostScreen '+m_HostScreen+'.');
					return;
				}
			}
			else if(m_HostServiceName == 'spec_cancel'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'end_session'){
					scr.nextScreen(start, 1);
					return;
				}
				else{
					scr.nextScreen(msgResult,"Ошибка");
					return;
				}
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else{
				scr.nextScreen(msgResult,"Ошибка");
				return;
			}
		}
		else if(m_HostServiceState == 'cancel'){
			alertMsgLog(scr.name+' from host status cancel, return to start');
			scr.cancel();
			return;
		}
		else{
			alertMsgLog(scr.name+' from host unknown state'+m_HostServiceState+', cancelling.');
			scr.cancel();
			return;
		}

	}
	//scr.addCall("HostScriptAddon", onService);

	var _message = [], _type = 'info';
	if(typeof args != 'undefined' && args.constructor === Array && args.length > 0){
		_type = args[0];
		if(args.length > 1)
			for(var j = 1; j < args.length; ++j)
				_message[j-1] = args[j];
	}
	else if(typeof args != 'undefined')
		_message[0] = args;
	
	var amount = 0;
	var m_FastButtonFlag = false;
	var checkboxShow = false;
	m_CheckFlag = false;
	setBalanceAndPrintButtons(balanceShowReq, balancePrintNeed);
	scr.setButton("logout", "Забрать карту", "{\"icon\": \"../../graphics/icon-pick-card-white-small.svg\"}", onCancel);
	scr.setInput("sum", "100000", "", "100000", false, true, '{"maxsum": 100000, "maxlength": 9, "empty": "0"}', "text", onInput, "None");					

	m_FastCash = new FastCash(m_ATMMoneyInfo, m_MoneyHistory, m_session.balance, true);
	m_Currency = new CurrencyClass(m_FastCash.getCurrencyList());//['rub', 'euro', 'dollar']
	m_session.fashCash = m_FastCash.getGeneral(m_Currency.getSelectedCode(),0);
	
	alertMsgLog('fastcash: ' + m_session.fashCash.join());
	
	//var fastCashHelp = '';
	//for(var j = 0; j < m_session.fashCash.length; ++j)
	//	fastCashHelp += (fastCashHelp==''?'{':',{')+'"text":"'+AddSpace(m_session.fashCash[j])+' '+m_Currency.getSelectedSymbol()+'","value":'+m_session.fashCash[j]+'}';
	//if(fastCashHelp != '')
	scr.setLabel("title_popular", "Быстрый набор:", '{"values":[{"text": "4 900 ₽","value": 4900},{"text": "500 ₽","value": 500},{"text": "10 000 ₽","value": 10000}], "display_group":"fast_btns"}');
	scr.setLabel("title_popular2", "Быстрый набор:", '{"values":[{"text": "100 ₽","value": 100},{"text": "600 ₽","value": 600},{"text": "20 000 ₽","value": 20000}], "display_group":"fast_btns2"}');
	scr.setLabel("title_popular3", "Быстрый набор:", '{"values":[{"text": "250 000 ₽","value": 250000}], "display_group":"fast_btns3"}');
	scr.setList("currency", m_Currency.getNames(), m_Currency.selected, m_Currency.getJSON(), onList);
	
	fastCashHelp = '';
	//alert('digits: '+m_FastCash.button);
	for(var j = 1; j < 10; ++j)
		fastCashHelp += (fastCashHelp==''?'{':',{')+'"text":"'+j+'","type":"digit"'+((!m_FastCash.button[j])?',"disabled": true':'')+'}';
	scr.setLabel("keyboard", "", '{"values": ['+fastCashHelp+',{"text": "Размен","type": "bynotes","disabled": true},{"text": "0","type": "digit"'+((!m_FastCash.button[0])?',"disabled": true':'')+'},{"text": "Удалить","type": "delete"}],"display_group": "fast_btns"}');
	
	scr.setButton("back", "Вернуться в меню", true, true, "{\"icon\": \"\"}", onButton7);
	scr.setButton("take", "Снять наличные", true, false, "{\"icon\": \"\"}", onButton3);

	scr.setLabel("print_receipt", "Распечатать чек<br>после операции?", '{"display_group": "print_receipt"}');
	scr.setInput("checkbox_print", "true", "", "", checkboxShow, checkboxShow, "", "checkbox", onCheckBox, "", getUserDataField('printcheck'));
	
	scr.setButton("receipt", "receipt", "{\"icon\": \"../../graphics/icon_check.png\"}", onButton6);
	
	scr.setImage("bg","../../graphics/BG_blur.jpg","");
	scr.setLabel("card","1088", '{"icon": "../../graphics/cards/our-visa.svg", "themes": ["our"]}');
	scr.setImage("offer","../../graphics/offer-icon.png","");

	scr.setLabel("title_sum", "Введите сумму:", "");	
	
	scr.setButton("card_return", "Вернуть карту", "{\"icon\": \"../../graphics/icon_return.png\"}", onButton2);							
	if(m_session.serviceName == '06-1'){
		m_session.serviceName = '06-2';
		if(_message.length > 0){
			if(_type == 'info'){
				if(_message.length > 1){
					scr.setLabel('error_text1',_message[0],'{"display_group":"sum_error", "themes":["blue"]}');
					scr.setLabel('error_text2',_message[1],'{"display_group":"sum_error", "themes":["blue"]}');
				}
				else{
					scr.setLabel('error_text1',' ','{"display_group":"sum_error", "themes":["blue"]}');
					scr.setLabel('error_text2',_message[0],'{"display_group":"sum_error", "themes":["blue"]}');
				}
				scr.setImage("error","../../graphics/mes-info.svg","");
			}
			else if (_type == "err"){
				if(_message.length > 1){
					scr.setLabel('error_text1',_message[0],'{"display_group":"sum_error"}');
					scr.setLabel('error_text2',_message[1],'{"display_group":"sum_error"}');
				}
				else{
					scr.setLabel('error_text1',' ','{"display_group":"sum_error"}');
					scr.setLabel('error_text2',_message[0],'{"display_group":"sum_error"}');
				}
				scr.setImage("error","../../graphics/mes-error.svg","");
			}
			else {
				if(_message.length > 1){
					scr.setLabel('error_text1',_message[0],'{"display_group":"sum_error", "themes":["grey"]}');
					scr.setLabel('error_text2',_message[1],'{"display_group":"sum_error", "themes":["grey"]}');
				}
				else{
					scr.setLabel('error_text1',' ','{"display_group":"sum_error", "themes":["grey"]}');
					scr.setLabel('error_text2',_message[0],'{"display_group":"sum_error", "themes":["grey"]}');
				}
				scr.deleteImage("error");
			}
		}
		setTimeout('scr.nextScreen(cashoutInputAmount, ["info", "Кредитный лимит этой карты", "не позволяет снять такую сумму"]);', 3000);
	}
	else if(m_session.serviceName == '06-2'){
		m_session.serviceName = '06-3';
		scr.deleteLabel('title_popular');
		scr.deleteLabel('title_popular2');
		scr.deleteLabel('title_popular3');
		scr.setInput("checkbox_print", "true", "", "", true, true, "", "checkbox", onCheckBox, "", true);
		scr.setButton("take", "Снять наличные", true, true, "{\"icon\": \"\"}", onButton3);
		if(_message.length > 0){
			if(_type == 'info'){
				if(_message.length > 1){
					scr.setLabel('error_text1',_message[0],'{"display_group":"sum_error", "themes":["blue"]}');
					scr.setLabel('error_text2',_message[1],'{"display_group":"sum_error", "themes":["blue"]}');
				}
				else{
					scr.setLabel('error_text1',' ','{"display_group":"sum_error", "themes":["blue"]}');
					scr.setLabel('error_text2',_message[0],'{"display_group":"sum_error", "themes":["blue"]}');
				}
				scr.setImage("error","../../graphics/mes-info.svg","");
			}
			else if (_type == "err"){
				if(_message.length > 1){
					scr.setLabel('error_text1',_message[0],'{"display_group":"sum_error"}');
					scr.setLabel('error_text2',_message[1],'{"display_group":"sum_error"}');
				}
				else{
					scr.setLabel('error_text1',' ','{"display_group":"sum_error"}');
					scr.setLabel('error_text2',_message[0],'{"display_group":"sum_error"}');
				}
				scr.setImage("error","../../graphics/mes-error.svg","");
			}
			else {
				if(_message.length > 1){
					scr.setLabel('error_text1',_message[0],'{"display_group":"sum_error", "themes":["grey"]}');
					scr.setLabel('error_text2',_message[1],'{"display_group":"sum_error", "themes":["grey"]}');
				}
				else{
					scr.setLabel('error_text1',' ','{"display_group":"sum_error", "themes":["grey"]}');
					scr.setLabel('error_text2',_message[0],'{"display_group":"sum_error", "themes":["grey"]}');
				}
			}
		}
		setTimeout('scr.nextScreen(cashoutInputAmount, ["err", "Кредитный лимит этой карты", "не позволяет снять такую сумму"]);', 3000);
	}
	else if(m_session.serviceName == '06-3'){
		m_session.serviceName = '06-4';
		if(_message.length > 0){
			if(_type == 'info'){
				if(_message.length > 1){
					scr.setLabel('error_text1',_message[0],'{"display_group":"sum_error", "themes":["blue"]}');
					scr.setLabel('error_text2',_message[1],'{"display_group":"sum_error", "themes":["blue"]}');
				}
				else{
					scr.setLabel('error_text1',' ','{"display_group":"sum_error", "themes":["blue"]}');
					scr.setLabel('error_text2',_message[0],'{"display_group":"sum_error", "themes":["blue"]}');
				}
				scr.setImage("error","../../graphics/mes-info.svg","");
			}
			else if (_type == "err"){
				if(_message.length > 1){
					scr.setLabel('error_text1',_message[0],'{"display_group":"sum_error"}');
					scr.setLabel('error_text2',_message[1],'{"display_group":"sum_error"}');
				}
				else{
					scr.setLabel('error_text1',' ','{"display_group":"sum_error"}');
					scr.setLabel('error_text2',_message[0],'{"display_group":"sum_error"}');
				}
				scr.setImage("error","../../graphics/mes-error.svg","");
			}
			else {
				if(_message.length > 1){
					scr.setLabel('error_text1',_message[0],'{"display_group":"sum_error", "themes":["grey"]}');
					scr.setLabel('error_text2',_message[1],'{"display_group":"sum_error", "themes":["grey"]}');
				}
				else{
					scr.setLabel('error_text1',' ','{"display_group":"sum_error", "themes":["grey"]}');
					scr.setLabel('error_text2',_message[0],'{"display_group":"sum_error", "themes":["grey"]}');
				}
			}
		}
		setTimeout('scr.nextScreen(cashoutInputAmount, ["", "Кредитный лимит этой карты", "не позволяет снять такую сумму"]);', 3000);
	}
	else if(m_session.serviceName == '06-4'){
		m_session.serviceName = '06-5';
		if(_message.length > 0){
			if(_type == 'info'){
				if(_message.length > 1){
					scr.setLabel('error_text1',_message[0],'{"display_group":"sum_error", "themes":["blue"]}');
					scr.setLabel('error_text2',_message[1],'{"display_group":"sum_error", "themes":["blue"]}');
				}
				else{
					scr.setLabel('error_text1',' ','{"display_group":"sum_error", "themes":["blue"]}');
					scr.setLabel('error_text2',_message[0],'{"display_group":"sum_error", "themes":["blue"]}');
				}
				scr.setImage("error","../../graphics/mes-info.svg","");
			}
			else if (_type == "err"){
				if(_message.length > 1){
					scr.setLabel('error_text1',_message[0],'{"display_group":"sum_error"}');
					scr.setLabel('error_text2',_message[1],'{"display_group":"sum_error"}');
				}
				else{
					scr.setLabel('error_text1',' ','{"display_group":"sum_error"}');
					scr.setLabel('error_text2',_message[0],'{"display_group":"sum_error"}');
				}
				scr.setImage("error","../../graphics/mes-error.svg","");
			}
			else {
				if(_message.length > 1){
					scr.setLabel('error_text1',_message[0],'{"display_group":"sum_error", "themes":["grey"]}');
					scr.setLabel('error_text2',_message[1],'{"display_group":"sum_error", "themes":["grey"]}');
				}
				else{
					scr.setLabel('error_text1',' ','{"display_group":"sum_error", "themes":["grey"]}');
					scr.setLabel('error_text2',_message[0],'{"display_group":"sum_error", "themes":["grey"]}');
				}
			}
		}
		setTimeout('scr.nextScreen(cashoutInputAmount);', 3000);
	}
	else if(m_session.serviceName == '06-5'){
		m_session.serviceName = '19';
		scr.deleteLabel('title_popular');
		scr.deleteLabel('title_popular2');
		scr.deleteLabel('title_popular3');
		scr.setInput("checkbox_print", "true", "", "", true, true, "", "checkbox", onCheckBox, "", true);
		scr.setButton("take", "Снять наличные", true, true, "{\"icon\": \"\"}", onButton3);
		setTimeout('scr.nextScreen(cashoutInputAmount);', 3000);
	}
	else if(m_session.serviceName == '19'){
		scr.setLabel('wait_text2', 'Сначала заберите карту', "");	
		scr.setWait(true, "Успешно.", '{"icon": "../../graphics/icon-pick-card.svg", "rotate":true,"loader":"countdown","count":30}');
		scr.render("cashout_amount");
		setTimeout('scr.nextScreen(main);', 3000);
		return;
	}

	scr.render("cashout_amount");
}

var settingsMenu = function(type) {
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }	
	scr.addOnError(onError);
	
	var onButton1 = function(name){
		if(type == 0)
			scr.nextScreen(settingsChangePin,1);
		else
			scr.nextScreen(settingsInternetBank);
	}
	var onButton2 = function(name){		
		if(type == 0)
			scr.nextScreen(settingsAddSIM,1);
		else
			scr.nextScreen(settingsCardRequisites,1);
	}
	
	var onButton3 = function(name){		
		if(type == 0)
			scr.nextScreen(msgResult,"3DSecure бла бла бла");
		else
			scr.nextScreen(msgResult,"Что-то с лимитами");
	}
	
	var onButton4 = function(name){		
		if(type == 0)
			scr.nextScreen(settingsSMSInfo);		
	}
	
	var onCancel = function(name){		
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onButton5 = function(name){		
		scr.nextScreen(serviceSelect);
	}
	var onList = function(name){		
		type = type == 0?1:0;
		//scr.nextScreen(settingsMenu,type);
	}
	var onButtonHistory = function(name){
		scr.nextScreen(historyCheque, 1);
	}
	
	scr.setButton("tab_security", "Безопасность", "{\"icon\": \"\"}", onList);
	scr.setButton("tab_services", "Услуги", "{\"icon\": \"\"}", onList);
	scr.setButton("change_pin", "Изменить ПИН", "{\"icon\": \"img/ml-icon-1.svg\",\"group\": \"tablist1\"}", onButton1);
	scr.setButton("link_sim", "Привязать SIM-карту", true, false, "{\"icon\": \"img/ml-icon-2.svg\",\"group\": \"tablist1\"}", onButton2);
	scr.setButton("secure3d", "3D Secure", true, false, "{\"icon\": \"img/ml-icon-3.svg\",\"group\": \"tablist1\"}", onButton3);
	scr.setButton("sms_info", "SMS-Инфо", true, false, "{\"icon\": \"img/ml-icon-4.svg\",\"group\": \"tablist1\"}", onButton4);	
	scr.setButton("internet_bank", "Подключить Интернет-банк", true, false, "{\"icon\": \"img/ml-icon-5.svg\",\"group\": \"tablist2\"}", onButton1);
	scr.setButton("bank_params", "Реквизиты и счета", true, false, "{\"icon\": \"img/ml-icon-6.svg\",\"group\": \"tablist2\"}", onButton2);
	scr.setButton("limits", "Лимиты", true, false, "{\"icon\": \"img/ml-icon-7.svg\",\"group\": \"tablist2\"}", onButton3);
	//scr.setButton("history", "Напечатать минивыписку", "{\"icon\": \"img/ml-icon-7.svg\",\"group\": \"tablist2\"}", onButtonHistory);
	
	
	//scr.setButton("cancel", "Вернуться в меню", "{\"icon\": \"\"}", onButton5);
	//scr.setButton("logout2", "Выйти", '{"icon": "../../graphics/icon-logout-red.svg"}', onCancel);
	scr.setButton("logout2", "Закрыть", '{"icon": ""}', onButton5);
	
	
	scr.render("card_settings_menu");
}
var settingsChangePin = function(step){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onEmptyButton = function(name){
	}
	var onButton1 = function(name){
		alertMsgLog(scr.name+' onButton1 '+name);
		serviceName = 'return';
		window.external.exchange.ExecNdcService("cancel", "");
	}
	var onButton2 = function(name){
		alertMsgLog(scr.name+' onButton2 '+name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
	}
	var onButton3 = function(name){
		alertMsgLog(scr.name+' onButton2 '+name);
		window.external.exchange.ExecNdcService("pin", "");
		serviceName = "return";
	}
	var onButton4 = function(name){
		alertMsgLog(scr.name+' onButton2 '+name);
		window.external.exchange.ExecNdcService("main_menu", "");
		serviceName = "return";
	}
	var onButton5 = function(name){
		alertMsgLog(scr.name+' onButton2 '+name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancelspec", "");
	}
	var onContinue = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжаем на serviceSelect.');
		scr.nextScreen(serviceSelect);
	}
	var onMainMenu = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжаем на serviceSelect.');
		window.external.exchange.ExecNdcService("pin", "");
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancelspec", "");
		//scr.cancel();
	}
	var onInput = function(args){
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = args;
			else {
				pKey = args[0];
				help = args[1];
			}
		}
		pinLength = controlPinLength(help);
		pinValue = help;
	}
	var onCall = function(args) {
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onCall, value: '+_args);
		
		if(_args == callOptions[0]){
			scr.setModalMessage("", "", -1, false, "", onTimeoutButton);
			window.external.exchange.ExecNdcService("main_menu", "");
			serviceName = "return";
		}
		else{
			window.external.exchange.ExecNdcService("cancelspec", "");
			serviceName = "cancel";
		}
	}
	var onGoodCall = function(args) {
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onCall, value: '+_args);
		
		if(_args == goodCallOptions[0]){
			scr.setModalMessage("", "", -1, false, "", onTimeoutButton);
			window.external.exchange.ExecNdcService("pin", "");
			serviceName = "return";
		}
		else{
			window.external.exchange.ExecNdcService("cancelspec", "");
			serviceName = "cancel";
		}
	}
	var onMoreTimeCall = function(args){
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onMoreTime, value: '+_args);
		
		if(_args == onMoreTime[0]){
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("nomoretime", "");
			serviceName = "return";
		}
		else{
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("moretime", "");
			serviceName = "moretime";
		}
	}
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					//scr.nextScreen(wait);
					return;
				}
				else {
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "webius_menu"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					scr.nextScreen(serviceSelect);
					//scr.nextScreen(settingsChangePin, 7);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "pinchange"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'pin'){
					scr.nextScreen(settingsChangePin, 2);
					return;
				}
				else if(m_HostScreen == 'pin_second'){
					scr.nextScreen(settingsChangePin, 3);
					return;
				}
				else if(m_HostScreen == 'pin_error'){
					scr.nextScreen(settingsChangePin, 4);
					return;
				}
				else if(m_HostScreen == 'wait_request'){
					//scr.nextScreen(settingsChangePin, 5);
					scr.setButton("logout", "Забрать карту", true, false, '{"icon": "../../graphics/icon-pick-card-red.svg", "display_group":"bottom_line"}', onButton2);
					scr.setButton("back", "Вернуться в меню", true, false, '{"icon": "","display_group": "bottom_line"}', onButton1);
					scr.setInput("pin_code", "", "","",false,true,"{\"pin_code\": true,\"length\": "+pinLength+"}","text" ,onInput, "Wait");
					window.external.exchange.RefreshScr();
					
					return;
				}
				else if(m_HostScreen == 'wait'){
					//scr.nextScreen(settingsChangePin, 5);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(settingsChangePin, 6);
					return;
				}
				else if(m_HostScreen == 'pin_cancel'){
					if(serviceName == 'return')
						window.external.exchange.ExecNdcService('main_menu','');
					else
						window.external.exchange.ExecNdcService('cancelspec','');
					return;
				}
				else if(m_HostScreen == 'need_more_time'){
					var help = onMoreTime.join().toString();
					scr.setModalMessage('Вам требуется еще время?', help, -1, true, '{"icon": "","rotate":true,"loader":"countdown","count": 25,"options_settings":[{"name":"logout","icon":"", "theme":"btn-red"},{"name":"cancel","icon":""}]}', onMoreTimeCall);
					scr.render("deposit_select_currency");
					//window.external.exchange.RefreshScr();
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+', Service: '+m_HostServiceName+', HostScreen: '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'request_ok'){
				scr.nextScreen(requestResult, ['ok', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'request_err'){
				scr.nextScreen(requestResult, ['err', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'request_impossible'){
				scr.nextScreen(requestResult, ['req_impossible', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'request_not_performed'){
				scr.nextScreen(requestResult, ['request_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "err"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_allowed'){
				scr.nextScreen(requestResult,["request_not_allowed", "pinchange"]);
				return;
			}
			else if(m_HostServiceName == 'incorrect_amount'){
				scr.nextScreen(requestResult,["incorrect_amount", "pinchange"]);
				return;
			}
			else if(m_HostServiceName == 'incorrect_amount2'){
				scr.nextScreen(requestResult,["incorrect_amount2", "pinchange"]);
				return;
			}
			else if(m_HostServiceName == 'not_enough_money'){
				scr.nextScreen(requestResult, ['not_enough_money', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'limit_exceeded'){
				scr.nextScreen(requestResult, ['limit_exceeded', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'amount_to_big'){
				scr.nextScreen(requestResult, ['amount_to_big', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'pinchange_not_allowed'){
				scr.nextScreen(requestResult,['pinchange_not_allowed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'card_seized'){
				scr.nextScreen(msgResult,['Ваша карта задержана<br>по требованию банка-эмитента', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'card_not_serviced'){
				scr.nextScreen(msgResult,['Извините, карта не обслуживается', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'card_expired'){
				scr.nextScreen(msgResult,['Ваша карта просрочена', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'no_account'){
				scr.nextScreen(msgResult,['Нет требуемого счета', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'pin_try_exceeded'){
				scr.nextScreen(msgResult,['Превышено максимально допустимое<br>число попыток ввода пин-кода', 'err']);
				return;
			}
			else if(m_HostServiceName == "spec_cancel"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					scr.nextScreen(specCancelWait, 1);
					return;
				}
				else if(m_HostScreen == 'return_card'){
					scr.nextScreen(specCancelWait, 2);
					return;
				}
				else if(m_HostScreen == 'end_session'){
					scr.nextScreen(specCancelWait, 3);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(specCancelWait, 4);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+', Service: '+m_HostServiceName+', HostScreen: '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "before_pin"){
				if(m_HostScreen == 'main'){
					if(m_session.ownCard)
						window.external.exchange.ExecNdcService("pin","");
					else
						window.external.exchange.ExecNdcService("pin_other","");
					return;
				}
				else if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					//scr.nextScreen(wait);
					return;
				}
				else {
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "oos"){
				scr.nextScreen(oos);
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	var callOptions = ['Вернуться в меню', 'Забрать карту'];
	var goodCallOptions = ['Продолжить', 'Забрать карту'];
	var onMoreTime = ['Завершить', 'Продолжить'];
	
	{//общий набор элементов на экране
		scr.setButton("back", "Вернуться в меню", '{"icon": "","display_group": "bottom_line"}', onButton1);
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "display_group":"bottom_line"}', onButton2);
		scr.setImage("bg","../../graphics/BG_blur.jpg","");
		scr.setLabel("card","1088", '{"icon": "../../graphics/cards/our-visa.svg", "themes": ["our"]}');
		
		pinLength = controlPinLength('', 4);
		scr.setInput("pin_code", "", "","",false,true,"{\"pin_code\": true,\"length\": "+pinLength+"}","text" ,onInput);
	}
	
	if(step == 1){
		window.external.exchange.ExecNdcService("pinchange","");
	}
	else if(step ==  2){
		scr.setLabel("title", "Введите новый пин", "");
		scr.setLabel("safety_pin_text", "Никогда не сообщайте Ваш ПИН сторонним лицам", "");
		scr.setLabel("safety_pin_text2", "Не пишите ПИН на карте и не храните рядом с картой", "");

		scr.render("pin_code");
	}
	else if(step ==  3){
		scr.setLabel("title", "Повторите новый пин", "");
		scr.setLabel("safety_pin_text", "Никогда не сообщайте Ваш ПИН сторонним лицам", "");
		scr.setLabel("safety_pin_text2", "Не пишите ПИН на карте и не храните рядом с картой", "");

		scr.render("pin_code");
	}
	else if(step == 4){
		scr.setLabel("title", "Введите новый пин", "");
		scr.setLabel("safety_pin_text", "Введенные значения не совпадают", "");
		scr.setLabel("safety_pin_text2", "Введите новый пин-код заново", "");
		scr.setInput("pin_code", "", "","",false,true,"{\"pin_code\": true,\"length\": "+pinLength+"}","text" ,onInput, "Error");
		
		scr.render("pin_code");
	}
	else if(step == 5){
		scr.setButton("logout", "Забрать карту", true, false, '{"icon": "../../graphics/icon-pick-card-red.svg", "display_group":"bottom_line"}', onButton2);
		scr.setButton("back", "Вернуться в меню", true, false, '{"icon": "","display_group": "bottom_line"}', onButton1);
		scr.setInput("pin_code", "", "","",false,true,"{\"pin_code\": true,\"length\": "+pinLength+"}","text" ,onInput, "Wait");

		scr.render("pin_code");

		//scr.setWait(true, "Секундочку...", "{\"icon\": \"\", \"rotate\": true, \"loader\":\"loader\"}");
		//scr.render("pin_code");
		//scr.deleteButton("back");
		//scr.deleteButton("logout");
		//scr.deleteInput("pin_code");
		//scr.setButton("menu_main", "Продолжить", '{"icon": ""}', onButton3);
		//scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "theme":"btn-white-red"}', onButton5);
		//scr.setImage("smile","../../graphics/icon-smile-1.svg","");
		//scr.setLabel("text", 'Секундочку...', "");
		//scr.setLabel("loader", "60", '{"loader":"loader"}');
		//scr.render("wait_message");
	}
	else if(step == 6){
		//scr.setWait(true, "Операция не может быть выполнена", "{\"icon\": \"../../graphics/icon-smile-2.svg\", \"theme\":\"blue\"}");
		//scr.render("pin_code");
		scr.deleteButton("back");
		scr.deleteButton("logout");
		scr.deleteInput("pin_code");
		//scr.setButton("menu_main", "Продолжить", '{"icon": ""}', onButton3);
		//scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "theme":"btn-white-red"}', onButton5);
		//scr.setImage("smile","../../graphics/icon-smile-1.svg","");
		scr.setLabel("text", 'Операция не может быть выполнена', "");
		scr.setLabel("loader", "60", '{"loader":"ellipse", "icon": "../../graphics/icon-smile-3.svg"}');
		scr.render("wait_message");	
	}
	else if(step == 7){
		//scr.setModalMessage('Ваш ПИН успешно изменен', goodCallOptions.join(), -1, true, '{"icon": "../../graphics/icon-smile-1.svg","options_settings":[{"name":"cancel","icon":""},{"name":"logout","icon":"../../graphics/icon-logout.svg"}]}', onGoodCall);
		//scr.render("pin_code");
		scr.deleteButton("back");
		scr.deleteButton("logout");
		scr.deleteInput("pin_code");
		scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButton3);
		//scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "theme":"btn-white-red"}', onButton5);
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-logout.svg"}', onButton5);
		scr.setLabel("warning", 'Ваш ПИН успешно изменен', "");
		scr.setImage("smile","../../graphics/icon-smile-1.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(step == 8){
		var help = callOptions.join().toString();
		//scr.setModalMessage('Невозможно выполнить операцию', callOptions.join(), -1, true, '{"icon": "../../graphics/icon-smile-3.svg","options_settings":[{"name":"cancel","icon":""},{"name":"logout","icon":"../../graphics/icon-logout.svg"}]}', onCall);

		//scr.render("pin_code");
		scr.deleteButton("back");
		scr.deleteButton("logout");
		scr.deleteInput("pin_code");
		scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButton4);
		//scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-pick-card-red.svg", "theme":"btn-white-red"}', onButton5);
		scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-logout.svg"}', onButton5);
		scr.setLabel("warning", 'Невозможно выполнить операцию', "");
		scr.setImage("smile","../../graphics/icon-smile-3.svg","");
		scr.render("wait_message_buttons");	
	}
}
var settingsAddSIM = function(step){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	var onButton1 = function(name){		
		scr.nextScreen(msgResult,"Вы успешно привязали новую SIM-карту");
	}
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	
	var onInput = function(args){		
	}
	
	switch(step)
	{
		case 1:
			scr.setInput("phone", "", "", "Введите номер телефона для привязки новой SIM-карты", "", onInput);
			scr.setButton("button1", "Продолжить", "", onButton1);
			if(m_session.isCard)
				scr.setButton("logout", "Забрать карту", "{\"icon\": \"../../graphics/icon-pick-card-white-small.svg\"}", onCancel);
			else
				scr.setButton("logout", "Выйти", "{\"icon\": \"../../graphics/icon-logout.svg\"}", onCancel);				
			break;
		default:
			scr.nextScreen(msgResult,"Ошибка привязки SIM-карты");
			return;
			break;
	}
		
	
	
	scr.render("test");
}
var settingsSMSInfo = function(){
	scr.nextScreen(msgResult,"Вы успешно подключили SMS-Инфо");
}
var settingsInternetBank = function(){
	scr.nextScreen(msgResult,"Вы успешно подключили Интернет-Банк");
}
var settingsCardRequisites = function(step){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	var onButton1 = function(name){
		if(step == 2)
			scr.nextScreen(msgResult,"Реквизиты отправлены");	
	}
	var onButton2 = function(name){
		scr.nextScreen(settingsCardRequisites,2);
	}
	var onButton3 = function(name){
		scr.nextScreen(settingsMenu,1);
	}
	
	var onInput = function(args){		
	}
	
	switch(step)
	{
		case 1:
			
			scr.setLabel("requisites1", "Банк получатель: ФК Открытие", "{\"group\": \"requisites\",\"title\": \"Банк получатель\"}");
			scr.setLabel("requisites2", "3010282619288172928", "{\"group\": \"requisites\",\"title\": \"Корр. счет\"}");
			scr.setLabel("requisites3", "029273628", "{\"group\": \"requisites\",\"title\": \"БИК\"}");
			scr.setLabel("requisites4", "Константин Джон", "{\"group\": \"requisites\",\"title\": \"Получатель\"}");
			scr.setLabel("requisites5", "1029171888881728918", "{\"group\": \"requisites\",\"title\": \"Счет получателя платежа\"}");
			scr.setButton("print_page", "Распечатать", "{\"icon\": \"\"}", onButton1);
			scr.setButton("forward", "Переслать", "{\"icon\": \"\"}", onButton2);
			scr.setButton("return", "Выйти", "{\"icon\": \"\"}", onButton3);		
			scr.render("requisites");
			return;
			break;
		case 2:
			scr.setLabel("title", "Введите номер телефона для отправки реквизитов счета", "");			
			scr.setInput("phone", "+8 (___)___-__-__", "+8 (___)___-__-__", "Номер", true, true, "", "phone", onInput);			
			scr.setButton("continue", "Переслать", "{\"icon\": \"\"}", onButton1);
			scr.setButton("return", "Назад", "{\"icon\": \"\"}", onButton1);
			scr.render("ipnut_phone");
			return;
			break;			
		default:
			scr.nextScreen(msgResult,"Ошибка отправки реквизитов");
			return;
			break;
	}
	scr.render("test");
	
}

var requestResult = function(args){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButtonCancel = function(name){
		alertMsgLog(scr.name+' onButtonCancel '+name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancelspec", "");
	}
	var onButtonMainMenu = function(name){
		alertMsgLog(scr.name+' onButtonMainMenu '+name);
		serviceName = 'webius_menu';
		scr.nextScreen(serviceSelect);
	}
	
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					//scr.nextScreen(wait);
					return;
				}
				else {
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "webius_menu"){
				alertMsgLog('[SCRIPT] '+scr.name+', Service: '+m_HostServiceName+', HostScreen: '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					//scr.nextScreen(serviceSelect);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+', Service: '+m_HostServiceName+', HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'request_ok'){
				scr.nextScreen(requestResult, ['ok', _servName]);
				return;
			}
			else if(m_HostServiceName == 'request_err'){
				scr.nextScreen(requestResult, ['ok', _servName]);
				return;
			}
			else if(m_HostServiceName == 'request_impossible'){
				scr.nextScreen(requestResult, ['req_impossible', _servName]);
				return;
			}
			else if(m_HostServiceName == 'request_not_performed'){
				scr.nextScreen(requestResult, ['request_not_performed', _servName]);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "err"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_allowed'){
				scr.nextScreen(requestResult,["request_not_allowed", _servName]);
				return;
			}
			else if(m_HostServiceName == 'incorrect_amount'){
				scr.nextScreen(requestResult,["incorrect_amount", _servName]);
				return;
			}
			else if(m_HostServiceName == 'incorrect_amount2'){
				scr.nextScreen(requestResult,["incorrect_amount2", _servName]);
				return;
			}
			else if(m_HostServiceName == 'not_enough_money'){
				scr.nextScreen(requestResult, ['not_enough_money', _servName]);
				return;
			}
			else if(m_HostServiceName == 'limit_exceeded'){
				scr.nextScreen(requestResult, ['limit_exceeded', _servName]);
				return;
			}
			else if(m_HostServiceName == 'amount_to_big'){
				scr.nextScreen(requestResult, ['amount_to_big', _servName]);
				return;
			}
			else if(m_HostServiceName == 'pinchange_not_allowed'){
				scr.nextScreen(requestResult, ['pinchange_not_allowed', _servName]);
				return;
			}
			else if(m_HostServiceName == 'card_seized'){
				scr.nextScreen(msgResult,['Ваша карта задержана<br>по требованию банка-эмитента', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'card_not_serviced'){
				scr.nextScreen(msgResult,['Извините, карта не обслуживается', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'card_expired'){
				scr.nextScreen(msgResult,['Ваша карта просрочена', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'no_account'){
				scr.nextScreen(msgResult,['Нет требуемого счета', 'err']);
				//scr.nextScreen(requestResult, ['req_not_performed', 'pinchange']);
				return;
			}
			else if(m_HostServiceName == 'pin_try_exceeded'){
				scr.nextScreen(msgResult,['Превышено максимально допустимое<br>число попыток ввода пин-кода', 'err']);
				return;
			}
			else if(m_HostServiceName == "spec_cancel"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					scr.nextScreen(specCancelWait, 1);
					return;
				}
				else if(m_HostScreen == 'return_card'){
					scr.nextScreen(specCancelWait, 2);
					return;
				}
				else if(m_HostScreen == 'end_session'){
					scr.nextScreen(specCancelWait, 3);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(specCancelWait, 4);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "oos"){
				scr.nextScreen(oos);
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	var _state, _servName = '';
	if(typeof args != 'undefined' && args.constructor === Array && args.length > 0){
		_state = args[0];
		if(args.length > 1)
			_servName = args[1];
	}
	else
		_state = args;

	if(_state == 'ok'){
		if(_servName != 'pin_balance')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonMainMenu);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-logout.svg"}', onButtonCancel);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onButtonCancel);
		if(_servName == 'pinchange')
			scr.setLabel("warning", 'Ваш ПИН успешно изменен', "");
		else
			scr.setLabel("warning", 'Запрос успешно обработан', "");
		scr.setImage("smile","../../graphics/icon-smile-1.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(_state == 'err'){
		if(_servName != 'pin_balance')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonMainMenu);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-logout.svg"}', onButtonCancel);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onButtonCancel);
		scr.setLabel("warning", 'Невозможно выполнить операцию', "");
		scr.setImage("smile","../../graphics/icon-smile-2.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(_state == 'req_impossible'){
		scr.setLabel("text", 'Извините,<br>операция невозможна', "");
		scr.setLabel("loader","60", '{"loader":"ellipse","icon":"../../graphics/icon-smile-2.svg"}');
		scr.render("wait_message");
		setTimeout('scr.nextScreen(serviceSelect);', 5000);
	}
	else if(_state == 'request_not_performed'){
		scr.setLabel("text", 'Невозможно выполнить операцию', "");
		scr.setLabel("loader","60", '{"loader":"ellipse","icon":"../../graphics/icon-smile-2.svg"}');
		scr.render("wait_message");
		setTimeout('scr.nextScreen(serviceSelect);', 5000);
	}
	else if(_state == 'request_not_allowed'){
		if(_servName != 'pin_balance')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonMainMenu);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-logout.svg"}', onButtonCancel);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onButtonCancel);
		scr.setLabel("warning", 'Операция не разрешена', "");
		scr.setImage("smile","../../graphics/icon-smile-2.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(_state == 'pinchange_not_allowed'){
		if(_servName != 'pin_balance')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonMainMenu);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-logout.svg"}', onButtonCancel);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onButtonCancel);
		scr.setLabel("warning", 'К сожалению, для вашей карты<br>недоступна смена пин-кода в банкомате', "");
		scr.setImage("smile","../../graphics/icon-smile-2.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(_state == 'incorrect_amount'){
		if(_servName != 'pin_balance')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonMainMenu);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-logout.svg"}', onButtonCancel);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onButtonCancel);
		scr.setLabel("warning", 'Запрошена некорректная сумма', "");
		scr.setImage("smile","../../graphics/icon-smile-2.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(_state == 'incorrect_amount2'){
		if(_servName != 'pin_balance')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonMainMenu);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-logout.svg"}', onButtonCancel);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onButtonCancel);
		scr.setLabel("warning", 'Пожалуйста, выберите другую сумму<br>(должна быть кратна 100)', "");
		scr.setImage("smile","../../graphics/icon-smile-2.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(_state == 'limit_exceeded'){
		if(_servName != 'pin_balance')
			scr.setButton("main_menu", "В главное меню", '{"icon": ""}', onButtonMainMenu);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", '{"icon": "../../graphics/icon-logout.svg"}', onButtonCancel);
		else
			scr.setButton("logout", "Выйти", '{"icon": "../../graphics/icon-logout.svg"}', onButtonCancel);
		scr.setLabel("warning", 'Превышен лимит выдачи', "");
		scr.setImage("smile","../../graphics/icon-smile-2.svg","");
		scr.render("wait_message_buttons");	
	}
	else if(_state == 'amount_to_big'){
		scr.nextScreen(cashoutInputAmount, ['err', 'Пожалуйста,', 'выберите меньшую сумму']);
		return;
	}
	else if(_state == 'not_enough_money'){
		scr.nextScreen(cashoutInputAmount, ['err', 'Недостаточно средств на счете']);
		return;
	}
	else{
		scr.setLabel("text", "Невозможно выполнить операцию", "");	
		scr.setLabel("loader","60", '{"loader":"ellipse","icon":"../../graphics/icon-smile-2.svg"}');
		scr.render("wait_message");	
		setTimeout('scr.nextScreen(serviceSelect);', 5000);
		return;
	}
}

var transferMenu = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	
	var onButton1 = function(name){
		scr.nextScreen(transferChooseIdType);		
	}
	var onButton2 = function(name){
		scr.nextScreen(transferToCard);		
	}
	var onButton3 = function(name){
		scr.nextScreen(transferToSchet);
	}
	var onButton4 = function(name){
		scr.nextScreen(transferToCompany);
	}
	var onButton5 = function(name){
	}	
	
	var onCancel = function(name){		
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	
	
	scr.setButton("quit", "Выйти", "{\"icon\": \"../../graphics/icon-logout.svg\"}", onCancel);
	scr.setLabel("header", "Переводы", "");
	scr.setLabel("switch_lang", "EN", "");
	scr.setButton("other_account", "Перевести другим людям", "{\"icon\": \"img/qs-icon-1.svg\",\"group\": \"actions\"}", onButton1);
	scr.setButton("other_card", "С карты на карту", "{\"icon\": \"img/qs-icon-2.svg\",\"group\": \"actions\"}", onButton2);
	scr.setButton("my_account", "Между моими счетами", "{\"icon\": \"img/qs-icon-3.svg\",\"group\": \"actions\"}", onButton3);	
	scr.setButton("by_requisites", "По реквизитам юр. лицам", "{\"icon\": \"img/qs-icon-4.svg\",\"group\": \"actions\"}", onButton4);	
	
	scr.setButton("switch_lang", "EN", "", onButton5);	
	
	
	scr.render("transfer_menu");	
}
var transferToCompany = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	var onButton3 = function(name){
		scr.nextScreen(transferInputAmount);
	}	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onInput = function(args){		
	}
	
	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", "{\"icon\": \"\"}", onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", "{\"icon\": \"../../graphics/icon-pick-card-white-small.svg\"}", onCancel);
	else
		scr.setButton("logout", "Выйти", "{\"icon\": \"../../graphics/icon-logout.svg\"}", onCancel);	
	scr.setLabel("title", "Введите реквизиты получателя", "");
	scr.setInput("bik", "", "", "БИК банка-получателя", "", onInput);
	scr.setInput("recipient", "", "", "Получатель", "", onInput);
	scr.setInput("schet", "", "", "Номер счета получателя", "", onInput);
	scr.setInput("inn", "", "", "ИНН", "", onInput);
	scr.setInput("kpp", "", "", "КПП", "", onInput);
	scr.setInput("nds", "", "", "Выберите НДС", "", onInput);
	scr.setButton("button3", "Продолжить", "", onButton3);	
	
	scr.render("test")
}
var transferToSchet = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	var onButton3 = function(name){
		scr.nextScreen(transferInputAmount);
	}	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onInput = function(args){		
	}
	
	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", "{\"icon\": \"\"}", onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", "{\"icon\": \"../../graphics/icon-pick-card-white-small.svg\"}", onCancel);
	else
		scr.setButton("logout", "Выйти", "{\"icon\": \"../../graphics/icon-logout.svg\"}", onCancel);	
	scr.setLabel("source", "Выберите источник платежа", "");
	scr.setList("sourcekList", "1234567890,0987654321,5555555565", 0, "", onList);
	scr.setLabel("source", "Выберите получателя платежа", "");
	scr.setList("sourcekList", "3333333333,44444444444444,7777777777", 0, "", onList);
	
	scr.setButton("button3", "Продолжить", "", onButton3);	
	
	scr.render("test");
}
var transferToCard = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	var onButton3 = function(name){
		scr.nextScreen(transferToCardRecipient);
	}	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onInput = function(args){		
	}
	
	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", "{\"icon\": \"\"}", onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", "{\"icon\": \"../../graphics/icon-pick-card-white-small.svg\"}", onCancel);
	else
		scr.setButton("logout", "Выйти", "{\"icon\": \"../../graphics/icon-logout.svg\"}", onCancel);	
	
	scr.setLabel("source", "Ист очник платежа", "");
	scr.setInput("card", "", "", "Номер карты", "", onInput);		
	
	scr.setLabel("note", "Введите сумму", "");	
	scr.setInput("expire", "", "", "Месяц и год", "", onInput);	
	
	scr.setButton("button3", "Продолжить", "", onButton3);		
	scr.render("test");
}
var transferToCardRecipient = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	var onButton3 = function(name){
		scr.nextScreen(transferSend);
	}	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onInput = function(args){		
	}
	
	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", "{\"icon\": \"\"}", onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", "{\"icon\": \"../../graphics/icon-pick-card-white-small.svg\"}", onCancel);
	else
		scr.setButton("logout", "Выйти", "{\"icon\": \"../../graphics/icon-logout.svg\"}", onCancel);		
	
	scr.setInput("card", "", "", "Номер карты получателя платежа", "", onInput);
	scr.setInput("amuont", "", "", "Введите сумму", "", onInput);	
	
	scr.setButton("button3", "Продолжить", "", onButton3);		
	scr.render("test");
}
var transferChooseIdType = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	
	var onButton3 = function(name){		
		scr.nextScreen(transferRequisitesInput);
	}
	
	var onButton4 = function(name){
		scr.nextScreen(transferCardInput);
	}
	var onButton7 = function(name){		
	scr.nextScreen(helpMenu);
	}
	var onButton8 = function(name){		
		scr.nextScreen(settingsMenu, 1);
	}
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			alertMsgLog('[SCRIPT] '+scr.name+', Service '+m_HostServiceName+'.');
			if(m_HostServiceName == 'webius_menu'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					scr.nextScreen(wait);
					return;
				}
				else if(m_HostScreen == 'main'){
					scr.nextScreen(serviceSelect);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'balance'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					scr.nextScreen(wait);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' state is '+m_HostServiceState+', need to cancel.');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	//scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	//scr.setButton("print", "", "{\"icon\": \"\"}", onBalancePrintButton);
	setBalanceAndPrintButtons(balanceShowReq, balancePrintNeed);
	scr.setButton("requisites", "По реквизитам счета", "{\"icon\": \"img/icon-4-1.svg\"}", onButton3);		
	scr.setButton("numbercard", "По номеру карты", "{\"icon\": \"img/icon-4-2.svg\"}", onButton4);
	scr.setButton("help", "Помощь", "{\"icon\": \"../../graphics/icon_help.svg\"}", onButton7);
	scr.setButton("settings", "Настройки карты", true, /*m_CardIcon.our*/true, "{\"icon\": \"../../graphics/icon_settings.svg\"}", onButton8);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", "{\"icon\": \"../../graphics/icon-pick-card-white-small.svg\"}", onCancel);
	else
		scr.setButton("logout", "Выйти", "{\"icon\": \"../../graphics/icon-logout.svg\"}", onCancel);
	
	scr.setLabel("balance", "Ваш баланс...", "");
	
	
	
	scr.render("transfer_choose_id_type");
	
}
var transferRequisitesInput = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	var onButton3 = function(name){
		scr.nextScreen(transferInputAmount);
	}	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onInput = function(args){		
	}
	
	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", "{\"icon\": \"\"}", onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", "{\"icon\": \"../../graphics/icon-pick-card-white-small.svg\"}", onCancel);
	else
		scr.setButton("logout", "Выйти", "{\"icon\": \"../../graphics/icon-logout.svg\"}", onCancel);	
	scr.setLabel("source", "Выберите источник платежа", "");
	scr.setList("mbkList", "1234567890,0987654321,5555555565", 0, "", onList);
	scr.setInput("bik", "", "", "БИК банка-получателя", "", onInput);
	scr.setInput("recipient", "", "", "Получатель", "", onInput);
	scr.setInput("schet", "", "", "Номер счета", "", onInput);
	scr.setInput("uip", "", "", "УИП (если есть)", "", onInput);
	scr.setButton("button3", "Продолжить", "", onButton3);	
	
	scr.render("test");
	
}
var transferCardInput = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	var onButton3 = function(name){
		scr.nextScreen(transferInputAmount);
	}	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onInput = function(args){		
	}
	
	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", "{\"icon\": \"\"}", onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", "{\"icon\": \"../../graphics/icon-pick-card-white-small.svg\"}", onCancel);
	else
		scr.setButton("logout", "Выйти", "{\"icon\": \"../../graphics/icon-logout.svg\"}", onCancel);	
	
	scr.setLabel("source", "Выберите источник платежа", "");
	scr.setList("mbkList", "1234567890,0987654321,5555555565", 0, "", onList);
	
	scr.setInput("card", "", "", "Введите номер карты получателя:", "", onInput);	
	
	scr.setButton("button3", "Продолжить", "", onButton3);		
	scr.render("test");
}
var transferInputAmount = function(){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){		
		scr.setLabel("balance", "12 3000.00 Руб.", "");
		window.external.exchange.refreshScr();		
	}
	
	var onButton3 = function(name){
		scr.nextScreen(transferSend);
	}	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	
	var onList = function(){
	}
	
	var onInput = function(args){		
	}
	
	
	scr.setLabel("balance", "Ваш баланс...", "");
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onButton1);		
	scr.setButton("print", "", "{\"icon\": \"\"}", onBalancePrintButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", "{\"icon\": \"../../graphics/icon-pick-card-white-small.svg\"}", onCancel);
	else
		scr.setButton("logout", "Выйти", "{\"icon\": \"../../graphics/icon-logout.svg\"}", onCancel);	
	scr.setInput("amount", "", "", "Введите сумму", "", onInput);	
	scr.setInput("nazn", "", "", "Введите назначение платежа:", "", onInput);	
	scr.setLabel("comment", "10% - комиссия при превышении суточного лимита снятия. Максимальная сумма для снятия 200 000 руб.", "");	
	scr.setButton("button3", "Перевести", "", onButton3);		
	scr.render("test");
}
var transferSend = function(){
	scr.nextScreen(msgResult,"Успешно зачисленно 12 250 Руб.");
}

var wait = function(waitType) {
    var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);

	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			if(m_HostServiceName == 'webius_menu'){
				alertMsgLog('[SCRIPT] '+scr.name+' host '+m_HostScreen+' in '+m_HostServiceName);
				if(m_HostScreen == 'wait'){
					scr.nextScreen(wait);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(errorWait);
					return;
				}
				else if(m_HostScreen == 'balance'){
					scr.nextScreen(balanceScr);
					return;
				}
				else if(m_HostScreen == 'main'){
					scr.nextScreen(serviceSelect);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'balance'){
				alertMsgLog('[SCRIPT] '+scr.name+' host '+m_HostScreen+' in '+m_HostServiceName);
				if(m_HostScreen == 'wait'){
					scr.nextScreen(wait);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(errorWait);
					return;
				}
				else if(m_HostScreen == 'balance'){
					scr.nextScreen(balanceScr);
					return;
				}
				else if(m_HostScreen == 'main'){
					scr.nextScreen(serviceSelect);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'cashout'){
				alertMsgLog('[SCRIPT] '+scr.name+' host '+m_HostScreen+' in '+m_HostServiceName);
				if(m_HostScreen == 'wait'){
					scr.nextScreen(wait);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(errorWait);
					return;
				}
				else if(m_HostScreen == 'card_return'){
					scr.nextScreen(wait, 'card_return');
					return;
				}
				else if(m_HostScreen == 'end_session'){
					scr.nextScreen(wait, 'end_session');
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'idle'){
				alertMsgLog('[SCRIPT] '+scr.name+' host '+m_HostScreen+' in '+m_HostServiceName);
				if(m_HostScreen == 'wait'){
					scr.nextScreen(wait);
					return;
				}
				else if(m_HostScreen == 'pin'){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					scr.nextScreen(main);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' state is '+m_HostServiceState+', need to cancel.');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	scr.setLabel("title", "Экран ожидания", "");
	if(typeof waitType == 'undefined'){
		scr.setLabel("text", "Запрос на сервер", "");
		//scr.setButton("quit","123",false,false,"{\"icon\": \"\"}",onError);
		//scr.setButton("main_menu","12321",false,false,"{\"icon\": \"\"}",onError);
		//scr.setImage("smile","../../graphics/icon-smile-1.svg","");
		scr.setLabel("loader","60", '{"loader":"loader"}');
		scr.render("wait_message");
	}
	else {
		if(waitType == 'card_return'){
			scr.setLabel("text", "Не забудьте Вашу карту!", "");
			//scr.setButton("quit","123",false,false,"{\"icon\": \"\"}",onError);
			//scr.setButton("main_menu","12321",false,false,"{\"icon\": \"\"}",onError);
			//scr.setImage("smile","../../graphics/icon-smile-1.svg","");
			scr.setLabel("loader","60", '{"loader":"ellipse", "icon":"../../graphics/icon-smile-1.svg"}');
			scr.render("wait_message");
		}
		else if(waitType == 'end_session'){
			//scr.setLabel("note", "Не забудьте Вашу карту!", "");
			//scr.setLabel("warning", "Сеанс окончен. До свидания!", "");
			//scr.setButton("quit","123",false,false,"{\"icon\": \"\"}",onError);
			//scr.setButton("main_menu","12321",false,false,"{\"icon\": \"\"}",onError);
			//scr.setImage("smile","../../graphics/icon-smile-1.svg","");
			//scr.render("thank_you");
			
			scr.setWait(true, "Пожалуйста, подождите...", "{\"icon\": \"../../graphics/icon-ok.svg\", \"loader\":\"ellipse\",\"theme\":\"blue\"}");
			scr.render("deposit_select_currency");
		}
		else{
			scr.setLabel("text", waitType, "");
			scr.setLabel("loader","60", '{"loader":"ellipse", "icon":"../../graphics/icon-smile-1.svg"}');
			//scr.setButton("quit","123",false,false,"{\"icon\": \"\"}",onError);
			//scr.setButton("main_menu","12321",false,false,"{\"icon\": \"\"}",onError);
			//scr.setImage("smile","../../graphics/icon-smile-1.svg","");
			scr.render("wait_message");
		}
	}
	alertMsgLog('[SCRIPT] '+scr.name+'. Экран ожидания.');
}
var errorWait = function() {
    var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);

	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			if(m_HostServiceName == 'webius_menu'){
				if(m_HostScreen == 'wait'){
					alertMsgLog('[SCRIPT] '+scr.name+' host '+m_HostScreen+' in '+m_HostServiceName);
					scr.nextScreen(wait);
					return;
				}
				else if(m_HostScreen == 'error'){
					alertMsgLog('[SCRIPT] '+scr.name+' host '+m_HostScreen+' in '+m_HostServiceName);
					scr.nextScreen(errorWait);
					return;
				}
				else if(m_HostScreen == 'balance'){
					alertMsgLog('[SCRIPT] '+scr.name+' host '+m_HostScreen+' in '+m_HostServiceName);
					scr.nextScreen(balanceScr);
					return;
				}
				else if(m_HostScreen == 'main'){
					alertMsgLog('[SCRIPT] '+scr.name+' host '+m_HostScreen+' in '+m_HostServiceName);
					scr.nextScreen(serviceSelect);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'idle'){
				alertMsgLog('[SCRIPT] '+scr.name+' host '+m_HostScreen+' in '+m_HostServiceName);
				if(m_HostScreen == 'wait'){
					scr.nextScreen(wait);
					return;
				}
				else if(m_HostScreen == 'pin'){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					scr.nextScreen(main);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' state is '+m_HostServiceState+', need to cancel.');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	scr.setLabel("title", "Экран ожидания", "");
	scr.setLabel("note1", "Приносим наши извинения", "");
	scr.setLabel("note2", "Операцию провести не удалось", "");
	//scr.setButton("cancel", "Отмена", "", onCancel);
	
	scr.render("test");
	alertMsgLog('[SCRIPT] '+scr.name+'. Экран ожидания.');
}
var balanceScr = function() {
    var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);

	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onContinue = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Continue ' + name);
		scr.nextScreen(serviceSelect);
		return;
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			if(m_HostServiceName == 'webius_menu'){
				alertMsgLog('[SCRIPT] '+scr.name+' host '+m_HostScreen+' in '+m_HostServiceName);
				if(m_HostScreen == 'wait'){
					scr.nextScreen(wait);
					return;
				}
				else if(m_HostScreen == 'balance'){
					scr.nextScreen(balanceScr);
					return;
				}
				else if(m_HostScreen == 'main'){
					//scr.nextScreen(balanceScr);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' state is '+m_HostServiceState+', need to cancel.');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	scr.setLabel("title", "Баланс", "");
	scr.setLabel("ScreenText", m_HostScreenText, "");
	scr.setButton("continue", "Продолжить", "", onContinue);
	scr.setButton("cancel", "Отмена", "", onCancel);
	
	scr.render("test");
	alertMsgLog('[SCRIPT] '+scr.name+'. Экран ожидания.');
}
/*var cashoutAmount = function() {
    var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);

	var on100 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача ввод суммы.');
			var help = {};
				help['amount'] = '100';
			window.external.exchange.ExecNdcService("cashout", JSON.stringify(help));
	}
	var on500 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача ввод суммы.');
			var help = {};
				help['amount'] = '500';
			window.external.exchange.ExecNdcService("cashout", JSON.stringify(help));
	}
	var on1000 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача ввод суммы.');
			var help = {};
				help['amount'] = '1000';
			window.external.exchange.ExecNdcService("cashout", JSON.stringify(help));
	}
	var on2000 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача ввод суммы.');
			var help = {};
				help['amount'] = '2000';
			window.external.exchange.ExecNdcService("cashout", JSON.stringify(help));
	}
	var on3000 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача ввод суммы.');
			var help = {};
				help['amount'] = '3000';
			window.external.exchange.ExecNdcService("cashout", JSON.stringify(help));
	}
	var on4000 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача ввод суммы.');
			var help = {};
				help['amount'] = '4000';
			window.external.exchange.ExecNdcService("cashout", JSON.stringify(help));
	}
	var on5000 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача ввод суммы.');
			var help = {};
				help['amount'] = '5000';
			window.external.exchange.ExecNdcService("cashout", JSON.stringify(help));
	}
	var onAmount = function(args){
		alertMsgLog('[SCRIPT] '+scr.name+' onAmount args '+args);
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = parseFloat(arg);
			else {
				pKey = args[0];
				help = parseFloat(args[1]);
			}
			if(typeof help == 'number')
				amount = help;
			else
				amount = 0;
		}
		else
			amount = 0;
	}
	var onContinue = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача ввод суммы.');
		if(amount != 0){
			var help = {};
				help['amount'] = amount;
			window.external.exchange.ExecNdcService("cashout", JSON.stringify(help));
		}
		else {
			scr.setInput("amount", "", "", "сумма выдачи должна быть больше нуля", "", onAmount);
			//window.external.exchange.RefreshScr();
			scr.render("test");
		}
	}
	var onClear = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Сброс суммы.');
		amount = 0;
		scr.setInput("amount", "", "", "сумма выдачи", "", onAmount);
		//window.external.exchange.RefreshScr();
		scr.render("test");
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			alertMsgLog('[SCRIPT] '+scr.name+', Service '+m_HostServiceName+'.');
			if(m_HostServiceName == 'webius_menu'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					alertMsgLog('[SCRIPT] '+scr.name+' host balance choosen');
					scr.nextScreen(wait);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'cashout'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					alertMsgLog('[SCRIPT] '+scr.name+' host cashout choosen');
					scr.nextScreen(wait);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' state is '+m_HostServiceState+', need to cancel.');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	var amount = 0;
	alertMsgLog('[SCRIPT] '+scr.name+'. Выберите сумму выдачи '+m_HostScreen);
	
	scr.setInput("amount", "", "", "сумма выдачи", "", onAmount);

	scr.setButton("button1", "100", "", on100);
	scr.setButton("button2", "500", "", on500);
	scr.setButton("button3", "1 000", "", on1000);
	scr.setButton("button4", "2 000", "", on2000);
	scr.setButton("button5", "3 000", "", on3000);
	scr.setButton("button6", "4 000", "", on4000);
	scr.setButton("button7", "5 000", "", on5000);
	
	scr.setButton("continue", "Продолжить", "", onContinue);
	scr.setButton("clear", "Сбросить", "", onClear);
	scr.setButton("cancel", "Отмена", "", onCancel);
	
	scr.setLabel("title", "Выберите или введите требуемую сумму выдачи.", "");
	
	scr.render("test");
	alertMsgLog('[SCRIPT] '+scr.name+'. Ввод суммы выдачи.');
}
*/
var cashin = function(step) {
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	function deleteElements(){
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(false, "Жду ответа...", "");
		scr.deleteLabel("modal_text2");
		scr.deleteLabel("popup_text");
		scr.deleteLabel("popup_sum");
		scr.deleteLabel("popup_title_sum");
		scr.deleteLabel("popup_comission");
		scr.deleteLabel("bynotes");
	}
	var onEmptyButton = function(){
	}
	var onMoneyCall = function(args) {
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onMoneyCall, value: '+_args);
		
		if(_args == onMoneyCallOptions[0]){
			scr.setModalMessage("", "", -1, false, "", onTimeoutButton);
			window.external.exchange.ExecNdcService("return", "");
			serviceName = "return";
		}
		else{
			window.external.exchange.ExecNdcService("cancel", "");
			serviceName = "cancel";
		}
	}
	var onMoneyAccept = function(args){
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onMoneyAccept, value: '+_args);
		
		if(_args == onMoneyAcceptOptions[0]){
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("return", "");
			serviceName = "return";
		}
		else if(_args == onMoneyAcceptOptions[1]){
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("add", "");
			serviceName = "add";
		}
		else{
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("accept", "");
			serviceName = "accept";
		}
	}
	var onMoreTimeCall = function(args){
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onMoreTime, value: '+_args);
		
		if(_args == onMoreTime[0]){
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("nomoretime", "");
			serviceName = "return";
		}
		else{
			scr.setModalMessage("", "", -1, false, "", onEmptyButton);
			window.external.exchange.ExecNdcService("moretime", "");
			serviceName = "moretime";
		}
	}
	var onButton1 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. cashin зачислить ' + name);
		window.external.exchange.ExecNdcService("accept", "");
	}
	var onButton2 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. cashin добавить ' + name);
		window.external.exchange.ExecNdcService("add", "");
	}
	var onButton3 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. cashin отменить ' + name);
		window.external.exchange.ExecNdcService("cancel", "");
	}
	var onButton4 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжить для запроса.');
		window.external.exchange.ExecNdcService("request", "");
	}
	var onCancel = function(name){
		scr.setLabel("text", "Ну как так?", "");	
		scr.setLabel("loader","60", '{"loader":"ellipse", "icon":"../../graphics/icon-smile-1.svg"}');
		//scr.setButton("quit","123",false,false,"{\"icon\": \"\"}",onError);
		//scr.setButton("main_menu","12321",false,false,"{\"icon\": \"\"}",onError);
		//scr.setImage("smile","../../graphics/icon-smile-3.svg","");
		scr.render("wait_message");		
		window.external.exchange.ExecNdcService("cancel", "");
	}
	var onSpecCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'f015cancel';
		window.external.exchange.ExecNdcService("f015cancel", "");
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			alertMsgLog('[SCRIPT] '+scr.name+', Service '+m_HostServiceName+'.');
			if(m_HostServiceName == 'webius_menu'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					//scr.nextScreen(wait);
					return;
				}
				else if(m_HostScreen == 'main'){
					if(step != 1){
						if(serviceName == "cancel"){
							alertMsgLog('ServiceName cancelspec');
							window.external.exchange.ExecNdcService("cancelspec","");
						}
						else{
							alertMsgLog('return to serviceSelect');
							scr.nextScreen(serviceSelect);
						}
					}
					else
						alertMsgLog('previous operation finished');
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "spec_cancel"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					scr.nextScreen(specCancelWait, 1);
					return;
				}
				else if(m_HostScreen == 'return_card'){
					scr.nextScreen(specCancelWait, 2);
					return;
				}
				else if(m_HostScreen == 'end_session'){
					scr.nextScreen(specCancelWait, 3);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(specCancelWait, 4);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'pin_error'){
				scr.nextScreen(msgResult,["Неверно введенный пин-код", "end"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "end"]);
				return;
			}
			else if(m_HostServiceName == 'cashin'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				var canRefresh = false;
				if(step != 3 && step != 5 && step !=11)
					canRefresh = true;
				if(m_HostScreen == 'wait'){
					step = 2;
					return;
				}
				else if(m_HostScreen == 'wait_show'){
					if(step == 14)
						return;
					else {
						step = 4;
						//scr.nextScreen(cashin, 4);
						deleteElements();
						scr.setWait(true, "Считаем...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
						if(canRefresh)
							window.external.exchange.RefreshScr();
						else
							scr.render("deposit_select_currency");
					}
					return;
				}
				else if(m_HostScreen == 'wait_device'){
					step = 13;
					//scr.nextScreen(cashin, 13);
					deleteElements();
					scr.setWait(true, "Подготавливаем оборудование...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
					if(canRefresh)
						window.external.exchange.RefreshScr();
					else
						scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'wait_device_close'){
					step = 15;
					//scr.nextScreen(cashin, 15);
					deleteElements();
					scr.setWait(true, "Закрываем купюроприемник...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
					if(canRefresh)
						window.external.exchange.RefreshScr();
					else
						scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'wait_money_check_return'){
					step = 14;
					//scr.nextScreen(cashin, 14);
					deleteElements();
					scr.setWait(true, "Отсчитываем деньги...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
					if(canRefresh)
						window.external.exchange.RefreshScr();
					else
						scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'money_insert'){
					step = 3;
					//scr.nextScreen(cashin, 3);
					deleteElements();
					scr.setLabel("modal_text2",'не более 200 купюр', "");
					var help = onMoneyCallOptions.join().toString();
					//scr.setModalMessage('Вносите рубли пачкой', onMoneyCallOptions.join(), -1, true, '{"icon": "","loader":"countdown","count": 90,"options_settings":[{"name":"cancel","icon":""},{"name":"logout","icon":"../../graphics/icon-logout.svg"}]}', onMoneyCall);
					scr.setModalMessage('Вносите рубли пачкой', onMoneyCallOptions.join(), -1, true, '{"icon": "","rotate":true,"loader":"countdown","count": 90,"options_settings":[{"name":"logout","icon":"","theme":"btn-white-red"}]}', onMoneyCall);
					//window.external.exchange.RefreshScr();
					scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'money_check'){
					if(step == 11)
						return;
					else{
						step = 4;
						//scr.nextScreen(cashin, 4);
						deleteElements();
						scr.setWait(true, "Считаем...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
						if(canRefresh)
							window.external.exchange.RefreshScr();
						else
							scr.render("deposit_select_currency");
					}
					return;
				}
				else if(m_HostScreen == 'money_menu'){
					step = 5;
					//scr.nextScreen(cashin, 5);
					deleteElements();
					scr.setLabel("popup_text", 'Будет зачислено', "");
					scr.setLabel("popup_sum", m_HostScreenText.trim()+' ₽', "");
					scr.setLabel("popup_title_sum", 'Внесено: '+m_HostScreenText.trim()+' ₽', "");
					scr.setLabel("popup_comission", 'Комиссия: 0 ₽ (0 %)', "");
					scr.setLabel("bynotes", 'Купюры:', '{"values":['+window.external.exchange.getAllAcceptedNotes("643")+'],"display_group": "bynotes"}');
					var help = onMoneyAcceptOptions.join().toString();
					scr.setModalMessage('', help, 0, true, '{"options_settings":[{"name":"back","icon":""},{"name":"add","icon":""},{"name":"deposit","icon":"","theme":"btn-green"}]}', onMoneyAccept);
					//window.external.exchange.RefreshScr();
					scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'money_info'){
					step = 6;
					//scr.nextScreen(cashin, 6);
					deleteElements();
					scr.setLabel("warning", "Будет зачисленно: "+m_HostAmount, "");			
					scr.setLabel("loader","60", '{"loader":"ellipse", "icon":"../../graphics/icon-smile-1.svg"}');
					if(canRefresh)
						window.external.exchange.RefreshScr();
					else
						scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'wait_request'){
					step = 7;
					//scr.nextScreen(cashin, 7);
					deleteElements();
					scr.setModalMessage("", "", -1, false, "", onEmptyButton);
					scr.setWait(true, "Зачисляем...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
					if(canRefresh)
						window.external.exchange.RefreshScr();
					else
						scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'error'){
					step = 8;
					//scr.nextScreen(cashin, 8);
					deleteElements();
					scr.setModalMessage("", "", -1, false, "", onEmptyButton);
					scr.setWait(true, "Ошибка зачисления...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
					if(canRefresh)
						window.external.exchange.RefreshScr();
					else
						scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'card_return'){
					step = 9;
					//scr.setWait(true, "Заберите Вашу карту", '{"icon": "../../graphics/icon-pick-card.svg", "rotate":true,"loader":"countdown","count":30}');
					//scr.render("deposit_select_currency");
					//return;

					deleteElements();
					scr.setLabel("text", "Заберите Вашу карту", "");
					scr.setLabel("loader","30", '{"loader":"countdown"}');
					scr.setImage("bg","../../graphics/BG_blur.jpg","");
					scr.render("wait_message");
					m_session.balance = 0;
					return;
				}
				else if(m_HostScreen == 'end_session'){
					step = 10;
					//scr.nextScreen(cashin, 10);
					deleteElements();
					scr.setWait(true, "Пожалуйста, подождите...", "{\"icon\": \"../../graphics/icon-ok.svg\", \"loader\":\"ellipse\",\"theme\":\"blue\"}");
					if(canRefresh)
						window.external.exchange.RefreshScr();
					else
						scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'money_return'){
					step = 11;
					//scr.nextScreen(cashin, 11);
					deleteElements();
					scr.setLabel("wait_text2", "банкноты", "");
					scr.setModalMessage("", "", -1, false, "", onEmptyButton);
					scr.setWait(true, "Заберите непринятые", '{"icon": "","rotate":true,"loader":"countdown","count":90}');
					scr.render("deposit_select_currency");
					return;
				}
				else if(m_HostScreen == 'need_more_time'){
					step = 12;
					//scr.nextScreen(cashin, 12);
					deleteElements();
					var help = onMoreTime.join().toString();
					scr.setModalMessage('Вам требуется еще время?', help, -1, true, '{"icon": "","loader":"countdown","count": 25,"options_settings":[{"name":"logout","icon":""},{"name":"cancel","icon":""}]}', onMoreTimeCall);
					scr.render("deposit_select_currency");
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'idle'){
				alertMsgLog('[SCRIPT] '+scr.name+' host '+m_HostScreen+' in '+m_HostServiceName);
				if(m_HostScreen == 'wait'){
					scr.nextScreen(wait);
					return;
				}
				else if(m_HostScreen == ''){
					scr.nextScreen(main);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' state is '+m_HostServiceState+', need to cancel.');
			scr.cancel();
			return;
		}
	}
	//scr.addCall("HostScriptAddon", onService);
	var onMoneyAcceptOptions = ['Вернуть','Добавить','Зачислить'];
	var onMoneyCallOptions = ['Отменить операцию'];
	//var onMoneyCallOptions = ['Отменить операцию','Выйти'];
	var onMoreTime = ['Завершить', 'Продолжить'];
	{//кнопки на верхней части подложки
		scr.setImage("bg","../../graphics/BG_blur.jpg","");
		scr.setLabel("card","1088", '{"icon": "../../graphics/cards/our-visa.svg", "themes": ["our"]}');
		//scr.setImage("card","../../graphics/card.svg","");
		scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"pair\":\"showremainson\"}", onEmptyButton);
		scr.setButton("print", "", "{\"icon\": \"\"}", onEmptyButton);
		if(m_session.isCard)
			scr.setButton("logout", "Забрать карту", "{\"icon\": \"\"}", onEmptyButton);
		else
			scr.setButton("logout", "Выйти", "{\"icon\": \"\"}", onEmptyButton);
		scr.setButton("roubles", "Рубли", true, false, "{\"icon\": \"\"}", onEmptyButton);
		scr.setButton("dollars", "Доллары",true,false, "{\"icon\": \"\"}","", onEmptyButton);
		scr.setButton("euro", "Евро", true,false,"{\"icon\": \"\"}","",onEmptyButton);
	}

	if(step == 1){
		m_session.balance = 0;
		scr.setWait(true, "Подготавливаем оборудование...", '{"icon": "", "loader":"loader"}');
		scr.render("deposit_select_currency");
		setTimeout('scr.nextScreen(cashin, 3);', 3000);
		//window.external.exchange.ExecNdcService("cashin", "");
	}
	else if(step == 2){
		scr.setLabel("warning", "Подготавливаем оборудование", "");	
		scr.setImage("smile","../../graphics/icon-smile-1.svg","");
		scr.render("deposit_select_currency");
	}
	else if(step == 3){
		//alert('1');
		alertMsgLog('[SCRIPT] '+scr.name+'. Ожидание: устройство принимает не более 40 купюр за одну операцию. '+m_HostScreen);
		scr.setLabel("modal_text2",'не более 200 купюр', "");
		var help = onMoneyCallOptions.join().toString();
		//scr.setModalMessage('Вносите рубли пачкой', onMoneyCallOptions.join(), -1, true, '{"icon": "","loader":"countdown","count": 90,"options_settings":[{"name":"cancel","icon":""},{"name":"logout","icon":"../../graphics/icon-logout.svg"}]}', onMoneyCall);
		scr.setModalMessage('Вносите рубли пачкой', onMoneyCallOptions.join(), -1, true, '{"icon": "","rotate":true,"loader":"countdown","count": 90,"options_settings":[{"name":"logout","icon":"","theme":"btn-white-red"}]}', onMoneyCall);
		scr.render("deposit_select_currency");
		setTimeout('scr.nextScreen(cashin, 4);', 3000);
	}
	else if(step == 4){
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(true, "Считаем...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
		scr.render("deposit_select_currency");
		setTimeout('scr.nextScreen(cashin, 5);', 3000);
	}
	else if(step == 5){
		//scr.setLabel("modal_left_text", 'Вы внесли '+m_HostScreenText+' ₽', "");
		//scr.setLabel("modal_left_text2", 'Будет зачислено', "");
		//scr.setLabel("modal_big_text", m_HostScreenText+' ₽', "");
		//getVar("0"+i);
		//var bynotesLabel = "", notesCount = 0;
		//var notesHelp = [{value:10, text:'10 ₽'},{value:10, text:'10 ₽'},{value:50, text:'50 ₽'},{value:100, text:'100 ₽'},{value:500, text:'500 ₽'},{value:1000, text:'1000 ₽'},{value:5000, text:'5000 ₽'}];
		//for(var i = 1; i < 6;++i
		//{
		//	notesCount = getVar("0"+i);
		//	bynotesLabel += (bynotesLabel != '' ? ',{':'{')+'"text":"'+notesHelp[i].text+'", "value":'+notesHelp[i].value+', "left":50,"quantity":'+notesCount+'}';
		//}
		/*bynotes:
		{
			value: 'Купюры:',
			ext: {
				values: [
					{
						text: '5000 ₽',
						value: 5000,
						left: 50,
						quantity: 4,
					},
					{
						text: '200 ₽',
						value: 200,
						left: 0,
						quantity: 0,
					},
					{
						text: '2000 ₽',
						value: 2000,
						left: 20,
						quantity: 2,
					},
					{
						text: '100 ₽',
						value: 100,
						left: 0,
						quantity: 0,
					},
					{
						text: '1000 ₽',
						value: 1000,
						left: 500,
						quantity: 200,
					},
					{
						text: '50 ₽',
						value: 50,
						left: 0,
						quantity: 0,
					},
					{
						text: '500 ₽',
						value: 500,
						left: 100,
						quantity: 1,
					},
					{
						text: '10 ₽',
						value: 10,
						left: 100,
						quantity: 3,
					},
				],
				display_group: 'bynotes',
			},
		}*/
		

		scr.setLabel("popup_text", 'Будет зачислено:', "");
		scr.setLabel("popup_sum", '52 000 ₽', "");
		scr.setLabel("popup_title_sum", 'Внесено: 53 530 ₽', "");
		scr.setLabel("popup_comission", 'Комиссия: 2500 ₽ (1,5%)', "");
		//scr.setLabel("bynotes", 'Купюры:', '{"values":['+window.external.exchange.getAllAcceptedNotes("643")+'],"display_group": "bynotes"}');
		scr.setLabel("bynotes", 'Купюры:', '{"values": [{	"text": "5000 ₽",	"value": 5000,"left": 50,	"quantity": 4},{	"text": "200 ₽",	"value": 200,	"left": 0,	"quantity": 0},{	"text": "2000 ₽",	"value": 2000,	"left": 20,	"quantity": 2},{	"text": "100 ₽",	"value": 100,	"left": 0,	"quantity": 0},{	"text": "1000 ₽",	"value": 1000,	"left": 500,	"quantity": 200},{	"text": "50 ₽",	"value": 50,	"left": 0,	"quantity": 0},{	"text": "500 ₽",	"value": 500,	"left": 100,	"quantity": 1},{	"text": "10 ₽",	"value": 10,	"left": 100,	"quantity": 3}],	"display_group": "bynotes"}');
		var help = onMoneyAcceptOptions.join().toString();
		scr.setModalMessage('', help, 0, true, '{"options_settings":[{"name":"back","icon":""},{"name":"add","icon":""},{"name":"deposit","icon":"","theme":"btn-green"}]}', onMoneyAccept);
		
		scr.render("deposit_select_currency");
		//scr.render("deposit_amount_accepted");
		setTimeout('scr.nextScreen(cashoutInputAmount, ["info", "Кредитный лимит этой карты", "не позволяет снять такую сумму"]);', 3000);
		m_session.serviceName = '06-1';
	}
	else if(step == 6){
		scr.setLabel("warning", "Будет зачисленно: "+m_HostAmount, "");			
		scr.setLabel("loader","60", '{"loader":"ellipse", "icon":"../../graphics/icon-smile-1.svg"}');
		//scr.setButton("quit","Продолжить",true,true,"{\"icon\": \"\"}",onButton4);
		//scr.setImage("smile","../../graphics/icon-smile-1.svg","");		
		scr.render("wait_message");				
	}
	else if(step == 7){
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(true, "Зачисляем...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
		scr.render("deposit_select_currency");
	}
	else if(step == 8){
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(true, "Ошибка зачисления...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
		scr.render("deposit_select_currency");
	}
	else if(step == 9){
		scr.setWait(true, "Заберите Вашу карту", '{"icon": "../../graphics/icon-pick-card.svg", "rotate":true,"loader":"countdown","count":30}');
		scr.render("deposit_select_currency");
	}
	else if(step == 10){
		scr.setWait(true, "Пожалуйста, подождите...", "{\"icon\": \"../../graphics/icon-ok.svg\", \"loader\":\"ellipse\",\"theme\":\"blue\"}");
		scr.render("deposit_select_currency");
	}
	else if(step == 11){
		scr.setLabel("wait_text2", "банкноты", "");
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(true, "Заберите непринятые", '{"icon": "","rotate":true,"loader":"countdown","count":90}');
		scr.render("deposit_select_currency");
	}
	else if(step == 12){
		var help = onMoreTime.join().toString();
		scr.setModalMessage('Вам требуется еще время?', help, -1, true, '{"icon": "","loader":"countdown","count": 25,"options_settings":[{"name":"logout","icon":""},{"name":"cancel","icon":""}]}', onMoreTimeCall);
		scr.render("deposit_select_currency");
	}
	else if(step == 13){
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(true, "Подготавливаем оборудование...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
		scr.render("deposit_select_currency");
	}
	else if(step == 14){
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(true, "Отсчитываем деньги...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
		scr.render("deposit_select_currency");
	}
	else if(step == 15){
		scr.setModalMessage("", "", -1, false, "", onEmptyButton);
		scr.setWait(true, "Закрываем купюроприемник...", '{"icon": "../../graphics/icon-loader.svg","loader":"loader"}');
		scr.render("deposit_select_currency");
	}
}

var balanceService = function(step) {
//step 1 -> on cheque or on screen
//step 2 -> pin-code enter
//step 3 -> wait on req
//step 41 -> screen 0141 balance continue req
//step 42 -> screen 0045 continue req
//step 5 -> wait 0168
//step 6 -> wait 0025
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Баланс на чеке.');
		serviceName = 'balance_type';
		var help = {};
			help['cheque'] = 'button.120';
		window.external.exchange.ExecNdcService("balance_type", JSON.stringify(help));
	}
	var onButton2 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Баланс на экране.');
		serviceName = 'balance_type';
		var help = {};
			help['cheque'] = 'button.119';
		window.external.exchange.ExecNdcService("balance_type", JSON.stringify(help));
	}
	var onButton3 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Завершить на Вводе пин-кода.');
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onButton4 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжить на Вводе вин-кода.');
		window.external.exchange.ExecNdcService("enter", "");
	}
	var onButton5 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжить на HostScreen '+m_HostScreen);
		var help = {};
			help['continue'] = 'button.118';
		window.external.exchange.ExecNdcService("cont_button", JSON.stringify(help));
	}
	var onButton6 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Завершить на HostScreen '+m_HostScreen);
		var help = {};
			help['continue'] = 'button.117';
		window.external.exchange.ExecNdcService("cont_button", JSON.stringify(help));
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceName == 'balance'){
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'cheq_0022'){
					alertMsgLog('[SCRIPT] '+scr.name+' host balance choosen');
					scr.nextScreen(balanceService, 1);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' balance state is ok, HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else {
				alertMsgLog('[SCRIPT] '+scr.name+' service balance on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else if(m_HostServiceName == 'webius_menu'){
			if(m_HostServiceState == 'cancel'){
				alertMsgLog('[SCRIPT] '+scr.name+' from host webius_menu status cancel, return to start');
				scr.cancel();
				return;
			}
			else if(m_HostServiceState == 'ok'){
				alertMsgLog('[SCRIPT] '+scr.name+'. service: '+m_HostServiceName+'; state is ok');
				scr.nextScreen(serviceSelect);
				return;
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host webius_menu unknown state'+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else if(m_HostServiceName == 'pin'){
			alertMsgLog('[SCRIPT] '+scr.name+' pin '+m_HostServiceState+', scr: '+m_HostScreen);
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'pin'){
					alertMsgLog('[SCRIPT] '+scr.name+' pin screen '+m_HostScreen);
					scr.nextScreen(balanceService, 2);
					return;
				}
				else if(m_HostScreen == 'wait_0023'){
					alertMsgLog('[SCRIPT] '+scr.name+' pin screen '+m_HostScreen);
					scr.nextScreen(balanceService, 3);
					return;
				}
				else if(m_HostScreen == 'cont_0141'){
					alertMsgLog('[SCRIPT] '+scr.name+' pin screen '+m_HostScreen+', screenText: '+m_HostScreenText);
					scr.nextScreen(balanceService, 41);
					return;
				}
				else if(m_HostScreen == 'cont_0045'){
					alertMsgLog('[SCRIPT] '+scr.name+' pin screen '+m_HostScreen);
					scr.nextScreen(balanceService, 42);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' pin with unknown screen '+m_HostScreen);
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' service pin on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else if(m_HostServiceName == 'cont_choice'){
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'cont_0141'){
					alertMsgLog('[SCRIPT] '+scr.name+' cont_choice screen '+m_HostScreen+', screenText: '+m_HostScreenText);
					scr.nextScreen(balanceService, 41);
					return;
				}
				else if(m_HostScreen == 'cont_0045'){
					alertMsgLog('[SCRIPT] '+scr.name+' cont_choice screen '+m_HostScreen);
					scr.nextScreen(balanceService, 42);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' cont_choice with unknown screen '+m_HostScreen+', cancelling.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' service cont_choice on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else if(m_HostServiceName == 'end_choice'){
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'wait_0168'){
					alertMsgLog('[SCRIPT] '+scr.name+' end_choice screen '+m_HostScreen);
					scr.nextScreen(balanceService, 5);
					return;
				}
				else if(m_HostScreen == 'wait_0025'){
					alertMsgLog('[SCRIPT] '+scr.name+' end_choice screen '+m_HostScreen);
					scr.nextScreen(balanceService, 6);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' end_choice with unknown screen '+m_HostScreen+', cancelling.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' service end_choice on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	if(step == 1){
		alertMsgLog('[SCRIPT] '+scr.name+'. Как показать баланс. HostScreen '+m_HostScreen);
		scr.setButton("button1", "Баланс на чеке", "", onButton1);
		scr.setButton("button2", "Баланс на экране", "", onButton2);
		//scr.setButton("button3", "Внесение наличных", "", onButton3);
		scr.setButton("cancel", "Отмена", "", onCancel);
		
		scr.setLabel("title", "Как вывести баланс?", "");
		scr.render("test");
	}
	else if(step == 2){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ввод пин-кода. HostScreen' + m_HostScreen);
		scr.setButton("cancel", "Завершить", "", onButton3);
		scr.setButton("continue", "Продолжить", "", onButton4);
		var pinStr = "    ";
		scr.setLabel("pin", pinStr, "");
		
		scr.setLabel("title", "Ввод пин-кода", "");
		scr.setLabel("note", "Введите пин-код карты", "");
		scr.render("test");
	}
	else if(step == 3){
		scr.setLabel("title", "После завершения всех транзакций не забудьте забрать Вашу карту", "");
		scr.render("test");
	}
	else if(step == 41){
		alertMsgLog('[SCRIPT] '+scr.name+'.  Баланс на экране и запрос на продолжение. '+m_HostScreen);
		scr.setButton("button5", "Продолжить", "", onButton5);
		scr.setButton("button6", "Завершить", "", onButton6);

		scr.setButton("cancel", "Отмена", "", onCancel);
		
		scr.setLabel("title", "Выберите услугу", "");
		{
			//|'80m|(1|FM +137713.15 KZT|
			var helpIndex = -1;
			var balanceString = '';
			for(var i = 0; i < 3 ; ++i) {
				helpIndex = m_HostScreenText.indexOf('|', helpIndex+1);
			}
			if(helpIndex != -1){
				balanceString = m_HostScreenText.substring(helpIndex+4, m_HostScreenText.indexOf('|', helpIndex+1) != -1 ? m_HostScreenText.indexOf('|', helpIndex+1) : 0);
				scr.setLabel("balance", balanceString, "");
			}
		}
		scr.render("test");
	}
	else if(step == 42){
		alertMsgLog('[SCRIPT] '+scr.name+'.  Запрос на продолжение. '+m_HostScreen);
		scr.setButton("button5", "Продолжить", "", onButton5);
		scr.setButton("button6", "Завершить", "", onButton6);

		scr.setButton("cancel", "Отмена", "", onCancel);
		
		scr.setLabel("title", "Выберите услугу", "");
		scr.render("test");
	}
	else if(step == 5){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ожидание завершения. '+m_HostScreen);
		scr.setLabel("title", "Пожалуйста, подождите", "");
		scr.render("test");
	}
	else if(step == 6){
		alertMsgLog('[SCRIPT] '+scr.name+'.  Ожидание завершения по запросу клиента. '+m_HostScreen);
		scr.setLabel("title", "Транзакция прекращена по Вашей инициативе, до свидания", "");
		scr.render("test");
		scr.nextScreen(start);
	}
}
var historyCheque = function(step) {
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	var onContinue = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжаем на serviceSelect.');
		scr.nextScreen(serviceSelect);
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancelspec", "");
		//scr.cancel();
	}
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					//scr.nextScreen(wait);
					return;
				}
				else {
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "webius_menu"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					//scr.nextScreen(serviceSelect);
					scr.nextScreen(historyCheque, 6);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "history"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					scr.nextScreen(historyCheque, 2);
					return;
				}
				else if(m_HostScreen == 'return_card'){
					scr.nextScreen(historyCheque, 3);
					return;
				}
				else if(m_HostScreen == 'end_session'){
					scr.nextScreen(historyCheque, 4);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(historyCheque, 5);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "spec_cancel"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					scr.nextScreen(specCancelWait, 1);
					return;
				}
				else if(m_HostScreen == 'return_card'){
					scr.nextScreen(specCancelWait, 2);
					return;
				}
				else if(m_HostScreen == 'end_session'){
					scr.nextScreen(specCancelWait, 3);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(specCancelWait, 4);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "oos"){
				scr.nextScreen(oos);
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	if(step == 1){
		window.external.exchange.ExecNdcService("history", "");
	}
	else if(step == 2){
		alertMsgLog('wait запрос на сервер');
		//scr.setLabel("title", "выполняется запрос на сервер", "");
		//scr.render("test");
		scr.setImage("bg","../../graphics/BG_blur.jpg","");
		//scr.setImage("card","../../graphics/card.svg","");
		scr.setLabel("card","1088", '{"icon": "../../graphics/cards/our-visa.svg", "themes": ["our"]}');
		scr.setImage("error","../../graphics/mes-error.svg","");
		scr.setImage("offer","../../graphics/offer-icon.png","");
		scr.setWait(true, "Жду ответа...", "{\"icon\": \"\", \"rotate\": true, \"loader\":\"loader\"}");
		scr.render("cashout_amount");

	}
	else if(step == 3){
		//alertMsgLog('wait возврат карты');
		//scr.setLabel("note1", "Заберите Вашу карту.", "");
		//scr.render("test");
		scr.setWait(true, "Заберите карту.", '{"icon": "../../graphics/icon-pick-card.svg", "loader":"countdown", "count":60, "rotate": true}');
		//scr.render('cashout_amount');
		window.external.exchange.refreshScr();
	}
	else if(step == 4){
		//alertMsgLog('wait завершение сеанса');
		//scr.setLabel("note1", "Не забудьте Вашу карту.", "");
		//scr.setLabel("note2", "Сеанс окончен. До свидания!", "");
		//scr.render("test");
		//scr.setWait(true, "Спасибо", "{\"icon\": \"../../graphics/icon-ok.svg\", \"theme\":\"blue\"}");
		scr.setWait(true, "Пожалуйста, подождите...", "{\"icon\": \"../../graphics/icon-ok.svg\", \"loader\":\"ellipse\",\"theme\":\"blue\"}");
		window.external.exchange.refreshScr();
	}
	else if(step == 5){
		alertMsgLog('wait ошибка запроса');
		//scr.setLabel("note1", "не удалось получить ответ от сервера", "");
		//scr.setLabel("note2", "Попробуйте ещё раз.", "");
		//scr.render("test");
		
		scr.setWait(true, "Операция не может быть выполнена", "{\"icon\": \"../../graphics/icon-smile-2.svg\", \"theme\":\"blue\"}");
		window.external.exchange.refreshScr();
	}
	else if(step == 6){
		//alertMsgLog('main_menu_history');
		//scr.setLabel("note1", "попробуем напечатать чек!", "");
		//scr.setButton("continue","Продолжаем","",onContinue);
		//scr.setButton("cancel","Завершить","",onCancel);
		//scr.render("test");
		scr.nextScreen(serviceSelect);
	}
}
var specCancelWait = function(step) {
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					//scr.nextScreen(wait);
					return;
				}
				else {
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "webius_menu"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					//scr.nextScreen(serviceSelect);
					scr.nextScreen(histroyCheque, 6);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "spec_cancel"){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					scr.nextScreen(specCancelWait, 1);
					return;
				}
				else if(m_HostScreen == 'return_card'){
					scr.nextScreen(specCancelWait, 2);
					return;
				}
				else if(m_HostScreen == 'end_session'){
					scr.nextScreen(specCancelWait, 3);
					return;
				}
				else if(m_HostScreen == 'error'){
					scr.nextScreen(specCancelWait, 4);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == "oos"){
				scr.nextScreen(oos);
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	if(step == 1){
		alertMsgLog('wait экран ожидания');
		scr.setLabel("title", "Пожалуйста, подождите.", "");
		scr.render("test");
	}
	else if(step == 2){
		alertMsgLog('wait возврат карты');
		scr.setLabel("note1", "Заберите Вашу карту.", "");
		scr.render("test");
	}
	else if(step == 3){
		//alertMsgLog('wait завершение сеанса');
		//scr.setLabel("note1", "Не забудьте Вашу карту.", "");
		//scr.setLabel("note2", "Сеанс окончен. До свидания!", "");
		//scr.render("test");

		//scr.setWait(true, "Спасибо", "{\"icon\": \"../../graphics/icon-ok.svg\", \"loader\":\"ellipse\",\"theme\":\"blue\"}");
		//scr.render('deposit_select_source');
		scr.setLabel("text", "Пожалуйста, подождите...", "");	
		scr.setLabel("loader","60", '{"loader":"loader"}');
		//scr.setButton("quit","123",false,false,"{\"icon\": \"\"}",onError);
		//scr.setButton("main_menu","12321",false,false,"{\"icon\": \"\"}",onError);
		//scr.setImage("smile","../../graphics/icon-ok.svg","");
		scr.render("wait_message");

	}
	else if(step == 4){
		alertMsgLog('wait ошибка запроса');
		scr.setLabel("note1", "не удалось получить ответ от сервера", "");
		scr.setLabel("note2", "Попробуйте ещё раз.", "");
		scr.render("test");
	}
}
var transfer = function(step) {
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	var cardnum = '';
	var onButton1 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс на свою карту.');
		serviceName = 'transfer';
		var help = {};
		help['type'] = 'button.120';
		window.external.exchange.ExecNdcService("type", JSON.stringify(help));
	}
	var onButton2 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс на Сбер.');
		serviceName = 'transfer';
		var help = {};
		help['type'] = 'button.119';
		window.external.exchange.ExecNdcService("type", JSON.stringify(help));
	}
	var onButton3 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс на визу или мастер.');
		serviceName = 'transfer';
		var help = {};
		help['type'] = 'button.118';
		window.external.exchange.ExecNdcService("type", JSON.stringify(help));
	}
	var onButton4 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс ввод номера карты.');
		var help = {};
			help['cardnum'] = cardnum;
		window.external.exchange.ExecNdcService("cardnum", JSON.stringify(help));
	}
	var onButton5 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс отмета ввода номера карты.');
		//serviceName = 'balance_type';
		//var help = {};
		//	help['cheque'] = 'button.120';
		//window.external.exchange.ExecNdcService("balance_type", JSON.stringify(help));
	}
	var onButton6 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс выбор валюты тенге.');
		//serviceName = 'balance_type';
		var help = {};
			help['currency'] = 'button.120';
		window.external.exchange.ExecNdcService("currency", JSON.stringify(help));
	}
	var onButton7 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс выбор валюты доллар.');
		//serviceName = 'balance_type';
		var help = {};
			help['currency'] = 'button.119';
		window.external.exchange.ExecNdcService("currency", JSON.stringify(help));
	}
	var onLimitYes = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс лимит согласен.');
		serviceName = 'limit';
		var help = {};
			help['limit'] = 'button.117';
		window.external.exchange.ExecNdcService("limit", JSON.stringify(help));
	}
	var onLimitNo = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс лимит отказ.');
		serviceName = 'limit';
		var help = {};
			help['limit'] = 'button.113';
		window.external.exchange.ExecNdcService("limit", JSON.stringify(help));
	}
	var onButton8 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс отмена выбора валюты.');
		//serviceName = 'balance_type';
		//var help = {};
		//	help['cheque'] = 'button.120';
		//window.external.exchange.ExecNdcService("balance_type", JSON.stringify(help));
	}
	var onButton9 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс ввод суммы.');
		//serviceName = 'balance_type';
		var help = {};
			help['amount'] = amount;
		window.external.exchange.ExecNdcService("amount", JSON.stringify(help));
	}
	var onButton10 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс отмена ввода суммы.');
		//serviceName = 'balance_type';
		//var help = {};
		//	help['cheque'] = 'button.120';
		//window.external.exchange.ExecNdcService("balance_type", JSON.stringify(help));
	}
	var onButton11 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Завершить на Вводе пин-кода.');
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onButton12 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжить на Вводе вин-кода.');
		window.external.exchange.ExecNdcService("enter", "");
	}
	var onButton13 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжить на HostScreen '+m_HostScreen);
		var help = {};
			help['continue'] = 'button.118';
		window.external.exchange.ExecNdcService("cont_button", JSON.stringify(help));
	}
	var onButton14 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Завершить на HostScreen '+m_HostScreen);
		var help = {};
			help['continue'] = 'button.117';
		window.external.exchange.ExecNdcService("cont_button", JSON.stringify(help));
	}
	var onCashOnCheck = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс, заказать нал, код на чеке.');
		serviceName = 'transfer';
		var help = {};
		help['type'] = 'button.117';
		window.external.exchange.ExecNdcService("type", JSON.stringify(help));
	}
	var onCashPhone = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс, заказать нал, номер телефона.');
		if(cardnum == ''){
			alertMsgLog('[SCRIPT] '+scr.name+' host transfer cardnum input, phonenum invalid');
			scr.nextScreen(transfer, 31);
			return;
		}
		else {
			serviceName = 'transfer';
			var help = {};
			help['phone'] = cardnum;
			window.external.exchange.ExecNdcService("phone", JSON.stringify(help));
		}
	}
	var onCashAmount = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Заказ ввод суммы.');
		//serviceName = 'balance_type';
		var help = {};
			help['amount'] = amount;
		window.external.exchange.ExecNdcService("phoneamount", JSON.stringify(help));
	}
	var onPhoneNum = function(args){
		alertMsgLog('[SCRIPT] '+scr.name+' onPhone args '+args);
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = arg;
			else {
				pKey = args[0];
				help = args[1];
			}
			if(help.length <= 10)
				cardnum = help;
			else
				cardnum = '';
		}
		else
			cardnum = '';
	}
	var onCashConfirm = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс, заказать нал, подтверждение.');
		serviceName = 'confirm';
		var help = {};
		help['confirm'] = 'button.117';
		window.external.exchange.ExecNdcService("confirm", JSON.stringify(help));
	}
	var onCashCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Транс, заказать нал, отказ подтверждения.');
		serviceName = 'confirm';
		var help = {};
		help['confirm'] = 'button.113';
		window.external.exchange.ExecNdcService("confirm", JSON.stringify(help));
	}
	var onCardnum = function(args){
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = parseInt(arg);
			else {
				pKey = args[0];
				help = parseInt(args[1]);
			}
			if(typeof help == 'number')
				cardnum = help;
			else
				cardnum = '';
		}
		else
			cardnum = '';
	}
	var onAmount = function(args){
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = parseFloat(arg);
			else {
				pKey = args[0];
				help = parseFloat(args[1]);
			}
			if(typeof help == 'number')
				amount = help;
			else
				amount = 0;
		}
		else
			amount = 0;
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceName == 'webius_menu'){
			if(m_HostServiceState == 'cancel'){
				alertMsgLog('[SCRIPT] '+scr.name+' from host webius_menu status cancel, return to start');
				scr.cancel();
				return;
			}
			else if(m_HostServiceState == 'ok'){
				alertMsgLog('[SCRIPT] '+scr.name+'. service: '+m_HostServiceName+'; state is ok');
				scr.nextScreen(serviceSelect);
				return;
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host webius_menu unknown state'+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		if(m_HostServiceName == 'transfer'){
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'menu_0075'){
					alertMsgLog('[SCRIPT] '+scr.name+' host transfer choose type');
					scr.nextScreen(transfer, 2);
					return;
				}
				else if(m_HostScreen == 'inpt_0055'){
					alertMsgLog('[SCRIPT] '+scr.name+' host transfer cardnum input');
					scr.nextScreen(transfer, 3);
					return;
				}
				else if(m_HostScreen == 'menu_0016'){
					alertMsgLog('[SCRIPT] '+scr.name+' host transfer currency');
					scr.nextScreen(transfer, 4);
					return;
				}
				else if(m_HostScreen == 'menu_0163'){
					alertMsgLog('[SCRIPT] '+scr.name+' host transfer visa limit');
					scr.nextScreen(transfer, 41);
					return;
				}
				else if(m_HostScreen == 'inpt_0051'){
					alertMsgLog('[SCRIPT] '+scr.name+' host transfer cardnum input');
					scr.nextScreen(transfer, 51);
					return;
				}
				else if(m_HostScreen == 'inpt_0052'){
					alertMsgLog('[SCRIPT] '+scr.name+' host transfer cardnum input');
					scr.nextScreen(transfer, 52);
					return;
				}
				else if(m_HostScreen == 'inpt_0120'){
					alertMsgLog('[SCRIPT] '+scr.name+' host transfer cardnum input');
					scr.nextScreen(transfer, 31);
					return;
				}
				else if(m_HostScreen == 'inpt_0121'){
					alertMsgLog('[SCRIPT] '+scr.name+' host cashcheck amount input');
					scr.nextScreen(transfer, 53);
					return;
				}
				else if(m_HostScreen == 'menu_0122'){
					alertMsgLog('[SCRIPT] '+scr.name+' host cashcheck confirm ');
					scr.nextScreen(transfer, 54);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' service transfer unknown screen '+m_HostScreen+', cancelling.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' service transfer on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else if(m_HostServiceName == 'pin'){
			alertMsgLog('[SCRIPT] '+scr.name+' pin '+m_HostServiceState+', scr: '+m_HostScreen);
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'pin'){
					alertMsgLog('[SCRIPT] '+scr.name+' pin screen '+m_HostScreen);
					scr.nextScreen(transfer, 6);
					return;
				}
				else if(m_HostScreen == 'wait_0023'){
					alertMsgLog('[SCRIPT] '+scr.name+' pin screen '+m_HostScreen);
					scr.nextScreen(transfer, 7);
					return;
				}
				else if(m_HostScreen == 'cont_0045'){
					alertMsgLog('[SCRIPT] '+scr.name+' pin screen '+m_HostScreen);
					scr.nextScreen(transfer, 8);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' pin with unknown screen '+m_HostScreen);
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' service pin on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else if(m_HostServiceName == 'cont_choice'){
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'cont_0045'){
					alertMsgLog('[SCRIPT] '+scr.name+' cont_choice screen '+m_HostScreen);
					scr.nextScreen(transfer, 8);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' cont_choice with unknown screen '+m_HostScreen+', cancelling.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' service cont_choice on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else if(m_HostServiceName == 'end_choice'){
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'wait_0168'){
					alertMsgLog('[SCRIPT] '+scr.name+' end_choice screen '+m_HostScreen);
					scr.nextScreen(transfer, 9);
					return;
				}
				else if(m_HostScreen == 'wait_0025'){
					alertMsgLog('[SCRIPT] '+scr.name+' end_choice screen '+m_HostScreen);
					scr.nextScreen(transfer, 10);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' end_choice with unknown screen '+m_HostScreen+', cancelling.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' service end_choice on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	if(step == 1){
		window.external.exchange.ExecNdcService("transfer", "");
	}
	else if(step == 2){
		alertMsgLog('[SCRIPT] '+scr.name+'. Тип перевода '+m_HostScreen);
		scr.setButton("button1", "Перевести внутри своих счетов", "", onButton1);
		scr.setButton("button2", "Перевести на карту Сбербанка", "", onButton2);
		scr.setButton("button3", "Перевести на карту VISA или MASTERCARD", "", onButton3);
		scr.setButton("button4", "Заказать наличные", "", onCashOnCheck);
		scr.setButton("cancel", "Отмена", "", onCancel);
		
		scr.setLabel("title", "Выберите тип перевода", "");
		scr.render("test");
	}
	else if(step == 3){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ввод номера счета карты '+m_HostScreen);
		
		scr.setInput("cardnum", "", "", "номер карты", "", onCardnum);
		scr.setButton("button1", "Ввод", "", onButton4);
		scr.setButton("button2", "Отмена", "", onButton5);
		scr.setButton("cancel", "Завершить", "", onCancel);
		
		scr.setLabel("title", "Введите номер карты", "");
		scr.render("test");
	}
	else if(step == 31){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ввод номера телефона '+m_HostScreen);
		
		scr.setInput("phonenum", "", "", "номер телефона", "", onPhoneNum);
		scr.setButton("button1", "Продолжить", "", onCashPhone);
		scr.setButton("cancel", "Завершить", "", onCancel);
		
		scr.setLabel("title", "Введите номер телефона", "");
		scr.setLabel("label1", "Код состоит из двух частей. Первая качть кода будет", "");
		scr.setLabel("label2", "распечатана на чеке. Вторая часть будет отправлена", "");
		scr.setLabel("label3", "в виде sms на указанный номер телефона. Получить", "");
		scr.setLabel("label4", "наличные можно только после ввода обеих частей кода.", "");
		scr.render("test");
	}
	else if(step == 4){
		alertMsgLog('[SCRIPT] '+scr.name+'. Выбор валюты '+m_HostScreen);
		
		scr.setButton("button1", "Тенге", "", onButton6);
		scr.setButton("button2", "Доллар", "", onButton7);
		scr.setButton("button3", "Отмена", "", onButton8);
		scr.setButton("cancel", "Завершить", "", onCancel);
		
		scr.setLabel("title", "Выбор валюты перевода", "");
		scr.render("test");
	}
	else if(step == 41){
		alertMsgLog('[SCRIPT] '+scr.name+'. Транс на визу_мастеркард, лимит '+m_HostScreen);
		
		scr.setButton("button1", "Продолжить", "", onLimitYes);
		scr.setButton("button2", "Отмена", "", onLimitNo);
		scr.setButton("cancel", "Завершить", "", onCancel);
		
		scr.setLabel("label1", "Максимальная сумма одной", "");
		scr.setLabel("label2", "транзакции - 2500 долларов США", "");
		scr.setLabel("label3", "(эквивалент в тенге по учетному", "");
		scr.setLabel("label4", "курсу Банка)", "");
		scr.setLabel("label5", "Максимальная сумма перевода в", "");
		scr.setLabel("label6", "сутки - 25 000  долларов США", "");
		scr.setLabel("label7", "(эквивалент в тенге по учетному", "");
		scr.setLabel("label8", "курсу Банка)", "");
		scr.render("test");
	}
	else if(step == 51){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ввод суммы перевода '+m_HostScreen);
		
		scr.setInput("amount", "", "", "сумма в тенге", "", onAmount);
		scr.setButton("button1", "Ввод", "", onButton9);
		scr.setButton("button2", "Отмена", "", onButton10);
		scr.setButton("cancel", "Завершить", "", onCancel);
		
		scr.setLabel("title", "Введите сумму перевода в тенге", "");
		scr.render("test");
	}
	else if(step == 52){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ввод суммы перевода '+m_HostScreen);
		
		scr.setInput("amount", "", "", "сумма в долларах", "", onAmount);
		scr.setButton("button1", "Ввод", "", onButton9);
		scr.setButton("button2", "Отмена", "", onButton10);
		scr.setButton("cancel", "Завершить", "", onCancel);
		
		scr.setLabel("title", "Введите сумму перевода в долларах", "");
		scr.render("test");
	}	
	else if(step == 53){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ввод суммы для заказа '+m_HostScreen);
		
		scr.setInput("amount", "", "", "сумма в тенге", "", onAmount);
		scr.setButton("button1", "Продолжить", "", onCashAmount);
		scr.setButton("cancel", "Завершить", "", onCancel);
		
		scr.setLabel("title", "Введите сумму для снятия", "");
		scr.setLabel("label1", "Пожалуйста, укажите сумму для снятия", "");
		scr.setLabel("label2", "кратную 1000 тенге. Комиссия за снятие наличных", "");
		scr.setLabel("label3", "по коду составит 200 тенге вне зависимости от суммы.", "");
		scr.render("test");
	}	
	else if(step == 54){
		alertMsgLog('[SCRIPT] '+scr.name+'.  Запрос на подтверждение заказа. '+m_HostScreen);
		scr.setButton("button1", "Я согласен", "", onCashConfirm);
		scr.setButton("button2", "Отмена", "", onCashCancel);

		scr.setButton("cancel", "Отмена", "", onCancel);
		
		scr.setLabel("title", "Нажимая кнопку «Я согласен» Вы принимаете", "");
		scr.setLabel("label1", "ответственность за сохранность кода и", "");
		scr.setLabel("label2", "его дальнейшее использование.", "");
		scr.setLabel("label3", "Банк не несет ответственности и не возмещает убытки,", "");
		scr.setLabel("label4", "возникшие по причине несанкционированного доступа", "");
		scr.setLabel("label5", "третьих лиц к коду.", "");
		scr.render("test");
	}
	else if(step == 6){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ввод пин-кода. HostScreen' + m_HostScreen);
		scr.setButton("cancel", "Завершить", "", onButton11);
		scr.setButton("continue", "Продолжить", "", onButton12);
		var pinStr = "    ";
		scr.setLabel("pin", pinStr, "");
		
		scr.setLabel("title", "Ввод пин-кода", "");
		scr.setLabel("note", "Введите пин-код карты", "");
		scr.render("test");
	}
	else if(step == 7){
		scr.setLabel("title", "После завершения всех транзакций не забудьте забрать Вашу карту", "");
		scr.render("test");
	}
	else if(step == 8){
		alertMsgLog('[SCRIPT] '+scr.name+'.  Запрос на продолжение. '+m_HostScreen);
		scr.setButton("button5", "Продолжить", "", onButton13);
		scr.setButton("button6", "Завершить", "", onButton14);

		scr.setButton("cancel", "Отмена", "", onCancel);
		
		scr.setLabel("title", "Выберите услугу", "");
		scr.render("test");
	}
	else if(step == 9){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ожидание завершения. '+m_HostScreen);
		scr.setLabel("title", "Пожалуйста, подождите", "");
		scr.render("test");
	}
	else if(step == 10){
		alertMsgLog('[SCRIPT] '+scr.name+'.  Ожидание завершения по запросу клиента. '+m_HostScreen);
		scr.setLabel("title", "Транзакция прекращена по Вашей инициативе, до свидания", "");
		scr.render("test");
		scr.nextScreen(start);
	}
}
var cashout = function(step) {
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	var onButton1 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача наличных.');
		serviceName = 'cashsum';
		var help = {};
		help['fastcash'] = 'button.116';
		window.external.exchange.ExecNdcService("cashsum", JSON.stringify(help));
	}
	var onButton2 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача наличных.');
		serviceName = 'cashsum';
		var help = {};
		help['fastcash'] = 'button.115';
		window.external.exchange.ExecNdcService("cashsum", JSON.stringify(help));
	}
	var onButton3 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача наличных.');
		serviceName = 'cashsum';
		var help = {};
		help['fastcash'] = 'button.114';
		window.external.exchange.ExecNdcService("cashsum", JSON.stringify(help));
	}
	var onButton4 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача наличных.');
		serviceName = 'cashsum';
		var help = {};
		help['fastcash'] = 'button.113';
		window.external.exchange.ExecNdcService("cashsum", JSON.stringify(help));
	}
	var onButton5 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача наличных.');
		serviceName = 'cashsum';
		var help = {};
		help['fastcash'] = 'button.120';
		window.external.exchange.ExecNdcService("cashsum", JSON.stringify(help));
	}
	var onButton6 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача наличных.');
		serviceName = 'cashsum';
		var help = {};
		help['fastcash'] = 'button.119';
		window.external.exchange.ExecNdcService("cashsum", JSON.stringify(help));
	}
	var onButton7 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача наличных.');
		serviceName = 'cashsum';
		var help = {};
		help['fastcash'] = 'button.118';
		window.external.exchange.ExecNdcService("cashsum", JSON.stringify(help));
	}
	var onButton8 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача наличных.');
		serviceName = 'cashsum';
		var help = {};
		help['fastcash'] = 'button.117';
		window.external.exchange.ExecNdcService("cashsum", JSON.stringify(help));
	}
	var onButton9 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача ввод суммы.');
		//serviceName = 'balance_type';
		var help = {};
			help['amount'] = amount;
		window.external.exchange.ExecNdcService("cashamount", JSON.stringify(help));
	}
	var onButton10 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача отмена ввода суммы.');
		//serviceName = 'balance_type';
		//var help = {};
		//	help['cheque'] = 'button.120';
		//window.external.exchange.ExecNdcService("balance_type", JSON.stringify(help));
	}
	var onButton11 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача наличных печать чека.');
		serviceName = 'cashcheque';
		var help = {};
		help['cheque'] = 'button.120';
		window.external.exchange.ExecNdcService("cashcheque", JSON.stringify(help));
	}
	var onButton12 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Выдача наличных. без печати чека');
		serviceName = 'cashcheque';
		var help = {};
		help['cheque'] = 'button.119';
		window.external.exchange.ExecNdcService("cashcheque", JSON.stringify(help));
	}
	var onButton13 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Завершить на Вводе пин-кода.');
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onButton14 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжить на Вводе вин-кода.');
		window.external.exchange.ExecNdcService("enter", "");
	}
	var onButton15 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Продолжить на HostScreen '+m_HostScreen);
		var help = {};
			help['continue'] = 'button.118';
		window.external.exchange.ExecNdcService("cont_button", JSON.stringify(help));
	}
	var onButton16 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Завершить на HostScreen '+m_HostScreen);
		var help = {};
			help['continue'] = 'button.117';
		window.external.exchange.ExecNdcService("cont_button", JSON.stringify(help));
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onAmount = function(args){
		var pKey = "";
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			var help;
			if(typeof args[1] == 'undefined')
				help = parseFloat(arg);
			else {
				pKey = args[0];
				help = parseFloat(args[1]);
			}
			if(typeof help != 'number')
				amount = help;
			else
				amount = 0;
		}
		else
			amount = 0;
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceName == 'webius_menu'){
			if(m_HostServiceState == 'cancel'){
				alertMsgLog('[SCRIPT] '+scr.name+' from host webius_menu status cancel, return to start');
				scr.cancel();
				return;
			}
			else if(m_HostServiceState == 'ok'){
				alertMsgLog('[SCRIPT] '+scr.name+'. service: '+m_HostServiceName+'; state is ok');
				scr.nextScreen(serviceSelect);
				return;
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host webius_menu unknown state'+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else if(m_HostServiceName == 'cashout'){
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'menu_0018'){
					alertMsgLog('[SCRIPT] '+scr.name+' host cashout choose sum');
					scr.nextScreen(cashout, 2);
					return;
				}
				else if(m_HostScreen == 'inpt_0020'){
					alertMsgLog('[SCRIPT] '+scr.name+' host cashout sum input');
					scr.nextScreen(cashout, 3);
					return;
				}
				else if(m_HostScreen == 'cheq_0022'){
					alertMsgLog('[SCRIPT] '+scr.name+' host cashout cheque');
					scr.nextScreen(cashout, 4);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' service cashout unknown screen '+m_HostScreen+', cancelling.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' service cashout on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else if(m_HostServiceName == 'pin'){
			alertMsgLog('[SCRIPT] '+scr.name+' pin '+m_HostServiceState+', scr: '+m_HostScreen);
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'pin'){
					alertMsgLog('[SCRIPT] '+scr.name+' pin screen '+m_HostScreen);
					scr.nextScreen(cashout, 5);
					return;
				}
				else if(m_HostScreen == 'wait_0071'){
					alertMsgLog('[SCRIPT] '+scr.name+' wait money screen '+m_HostScreen);
					scr.nextScreen(cashout, 6);
					return;
				}
				else if(m_HostScreen == 'wait_0023'){
					alertMsgLog('[SCRIPT] '+scr.name+' pin screen '+m_HostScreen);
					scr.nextScreen(cashout, 7);
					return;
				}
				else if(m_HostScreen == 'cont_0045'){
					alertMsgLog('[SCRIPT] '+scr.name+' pin screen '+m_HostScreen);
					scr.nextScreen(cashout, 8);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' pin with unknown screen '+m_HostScreen);
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' service pin on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else if(m_HostServiceName == 'cont_choice'){
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'cont_0045'){
					alertMsgLog('[SCRIPT] '+scr.name+' cont_choice screen '+m_HostScreen);
					scr.nextScreen(cashout, 8);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' cont_choice with unknown screen '+m_HostScreen+', cancelling.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' service cont_choice on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else if(m_HostServiceName == 'end_choice'){
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'wait_0168'){
					alertMsgLog('[SCRIPT] '+scr.name+' end_choice screen '+m_HostScreen);
					scr.nextScreen(cashout, 9);
					return;
				}
				else if(m_HostScreen == 'wait_0025'){
					alertMsgLog('[SCRIPT] '+scr.name+' end_choice screen '+m_HostScreen);
					scr.nextScreen(cashout, 10);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' end_choice with unknown screen '+m_HostScreen+', cancelling.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' service end_choice on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	if(step == 1){
		window.external.exchange.ExecNdcService("cashout", "");
	}
	else if(step == 2){
		alertMsgLog('[SCRIPT] '+scr.name+'. Выберите сумму выдачи '+m_HostScreen);
		scr.setButton("button1", "100 000", "", onButton1);
		scr.setButton("button2", "75 000", "", onButton2);
		scr.setButton("button3", "50 000", "", onButton3);
		scr.setButton("button4", "30 000", "", onButton4);
		scr.setButton("button5", "20 000", "", onButton5);
		scr.setButton("button6", "10 000", "", onButton6);
		scr.setButton("button7", "5 000", "", onButton7);
		scr.setButton("button8", "Другая сумма", "", onButton8);
		scr.setButton("cancel", "Отмена", "", onCancel);
		
		scr.setLabel("title", "Выберите сумму в тенге", "");
		scr.render("test");
	}
	else if(step == 3){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ввод суммы выдачи '+m_HostScreen);
		
		scr.setInput("amount", "", "", "сумма выдачи", "", onAmount);
		scr.setButton("button1", "Правильно", "", onButton9);
		scr.setButton("button2", "Неправильно", "", onButton10);
		scr.setButton("cancel", "Завершить", "", onCancel);
		
		scr.setLabel("title", "Ввод суммы", "");
		scr.setLabel("note", "Введите сумму в тенге", "");
		scr.render("test");
	}
	else if(step == 4){
		alertMsgLog('[SCRIPT] '+scr.name+'. Печатать чек. HostScreen '+m_HostScreen);
		scr.setButton("button1", "Да", "", onButton11);
		scr.setButton("button2", "Нет", "", onButton12);
		//scr.setButton("button3", "Внесение наличных", "", onButton3);
		scr.setButton("cancel", "Отмена", "", onCancel);
		
		scr.setLabel("title", "Печатать чек?", "");
		scr.render("test");
	}
	else if(step == 5){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ввод пин-кода. HostScreen' + m_HostScreen);
		scr.setButton("cancel", "Завершить", "", onButton13);
		scr.setButton("continue", "Продолжить", "", onButton14);
		var pinStr = "    ";
		scr.setLabel("pin", pinStr, "");
		
		scr.setLabel("title", "Ввод пин-кода", "");
		scr.setLabel("note", "Введите пин-код карты", "");
		scr.render("test");
	}
	else if(step == 6){
		scr.setLabel("title", "Пожалуйста, возьмите деньги", "");
		scr.render("test");
	}
	else if(step == 7){
		scr.setLabel("title", "После завершения всех транзакций не забудьте забрать Вашу карту", "");
		scr.render("test");
	}
	else if(step == 8){
		alertMsgLog('[SCRIPT] '+scr.name+'.  Запрос на продолжение. '+m_HostScreen);
		scr.setButton("button5", "Продолжить", "", onButton15);
		scr.setButton("button6", "Завершить", "", onButton16);

		scr.setButton("cancel", "Отмена", "", onCancel);
		
		scr.setLabel("title", "Выберите услугу", "");
		scr.render("test");
	}
	else if(step == 9){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ожидание завершения. '+m_HostScreen);
		scr.setLabel("title", "Пожалуйста, подождите", "");
		scr.render("test");
	}
	else if(step == 10){
		alertMsgLog('[SCRIPT] '+scr.name+'.  Ожидание завершения по запросу клиента. '+m_HostScreen);
		scr.setLabel("title", "Транзакция прекращена по Вашей инициативе, до свидания", "");
		scr.render("test");
		scr.nextScreen(start);
	}
}

var giveMoney = function(args){
		
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }	
	scr.addOnError(onError);
	var onContinueCall = function(args) {
		var _name, _args;
		if(typeof args != 'undefined' && args.constructor === Array && args.length > 0) {
			_name = args[0];
			if(args.length > 1)
				_args = args[1];
			else
				_args = "";
		}
		else {
			_name = "";
			_args = args;
		}
		alertMsgLog(scr.name+' onContinueCall, value: '+_args);
		
		if(_args == onContinueOptions[1]){
			scr.setModalMessage("", "", -1, false, "", onTimeoutButton);
			window.external.exchange.ExecNdcService("cancel", "");
		}
		else{
			window.external.exchange.ExecNdcService("continue", "");
		}
	}
	var onEmptyButton = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+', onEmptyButton '+name+'.');
	}
	var onTimeout = function(args){
		alertMsgLog(scr.name+' onTimeout');
		window.external.exchange.refreshScr();
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok'){
			alertMsgLog('[SCRIPT] '+scr.name+', Service '+m_HostServiceName+'.');
			if(m_HostServiceName == 'cashout'){
				step  = 0;
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'wait'){
					alertMsgLog('[SCRIPT] '+scr.name+' host cashout choosen');
					//scr.setWait(true, "Секундочку, я считаю...", "{\"icon\": \"\", \"rotate\": true, \"loader\":\"loader\"}");
					//window.external.exchange.refreshScr();
					return;
				}
				else if(m_HostScreen == 'continue'){
					scr.setLabel("modal_text2",'Продолжить работу?', "");
			
					var help = onContinueOptions.join().toString();
					scr.setModalMessage('Операция не разрешена.', onContinueOptions.join(), -1, true, '{"icon": "","options_settings":[{"name":"continue","icon":""},{"name":"logout","icon":"../../graphics/icon-logout.svg"}]}', onContinueCall);
					scr.setWait(false, "Жду ответа...", "");
					window.external.exchange.refreshScr();
				}
				else if(m_HostScreen == 'card_return'){
					scr.setLabel('wait_text2', 'Сначала заберите карту', "");	
					scr.setWait(true, "Успешно.", '{"icon": "../../graphics/icon-pick-card.svg", "rotate":true,"loader":"countdown","count":30}');
					scr.render("cashout_amount");
					//window.external.exchange.refreshScr();
					//scr.render("take_card");
				}
				else if(m_HostScreen == 'money_take'){
					window.external.exchange.scrData.flush();
					//scr.setLabel("wait_text2", "", "");
					//scr.setModalMessage("", "", -1, false, "", onEmptyButton);
					//scr.setWait(true, "Возьмите деньги", '{"icon": "../../graphics/icon-pick-card.svg","rotate":true,"loader":"countdown","count":60}');
					//window.external.exchange.refreshScr();
					//scr.render('cashout_amount');
					scr.setLabel("text", "Возьмите деньги", "");
					scr.setLabel("loader","30", '{"loader":"countdown"}');
					scr.setImage("bg","../../graphics/BG_blur.jpg","");
					scr.render("wait_message");
					m_session.balance = 0;
					return;
				}
				else if(m_HostScreen == 'end_session'){
					alertMsgLog('[Print]: '+m_CheckFlag);
					saveToHistory(amount, m_Currency.getSelectedCode(), m_CheckFlag);
					scr.nextScreen(msgResult, ["Операция завершена...", "end"]);
					return;

					//scr.setWait(true, "Спасибо", "{\"icon\": \"../../graphics/icon-ok.svg\", \"loader\":\"ellipse\",\"theme\":\"blue\"}");
					//window.external.exchange.refreshScr();
					//window.external.exchange.refreshScr();
					//scr.render('cashout_amount');
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' HostScreen '+m_HostScreen+', need to cancel.');
					scr.cancel();
					return;
				}
			}
			else if(m_HostServiceName == 'pin_error'){
				scr.nextScreen(msgResult,["Неверно введенный пин-код", "end"]);
				return;
			}
			else if(m_HostServiceName == 'request_not_made'){
				scr.nextScreen(msgResult,["Ваш запрос не выполнен", "end"]);
				return;
			}
			else if(m_HostServiceName == 'idle'){
				scr.nextScreen(main);
				return;
			}
			else if(m_HostServiceName == 'webius_menu'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'main'){
					if(step != 1)
						scr.nextScreen(serviceSelect);
					else
						alertMsgLog('previous operation finished');
					return;
				}
				else{
					scr.nextScreen(msgResult,["Ошибка выдачи денег", "err"]);
					return;
				}
			}
			else if(m_HostServiceName == 'spec_cancel'){
				alertMsgLog('[SCRIPT] '+scr.name+', HostScreen '+m_HostScreen+'.');
				if(m_HostScreen == 'end_session'){
					scr.nextScreen(start, 1);
					return;
				}
				else{
					scr.nextScreen(msgResult,["Ошибка выдачи денег", "err"]);
					return;
				}
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else{
				scr.nextScreen(msgResult,["Ошибка выдачи денег", "err"]);
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' state is '+m_HostServiceState+', need to cancel.');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	var onContinueOptions = ['Продолжить','Выйти'];
	var amount = 0, step = 0;
	if(typeof args != 'undefined' && args.constructor === Array && args.length > 0){
		amount = args[0];
		if(args.length > 1)
			step = args[1];
	}
	else
		amount = args;
	
	scr.setButton("showremains", "Показать остаток", "{\"icon\": \"\",\"state\":\"\"}", onEmptyButton);
	scr.setButton("print", "", true, false, "{\"icon\": \"\"}", onEmptyButton);
	if(m_session.isCard)
		scr.setButton("logout", "Забрать карту", "{\"icon\": \"\"}", onEmptyButton);
	else
		scr.setButton("logout", "Выйти", "{\"icon\": \"\"}", onEmptyButton);	
	scr.setInput("sum", amount.toString(), "", "0", false, true, "", "text", onEmptyButton, "None");
	scr.setInput("checkbox_print", "true", "", "", true, true, "", "checkbox", onEmptyButton, "", m_CheckFlag);

	scr.setList("currency", m_Currency.getNames(), m_Currency.selected, m_Currency.getJSON(), onEmptyButton);
	
	
	scr.setButton("receipt", "receipt", "{\"icon\": \"../../graphics/icon_check.png\"}", onEmptyButton);
	
	scr.setImage("bg","../../graphics/BG_blur.jpg","");
	//scr.setImage("card","../../graphics/card.svg","");
	scr.setLabel("card","1088", '{"icon": "../../graphics/cards/our-visa.svg", "themes": ["our"]}');
	scr.setImage("error","../../graphics/mes-error.svg","");
	scr.setImage("offer","../../graphics/offer-icon.png","");

	scr.setLabel("title_sum", "Введите сумму:", "");	
	
	scr.setLabel("print_receipt", "Распечатать чек<br>после операции?", '{"display_group": "print_receipt"}');

	var fastCashHelp = '';
	for(var j = 0; j < m_session.fashCash.length; ++j)
		fastCashHelp += (fastCashHelp==''?'{':',{')+'"text":"'+AddSpace(m_session.fashCash[j])+' '+m_Currency.getSelectedSymbol()+'","value":'+m_session.fashCash[j]+'}';
	if(fastCashHelp != '')
		scr.setLabel("title_popular", "", "{\"values\":["+fastCashHelp+"], \"display_group\":\"fast_btns\"}");
	
	scr.setLabel("keyboard", "", '{"values": [{"text": "1","type": "digit"},{"text": "2","type": "digit"},{"text": "3","type": "digit"},{"text": "4","type": "digit"},{"text": "5","type": "digit"},{"text": "6","type": "digit"},{"text": "7","type": "digit"},{"text": "8","type": "digit"},{"text": "9","type": "digit"},{"text": "Размен","type": "bynotes","disabled": true},{"text": "0","type": "digit"},{"text": "Удалить","type": "delete"}],"display_group": "fast_btns"}');
	
	scr.setButton("take", "Снять наличные", "{\"icon\": \"\"}", onEmptyButton);		
	scr.setButton("card_return", "Вернуть карту", "{\"icon\": \"../../graphics/icon_return.png\"}", onEmptyButton);
	//scr.render("cashout_amount");
	
	scr.setWait(true, "Жду ответа...", "{\"icon\": \"\", \"rotate\": true, \"loader\":\"loader\"}");
	window.external.exchange.RefreshScr();
		
	//scr.setWait(true, "Жду ответа...", "{\"icon\": \"\", \"rotate\": true, \"loader\":\"loader\"}");
	//scr.render("money_calculating");
	
	var help = {};
	help['amount'] = amount;
	
	
	//window.external.exchange.refreshScr();
	if(m_CheckFlag)
		window.external.exchange.ExecNdcService("cashoutprint", JSON.stringify(help));
	else
		window.external.exchange.ExecNdcService("cashout", JSON.stringify(help));
}



var spec = function(step) {
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	var onButton1 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. spec установить пин' + name);
		serviceName = 'option';
		var help = {};
		help['option'] = 'button.120';
		window.external.exchange.ExecNdcService("option", JSON.stringify(help));
	}
	var onButton2 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. spec изменить пин' + name);
		serviceName = 'option';
		var help = {};
		help['option'] = 'button.119';
		window.external.exchange.ExecNdcService("option", JSON.stringify(help));
	}
	var onButton3 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. spec печать кодового слова' + name);
		serviceName = 'option';
		var help = {};
		help['option'] = 'button.118';
		window.external.exchange.ExecNdcService("option", JSON.stringify(help));
	}
	var onButton4 = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. spec назад' + name);
		serviceName = 'option';
		var help = {};
		help['option'] = 'button.113';
		window.external.exchange.ExecNdcService("option", JSON.stringify(help));
	}
	var onButton5 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Отмена установки нового пин-кода.');
		var help = {};
		help['option'] = 'button.27';
		window.external.exchange.ExecNdcService(serviceName, JSON.stringify(help));
	}
	var onButton6 = function(name) {
		alertMsgLog('[SCRIPT] '+scr.name+', ' + name + ', Ввод на установке нового пин-кода.');
		var help = {};
		help['option'] = 'button.120';
		window.external.exchange.ExecNdcService(serviceName, JSON.stringify(help));
	}
	var onCancel = function(name){
		alertMsgLog('[SCRIPT] '+scr.name+'. Cancel ' + name);
		serviceName = 'cancel';
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}
	var onService = function(args){
		parseHostAnswer(args);
		if(m_HostServiceName == 'webius_menu'){
			if(m_HostServiceState == 'cancel'){
				alertMsgLog('[SCRIPT] '+scr.name+' from host webius_menu status cancel, return to start');
				scr.cancel();
				return;
			}
			else if(m_HostServiceState == 'ok'){
				alertMsgLog('[SCRIPT] '+scr.name+'. service: '+m_HostServiceName+'; state is ok');
				scr.nextScreen(serviceSelect);
				return;
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' from host webius_menu unknown state'+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else if(m_HostServiceName == 'spec'){
			alertMsgLog('[SCRIPT] '+scr.name+' spec '+m_HostServiceState+', scr: '+m_HostScreen);
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'menu_0080'){
					alertMsgLog('[SCRIPT] '+scr.name+' service spec option '+m_HostScreen);
					scr.nextScreen(spec, 2);
					return;
				}
				else if(m_HostScreen == 'pinc_0082'){
					alertMsgLog('[SCRIPT] '+scr.name+' service spec set pin '+m_HostScreen);
					scr.nextScreen(spec, 3);
					return;
				}
				else if(m_HostScreen == 'pinc_0083'){
					alertMsgLog('[SCRIPT] '+scr.name+' service spec second pin '+m_HostScreen);
					scr.nextScreen(spec, 4);
					return;
				}
				else if(m_HostScreen == 'pinc_0084'){
					alertMsgLog('[SCRIPT] '+scr.name+' service spec second pin '+m_HostScreen);
					scr.nextScreen(spec, 8);
					return;
				}
				else if(m_HostScreen == 'pinc_0085'){
					alertMsgLog('[SCRIPT] '+scr.name+' service spec phone pin '+m_HostScreen);
					scr.nextScreen(spec, 5);
					return;
				}
				else if(m_HostScreen == 'pinc_0086'){
					alertMsgLog('[SCRIPT] '+scr.name+' service spec phone pin '+m_HostScreen);
					scr.nextScreen(spec, 7);
					return;
				}
				else if(m_HostScreen == 'wait_0023'){
					alertMsgLog('[SCRIPT] '+scr.name+' service spec cancel screen '+m_HostScreen);
					scr.nextScreen(spec, 9);
					return;
				}
				else if(m_HostScreen == 'wait_0025'){
					alertMsgLog('[SCRIPT] '+scr.name+' service spec cancel screen '+m_HostScreen);
					scr.nextScreen(spec, 10);
					return;
				}
				else if(m_HostScreen == 'wait_0048'){
					alertMsgLog('[SCRIPT] '+scr.name+' service spec pin err screen '+m_HostScreen);
					scr.nextScreen(spec, 11);
					return;
				}
				else if(m_HostScreen == 'cont_0045'){
					alertMsgLog('[SCRIPT] '+scr.name+' service spec continue screen '+m_HostScreen);
					scr.nextScreen(pin, 5);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' service spec with unknown screen '+m_HostScreen);
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' service spec on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else if(m_HostServiceName == 'pin'){
			alertMsgLog('[SCRIPT] '+scr.name+' pin '+m_HostServiceState+', scr: '+m_HostScreen);
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'pin'){
					alertMsgLog('[SCRIPT] '+scr.name+' pin screen '+m_HostScreen);
					scr.nextScreen(pin, 1);
					return;
				}
				else if(m_HostScreen == 'wait_0071'){
					alertMsgLog('[SCRIPT] '+scr.name+' wait money screen '+m_HostScreen);
					scr.nextScreen(pin, 2);
					return;
				}
				else if(m_HostScreen == 'wait_0031'){
					alertMsgLog('[SCRIPT] '+scr.name+' wait money screen '+m_HostScreen+', screenText: '+m_HostScreenText);
					scr.nextScreen(pin, 3);
					return;
				}
				else if(m_HostScreen == 'wait_0023'){
					alertMsgLog('[SCRIPT] '+scr.name+' pin screen '+m_HostScreen);
					scr.nextScreen(pin, 4);
					return;
				}
				else if(m_HostScreen == 'cont_0045'){
					alertMsgLog('[SCRIPT] '+scr.name+' pin screen '+m_HostScreen);
					scr.nextScreen(pin, 5);
					return;
				}
				else if(m_HostScreen == 'cont_0141'){
					alertMsgLog('[SCRIPT] '+scr.name+' pin screen '+m_HostScreen+', screenText: '+m_HostScreenText);
					scr.nextScreen(pin, 6);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' pin with unknown screen '+m_HostScreen);
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' service pin on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else if(m_HostServiceName == 'cont_choice'){
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'cont_0045'){
					alertMsgLog('[SCRIPT] '+scr.name+' cont_choice screen '+m_HostScreen);
					scr.nextScreen(pin, 5);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' cont_choice with unknown screen '+m_HostScreen+', cancelling.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' service cont_choice on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else if(m_HostServiceName == 'end_choice'){
			if(m_HostServiceState == 'ok'){
				if(m_HostScreen == 'wait_0168'){
					alertMsgLog('[SCRIPT] '+scr.name+' end_choice screen '+m_HostScreen);
					scr.nextScreen(pin, 7);
					return;
				}
				else if(m_HostScreen == 'wait_0025'){
					alertMsgLog('[SCRIPT] '+scr.name+' end_choice screen '+m_HostScreen);
					scr.nextScreen(pin, 8);
					return;
				}
				else{
					alertMsgLog('[SCRIPT] '+scr.name+' end_choice with unknown screen '+m_HostScreen+', cancelling.');
					scr.cancel();
					return;
				}
			}
			else{
				alertMsgLog('[SCRIPT] '+scr.name+' service end_choice on state '+m_HostServiceState+', cancelling.');
				scr.cancel();
				return;
			}
		}
		else {
			alertMsgLog('[SCRIPT] '+scr.name+' from host unexpected service '+m_HostServiceName+', state '+m_HostServiceState+', cancelling');
			scr.cancel();
			return;
		}
	}
	scr.addCall("HostScriptAddon", onService);
	
	if(step == 1){
		window.external.exchange.ExecNdcService("spec", "");
	}
	else if(step == 2){
		alertMsgLog('[SCRIPT] '+scr.name+'. Вид операции. '+m_HostScreen);
		scr.setButton("button1", "Установить PIN-КОД", "", onButton1);
		scr.setButton("button2", "Изменить PIN-КОД", "", onButton2);
		scr.setButton("button3", "Печать кодового слова", "", onButton3);
		scr.setButton("button4", "Назад", "", onButton4);
		scr.setButton("cancel", "Завершить", "", onCancel);
		scr.setLabel("title", "Внесенная сумма", "");
		scr.setLabel("amount", m_HostScreenText, "");
		scr.render("test");
	}
	else if(step == 3){
		alertMsgLog('[SCRIPT] '+scr.name+'. Установить пин-код. HostScreen' + m_HostScreen);
		serviceName = 'newpin';
		scr.setButton("cancel", "Отмена", "", onButton5);
		scr.setButton("continue", "Ввод", "", onButton6);
		var pinStr = "    ";
		scr.setLabel("pin", pinStr, "");
		
		scr.setLabel("lable1", "Установите новый PIN-КОД", "");
		scr.setLabel("lable2", "После набора нажмите «Ввод»", "");
		scr.setLabel("lable3", "на клавиатуре устройства", "");
		scr.setLabel("lable4", "Для отказа от операции", "");
		scr.setLabel("lable5", "Нажмите «Отмена»", "");
		scr.setLabel("lable6", "Установите новый PIN-КОД", "");
		scr.render("test");
	}
	else if(step == 4){
		alertMsgLog('[SCRIPT] '+scr.name+'. Повторно новый пин-код. HostScreen' + m_HostScreen);
		serviceName = 'secondpin';
		scr.setButton("cancel", "Отмена", "", onButton5);
		scr.setButton("continue", "Ввод", "", onButton6);
		var pinStr = "    ";
		scr.setLabel("pin", pinStr, "");
		
		scr.setLabel("lable1", "Повторно введите", "");
		scr.setLabel("lable2", "новый PIN-КОД", "");
		scr.setLabel("lable3", "После набора нажмите «Ввод»", "");
		scr.setLabel("lable4", "на клавиатуре устройства", "");
		scr.setLabel("lable5", "Для отказа от операции", "");
		scr.setLabel("lable6", "Нажмите «Отмена»", "");
		scr.setLabel("lable7", "Подтвердите новый PIN-КОД", "");
		scr.render("test");
	}
	else if(step == 5){
		alertMsgLog('[SCRIPT] '+scr.name+'. Пин-код из sms. HostScreen' + m_HostScreen);
		serviceName = 'phonepin';
		scr.setButton("cancel", "Отмена", "", onButton5);
		scr.setButton("continue", "Ввод", "", onButton6);
		var pinStr = "    ";
		scr.setLabel("pin", pinStr, "");
		
		scr.setLabel("lable1", "Введите PIN-КОД из sms", "");
		scr.setLabel("lable2", "После набора нажмите «Ввод»", "");
		scr.setLabel("lable3", "на клавиатуре устройства", "");
		scr.setLabel("lable4", "Для отказа от операции", "");
		scr.setLabel("lable5", "Нажмите «Отмена»", "");
		scr.setLabel("lable6", "Введите одноразовый PIN-КОД", "");
		scr.render("test");
	}
	else if(step == 6){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ожидание: После завершения всех транзакций не забудьте забрать Вашу карту. '+m_HostScreen);
		scr.setLabel("title", "После завершения всех транзакций не забудьте забрать Вашу карту", "");
		scr.render("test");
	}
	else if(step == 7){
		alertMsgLog('[SCRIPT] '+scr.name+'. Смена пин-кода. HostScreen' + m_HostScreen);
		serviceName = 'changepin';
		scr.setButton("cancel", "Отмена", "", onButton5);
		scr.setButton("continue", "Ввод", "", onButton6);
		var pinStr = "    ";
		scr.setLabel("pin", pinStr, "");
		
		scr.setLabel("lable1", "Обратите внимание", "");
		scr.setLabel("lable2", "За операцию «смена PIN-КОДА»", "");
		scr.setLabel("lable3", "взимается комиссия в размере", "");
		scr.setLabel("lable4", "200 тенге", "");
		scr.setLabel("lable5", "Для защиты Вашего PIN-КОДА", "");
		scr.setLabel("lable6", "при его наборе прикрывайте", "");
		scr.setLabel("lable7", "клавиатуру другой рукой", "");
		scr.render("test");
	}
	else if(step == 8){
		alertMsgLog('[SCRIPT] '+scr.name+'. Пин-коды не совпадают. HostScreen' + m_HostScreen);
		serviceName = 'mismatchpin';
		scr.setButton("cancel", "Отмена", "", onButton5);
		scr.setButton("continue", "Ввод", "", onButton6);
		var pinStr = "    ";
		scr.setLabel("pin", pinStr, "");
		
		scr.setLabel("lable1", "PIN-КОДЫ не совпадают.", "");
		scr.setLabel("lable2", "Пожалуйста, повторите", "");
		scr.setLabel("lable3", "ввод или нажмите", "");
		scr.setLabel("lable5", "«Отмена» на клавиатуре", "");
		scr.setLabel("lable6", "Введите новый PIN-КОД", "");
		scr.render("test");
	}
	else if(step == 9){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ожидание: После завершения всех транзакций не забудьте забрать Вашу карту. '+m_HostScreen);
		scr.setLabel("title", "После завершения всех транзакций не забудьте забрать Вашу карту", "");
		scr.render("test");
	}
	else if(step == 10){
		alertMsgLog('[SCRIPT] '+scr.name+'.  Ожидание завершения по запросу клиента. '+m_HostScreen);
		scr.setLabel("title", "Транзакция прекращена по Вашей инициативе, до свидания", "");
		scr.render("test");
		scr.nextScreen(start);
	}
	else if(step == 11){
		alertMsgLog('[SCRIPT] '+scr.name+'. Ожидание: Вы превысили число попыток набора пин. '+m_HostScreen);
		scr.setLabel("label1", "Вы превысили число попыток набора пин", "");
		scr.setLabel("label2", "Обратитесь, пожалуйста, в Ваш банк.", "");
		scr.render("test");
	}
}
var msg_err = function(someArgs) {
	var onError =  function () {
        scr.cancel(); 
    }
	var onCancel = function() {
		alertMsgLog(scr.name+' onCancel');
		scr.cancel();
	}
	
	var msg = '';
	if(typeof someArgs != 'undefined') {
		if(someArgs.constructor === Array && someArgs.length > 0)
			msg = someArgs[0];
		else
			msg = someArgs;
	}
	else
		msg = 'Зафиксирована непредвиденная ошибка! Попробуйте провести операцию еще раз.';
	
	scr.addOnError(onError);
	scr.setLabel('message', msg, '');
	scr.setButton('cancel', 'Завершить', '', onCancel);
	scr.render('test');
}

var msgResult = function(args){
	var onError =  function () {
        scr.nextScreen(msg_err); 
    }
	scr.addOnError(onError);
	
	var onButton1 = function(name){
		scr.nextScreen(serviceSelect);
	}		
	
	var onCancel = function(name){
		window.external.exchange.ExecNdcService("cancel", "");
		scr.cancel();
	}	
	var onService = function(args) {
		parseHostAnswer(args);
		if(m_HostServiceState == 'ok') {
			if(m_HostServiceName == "idle"){
				if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					//scr.nextScreen();
					//scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else if(m_HostScreen == 'pin_other'){
					m_session.ownCard = false;
					return;
				}
				else 
					scr.cancel();
				return;
			}
			else if(m_HostServiceName == "before_pin"){
				if(m_HostScreen == "main"){
					//scr.nextScreen(pin);
					//alertMsgLog('PAN: '+m_HostPAN);
					if(m_session.ownCard)
						window.external.exchange.ExecNdcService("pin","");
					else
						window.external.exchange.ExecNdcService("pin_other","");
					m_CardIcon = getPaySystem(m_HostPAN);
					//alert("ACHTUNG");
					getHistory(m_HostPAN);
					//m_OpenOwnCard = isOpenCard(m_HostPAN);
					return;
				}
				else if(m_HostScreen == "pin"){
					scr.nextScreen(pin);
					return;
				}
				else if(m_HostScreen == ''){
					alertMsgLog('[SCRIPT] '+scr.name+' onService HostScreen is undefined');
					scr.nextScreen(main);
					return;
				}
				else if(m_HostScreen == 'wait'){
					//scr.nextScreen(wait);
					scr.nextScreen(msgResult, ['','wait']);
					return;
				}
				else
					scr.cancel();
			}
			else if(m_HostServiceName == "webius_menu"){
				if(m_HostScreen == 'main'){
					scr.nextScreen(serviceSelect);
					return;
				}
				else{
					window.external.exchange.ExecNdcService("cancel","");
					return;
				}
			}
			else if(m_HostServiceName == 'card_return'){
				scr.nextScreen(msgResult,["","card_return"]);
				return;
			}
			else if(m_HostServiceName == "cancel"){
				if(m_HostScreen == 'card_captured'){
					scr.nextScreen(msgResult,['Ваша карта задержана', 'err']);
					return;
				}
				else
					scr.cancel();
			}
			else if(m_HostServiceName == "oos")
				scr.nextScreen(oos);
			else
				scr.cancel();
		}
		else
			scr.cancel();
		return;
	}
	scr.addCall("HostScriptAddon", onService);
	
	var message, type;
	if(typeof args != 'undefined' && args.constructor === Array && args.length > 0){
		message = args[0];
		if(args.length > 1)
			type = args[1];
	}
	else
		message = args;
	
	if(typeof type == 'undefined'){
		scr.setLabel("text", message, "");	
		scr.setLabel("loader","60", '{"loader":"loader"}');
		//scr.setImage("smile","../../graphics/icon-loader.svg","");
		scr.setImage("bg","../../graphics/BG_blur.jpg","");
		scr.render("wait_message");
	}
	else{
		if(type == 'end'){
			//scr.setLabel("warning", "Пожалуйста, подождите...", "");	
			scr.setLabel("text", message, "");	
			scr.setLabel("loader","60", '{"loader":"loader"}');
			scr.setImage("bg","../../graphics/BG_blur.jpg","");
			//scr.setImage("smile","../../graphics/icon-loader.svg","");
			scr.render("wait_message");
		}
		else if(type == 'card_return'){
			scr.setLabel("text", "заберите карту...", "");	
			scr.setLabel("loader","30", '{"loader":"countdown"}');
			scr.setImage("bg","../../graphics/BG_blur.jpg","");
			//scr.setImage("smile","../../graphics/icon-loader.svg","");
			scr.render("wait_message");
		}
		else if(type == 'card_read'){
			scr.setLabel("text", "Считываю карту...", "");	
			scr.setLabel("loader","60", '{"loader":"loader"}');
			scr.setImage("bg","../../graphics/BG_blur.jpg","");
			//scr.setImage("smile","../../graphics/icon-loader.svg","");
			scr.render("wait_message");
		}
		else if(type == 'wait'){
			if(message == '')
				scr.setLabel("text", "Пожалуйста, подождите...", "");
			else
				scr.setLabel("text", message, "");
			//scr.setImage("smile","../../graphics/icon-loader.svg","");
			scr.setImage("bg","../../graphics/BG_blur.jpg","");
			scr.setLabel("loader","60", '{"loader":"loader"}');
			
			//text: 'Секундочку, я считаю...',
			//ext:
			//{
			//	icon: '',
			//	rotate: true,
			//	loader: 'loader',
			//}
			
			scr.render("wait_message");
		}
		else if(type == 'err'){
			scr.setLabel("text", message, "");
			scr.setImage("bg","../../graphics/BG_blur.jpg","");
			//scr.setImage("smile","../../graphics/icon-smile-2.svg","");
			scr.setLabel("loader","60", '{"loader":"ellipse", "icon":"../../graphics/icon-smile-2.svg"}');
			scr.render("wait_message");
		}
		else{
			scr.cancel();
		}
	}
}

function initScreens() {
	scr = new Screen(main,"");	
	start = new Screen(start, "start");
	main = new Screen(main,"main");
	language = new Screen(language,"language");
	oos = new Screen(oos, "oos");
	
	serviceSelect = new Screen(serviceSelect, "serviceSelect");
	wait = new Screen(wait, "wait");
	errorWait = new Screen(errorWait, "errorWait");
	balanceScr = new Screen(balanceScr, "balanceScr");	
	balanceService = new Screen(balanceService, "balanceService");
	historyCheque = new Screen(historyCheque, "historyCheque");
	specCancelWait = new Screen(specCancelWait, "specCancelWait");
	transfer = new Screen(transfer, "transfer");
	cashin = new Screen(cashin, "cashin");
	cashout = new Screen(cashout, "cashout");
	spec = new Screen(spec, "spec");
	pin = new Screen(pin, "pin");
	fakePin = new Screen(fakePin, "fakePin");
	msg_err = new Screen(msg_err, "msg_err");
	msgResult = new Screen(msgResult,"msgResult");
	requestResult = new Screen(requestResult,"requestResult");
	depositSelectAdjunctionFrom = new Screen(depositSelectAdjunctionFrom,"depositSelectAdjunctionFrom");
	depositSelectAdjunctionCurrency = new Screen(depositSelectAdjunctionCurrency,"depositSelectAdjunctionCurrency");
	depositSelectAdjunctionMyCard = new Screen(depositSelectAdjunctionMyCard,"selectAdjunctionCard");
	depositSelectAdjunctionAnotherCard = new Screen(depositSelectAdjunctionAnotherCard,"depositSelectAdjunctionAnotherCard");
	helpMenu = new Screen(helpMenu,"helpMenu");
	settingsMenu = new Screen(settingsMenu,"settingsMenu");
	settingsChangePin = new Screen(settingsChangePin,"settingsMenu");
	settingsAddSIM = new Screen(settingsAddSIM,"settingsMenu");
	settingsSecure3D = new Screen(settingsSMSInfo,"settingsMenu");
	settingsSMSInfo = new Screen(settingsSMSInfo,"settingsMenu");
	settingsInternetBank = new Screen(settingsInternetBank,"settingsInternetBank");
	settingsCardRequisites = new Screen(settingsCardRequisites,"settingsCardRequisites");
	settingsCardLimits = new Screen(settingsCardLimits,"settingsCardLimits");
	transferMenu = new Screen(transferMenu,"transferMenu");
	transferChooseIdType = new Screen(transferChooseIdType,"transferChooseIdType");
	transferToCard = new Screen(transferToCard,"transferToCard");
	transferToSchet = new Screen(transferToSchet,"transferToSchet");
	transferToCompany = new Screen(transferToCompany,"transferToCompany");
	transferCardInput = new Screen(transferCardInput,"transferCardInput");
	transferRequisitesInput = new Screen(transferRequisitesInput,"transferRequisitesInput");
	transferInputAmount = new Screen(transferInputAmount,"transferInputAmount");
	transferSend = new Screen(transferSend,"transferSend");
	transferToCardRecipient = new Screen(transferToCardRecipient,"transferToCardRecipient");
	cashoutInputAmount = new Screen(cashoutInputAmount,"cashoutInputAmount");
	giveMoney = new Screen(giveMoney,"giveMoney");
	
	ekassir = new Screen(ekassir,"ekassir");
}


function AddSpace(a)
{
	a = a.toString();
	var res = "";
	if(a.length <= 3)
		return a;
	var startIndex = a.length ;
	var subInd = a.indexOf(',');
	if(subInd == -1)
		subInd = a.indexOf('.');
	if(subInd > -1){
		startIndex = subInd;
		res = a.substr(subInd);
	}
	var step = 3;
	while(startIndex - step > 0){
		startIndex-=step;
	        res = a.substr(startIndex,step) + (res == ''?'':(' ' + res));
	}
	res =  a.substr(0,startIndex)+ (res == ''?'':(' ' + res));
	alertMsgLog("AddSapce: "+res);
	return res;
}