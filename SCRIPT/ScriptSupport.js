function CurrencyClass(currList) {
	this.values = [];
	this.symbols = [];
	this.maxsum = [];
	this.enable = [];
	this.text = [];
	this.code = [];
	this.length = 0;
	this.selected = 0;

	this.textOnCode = function(_code){
		if(_code == 840)
			return "Долл.";
		else if(_code == 978)
			return "Евро";
		else
			return "Руб.";
	}
	this.setSelected = function(name){
		for(var i=0;i<this.length;++i)
			if(this.text[i]==name || this.values[i]==name || this.code[i]==name){
				this.selected=i;
			}
	}
	this.getSelectedCode = function(){
		if(this.selected < this.code.length)
			return this.code[this.selected];
		else if(this.code.length > 0)
			return this.code[0];
		else
			return 643;
	}
	this.getSelectedSymbol = function(){
		if(this.selected < this.symbols.length)
			return this.symbols[this.selected];
		else if(this.symbols.length > 0)
			return this.symbols[0];
		else
			return "₽";
	}
	this.exists = function(name){
		for(var i=0;i<this.length;++i)
			if(this.values[i]==name || this.code[i]==name)
				return true;
		return false;
	}
	this.getJSON = function(){
		var symb = '', val = '', sum = '', enab = '';
		for(var i=0;i<this.length;++i){
			symb+=(symb==''?'':',')+'\"'+this.symbols[i]+'\"';
			val+=(val==''?'':',')+'\"'+this.values[i]+'\"';
			sum+=(sum==''?'':',')+'\"'+this.maxsum[i]+'\"';
			enab+=(enab==''?'':',')+this.enable[i];
		}
		return '{ \"display_group\": \"currency-list\",'+
		  '\"symbols\": ['+symb+'],'+
		  ' \"values\": ['+val+'],'+
		  '\"maxsum\": ['+sum+'],'+
		  '"enabled": ['+enab+']'+
		  '}';
	}
	this.getNames = function(){
		var symb = '';
		for(var i=0;i<this.length;++i)
			symb+=(symb==''?'':',')+this.text[i];
		return symb;
	}
	this.addCurrency = function(type){
		if(type == 'rub' || type == '643'){
			this['symbols'][this.length] = '₽';
			this['values'][this.length] = 'rub';
			this['maxsum'][this.length] = '100000';
			this['text'][this.length] = 'рубли';
			this['code'][this.length] = 643;
			this['enable'][this.length] = true;
			this.length ++;
		}
		else if(type == 'euro' || type == '978'){
			this['symbols'][this.length] = '€';
			this['values'][this.length] = 'euro';
			this['maxsum'][this.length] = '1500';
			this['text'][this.length] = 'евро';
			this['code'][this.length] = 978;
			this['enable'][this.length] = true;
			this.length ++;
		}
		else if(type == 'dollar' || type == '840'){
			this['symbols'][this.length] = '$';
			this['values'][this.length] = 'dollar';
			this['maxsum'][this.length] = '1500';
			this['text'][this.length] = 'доллары';
			this['code'][this.length] = 840;
			this['enable'][this.length] = true;
			this.length ++;
		}
	}

	var currHelp = [];
	if(typeof currList == 'string')
		currHelp = currList.split(',');
	else if(typeof currList != 'undefined' && currList.constructor === Array)
		currHelp = currList;
	for(var i = 0; i < currHelp.length; ++i)
		this.addCurrency(currHelp[i]);
}

function FastCash(moneyInATM, personalHistory, balance_info, client_info) {
	// --- FastCash Init BEG ---
	// Парсинг инфы по загрузке кассет
	this.atmMoney = JSON.parse(moneyInATM);
	
	// Парсинг истории операций
	function oper(amount,currency,count)
	{
		this.amount   = amount;
		this.currency = currency;
		this.count    = count;
	}
	this.cashoutHistory = [];
	
	this.setPersonalHistory = function(personalHistory)
	{
		function compareOper(a, b) 
		{
			return b.count-a.count;
		}
		if(personalHistory!=null)
		{
			//alert(personalHistory);
			this.history = JSON.parse(personalHistory);
			for(var i=0; i<this.history.operations.length; i++)
			{
				if(this.history.operations[i].type =="cashout")
				{
					this.cashoutHistory.push(new oper(this.history.operations[i].amount,
									  this.history.operations[i].currency,
									  this.history.operations[i].count));
				}
			}
			this.cashoutHistory.sort(compareOper);
		}
		else this.history = null;		
	}
	this.setPersonalHistory(personalHistory);
	
	this.button = [10];
	
	// Устанавливаем Баланс (0, если нет)
	this.balance = balance_info;
	
	// Признак "своя" / "чужая" карта
	this.isNativeClient = client_info;
	
	// Кол-во совпадающих знаков с балансом
	this.minEQdigits = 2;
	
	// Установка минимальных/максимальных опций fastCash
	this.minFastCash = {};
	this.maxFastCash = {};
	if(this.isNativeClient) // Настройки для своей карты
	{
		this.minFastCash[643] = 100;
		this.maxFastCash[643] = 100000;
		this.minFastCash[810] = 100;
		this.maxFastCash[810] = 100000;
		this.minFastCash[978] = 10;
		this.maxFastCash[978] = 1500;
		this.minFastCash[840] = 10;
		this.maxFastCash[840] = 1500;
	}
	else // Настройки по "чужой" карте
	{
		this.minFastCash[643] = 200;
		this.maxFastCash[643] = 100000;
		this.minFastCash[810] = 200;
		this.maxFastCash[810] = 100000;
		this.minFastCash[978] = 50;
		this.maxFastCash[978] = 1500;
		this.minFastCash[840] = 50;
		this.maxFastCash[840] = 1500;
	}
	{//MaxAmount on moneyInATM
		var money = this.atmMoney;
		if(typeof money != 'undefined' || money.length > 0){
			var amntMaxAtm = {};
			amntMaxAtm[643] = 0;
			amntMaxAtm[810] = 0;
			amntMaxAtm[840] = 0;
			amntMaxAtm[978] = 0;
			//money[i].currency==_currency&&money[i].count>0&&money[i].denomination < minNom
			//alert(money.length);
			for(var i = 0; i < money.length; ++i){
				//alert(money[i]);
				amntMaxAtm[money[i].currency] += parseInt(money[i].count)*parseInt(money[i].denomination);
			}
			if(amntMaxAtm[643] != 0)
				this.maxFastCash[643] = amntMaxAtm[643];
			else if(amntMaxAtm[810] != 0)
				this.maxFastCash[810] = amntMaxAtm[810];
			else if(amntMaxAtm[840] != 0)
				this.maxFastCash[840] = amntMaxAtm[840];
			else if(amntMaxAtm[978] != 0)
				this.maxFastCash[978] = amntMaxAtm[978];
		}
	}
	// Коррекция максимального порога в соответствии с балансом
	if(this.balance > 0 && this.balance < this.maxFastCash[643])
	{
		//only for rubles
		this.maxFastCash[643] = this.balance;
	}
	
	// Параметр определяющий по сколько листов брать из кассет
	// при генерации Опций FastCash
	this.defGeneratorNumbers = [1,5,10];
	// --- FastCash Init END ---
	
	this.setBalance = function(balance)
	{
		this.balance = balance;
		if(this.balance > 0 && this.balance < this.maxFastCash[643])
		{
			this.maxFastCash[643] = this.balance;
		}
	}
	
	this.getCurrencyList = function()
	{
		var money = this.atmMoney;
		var currencyList = [];
		for (var i = 0; i < money.length; i++)
		{
			var new_cur = true;
			for (var j = 0; j < currencyList.length; j++)
			{
				if(money[i].currency == currencyList[j])
				{
					new_cur = false;
					break;
				}
			}
			if(new_cur)
				currencyList.push(money[i].currency);
		}
		return currencyList;
	}
	
	this.getDefault = function(currency)
	{
		return this.getDefOptions(currency, this.defGeneratorNumbers);
	}
	
	this.getMinNominal = function(_currency){
		var money = this.atmMoney;
		var minNom = 10000;
		for (var i = 0; i < money.length; i++) {
			if(money[i].currency==_currency&&money[i].count>0&&money[i].denomination < minNom)
				minNom = money[i].denomination;
		}
		return minNom;
	}
	this.getDefOptions = function(currency, bancnote_count)
	{
		function compareNumeric(a, b) 
		{
			return a - b;
		}
		
		var money = this.atmMoney;
		var optionList = [];
		
		
		for (var c = 0; c < bancnote_count.length; c++) 
		for (var i = 0; i < money.length; i++) 
		{
			if(money[i].currency==currency&&money[i].count>=bancnote_count[c]) 
			{
				var x=money[i].denomination*bancnote_count[c];
				if(x>=this.minFastCash[currency]&&x<=this.maxFastCash[currency])
				{
					var new_opt = true;
					for (var j = 0; j < optionList.length; j++) 
					{
						if(money[i].denomination*bancnote_count[c] == optionList[j])
						{
							new_opt = false;
							break;
						}
					}
					if(new_opt)
						optionList.push(money[i].denomination*bancnote_count[c]); // ????????? ??????? ????????
				}
			}
		}
		return optionList.sort(compareNumeric);
	}
	
	this.validAmount = function(currency, amount)
	{
		if(amount == 0)
			return false;
		if(typeof m_CheckSum != 'undefined'){
			var helpInt = m_CheckSum.isAmountExist(amount, currency, false);
			if(helpInt == 1)
				return true;
			else
				return false;
		}
		// Тут надо вставить вызов нормальной проверки возможности выдачи произвольной суммы
		var denList = this.getDefOptions(currency,[1]);
		var x;
		var minNom = this.getMinNominal(currency);
		//if(currency == 643 || currency == 810)
		//	//x = amount % 100; // кратность меньшему номиналу
		//	x = amount % minNom; // кратность меньшему номиналу
		//else
		//	//x = amount % 10;
		//	x = amount % minNom;
		x = amount % minNom;
		
		if(x==0 && amount<=this.maxFastCash[currency])
			return true; // типа, можно выдать
		else 
			return false; //типа, нельзя
	}
	
	this.roundBalance = function(currency,balance,inputAmount,minEQdigits){
		function compareNumeric(a, b){
			return b-a;
		}
		var len = inputAmount.toString().length;
		var txt = balance.toString().substring(0,len);
		var optionList = [];
		
		if(txt == inputAmount && len>=minEQdigits){
			var denList = this.getDefOptions(currency,[1]);
			for (var i = 0; i < denList.length; i++)
			{
				var bal = balance-(balance % denList[i]);
				var new_opt = true;
				for (var j = 0; j < optionList.length; j++) // ???????? ?? ???????
				{
					if(bal == optionList[j])
					{
						new_opt = false;
						break;
					}
				}
				if(new_opt)
				{
					var txt = bal.toString().substring(0,len);
					if(txt == inputAmount)
					{
						if(this.validAmount(currency,bal)) // можно выдать
							optionList.push(bal); // Добавляем хороший вариант
					}
				}
			}
			return optionList.sort(compareNumeric);
		}
		else{
			return [];
		}
		
	}

	this.getDynOptions = function(currency,inputAmount, _dst, _maxDestLen, allOptions) {
		var optionList = [];
		
		if(inputAmount==0)
		//if(inputAmount<10)
		{
			for(var i = 0; i <10; i++)
				this.button[i] = true;
			return [];
		}
		var log="";
		
		for(var i = 0; i <10; i++)
			this.button[i] = false;
		if(this.balance > 0 && this.balance<this.maxFastCash[currency])
			var MAX = this.balance;
		else var MAX = this.maxFastCash[currency];
		
		var y = inputAmount * 10
		for(var x = 100; x <= MAX; x+=100)
		{
			var amt = inputAmount.toString();
			var sum = x.toString();
				
			
			if(sum.substr(0,amt.length)==amt)
			{
				//очень долго считает при рельной функции набора суммы имеющимися купюрами
				//if(this.validAmount(currency,x))
				{
					if(x<=MAX)
						optionList.push(x);
					// Добавим доступные кнопки
					//var ind = parseInt(sum.substr(amt.length,1));
					//{
					//	this.button[ind] = true;
					//}
				}
			}
		}
		//addNewByStepV2(_dst, optionList, _maxDestLen, this.button, this.validAmount);
		//addNewByStepV2(dst, src, maxDestLen, buttons, validAmountFunc)
		{
			if(_maxDestLen>0&&_dst.length>=_maxDestLen)
				return;
			var step =  Math.floor(optionList.length/(_maxDestLen - _dst.length));
			//alert('step='+step+';op.len='+optionList.length+';maxlen='+_maxDestLen+';res.len='+_dst.length);
			if(step==0)
				step=1;
			
			var amt = inputAmount.toString();
			var sum = '';
			
			for(var k=0; k < step; k++)
			{
				for(var j=0; j < optionList.length; j+=step)
				{
					var isNew=true;
					for(var i=0; i < _dst.length; i++)
					{
						if(optionList[j+k]==_dst[i])
						{
							isNew=false;
							break;
						}
					}	
					if(isNew && this.validAmount(currency,optionList[j+k])){
						_dst.push(optionList[j+k]);
						// Добавим доступные кнопки
						sum = (optionList[j+k]).toString();
						var ind = parseInt(sum.substr(amt.length,1));
						this.button[ind] = true;
					}
					if(_maxDestLen>0&&_dst.length>=_maxDestLen)
						break;
				}
				if(_maxDestLen>0&&_dst.length>=_maxDestLen)
						break;
			}
			// Проверим доступность  всех кнопок
			//var minNom = getMinNominal(currency);
			var sumToCheck = parseInt(amt+'0');
			if(this.validAmount(currency,sumToCheck)){
				this.button[0] = true;
			}
			for(var ind = 0; ind < 10; ++ind){
				if(!(this.button[ind])){
					sumToCheck = parseInt(amt+(ind.toString())+'0');
					if(this.validAmount(currency,sumToCheck)){
						this.button[ind] = true;
						continue;
					}
					sumToCheck = sumToCheck*10;
					if(this.validAmount(currency,sumToCheck)){
						this.button[ind] = true;
						continue;
					}
				}
			}
		}
		//alert("buttons: "+this.button.join());
		//return optionList;
		for(var i=0;i<optionList.length;++i)
			allOptions[i] = optionList[i];
		return _dst;
		
		/*
		//old
		for (var n = 100; n * inputAmount < this.maxFastCash; n=n*10) 
		{
			var sum = inputAmount * n ;
			if(sum < this.maxFastCash && 
				this.validAmount(currency,sum))
			{
				optionList.push(sum);
			}
			for(var m=1;m<10;m+=2)
			{
				var sum = inputAmount * n + (m*n/10);
				if(sum < this.maxFastCash && 
					this.validAmount(currency,sum))
				{
					optionList.push(sum);
				}
			}
		}*/
		//return optionList;				
		return _dst;				
	}
	
	this.getHistoryOptions = function(currency,inputAmount)
	{
		var operList = [];
		
		// Фильтруем историю выдачи
		// Оставляем только то, что:
		// 1. Можно выдать здесь и сейчас в заданой валюте
		// 2. Подходит под начатый набор суммы(если inputAmount не ноль)
		
		
					
		for(var i=0; i < this.cashoutHistory.length;i++)
		{
			/*
			alert(this.cashoutHistory[i].amount+","+
			this.cashoutHistory[i].currency+","+
			this.cashoutHistory[i].count);
			*/
			if(this.cashoutHistory[i].currency==currency)
			{
				// Если ввод суммы не начат - берем все валидные
				if(inputAmount==0) 
				{
					if(this.validAmount(currency,this.cashoutHistory[i].amount))
					{
						operList.push(this.cashoutHistory[i].amount);
					}
				} else
				{
					// Проверяем, что сумма правильно начинается
					

					var amt = inputAmount.toString();
					var sum = this.cashoutHistory[i].amount.toString();
					
					if(sum.substr(0,amt.length)==amt)
					{
						if(this.validAmount(currency,this.cashoutHistory[i].amount))
						{
							operList.push(this.cashoutHistory[i].amount);
						}
					} 
					
					
					
				}
			}
		}
		//alert(operList);
		return operList;
		
	}

	this.getGeneral = function(currency,inputAmount)
	{
		function compareNative(a, b) 
		{

				return a - b;
		
		}
		function compareOther(a, b) 
		{

				return b-a;
		
		}
		function addNew(dst, src, maxDestLen)
		{
			for(var j=0; j < src.length; j++)
			{
				var isNew=true;
				for(var i=0; i < dst.length; i++)
				{
					if(src[j]==dst[i])
					{
						isNew=false;
						break;
					}
				}	
				if(isNew)
					dst.push(src[j]);
				if(maxDestLen>0&&dst.length>=maxDestLen)
					break;
			}
		}
		function addNewByStep(dst, src, maxDestLen)
		{
			var step =  Math.floor(src.length/(maxDestLen - dst.length));
			
			if(step==0)
				step=1;
			for(var k=0; k < step; k++)
			{
				for(var j=0; j < src.length; j+=step)
				{
					var isNew=true;
					for(var i=0; i < dst.length; i++)
					{
						if(src[j+k]==dst[i])
						{
							isNew=false;
							break;
						}
					}	
					if(isNew)
						dst.push(src[j+k]);
					if(maxDestLen>0&&dst.length>=maxDestLen)
						break;
				}
				if(maxDestLen>0&&dst.length>=maxDestLen)
						break;
			}
		}
		
		
		var output="Результат генерации опций:\n\n";
		var start;
		var time;
		var generalList = [];
		var result_1, result_2, result_3, result_4;
		var buttNeedCount = 8;
		
		// Опции снятия баланса
		start = performance.now();
		result_1 = this.roundBalance(currency,this.balance,inputAmount,this.minEQdigits);
		if(result_1.length>0){
			addNew(generalList, result_1,1);
		}
		time = performance.now() - start;
		output+= "Баланс: \n["+result_1.join() + "]\n("+time+" msec)\n";
		
		// Опции для снятия из истории
		start = performance.now();
		var result_2 = this.getHistoryOptions(currency,inputAmount);
		if(result_2.length>0){
			addNew(generalList, result_2,buttNeedCount);
		}
		time = performance.now() - start;
		output+= "История: \n["+result_2.join() + "]\n("+time+" msec)\n";
	
		
		// Предиктивные опции
		if(generalList.length < buttNeedCount){
			start = performance.now();
			var result_3 = [];
			//var result_3 = this.getDynOptions(currency,inputAmount,buttNeedCount);
			generalList = this.getDynOptions(currency,inputAmount, generalList, buttNeedCount, result_3);
			time = performance.now() - start;
			output+= "Предиктивные: \n["+result_3.join() + "]\n("+time+" msec)\n";
		}
		
		// Опции на основе загрузки кассет
		if(generalList.length < buttNeedCount){
			start = performance.now();
			result_4 = this.getDefault(currency);
			if(inputAmount>=0&&generalList.length<buttNeedCount && result_4.length>0){
				addNewByStep(generalList, result_4,buttNeedCount);
			}
			time = performance.now() - start;
			output+= "Базовые: \n["+result_4.join() + "]\n("+time+" msec)\n";
		}
		
		if(this.isNativeClient)
			output+= "\nИтоговые: \n["+generalList.sort(compareNative).join() + "]\n";
		else
			output+= "\nИтоговые: \n["+generalList.sort(compareOther).join() + "]\n";
			
	// На банкомате перенаправить в лог?
		alertMsgLog('[FastCash.getGeneral]:\r\n'+output);
		if(this.isNativeClient) {
			generalList.sort(compareNative);
			generalList.splice(6, 0, 0);
			return generalList;
		}
		else {
			generalList.sort(compareOther);
			generalList.splice(2, 0, 0);
			return generalList;
		}
	}
}


Date.prototype.mmddyy = function () {
	var mm = this.getMonth() + 1; // getMonth() is zero-based
	var dd = this.getDate();
	var yy = this.getFullYear().toString().slice(-2);
	return [(mm > 9 ? '' : '0') + mm, (dd > 9 ? '' : '0') + dd, yy].join('.');
};
Date.prototype.hhmmss = function () {
	var hh = this.getHours();
	var mm = this.getMinutes();
	var ss = this.getSeconds();
	return [(hh > 9 ? '' : '0') + hh, (mm > 9 ? '' : '0') + mm, (ss > 9 ? '' : '0') + ss].join(':');
};

function checkText(type, args){
	var checkRes = ' \r\n \r\n \r\n';
	var today = new Date();
	//today.mmddyyyy().toString();
	if(type == 'balance'){
		//args[0] -> *1111
		//args[1] -> 12 458
		//args[2] -> Руб.
		checkRes += '  ДАТА:  '+today.mmddyy().toString()+'  '+today.hhmmss()
		+'\r\n  БАНКОМАТ N.: 0077903\r\n  ОПЕРАЦИЯ N: 813019235568\r\n  КАРТА: '+
		(typeof args[0] != 'undefined' ? args[0] : '')+
		'\r\n \r\n  ДОСТУПНЫЙ ОСТАТОК: '+(typeof args[1] != 'undefined' ? args[1] : '')+' '+
		(typeof args[2] != 'undefined' ? args[2] : '')+'\r\n  СПАСИБО!\r\n \r\n'+
		'[ESC:G]C:/scs/atm_h/TempReceipt/otk_logo.bmp';

	}
	return checkRes;
}

function SessionVariables(){
	this.fastCash = [];
	this.balance = 0;
	this.MaxPinLength = 4;
	this.isCard = true;
	this.ownCard = true;
	this.lang = 'ru';
	this.serviceName = '';
	this.checkFlag = true;
	this.cardIcon = {
		value:'*0000',
		ext:'{"icon":"../../graphics/cards/another-noname.svg"}', 
		our:false};
	this.pin = {
		length = controlPinLength('', 4),
		value = ''};
	this.balanceShow = false;
	this.balanceShowReq = false;
	this.balancePrintNeed = false;
	this.balancePrintReq = false;
}