﻿box = window.external.exchange;
function Screen(handler,screenName) {
	this.name = screenName;
    this.create = handler;
    this.handlers = {};
	this.handlersEvents = [];
    this.next_screen = this;
	
    this.type = "";
	this.args = [];
    
	this.addCall = function(event, callback) {
		if(indexOfInArray(this.handlersEvents,event) == -1)
			this.handlersEvents.push(event);
        this.handlers[event] = callback;
    }
	this.addNDCCaller = function(callback) {
		this.addCall("NDCScreen", callback);
	}
    this.call = function(event) {
		if(indexOfInArray(this.handlersEvents,event) != -1) {
			this.handlers[event]();
		}
    }
    this.call = function(event, params) {
		var tmpWebIUS_params = "";
		if(typeof params != 'undefined')
			tmpWebIUS_params = params;
		
		alertMsgScreen("[screen] typeof handler["+event+"]: "+ (typeof this.handlers[event]));
		
		if(indexOfInArray(this.handlersEvents,event) != -1) {
			this.handlers[event](tmpWebIUS_params);
		}
    }
    /*this.cancel = function() {
		this.call("close");
		var end = function() {
			scr.render("end-cancel");
		}
		this.next_screen = new Screen(end,"end_session");
		runScreen();
    }*/
    this.cancel = function() {
		this.call("close");
		//scr.render("end-cancel");
		//scr.setLabel("title", "Спасибо за доверие.", "");
		//scr.render("test");
		//scr.setWait(true, "Пожалуйста, подождите...", "{\"icon\": \"../../graphics/icon-ok.svg\", \"loader\":\"ellipse\",\"theme\":\"blue\"}");
		//scr.render("deposit_select_currency");
		//scr.setLabel("warning", "Пожалуйста, подождите...", "");	
		//scr.setImage("smile","../../graphics/icon-ok.svg","");
		//scr.setImage("smile","../../graphics/icon-loader.svg","");
		scr.setLabel("text", "Пожалуйста, подождите...", "");	
		scr.setLabel("loader","60", '{"loader":"loader"}');
		scr.setImage("bg","../../graphics/BG_blur.jpg","");
		scr.render("wait_message");

		
		//window.external.exchange.ExecNdcService("cancel", "");
		this.nextScreen(start, 1);
    }
    this.closeSession = function() {
		this.call("end_ok");
		var end = function() {
			scr.render("end-ok");
		}
		this.next_screen = new Screen(end,"end_ok");
		runScreen();
    }
    this.nextScreen = function(WebIUS_scrn) {
		if(typeof WebIUS_scrn.name == 'undefined')
			WebIUS_scrn.name = "start script";
		alertMsgScreen('[nextScreen] screen_name: ['+WebIUS_scrn.name+'] screen_params:[]');
        
		this.next_screen = WebIUS_scrn;
		runScreen();
    }
    this.nextScreen = function(WebIUS_scrn,WebIUS_params) {
		var tmpWebIUS_params = ""
		if(typeof WebIUS_scrn.name == 'undefined')
			this.name = "start script";
		if(typeof WebIUS_params != 'undefined')
			tmpWebIUS_params = WebIUS_params;
			
		alertMsgScreen('[nextScreen] screen_name: ['+WebIUS_scrn.name+'] screen_params:['+tmpWebIUS_params+']');
        this.next_screen = WebIUS_scrn;
		WebIUS_scrn.args = WebIUS_params;
		runScreen();
    }
    
	this.reqInf = function (context, handlerToCall) {
		if(typeof handlerToCall == 'undefined') {
			handlerToCall = context;
			context = 1;
		}
		var reqId = window.external.exchange.reqInf(context);
		requestHandlers[reqId] = handlerToCall;
		window.external.exchange.SendRequest(reqId);
		return reqId;
	}
	
	this.addButtonCancel = function(WebIUS_callback) {
        //this.BtnCancel = text;
        this.addCall('fCANCEL',WebIUS_callback);
    }
	this.addButtonEnter = function(WebIUS_callback) {
        //this.Btn10 = text;
        this.addCall('fENTER',WebIUS_callback);
    }
	this.addOnError = function(WebIUS_callback) {
        this.addCall('on_error',WebIUS_callback);
    }
//------------------------------------------------------------------------------------------------------------------------------------------------------------------
	this.setButtonJson = function(buttonObj, buttonCallback){
		if(!!buttonObj.name){
			buttonObj.objType = 'button';
			window.external.exchange.scrData.addJSONObject(buttonObj.name, JSON.stringify(buttonObj));
			this.addCall(buttonObj.name, buttonCallback);
		}
	}
	this.setButton = function(buttonName, buttonText, buttonVisible, buttonEnable, buttonExt, buttonCall) {
		var  btnHelp = {};
		btnHelp.name =  buttonName;
		if(typeof elementText != 'undefined' && typeof elementText[this.name] != 'undefined' && typeof elementText[this.name]['button'] != 'undefined' && typeof elementText[this.name]['button'][buttonName] != 'undefined' && typeof elementText[this.name]['button'][buttonName][buttonText] != 'undefined' && typeof elementText[this.name]['button'][buttonName][buttonText][m_session.lang] != 'undefined')
			btnHelp.text = elementText[this.name]['button'][buttonName][buttonText][m_session.lang];
		else
			btnHelp.text = buttonText;
		btnHelp.visible = buttonVisible;
		btnHelp.enable = buttonEnable;
		btnHelp.ext = buttonExt;
		if(typeof buttonEnable == 'undefined') {
			buttonCall = buttonVisible;
			btnHelp.ext = "";
			btnHelp.enable = true;
			btnHelp.visible = true;
		}
		else if(typeof buttonExt == 'undefined') {
			buttonCall = buttonEnable;
			btnHelp.ext = buttonVisible;
			btnHelp.enable = true;
			btnHelp.visible = true;
		}
		else if(typeof buttonCall == 'undefined') {
			buttonCall = buttonExt;
			btnHelp.ext = buttonEnable;
			btnHelp.enable = true;
		}
		//alert(btnHelp.name +": "+btnHelp.ext);
		if(btnHelp.ext != "")
			btnHelp.ext = JSON.parse(btnHelp.ext);
		this.setButtonJson(btnHelp, buttonCall);
		//window.external.exchange.scrData.addJSONObject(btnHelp.name, JSON.stringify(btnHelp));
		//window.external.exchange.scrData.addButton(buttonName, buttonText, buttonExt, buttonVisible, buttonEnable);
		//this.addCall(btnHelp.name, buttonCall);
	}
	this.setLabelJson = function(labelObj){
		if(!!labelObj.name){
			labelObj.objType = 'label';
			window.external.exchange.scrData.addJSONObject(labelObj.name, JSON.stringify(labelObj));
		}
	}
	this.setLabel = function(labelName, labelValue, labelExt) {
		var labelObj = {};
		labelObj.name = labelName;
		if(typeof elementText != 'undefined' && typeof elementText[this.name] != 'undefined' && typeof elementText[this.name]['label'] != 'undefined' && typeof elementText[this.name]['label'][labelName] != 'undefined' && typeof elementText[this.name]['label'][labelName][m_session.lang] != 'undefined')
			labelObj.value = elementText[this.name]['label'][labelName][m_session.lang];
		else
			labelObj.value = labelValue;
		if(labelExt != "")
			labelObj.ext = JSON.parse(labelExt);
		else
			labelObj.ext = "";
		this.setLabelJson(labelObj);
		//window.external.exchange.scrData.addLabel(labelName, labelValue, labelExt);
	}
	this.setInputJson = function(inputObj, inputCallback){
		if(!!inputObj.name){
			inputObj.objType = 'input';
			window.external.exchange.scrData.addJSONObject(inputObj.name, JSON.stringify(inputObj));
			this.addCall(inputObj.name, inputCallback);
		}
	}
	this.setInput = function(inputName, inputText, inputMask, inputHint, visible, validate, inputExt, inputType, inputCall, inputState, inputSelected) {
		var inputObj = {};
		inputObj.name = inputName;
		inputObj.text = inputText;
		inputObj.mask = inputMask;
		inputObj.hint = inputHint;
		inputObj.visible = visible;
		inputObj.validate = validate;
		inputObj.ext = inputExt;
		inputObj.type = inputType;
		if(typeof inputExt == 'undefined') {
			inputCall = validate;
			inputObj.type = '';
			inputObj.ext = visible;
			inputObj.validate = false;
			inputObj.visible = true;
		}
		else if(typeof inputType == 'undefined'){
			inputCall = inputExt;
			inputObj.type = '';
			inputObj.ext = validate;
			inputObj.validate = false;
		}
		else if(typeof inputCall == 'undefined') {
			inputCall = inputType;
			inputObj.type = '';
		}
		if(typeof inputState == 'undefined')
			inputObj.state = 'normal';
		if(typeof inputSelected == 'undefined' || inputSelected == '')
			inputObj.selected = false;
		else
			inputObj.selected = true;
		this.setInputJson(inputObj, inputCall);
		//window.external.exchange.scrData.addInput(inputName, inputText, inputMask, visible, true, false, inputHint, validate, "normal", inputType, inputExt);
		//window.external.exchange.writeLog('[SCRIPT]', 'setInput. name = "'+inputName+'", func = "'+inputCall+'"');
		//this.addCall(inputName, inputCall);
	}
	this.setListJson = function(listObj, listCallback) {
		if(!!listObj.name){
			listObj.objType = 'list';
			window.external.exchange.scrData.addJSONObject(listObj.name, JSON.stringify(listObj));
			this.addCall(listObj.name, listCallback);
		}
	}
	this.setList = function(listName, listText, listState, listExt, listCall) {
		var listObj = {}
		listObj.name = listName;
		listObj.text = listText;
		listObj.state = listState;
		listObj.ext = listExt;
		this.setListJson(listObj, listCall);
		//window.external.exchange.scrData.addList(listName, listText, listState, listExt);
		//this.addCall(listName, listCall);
	}
	this.setImageJson = function(imageObj) {
		if(!!imageObj.name){
			imageObj.objType = 'image';
			window.external.exchange.scrData.addJSONObject(imageObj.name, JSON.stringify(imageObj));
		}
	}
	this.setImage = function(imageName, imagePath, imageExt) {
		var imageObj = {};
		imageObj.name = imageName;
		imageObj.src = imagePath;
		if(imageExt != "")
			imageObj.ext = JSON.parse(imageExt);
		else
			imageObj.ext = "";
		this.setImageJson(imageObj);
		//window.external.exchange.scrData.addImage(imageName, imagePath, imageExt);
	}
	this.setWaitJson = function(waitObj) {
		waitObj.objType = 'wait';
		window.external.exchange.scrData.addJSONObject('wait', JSON.stringify(waitObj));
	}
	this.setWait = function(waitEnable, waitText, waitExt) {
		var waitObj = {};
		waitObj.enable = waitEnable;
		waitObj.text = waitText;
		waitObj.ext = waitExt;
		//window.external.exchange.scrData.addWait(waitEnable, waitText, waitExt);
		this.setWaitJson(waitObj);
	}
	this.setTimeoutJson = function(outObj, waitCall) {
		outObj.objType = 'timeout';
		//window.external.exchange.scrData.addTimeout(ms, outExt);
		window.external.exchange.scrData.addJSONObject('timeout', JSON.stringify(outObj));
		this.addCall("on_timeout", waitCall);
	}
	this.setTimeout = function(ms, outExt, waitCall) {
		var outObj = {};
		outObj.clientActivityTimeout = ms;
		if(outExt != "")
			outObj.ext = JSON.parse(outExt);
		else
			outObj.ext = "";
		//window.external.exchange.scrData.addTimeout(ms, outExt);
		this.setTimeoutJson(outObj, waitCall);
	}
	this.setModalMessageJson = function(modObj, modCall) {
		modObj.objType = 'modalMessage';
		//window.external.exchange.scrData.addTimeout(ms, outExt);
		window.external.exchange.scrData.addJSONObject('modalMessage', JSON.stringify(modObj));
		this.addCall("modal_message", modCall);
	}
	this.setModalMessage = function(modalText, modalOptions, modalSelected, modalVisible, modalExt, modCall) {
		var modObj = {};
		modObj.text = modalText;
		modObj.options = modalOptions;
		modObj.selected = modalSelected;
		modObj.visible = modalVisible;
		modObj.ext = modalExt;
		this.setModalMessageJson(modObj, modCall);
	}
	this.deleteElement = function(elementName){
		window.external.exchange.scrData.deleteJSONObject(elementName);
	}
	this.deleteLabel = function(labelName){
		this.deleteElement(labelName);
	}
	this.deleteButton= function(buttonName){
		this.deleteElement(buttonName);
	}
	this.deleteInput= function(inputName){
		this.deleteElement(inputName);
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------
	this.render = function(scrTypeName, scrAddress, scrCacheClear) {
		if (scrTypeName == 'undefined')
			scrTypeName = 'wait';
		//window.external.exchange.SetRenderScreen();
		if(typeof scrAddress == 'undefined')
			window.external.exchange.render(scrTypeName);
		else if(typeof scrCacheClear == 'undefined')
			window.external.exchange.render(scrTypeName, scrAddress);
		else
			window.external.exchange.render(scrTypeName, scrAddress, scrCacheClear);
	}
	this.clearScr = function() {
		this.handlersEvents = [];
        this.handlers = {};
		
		//this.handlersEvents.push("on_timeout");
		//this.handlers["on_timeout"] = onTimeout;
		//this.handlersEvents.push("HostScriptAddon");
		//this.handlers["HostScriptAddon"] = onServiceInit;
        
		this.next_screen = this;
        this.type = "";
    }
}

function indexOfInArray(WebIUS_arr,WebIUS_str) {
	for(var i = 0; i < WebIUS_arr.length; i++ ) {
		if(WebIUS_arr[i] == WebIUS_str)
			return i;
	}
	return -1;
}
function propertyInArray(WebIUS_arr,WebIUS_str) {
	for(var key in WebIUS_arr) {
		if (key == WebIUS_str)
			return 1;
	}
	return -1;
}
function maskCard(str) {
	if(str.length <16)
		return str;
	
	return str.substr(0,4)+'XXXXXXXX'+str.substr(11);
}
function chequeCenter(str) {
	var spaceCount = (40 - str.length)/2;
	var res = '';
	for(var i = 0; i < spaceCount; i++)
		res+=' ';
	return res + str;
}
function chequeUncenter(str) {
	var tmp = str.split('|');
	if(tmp.length != 2)
		return str;
	var spaceCount = 40 - tmp[0].length - tmp[1].length;
	var res = tmp[0];
	for(var i = 0; i < spaceCount; i++)
		res+=' ';
	return res + tmp[1];
}
function setValueOnMask(value,mask) {
	var res = mask;
	for(var i = 0; i < value.length; i++)
	{
		res = res.replace('*',value.charAt(i));
	}
	return res;
}
function filterMask(mask,value) {
	var result = "";
	for(var i = 0; i < value.length; i++)
		if(value.charAt(i) != mask.charAt(i))
			result += value.charAt(i);
	return result;
}