﻿	var elementText = {
		start:{
			label:{
				warning:{
					'Пожалуйста, подождите...':{
						ru:'Пожалуйста, подождите...',
						en:'Please wait...'
					}
				}
			},
			button:{
				quit:{
					ru:'',
					en:''
				},
				main_menu:{
					ru:'',
					en:''
				}
			}
		},
		main:{
			label:{
				header1:{
					'Здравствуйте!':{
						ru:'Здравствуйте!',
						en:'Hello!'
					}
				},
				header2:{
					'Для начала работы Вы можете:':{
						ru:'Для начала работы Вы можете:',
						en:'To start with, you can:'
					}
				},
				earphones:{
					'... или подключить наушники для активации голосового меню':{
						ru:'... или подключить наушники для активации голосового меню',
						en:'... or connect earphones for activation of the voice menu'
					}
				},
				switch_lang:{
					'EN':{
						ru:'EN',
						en:'RU'
					}
				}
			},
			button:{
				insert:{
					'Вставить карту':{
						ru:'Вставить карту',
						en:'Insert<br>card'
					}
				},
				apply:{
					'Приложить карту':{
						ru:'Приложить карту',
						en:'Attach<br>card'
					}
				},
				qrcode:{
					'Поднести штрих-код':{
						ru:'Поднести штрих-код',
						en:'Read<br>barcode'
					}
				},
				nocard:{
					'Войти без карты':{
						ru:'Войти без карты',
						en:'Start without card'
					}
				},
				switch_lang:{
					'Switch to English':{
						ru:'Switch to English',
						en:'Перейти на Русский язык'
					}
				}
			}
		},
		pin:{
			label:{
				switch_lang:{
					'EN':{
						ru:'EN',
						en:'RU'
					}
				},
				attempts_left:{
					'Неверный ПИН':{
						ru:'Неверный ПИН',
						en:'Incorrect PIN'
					}
				},
				forgot_pin:{
					'Забыли ПИН?':{
						ru:'Забыли ПИН?',
						en:'Can\'t remember PIN?'
					}
				},
				change_pin_text:{
					'Смените его на&nbsp;новый в&nbsp;мобильном приложении':{
						ru:'Смените его на&nbsp;новый в&nbsp;мобильном приложении',
						en:'Change it to&nbsp;new one&nbsp;in mobile app'
					}
				},
				change_pin_text2:{
					'или по&nbsp;телефону':{
						ru:'или по&nbsp;телефону',
						en:'or call&nbsp;us'
					}
				}
			},
			button:{
				switch_lang:{
					'Switch to English':{
						ru:'Switch to English',
						en:'Переключить на Русский язык'
					},
					'Перейти на Русский язык':{
						ru:'Switch to English',
						en:'Переключить на Русский язык'
					}
				},
				logout:{
					'Забрать карту':{
						ru:'Забрать карту',
						 en:'Return card'
					},
					'Продолжить':{
						ru:'Продолжить',
						en:'Continue'
					}
				}
			}
		}
	};
	